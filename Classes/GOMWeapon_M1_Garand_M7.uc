//=============================================================================
// GOMWeapon_M1_Garand_M7.uc
//=============================================================================
// ARVN M1 Garand Rifle.
// Modified for M7 Rifle Grenade functionality.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M1_Garand_M7 extends ROWeap_M1Garand_Rifle
	abstract;

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMWeapon_M1_Garand_M7_ActualContent"
	
	bCanThrow=false
}
