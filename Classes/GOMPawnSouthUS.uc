//=============================================================================
// GOMPawnSouthUS.uc
//=============================================================================
// Southern Pawn (United States).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawnSouthUS extends GOMPawnSouth;

DefaultProperties
{
	TunicMesh=SkeletalMesh'GOM3_CHR_US.Mesh.Tunic.GOM_CHR_US_Tunic_Long'
	BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST'
	
	HeadAndArmsMesh=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head4_Mesh'
	HeadAndArmsMICTemplate=MaterialInstanceConstant'GOM3_CHR_US.MIC.US_Head_04_Long'
	
	HeadgearMesh=SkeletalMesh'CHR_VN_AUS_Headgear.Mesh.AUS_Headgear_ShortBasic2'
	
	ArmsOnlyMeshFP=SkeletalMesh'CHR_VN_1stP_Hands_Master.Mesh.VN_1stP_ALL_Bare_Mesh'
	
	FieldgearMesh=SkeletalMesh'GOM3_CHR_US.Mesh.Gear.GOM_CHR_US_Gear_Long_Rifleman'
}
