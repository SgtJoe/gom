//=============================================================================
// GOMRoleInfoSouth_AUS_MachineGunner.uc
//=============================================================================
// Australian Army Machine Gunner Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_AUS_MachineGunner extends GOMRoleInfoSouth_AUS;

DefaultProperties
{
	RoleType=RORIT_MachineGunner
	ClassTier=2
	ClassIndex=`ROCI_MACHINEGUNNER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_L2A1_LMG',class'ROWeap_M60_GPMG')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_L2A1_LMG')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_L2A1_LMG',class'ROWeap_M60_GPMG')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_mg'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_mg'
}
