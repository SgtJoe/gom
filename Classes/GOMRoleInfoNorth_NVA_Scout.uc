//=============================================================================
// GOMRoleInfoNorth_NVA_Scout.uc
//=============================================================================
// North Vietnamese Army Scout Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_NVA_Scout extends GOMRoleInfoNorth_NVA;

DefaultProperties
{
	RoleType=RORIT_Scout
	ClassTier=2
	ClassIndex=`ROCI_SCOUT
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_K50M_SMG',class'GOMWeapon_PPS'),
		OtherItems=(class'ROWeap_RDG1_Smoke'),
		SquadLeaderItems=(class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'GOMWeapon_PPS',class'GOMWeapon_PPSH',class'GOMWeapon_M38_Carbine'),
		OtherItems=(class'ROWeap_RDG1_Smoke'),
		SquadLeaderItems=(class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_K50M_SMG',class'GOMWeapon_PPS',class'GOMWeapon_PPSH'),
		OtherItems=(class'ROWeap_RDG1_Smoke'),
		SquadLeaderItems=(class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_K50M_SMG',class'GOMWeapon_PPS',class'GOMWeapon_PPSH'),
		OtherItems=(class'ROWeap_RDG1_Smoke'),
		SquadLeaderItems=(class'ROItem_Binoculars')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_scout'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_scout'
}
