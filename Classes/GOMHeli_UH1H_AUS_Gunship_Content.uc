//=============================================================================
// GOMHeli_UH1H_AUS_Gunship_Content.uc
//=============================================================================
// UH-1H Iroquois Gunship Helicopter (RAAF)
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHeli_UH1H_AUS_Gunship_Content extends ROHeli_UH1H_Gunship_Content
	placeable;

`include(GOM3\Classes\GOMHelicoptersCommon.uci)
