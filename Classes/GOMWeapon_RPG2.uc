//=============================================================================
// GOMWeapon_RPG2.uc
//=============================================================================
// Viet Cong RPG-2 Rocket Launcher.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_RPG2 extends ROWeap_RPG7_RocketLauncher
	abstract;

DefaultProperties
{
	WeaponContentClass(0)="GOM3.GOMWeapon_RPG2_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'GOM3_UI.WP_Render.VN_Weap_RPG2_RocketLauncher'
	
	InvIndex=`WI_RPG2
	bIsModWeapon=true
	
	WeaponProjectiles(0)=class'GOMProjectile_RPG2Rocket'
	
	InitialNumPrimaryMags=5
	MaxNumPrimaryMags=5
	
	PlayerViewOffset=(X=-3,Y=3.5,Z=-1.5)
	ShoulderedPosition=(X=-3,Y=3.5,Z=-1.5)
	IronSightPosition=(X=-3,Y=-0.83,Z=0.0)
	
	SightRanges.Empty()
	SightRanges[0]=(SightRange=100,SightPitch=0,SightSlideOffset=0,SightPositionOffset=0, AddedPitch=0)
}