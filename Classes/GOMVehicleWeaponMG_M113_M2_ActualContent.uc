//=============================================================================
// GOMVehicleWeaponMG_M113_M2_ActualContent.uc
//=============================================================================
// M113 ACAV M2 Browning (Content class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleWeaponMG_M113_M2_ActualContent extends GOMVehicleWeaponMG_M113_M2
	HideDropDown;

DefaultProperties
{
	BarTexture=Texture2D'ui_textures.Textures.button_128grey'
}
