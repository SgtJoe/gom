//=============================================================================
// GOMWeapon_M7RifleGrenade_ActualContent.uc
//=============================================================================
// M7 Rifle Grenade attachment for the M1 Garand (Content class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M7RifleGrenade_ActualContent extends GOMWeapon_M7RifleGrenade;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WP_VN_ARVN_M1Garand_Rifle.Animation.WP_M1GarHands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'GOM3_WP_US_ROK_M7.Mesh.M7_RifleGrenade'
		PhysicsAsset=PhysicsAsset'WP_VN_ARVN_M1Garand_Rifle.Phys.ARVN_M1Gar_Physics'
		AnimSets(0)=AnimSet'WP_VN_ARVN_M1Garand_Rifle.Animation.WP_M1GarHands'
		AnimTreeTemplate=AnimTree'WP_VN_ARVN_M1Garand_Rifle.Animation.WP_M1GarandHands_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WP_VN_ARVN_3rd_Master.Mesh.M1Garand_3rd_Master'
		PhysicsAsset=PhysicsAsset'WP_VN_ARVN_3rd_Master.Phys.M1Garand_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_ARVN_3rd_Master.AnimTree.M1Garand_Rifle_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'GOMWeapon_M7RifleGrenade_Attach'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_MAS49.Play_WEP_MAS49_Single_3P_Grenade',FirstPersonCue=AkEvent'WW_WEP_MAS49.Play_WEP_MAS49_Fire_Single_Grenade')
}
