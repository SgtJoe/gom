//=============================================================================
// GOMHeli_OH6_ROK_Content.uc
//=============================================================================
// OH-6 "Loach" Observation Helicopter (ROK)
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHeli_OH6_ROK_Content extends ROHeli_OH6_Content
	placeable;

`include(GOM3\Classes\GOMHelicoptersCommon.uci)

DefaultProperties
{
	SeatProxies(1)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Pilot_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.PilotMesh.US_Headgear_Pilot_Base',
		HeadAndArmsMeshType=SkeletalMesh'GOM3_CHR_US_ROK_HEADS.Mesh.GOM_CHR_KOR_Head_1',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'GOM3_CHR_US_ROK_HEADS.MIC.KOR_Head_1_Pilot',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Pilot_A_INST',
		HeadgearSocket=helmet,
		SeatIndex=1,
		PositionIndex=0,
		bExposedToRain=true
	)}
}
