//=============================================================================
// GOMRoleInfoSouth_ROK_Commander.uc
//=============================================================================
// Republic of Korea Commander Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ROK_Commander extends GOMRoleInfoSouth_ROK;

DefaultProperties
{
	RoleType=RORIT_Commander
	ClassTier=4
	ClassIndex=`ROCI_COMMANDER
	bIsTeamLeader=true
	
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle',class'ROWeap_M2_Carbine'),
	
	OtherItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_commander'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_commander'
}
