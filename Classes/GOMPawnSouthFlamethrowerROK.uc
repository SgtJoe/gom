//=============================================================================
// GOMPawnSouthFlamethrowerROK.uc
//=============================================================================
// Southern Pawn w Flamethrower (South Korea).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawnSouthFlamethrowerROK extends GOMPawnSouthROK;

DefaultProperties
{
	bHasFlamethrower=true
	
	FieldgearMesh=SkeletalMesh'GOM3_CHR_US.Mesh.Gear.GOM_CHR_US_Gear_Long_Flamethrower'
}
