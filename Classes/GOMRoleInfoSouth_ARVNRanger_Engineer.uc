//=============================================================================
// GOMRoleInfoSouth_ARVNRanger_Engineer.uc
//=============================================================================
// Republic of Vietnam Rangers Combat Engineer Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ARVNRanger_Engineer extends GOMRoleInfoSouth_ARVN;

DefaultProperties
{
	RoleType=RORIT_Engineer
	ClassTier=3
	ClassIndex=`ROCI_ENGINEER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle',class'ROWeap_M37_Shotgun',class'ROWeap_M2_Carbine'),
		OtherItems=(class'ROWeap_M34_WP',class'ROWeap_C4_Explosive')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle_Late',class'ROWeap_M37_Shotgun',class'ROWeap_M2_Carbine'),
		OtherItems=(class'ROWeap_M34_WP',class'ROWeap_C4_Explosive')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sapper'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_sapper'
}
