//=============================================================================
// GOMHeli_UH1H_AUS_Transport_Content.uc
//=============================================================================
// UH-1H Iroquois Transport Helicopter (RAAF)
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHeli_UH1H_AUS_Transport_Content extends ROHeli_UH1H_Aus_Content
	placeable;

`include(GOM3\Classes\GOMHelicoptersCommon.uci)
