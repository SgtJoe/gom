//=============================================================================
// GOMHUDWatermark.uc
//=============================================================================
// HUD Widget that shows info on the mod.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHUDWatermark extends ROHUDWidgetWatermark;

function Initialize(PlayerController HUDPlayerOwner)
{
	super.Initialize(HUDPlayerOwner);
	
	HUDComponents[ROHUDWC_GameVersion].Text = "GOM";
	HUDComponents[ROHUDWC_Website].Text = "VER" @ `BUILDVER;
	HUDComponents[ROHUDWC_BuildID].Text = "";
	
	`gom(HUDPlayerOwner @ "watermark enabled?" @ !GOMPlayerController(HUDPlayerOwner).DisableModWatermark, 'HUD');
	
	if (GOMPlayerController(HUDPlayerOwner).DisableModWatermark)
	{
		`gom("Not drawing watermark.", 'HUD');
		HUDComponents[ROHUDWC_GameVersion].Text = "";
		HUDComponents[ROHUDWC_Website].Text = "";
		HUDComponents[ROHUDWC_BuildID].Text = "";
	}
}
