//=============================================================================
// GOMVehicleWeaponMG_M113_M60_L_ActualContent.uc
//=============================================================================
// M113 ACAV Left Side M60 (Content class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleWeaponMG_M113_M60_L_ActualContent extends GOMVehicleWeaponMG_M113_M60_L
	HideDropDown;

DefaultProperties
{
	BarTexture=Texture2D'ui_textures.Textures.button_128grey'
}
