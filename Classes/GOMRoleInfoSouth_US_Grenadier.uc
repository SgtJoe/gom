//=============================================================================
// GOMRoleInfoSouth_US_Grenadier.uc
//=============================================================================
// United States Army Grenadier Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_US_Grenadier extends GOMRoleInfoSouth_US;

DefaultProperties
{
	RoleType=RORIT_Support
	ClassTier=2
	ClassIndex=`ROCI_HEAVY
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M79_GrenadeLauncher')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M79_GrenadeLauncher')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M79_GrenadeLauncher')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_M79_GrenadeLauncher')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_grenadier'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_grenadier'
}
