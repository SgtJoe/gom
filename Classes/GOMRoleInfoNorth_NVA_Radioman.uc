//=============================================================================
// GOMRoleInfoNorth_NVA_Radioman.uc
//=============================================================================
// North Vietnamese Army Radio Operator Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_NVA_Radioman extends GOMRoleInfoNorth_NVA;

DefaultProperties
{
	RoleType=RORIT_Radioman
	ClassTier=3
	ClassIndex=`ROCI_RADIOMAN
	bIsRadioman=true
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle',class'GOMWeapon_M38_Carbine')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'GOMWeapon_M38_Carbine',class'ROWeap_SKS_Rifle')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_radioman'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_radioman'
}
