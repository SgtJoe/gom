//=============================================================================
// GOMPawnHandler.uc
//=============================================================================
// Properties about a player that are always replicated.
// Purely for overriding hardcoded PawnHandlerClass checks.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMPlayerReplicationInfo extends ROPlayerReplicationInfo;

simulated function ClientInitialize(Controller C)
{
	local GOMPlayerController ROPC;
	local bool bNewOwner;
	
	bNewOwner = (Owner != C);
	Super.ClientInitialize(C);
	
	if (bNewOwner)
	{
		self.UsedNames.Length = 0;
	}
	
	ROPC = GOMPlayerController(C);
	if ( bNewOwner && ROPC != None && LocalPlayer(ROPC.Player) != None )
	{
		ClientInitializeUnlocks();
	}
	
	PawnHandlerClass = class'GOMPawnHandler';
}

// Have to rename this function because for some damn reason compiler says "Redefinition of function ServerSetCustomCharConfig differs from original" even though it's literally copypasted??????
// We have to add new variables anyway since we can't work with a CharacterConfig
reliable server function ServerSetCustomCharConfigNew(byte TunicMesh, byte TunicMaterial, byte ShirtTexture, byte HeadMesh, byte HairMaterial, byte HeadgearMesh, byte HeadgearMaterial, byte FaceItemMesh, byte FacialHairMesh, byte TattooTex, byte CharClassIndex)
{
	local byte TeamIndex, ArmyIndex, bPilot;
	local GOMPlayerController PC;
	
	TeamIndex = GetTeamNum();
	
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		ArmyIndex = `getNF;
	}
	else
	{
		ArmyIndex = `getSF;
	}
	
	PawnHandlerClass = class'GOMPawnHandler';
	
	if (RoleInfo.bIsPilot)
	{
		bPilot = RoleInfo.bIsTransportPilot ? 1 : 2;
	}
	
	if (TeamIndex == `ALLIES_TEAM_INDEX && `getSF <= `USMC && `getMF == MF_MilitaryPolice && ClassIndex == `ROCI_SCOUT)
	{
		PawnHandlerClass.static.ValidateCharConfig(TeamIndex, `MP, bPilot, int(HonorLevel), TunicMesh, TunicMaterial, ShirtTexture, HeadMesh, HairMaterial, HeadgearMesh, HeadgearMaterial, FaceItemMesh, FacialHairMesh, TattooTex, self);
	}
	else
	{
		PawnHandlerClass.static.ValidateCharConfig(TeamIndex, ArmyIndex, bPilot, int(HonorLevel), TunicMesh, TunicMaterial, ShirtTexture, HeadMesh, HairMaterial, HeadgearMesh, HeadgearMaterial, FaceItemMesh, FacialHairMesh, TattooTex, self);
	}
	
	SetCustomCharConfigNew(TunicMesh, TunicMaterial, ShirtTexture, HeadMesh, HairMaterial, HeadgearMesh, HeadgearMaterial, FaceItemMesh, FacialHairMesh, TattooTex, CharClassIndex);
	
	PC = GOMPlayerController(Owner);
	
	if (PC != none)
	{
		if (TeamIndex == `AXIS_TEAM_INDEX)
		{
			PC.SetSuitableVoicePack(TeamIndex, ArmyIndex, 0);
		}
		else
		{
			PC.SetSuitableVoicePack(TeamIndex, ArmyIndex, PawnHandlerClass.static.GetSkinTone(TeamIndex, ArmyIndex, CurrentCharConfig.HeadMesh, bPilot));
		}
	}
	else
	{
		VoicePackIndex = rand(3);
		
		if (TeamIndex == `ALLIES_TEAM_INDEX)
		{
			VoicePackIndex = rand(2) + 1; // Prevent bots from having the black guy voice since there's a 78% chance they'll be white
		}
	}
}

function SetCustomCharConfigNew(byte TunicMesh, byte TunicMaterial, byte ShirtTexture, byte HeadMesh, byte HairMaterial, byte HeadgearMesh, byte HeadgearMaterial, byte FaceItemMesh, byte FacialHairMesh, byte TattooTex, byte CharClassIndex)
{
	// Same problem as in GOMPawnHandler::GetCharConfig WHAT THE FUUUUUUUU
	// Do the devs have different compilers than in the public sdk or what?!
	/*
	CurrentCharConfig.TunicMesh = MyCharConfig.TunicMesh;
	CurrentCharConfig.TunicMaterial = MyCharConfig.TunicMaterial;
	CurrentCharConfig.ShirtTexture = MyCharConfig.ShirtTexture;
	CurrentCharConfig.HeadMesh = MyCharConfig.HeadMesh;
	CurrentCharConfig.HairMaterial = MyCharConfig.HairMaterial;
	CurrentCharConfig.HeadgearMesh = MyCharConfig.HeadgearMesh;
	CurrentCharConfig.FaceItemMesh = MyCharConfig.FaceItemMesh;
	CurrentCharConfig.TattooTex = MyCharConfig.TattooTex;
	
	// This is CLEARLY defined in the struct IN THE ORIGIANL SOURCE FILES yet compiler says "Unknown member ClassIndex in struct CharacterConfig"???
	// CurrentCharConfig.ClassIndex = MyCharConfig.ClassIndex;
	*/
	
	CurrentCharConfig.TunicMesh = TunicMesh;
	CurrentCharConfig.TunicMaterial = TunicMaterial;
	CurrentCharConfig.ShirtTexture = ShirtTexture;
	CurrentCharConfig.HeadMesh = HeadMesh;
	CurrentCharConfig.HairMaterial = HairMaterial;
	CurrentCharConfig.HeadgearMesh = HeadgearMesh;
	CurrentCharConfig.HeadgearMaterial = HeadgearMaterial;
	CurrentCharConfig.FaceItemMesh = FaceItemMesh;
	CurrentCharConfig.FacialHairMesh = FacialHairMesh;
	CurrentCharConfig.TattooTex = TattooTex;
	CurrentCharConfig.ClassIndex = CharClassIndex;
	
	if( Role == ROLE_Authority && WorldInfo.NetMode != NM_Standalone )
		PackCharacterConfig();
}

simulated function ClientSetCustomCharConfig()
{
	// Because we cannot work with a CharacterConfig struct variable, we have to split it up into it's parts
	local byte TunicMesh;
	local byte TunicMaterial;
	local byte ShirtTexture;
	local byte HeadMesh;
	local byte HairMaterial;
	local byte HeadgearMesh;
	local byte HeadgearMaterial;
	local byte FaceItemMesh;
	local byte FacialHairMesh;
	local byte TattooTex;
	local byte CharClassIndex;
	
	local byte TeamIndex, ArmyIndex, bPilot;
	
	TeamIndex = GetTeamNum();
	
	if( TeamIndex == `AXIS_TEAM_INDEX )
		ArmyIndex = `getNF;
	else
		ArmyIndex = `getSF;
	
	// Set our temp CI to the main outer CI
	CharClassIndex = ClassIndex;
	
	if (RoleInfo.bIsPilot)
	{
		bPilot = RoleInfo.bIsTransportPilot ? 1 : 2;
	}
	
	if (RoleInfo != none)
	{
		if (TeamIndex == `ALLIES_TEAM_INDEX && `getSF <= `USMC && `getMF == MF_MilitaryPolice && ClassIndex == `ROCI_SCOUT)
		{
			class'GOMPawnHandler'.static.GetCharConfig(TeamIndex, `MP, bPilot, ClassIndex, int(HonorLevel), TunicMesh, TunicMaterial, ShirtTexture, HeadMesh, HairMaterial, HeadgearMesh, HeadgearMaterial, FaceItemMesh, FacialHairMesh, TattooTex, self, bBot);
		}
		else
		{
			class'GOMPawnHandler'.static.GetCharConfig(TeamIndex, ArmyIndex, bPilot, ClassIndex, int(HonorLevel), TunicMesh, TunicMaterial, ShirtTexture, HeadMesh, HairMaterial, HeadgearMesh, HeadgearMaterial, FaceItemMesh, FacialHairMesh, TattooTex, self, bBot);
		}
	}
	
	if (Role < ROLE_Authority)
	{
		ServerSetCustomCharConfigNew(TunicMesh, TunicMaterial, ShirtTexture, HeadMesh, HairMaterial, HeadgearMesh, HeadgearMaterial, FaceItemMesh, FacialHairMesh, TattooTex, CharClassIndex);
	}
	else
	{
		SetCustomCharConfigNew(TunicMesh, TunicMaterial, ShirtTexture, HeadMesh, HairMaterial, HeadgearMesh, HeadgearMaterial, FaceItemMesh, FacialHairMesh, TattooTex, CharClassIndex);
	}
	
	// Thank god none of those functions are called outside this class
	
	if (bNetOwner)
		bReplicatedInitialCharConfig = true;
}

function bool SelectRoleByClass(Controller C, class<RORoleInfo> RoleInfoClass, optional out WeaponSelectionInfo WeaponSelection, optional out byte NewSquadIndex, optional out byte NewClassIndex, optional class<ROVehicle> TankSelection)
{
	local RORoleInfo NewRoleInfo;
	local array<RORoleCount> Roles;
	local array<RORolesTaken> RolesTaken;
	local ROMapInfo ROMI;
	local GOMGameReplicationInfo ROGRI;
	local ROTeamInfo ROTI;
	local bool bSuccess, bUnlimitedRoles, bRoleAvailable;
	local GOMPlayerController ROPC;
	local ROAIController ROBot;
	local Controller BootedBot;
	local int i, DesiredRoleIndex;
	local byte OldSquadIndex;
	
	ROMI = ROMapInfo(WorldInfo.GetMapInfo());
	DesiredRoleIndex = -1;
	NewSquadIndex = SquadIndex;
	OldSquadIndex = SquadIndex;
	
	if (C == none)
	{
		return false;
	}
	
	if (ROAIController(C) != none)
	{
		return super.SelectRoleByClass(C, RoleInfoClass, WeaponSelection, NewSquadIndex, NewClassIndex, TankSelection);
	}
	
	`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass" @ Team @ RoleInfoClass @ WeaponSelection.PrimaryWeaponIndex @ WeaponSelection.SecondaryWeaponIndex @ ROMI @ Team,'RoleSelect');
	
	if ( ROMI != none && Team != none )
	{
		ROPC = GOMPlayerController(C);
		ROGRI = GOMGameReplicationInfo(WorldInfo.GRI);
		
		if( ROPC == none )
			ROBot = ROAIController(C);
		
		bUnlimitedRoles = class<ROGameInfo>(WorldInfo.GRI.GameClass).default.bUnlimitedRoles;
		
		`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass" @ ROPC @ ROGRI @ "bUnlimitedRoles="$bUnlimitedRoles @ "ClassIndex="$ClassIndex,'RoleSelect');
		
		if ( ROPC != none || ROBot != none )
		{
			// The "selected role is full" bug is caused by client/server desync with the roles, so we need to replace them here too
			
			ROPC.ReplaceRoles('RoleSelectRepLog');
			
			if ( Team.TeamIndex == `AXIS_TEAM_INDEX )
			{
				Roles = ROMapInfo(ROPC.WorldInfo.GetMapInfo()).NorthernRoles;
				RolesTaken = ROMapInfo(ROPC.WorldInfo.GetMapInfo()).NorthernRolesTaken;
			}
			else
			{
				Roles = ROMapInfo(ROPC.WorldInfo.GetMapInfo()).SouthernRoles;
				RolesTaken = ROMapInfo(ROPC.WorldInfo.GetMapInfo()).SouthernRolesTaken;
			}

			for ( i = 0; i < Roles.Length; i++ )
			{
				`gom("Role:"@Roles[i].RoleInfoClass.default.MyName$", ClassIndex="$Roles[i].RoleInfoClass.default.ClassIndex$", TakenByHumans="$RolesTaken[i].TakenByHumans$", Count:"$Roles[i].Count,'RoleSelect');
				if( Roles[i].RoleInfoClass.default.ClassIndex == RoleInfoClass.default.ClassIndex && (bUnlimitedRoles || RolesTaken[i].TakenByHumans < Roles[i].Count || ClassIndex == Roles[i].RoleInfoClass.default.ClassIndex) )
				{
					bRoleAvailable = true;
					DesiredRoleIndex = i;
					`gom("Desired Role Is Available! DesiredRoleIndex:"@DesiredRoleIndex,'RoleSelect');
					ROTI = ROTeamInfo(Team);
					if( RolesTaken[i].TotalTaken >= Roles[i].Count && ROTI != none )
					{
						for( i=0; i<`MAX_PLAYERS_PER_TEAM; i++ )
						{
							if( ROTI.TeamPRIArray[i] != none && ROTI.TeamPRIArray[i].bBot && ROTI.TeamPRIArray[i].ClassIndex == RoleInfoClass.default.ClassIndex )
							{
								BootedBot = Controller(ROTI.TeamPRIArray[i].Owner);
								break;
							}
						}
					}
					break;
				}
			}
		}
		
		if ( Team.TeamIndex == `AXIS_TEAM_INDEX )
		{
			`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - using Northern Team",'RoleSelect');
			if ( !bUnlimitedRoles && RoleInfoClass.default.bIsTeamLeader )
			{
				`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - wants Commander",'RoleSelect');
				if ( ROPC != none )
				{
					if ( ROMI.NorthernTeamLeader.Owner == none || ROMI.NorthernTeamLeader.Owner == ROPC || AIController(ROMI.NorthernTeamLeader.Owner) != none ||
						 !ROMI.OwnerHasRole(ROMI.NorthernTeamLeader.Owner, Team.TeamIndex, `SQUAD_INDEX_TEAMLEADER, 0, WorldInfo) )
					{
						BootedBot = AIController(ROMI.NorthernTeamLeader.Owner);
						ROMI.NorthernTeamLeader.Owner = ROPC;
						NewSquadIndex = `SQUAD_INDEX_TEAMLEADER;
						ROTeamInfo(Team).bHasTeamLeader = true;
						bSuccess = true;
						`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - took role" @ BootedBot @ ROMI.NorthernTeamLeader.Owner,'RoleSelect');
					}
				}
				else if ( ROMI.NorthernTeamLeader.Owner == none )
				{
					ROMI.NorthernTeamLeader.Owner = C;
					bSuccess = true;
				}
				`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - using ROMI.NorthernTeamLeader.RoleInfo",'RoleSelect');
				if ( bSuccess ) NewRoleInfo = ROMI.NorthernTeamLeader.RoleInfo;
			}
			else
			{
				Roles = ROMI.NorthernRoles;
			}
		}
		else
		{
			`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - using Southern Team",'RoleSelect');
			if ( !bUnlimitedRoles && RoleInfoClass.default.bIsTeamLeader )
			{
				`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - wants Commander",'RoleSelect');
				if ( ROPC != none )
				{
					if ( ROMI.SouthernTeamLeader.Owner == none || ROMI.SouthernTeamLeader.Owner == ROPC || AIController(ROMI.SouthernTeamLeader.Owner) != none ||
						 !ROMI.OwnerHasRole(ROMI.SouthernTeamLeader.Owner, Team.TeamIndex, `SQUAD_INDEX_TEAMLEADER, 0, WorldInfo) )
					{
						BootedBot = AIController(ROMI.SouthernTeamLeader.Owner);
						ROMI.SouthernTeamLeader.Owner = ROPC;
						NewSquadIndex = `SQUAD_INDEX_TEAMLEADER;
						ROTeamInfo(Team).bHasTeamLeader = true;
						bSuccess = true;
						`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - took role" @ BootedBot @ ROMI.SouthernTeamLeader.Owner,'RoleSelect');
					}
				}
				else if ( ROMI.SouthernTeamLeader.Owner == none )
				{
					ROMI.SouthernTeamLeader.Owner = C;
					NewSquadIndex = `SQUAD_INDEX_TEAMLEADER;
					bSuccess = true;
				}
				`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - using ROMI.SouthernTeamLeader.RoleInfo",'RoleSelect');
				if ( bSuccess ) NewRoleInfo = ROMI.SouthernTeamLeader.RoleInfo;
			}
			else
			{
				Roles = ROMI.SouthernRoles;
			}
		}
		
		if( bSuccess )
		{
			NewClassIndex = NewRoleInfo.ClassIndex;
			if( Squad != none && RoleIndex < `MAX_ROLES_PER_SQUAD )
			{
				Squad.LeaveSquad(ROPC, RoleIndex);
			}
		}
		else if ( DesiredRoleIndex > -1 && (bUnlimitedRoles || bRoleAvailable) )
		{
			NewRoleInfo = new RoleInfoClass;
			if( NewRoleInfo != none && NewRoleInfo.bIsPilot )
			{
				if( Squad != none )
					Squad.LeaveSquad(ROPC, RoleIndex);
				NewSquadIndex = `SQUAD_INDEX_PILOT;
			}
			else if( NewSquadIndex == `SQUAD_INDEX_TEAMLEADER || NewSquadIndex == `SQUAD_INDEX_PILOT )
			{
				NewSquadIndex = `SQUAD_INDEX_NONE;
			}
			`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - Unlimited Roles",'RoleSelect');
		}
		if ( NewRoleInfo != none && !bSuccess )
		{
			NewClassIndex = NewRoleInfo.ClassIndex;
			bSuccess = true;
			`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - Success" @ NewSquadIndex @ NewClassIndex,'RoleSelect');
		}
	}
	
	if ( bSuccess )
	{
		`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - Success = true",'RoleSelect');
		if ( !bUnlimitedRoles && ClassIndex != NewClassIndex )
		{
			`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - clearing old role",'RoleSelect');
			ClearRole(C);
		}
		
		if( ClassIndex != NewClassIndex && ROPC != none && ROPC.IsLocalPlayerController() )
		{
			ROPC.LastDisplayedClass = NewClassIndex;
			ROPC.SaveConfig();
		}
		
		RoleInfo = NewRoleInfo;
		ClassIndex = NewClassIndex;
		
		if( OldSquadIndex != NewSquadIndex )
		{
			SquadIndex = NewSquadIndex;
			if( NewSquadIndex > `MAX_SQUADS )
			{
				if( ROPC != none )
					ROPC.ClearPurpleSmokeMarker();
			}
			if( NewSquadIndex == `SQUAD_INDEX_NONE )
			{
				if( ROPC != none )
					ROPC.AutoSelectSquad();
			}
		}
		
		if ( Team.TeamIndex == `AXIS_TEAM_INDEX )
		{
			UpdateClassCount(`AXIS_TEAM_INDEX, DesiredRoleIndex);
		}
		else
		{
			UpdateClassCount(`ALLIES_TEAM_INDEX, DesiredRoleIndex);
		}
		
		if( Squad != none && RoleIndex < `MAX_ROLES_PER_SQUAD )
		{
			Squad.SquadMembers[RoleIndex].RoleInfo = RoleInfo;
		}
		
		ClassRank = 0;
		
		`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - NewValues" @ Squad @ SquadIndex @ RoleIndex @ ClassIndex @ ClassRank @ NewRoleInfo,'RoleSelect');
		
		if ( WorldInfo.NetMode != NM_DedicatedServer )
		{
			ReplicatedEvent('SquadIndex');
		}
		
		IsWeaponIndexValid(RoleInfo, GOMPlayerController(C), WeaponSelection.PrimaryWeaponIndex, WeaponSelection.SecondaryWeaponIndex);
		
		if (AIController(C) != None)
			WeaponSelection.PrimaryWeaponIndex = ChooseRandomPrimaryWeapon(RoleInfo, C, WeaponSelection.PrimaryWeaponIndex);
		
		RoleInfo.SetActiveWeaponLoadout(WeaponSelection);
		
		`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - NewWeapons" @ Squad @ WeaponSelection.PrimaryWeaponIndex @ WeaponSelection.SecondaryWeaponIndex,'RoleSelect');
		
		if( ROAIController(BootedBot) != none )
		{
			if( ROAIController(BootedBot).ChooseRole() )
			{
				if ( BootedBot.Pawn != none )
				{
					BootedBot.Pawn.Suicide();
				}
			}
			else
				BootedBot.Destroy();
		}
		
		if ( RoleInfo.bIsTeamLeader )
		{
			if ( ROPC != none )
			{
				if ( Team.TeamIndex == `AXIS_TEAM_INDEX )
				{
					for ( i = 0; i < ROMI.NorthernSquads.Length; i++ )
					{
						ROPC.SRIArray[i] = ROMI.NorthernSquads[i].SRI;
					}
				}
				else
				{
					for ( i = 0; i < ROMI.SouthernSquads.Length; i++ )
					{
						ROPC.SRIArray[i] = ROMI.SouthernSquads[i].SRI;
					}
				}
			}
		}
		else
		{
			if ( ROPC != none )
			{
				for ( i = 0; i < `MAX_SQUADS; i++ )
				{
					ROPC.SRIArray[i] = none;
				}
			}
		}
		
		if( ROGameInfo(WorldInfo.Game).bRoundHasBegun && bDead )
		{
			`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - setting SpawnSelection = 0",'RoleSelect');
			SpawnSelection = 0;
		}
		else
		{
			`gom(C.PlayerReplicationInfo.PlayerName @ "SelectRoleByClass - setting SpawnSelection = 128",'RoleSelect');
			SpawnSelection = 128;
		}
		
		if ( Collector != none )
		{
			Collector.UpdateRole(PlayerID, (RoleInfo == none) ? 0 : INT(RoleInfo.RoleType), RoleInfo.MyName, (Team == none) ? -1 : Team.TeamIndex);
		}
	}
	
	if( bBot )
	{
		PawnHandlerClass = class'GOMPawnHandler';
		ClientSetCustomCharConfig();
	}
	
	return bSuccess;
}

function bool IsWeaponIndexValid(RORoleInfo NewRoleInfo, ROPlayerController ROPC, out byte PrimaryWeaponIndex, out byte SecondaryWeaponIndex)
{
	local ROGameReplicationInfo ROGRI;
	local byte AdjustedClassRank;
	
	AdjustedClassRank = ClassRank;
	ROGRI = ROGameReplicationInfo(WorldInfo.GRI);
	
	PrimaryWeaponIndex = Max(PrimaryWeaponIndex, NewRoleInfo.GetPrimaryWeaponsStart(AdjustedClassRank));
	
	if (!ROGRI.bNoWeaponLimits || !ROGRI.IsMultiplayerGame())
	{
		PrimaryWeaponIndex = Min(PrimaryWeaponIndex, NewRoleInfo.GetPrimaryWeaponsNum(ROGRI.RoleInfoItemsIdx, AdjustedClassRank) - 1);
	}
	
	SecondaryWeaponIndex = Max(SecondaryWeaponIndex, NewRoleInfo.GetSecondaryWeaponsStart(AdjustedClassRank));
	
	if ( ROGRI.bRealisticPistolLoadouts )
	{
		// wtf is this hardcoded check for? Completely defeats the purpose of having customizable variables in RoleInfo!
		/* if( NewRoleInfo.RoleType == RORIT_Engineer && NewRoleInfo.Items[ROGRI.RoleInfoItemsIdx].PrimaryWeapons[PrimaryWeaponIndex].default.WeaponClassType != ROWCT_Incendiary && !bIsSquadLeader )
			SecondaryWeaponIndex = 255;
		else */
			SecondaryWeaponIndex = Min(SecondaryWeaponIndex, NewRoleInfo.GetSecondaryWeaponsNum(ROGRI, ROGRI.RoleInfoItemsIdx, AdjustedClassRank, bIsSquadLeader, PrimaryWeaponIndex) - 1);
	}
	
	return true;
}
