//=============================================================================
// GOMRoleInfoSouth_AUS_Pilot_Combat.uc
//=============================================================================
// Australian Army Combat Pilot Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_AUS_Pilot_Combat extends GOMRoleInfoSouth_AUS;

DefaultProperties
{
	RoleType=RORIT_AirCrew
	ClassTier=3
	ClassIndex=`ROCI_COMBATPILOT
	
	bIsPilot=true
	bBotSelectable=false
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_BHP_Pistol')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_BHP_Pistol')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_BHP_Pistol')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_combatpilot'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_combatpilot'
}
