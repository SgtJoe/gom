//=============================================================================
// GOMVoicePack_AUS_1.uc
//=============================================================================
// AUS Voice Pack.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVoicePack_AUS_1 extends ROVoicePackAusTeam01;

defaultproperties
{
	VoiceComs[`VOICECOM_TakeCover]=(Sound="AKEvent'WW_VOX_ANZAC_Voice1.Play_ANZAC_Soldier_1_TakingFireGeneric'")
	VoiceComs[`VOICECOM_AIConfirm]=(Sound="AKEvent'WW_VOX_ANZAC_Voice1.Play_ANZAC_Soldier_1_Confirm'")
	VoiceComs[`VOICECOM_HeloIdleSituation]=(Sound="AKEvent'WW_VOX_ANZAC_Voice1.Play_ANZAC_Pilot_1_HeloIdleGround'")
	
	Ethnicity=1
}
