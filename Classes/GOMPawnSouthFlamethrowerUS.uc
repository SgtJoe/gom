//=============================================================================
// GOMPawnSouthFlamethrowerUS.uc
//=============================================================================
// Southern Pawn w Flamethrower (United States).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawnSouthFlamethrowerUS extends GOMPawnSouthUS;

DefaultProperties
{
	bHasFlamethrower=true
	
	FieldgearMesh=SkeletalMesh'GOM3_CHR_US.Mesh.Gear.GOM_CHR_US_Gear_Long_Flamethrower'
}
