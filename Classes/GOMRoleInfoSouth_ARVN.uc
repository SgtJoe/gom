//=============================================================================
// GOMRoleInfoSouth_ARVN.uc
//=============================================================================
// Basic info for all Southern Republic of Vietnam Roles.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ARVN extends GOMRoleInfoSouth_US
	abstract;

DefaultProperties
{
	RoleRootClass=class'GOMRoleInfoSouth_ARVN'
}
