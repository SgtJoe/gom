//=============================================================================
// GOMRoleInfoNorth_VC_Scout.uc
//=============================================================================
// Viet Cong Scout Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_VC_Scout extends GOMRoleInfoNorth_VC;

DefaultProperties
{
	RoleType=RORIT_Scout
	ClassTier=2
	ClassIndex=`ROCI_SCOUT
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_MAT49_SMG',class'ROWeap_MP40_SMG',class'ROWeap_M1A1_SMG'),
		OtherItems=(class'ROWeap_Molotov',class'ROWeap_RDG1_Smoke'),
		SquadLeaderItems=(class'ROItem_TunnelTool',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_MAT49_SMG',class'ROWeap_MP40_SMG',class'ROWeap_IZH43_Shotgun'),
		OtherItems=(class'ROWeap_Molotov',class'ROWeap_RDG1_Smoke'),
		SquadLeaderItems=(class'ROItem_TunnelTool',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_MAT49_SMG',class'GOMWeapon_PPSH'),
		OtherItems=(class'ROWeap_Molotov',class'ROWeap_RDG1_Smoke'),
		SquadLeaderItems=(class'ROItem_TunnelTool',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_K50M_SMG',class'GOMWeapon_PPSH'),
		OtherItems=(class'ROWeap_Type67_Grenade',class'ROWeap_RDG1_Smoke'),
		SquadLeaderItems=(class'ROItem_TunnelTool',class'ROItem_Binoculars')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_scout'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_scout'
}
