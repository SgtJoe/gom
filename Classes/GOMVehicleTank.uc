//=============================================================================
// GOMVehicleTank.uc
//=============================================================================
// Base class for vehicles with turrets. Modified with code from
// ROVehicleTransport to allow entering/exiting.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleTank extends ROVehicleTank
	abstract;

var int CrewHitZoneStart, CrewHitZoneEnd;

var Animtree PassengerAnimTree;

simulated function DetachDriver(Pawn P)
{
	local ROPawn ROP;
	ROP = ROPawn(P);
	
	if (ROP != None)
	{
		ROP.Mesh.SetAnimTreeTemplate(ROP.Mesh.default.AnimTreeTemplate);
		ROSkeletalMeshComponent(ROP.Mesh).AnimSets[0]=ROSkeletalMeshComponent(ROP.Mesh).default.AnimSets[0];
		
		ROP.ThirdPersonHeadAndArmsMeshComponent.SetSkeletalMesh(ROP.HeadAndArmsMesh);
		ROP.ThirdPersonHeadgearMeshComponent.SetHidden(false);
		ROP.FaceItemMeshComponent.SetHidden(false);
		ROP.FacialHairMeshComponent.SetHidden(false);
		ROP.HideGear(false);
	}
	
	Super.DetachDriver(P);
}

simulated function SitDriver( ROPawn ROP, int SeatIndex )
{
	local ROPlayerController ROPC;
	local Pawn LocalPawn;
	
	super.SitDriver(ROP, SeatIndex);
	
	if( Seats[SeatIndex].SeatPawn != none && Seats[SeatIndex].SeatPawn.Driver != none )
	{
		ROPC = ROPlayerController(Seats[SeatIndex].SeatPawn.Driver.Controller);
	}
	
	if( ROPC == none && Seats[SeatIndex].SeatPawn != none )
	{
		ROPC = ROPlayerController(Seats[SeatIndex].SeatPawn.Controller);
	}
	
	if( ROPC == none )
	{
		if( ROP.DrivenVehicle.Controller != none && ROP.DrivenVehicle.Controller == GetALocalPlayerController() )
		{
			ROPC = ROPlayerController(ROP.DrivenVehicle.Controller);
		}
	}
	
	if( ROPC == none && SeatIndex == 0 )
	{
		if( GetALocalPlayerController() != none && GetALocalPlayerController().Pawn == self )
		{
			ROPC = ROPlayerController(GetALocalPlayerController());
		}
	}
	
	if( ROPC == none  )
	{
		LocalPawn = GetALocalPlayerController().Pawn;
		
		if( GetALocalPlayerController() != none && LocalPawn == Seats[SeatIndex].SeatPawn )
		{
			ROPC = ROPlayerController(GetALocalPlayerController());
		}
	}
	
	if( ROPC != none && (WorldInfo.NetMode == NM_Standalone || IsLocalPlayerInThisVehicle()) )
	{
		ROPC.SetRotation(rot(0,0,0));
	}
	
	if( ROP != none )
	{
		ROP.Mesh.SetAnimTreeTemplate(PassengerAnimTree);
		ROP.HideGear(true);
		
		if( ROP.CurrentWeaponAttachment != none )
		{
			ROP.PutAwayWeaponAttachment();
		}
		
		if( Role == ROLE_Authority )
		{
			UpdateSeatProxyHealth(GetSeatProxyIndexForSeatIndex(SeatIndex), ROP.Health, false);
		}
	}
	
	if( WorldInfo.NetMode != NM_DedicatedServer )
	{
		if ( ROPC != None && LocalPlayer(ROPC.Player) != none && (WorldInfo.NetMode == NM_Standalone || IsLocalPlayerInThisVehicle()) )
		{
			if ( Mesh.DepthPriorityGroup == SDPG_World )
			{
				SetVehicleDepthToForeground();
			}
			
			if (ROP != None)
			{
				if( ROP.ThirdPersonHeadphonesMeshComponent != none )
				{
					ROP.ThirdPersonHeadphonesMeshComponent.SetOwnerNoSee(true);
				}
				
				if( ROP.ThirdPersonHeadgearMeshComponent != none )
				{
					ROP.ThirdPersonHeadgearMeshComponent.SetHidden(true);
				}
				
				if( ROP.FaceItemMeshComponent != none )
				{
					ROP.FaceItemMeshComponent.SetHidden(true);
				}
				
				if( ROP.FacialHairMeshComponent != none )
				{
					ROP.FacialHairMeshComponent.SetHidden(true);
				}
				
				ROP.ThirdPersonHeadAndArmsMeshComponent.SetSkeletalMesh(ROP.ArmsOnlyMesh);
				ROP.ArmsMesh.SetHidden(true);
			}
		}
		
		SpawnOrReplaceSeatProxy(SeatIndex, ROP);
	}
	
	if( ROP != none )
	{
		ROP.SetRelativeRotation(Seats[SeatIndex].SeatRotation);
		ROP.UpdateVehicleIK(self, SeatIndex, SeatPositionIndex(SeatIndex,, true));
	}
}

simulated function CheckStartEngine()
{
	if( EngineStatus == ES_Running || bEngineDestroyed )
	{
		return;
	}
	else
	{
		if( bEngineDamaged )
			EngineStatus = ES_RunningDamaged;
		else
			EngineStatus = ES_Running;
		
		if( Role == ROLE_Authority )
		{
			EngineStatusUpdated();
		}
	}
}

simulated function CheckStopEngine()
{
	if( EngineStatus == ES_Off || bEngineDestroyed )
	{
		return;
	}
	else
	{
		EngineStatus = ES_Off;
		
		if( Role == ROLE_Authority )
		{
			EngineStatusUpdated();
		}
	}
}

simulated function EngineStatusUpdated()
{
	if( (EngineStatus == ES_Running || EngineStatus == ES_RunningDamaged) && OldEngineStatus == ES_Off && !bIsDisabled )
	{
		VehicleEvent('EngineStart');
		VehiclePlayEnterSound();
	}
	else if( EngineStatus == ES_Off && (OldEngineStatus == ES_Running || OldEngineStatus == ES_RunningDamaged) && Health > 0)
	{
		VehicleEvent('EngineStop');
		VehiclePlayExitSound();
	}
	
	OldEngineStatus = EngineStatus;
}

simulated function StartEngineSound()
{
	if (EngineSound != none)
	{
		if( EngineStatus == ES_RunningDamaged && EngineIdleDamagedSound != none )
		{
			EngineSound.PlayEvent(EngineIdleDamagedSound);
		}
		else
		{
			EngineSound.PlayEvent(EngineSoundEvent);
		}
	}
	
	ClearTimer('StartEngineSound');
	ClearTimer('StopEngineSound');
	
	if( IsLocalPlayerInThisVehicle() )
	{
		if(EngineIntLeftSound != None)
		{
			EngineIntLeftSound.PlayEvent(EngineIntLeftSoundEvent);
		}
		if(EngineIntRightSound != None)
		{
			EngineIntRightSound.PlayEvent(EngineIntRightSoundEvent);
		}
	}
	else
	{
		if(EngineIntLeftSound != None)
		{
			EngineIntLeftSound.StopEvents();
		}
		if(EngineIntRightSound != None)
		{
			EngineIntRightSound.StopEvents();
		}
	}
	
	if (TrackLeftSound != None)
	{
		TrackLeftSound.PlayEvent(TrackLeftSoundEvent);
	}
	if (TrackRightSound != None)
	{
		TrackRightSound.PlayEvent(TrackRightSoundEvent);
	}
}

simulated function SpawnOrReplaceSeatProxy(int SeatIndex, ROPawn ROP, optional bool bInternalVisibility)
{
	local int i;
	local VehicleCrewProxy CurrentProxyActor;
	
	if( WorldInfo.NetMode == NM_DedicatedServer )
	{
		return;
	}
	
	for ( i = 0; i < SeatProxies.Length; i++ )
	{
		if( SeatIndex == i && ROP != none )
		{
			if( SeatProxies[i].ProxyMeshActor != none && SeatProxies[i].ProxyMeshActor.bIsDismembered )
			{
				SeatProxies[i].ProxyMeshActor.Destroy();
			}
			
			if( SeatProxies[i].ProxyMeshActor == none )
			{
				SeatProxies[i].ProxyMeshActor = Spawn(class'VehicleCrewProxy',self);
				SeatProxies[i].ProxyMeshActor.MyVehicle = self;
				SeatProxies[i].ProxyMeshActor.SeatProxyIndex = i;
				CurrentProxyActor = SeatProxies[i].ProxyMeshActor;
				SeatProxies[i].TunicMeshType.Characterization = class'ROPawn'.default.PlayerHIKCharacterization;
				CurrentProxyActor.Mesh.SetShadowParent(Mesh);
				CurrentProxyActor.SetLightingChannels(InteriorLightingChannels);
				CurrentProxyActor.SetLightEnvironment(InteriorLightEnvironment);
				CurrentProxyActor.SetCollision( false, false);
				CurrentProxyActor.bCollideWorld = false;
				CurrentProxyActor.SetBase(none);
				CurrentProxyActor.SetHardAttach(true);
				CurrentProxyActor.SetLocation( Location );
				CurrentProxyActor.SetPhysics( PHYS_None );
				CurrentProxyActor.SetBase( Self, , Mesh, Seats[SeatProxies[i].SeatIndex].SeatBone);
				CurrentProxyActor.SetRelativeLocation( vect(0,0,0) );
				CurrentProxyActor.SetRelativeRotation( Seats[SeatProxies[i].SeatIndex].SeatRotation );
			}
			else
			{
				CurrentProxyActor = SeatProxies[i].ProxyMeshActor;
			}
			
			CurrentProxyActor.ReplaceProxyMeshWithPawn(ROP);
			
			if ( SeatProxyAnimSet != None )
			{
				CurrentProxyActor.Mesh.AnimSets[0] = SeatProxyAnimSet;
			}
			
			if( IsLocalPlayerInThisVehicle() )
				CurrentProxyActor.SetVisibilityToInterior();
			else
				CurrentProxyActor.SetVisibilityToExterior();
			
			CurrentProxyActor.HideMesh(true);
		}
	}
}

simulated function SetInputs(float InForward, float InStrafe, float InUp)
{
	if( IsTimerActive('StartEngineSound') )
	{
		InForward = 0;
		InStrafe = 0;
	}
	
	Super.SetInputs(InForward, InStrafe, InUp);
}

function rotator GetExitRotation(Controller C)
{
	local rotator rot;
	rot.Yaw = Rotation.Yaw;
	return rot;
}

simulated function ChangeCrewCollision(bool bEnable, int SeatIndex)
{
	local int i;
	
	for( i = CrewHitZoneStart; i <= CrewHitZoneEnd; i++ )
	{
		if( VehHitZones[i].CrewSeatIndex == SeatIndex )
		{
			if( bEnable )
			{
				Mesh.UnhideBoneByName(VehHitZones[i].CrewBoneName);
			}
			else
			{
				Mesh.HideBoneByName(VehHitZones[i].CrewBoneName,PBO_Disable);
			}
		}
	}
}

simulated function DrivingStatusChanged()
{
	super.DrivingStatusChanged();
	
	ChangeCrewCollision(bDriving, 0);
}

DefaultProperties
{
	bTankScuttleEnabled=false
	
	bInfantryCanUse=true
	
	PassengerAnimTree=AnimTree'CHR_Playeranimtree_Master.CHR_Tanker_animtree'
	
	CrewHitZoneStart=0
	CrewHitZoneEnd=0
}
