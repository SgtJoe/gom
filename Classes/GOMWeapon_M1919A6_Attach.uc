//=============================================================================
// GOMWeapon_M1919A6_Attach.uc
//=============================================================================
// ARVN M1919A6 Browning LMG (Attachment class).
// Modified to add barrel change functionality.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M1919A6_Attach extends ROWeaponAttachmentBipod;

defaultproperties
{
	TriggerHoldDuration=0.2
	
	CarrySocketName=WeaponSling
	ThirdPersonHandsAnim=M1919LMG_Handpose
	IKProfileName=MG34
	
	PivotBoneName=Anim_Bipod_ground
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'GOM3_WP_ARVN_M1919A6.Mesh.M1919A6_3rd'
		AnimTreeTemplate=AnimTree'GOM3_WP_ARVN_M1919A6.Anim.M1919A6_3rd_Tree'
		Animations=NONE
		AnimSets(0)=AnimSet'GOM3_WP_ARVN_M1919A6.Anim.M1919_3rd_Anims'
		PhysicsAsset=PhysicsAsset'GOM3_WP_ARVN_M1919A6.Phy.M1919A6_3rd_Physics'
		CullDistance=5000
	End Object
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_3rdP_Rifles_round'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	WeaponClass=class'GOMWeapon_M1919A6'
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons_Two.ShellEjects.FX_Wep_ShellEject_M1919'
	
	TracerClass=class'M1919BulletTracer'
	TracerFrequency=8
	
	CHR_AnimSet=AnimSet'GOM3_WP_ARVN_M1919A6.Anim.CHR_M1919'
	
	FireAnim=Shoot
	FireLastAnim=Shoot_Last
	IdleAnim=Idle
	IdleEmptyAnim=Idle_Empty
	
	WP_Prone_ReloadAnims(0)=Prone_Deploy_Reload_Empty
	WP_Prone_ReloadAnims(1)=Prone_Deploy_Reload_Half
	
	Prone_ReloadAnims(0)=Prone_Deploy_Reload_Empty
	Prone_ReloadAnims(1)=Prone_Deploy_Reload_Half
	
	ReloadAnims(0)=Reload_Empty
	ReloadAnims(1)=Reload_Half
}
