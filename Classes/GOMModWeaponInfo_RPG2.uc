//=============================================================================
// GOMModWeaponInfo_RPG2.uc
//=============================================================================
// UI information for the RPG-2 Rocket Launcher.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfo_RPG2 extends ROModWeaponInfo;

DefaultProperties
{
	ModWeaponClass=GOMWeapon_RPG2_ActualContent
	
	InventoryIconTexturePath="GOM3_UI.WP_Select.UI_HUD_WeaponSelect_RPG2"
	
	AmmoIconTexturePath="VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_RPG"
	
	KillIconTexturePath="GOM3_UI.WP_KillIcon.UI_Kill_Icon_RPG2"
	bHasSquareKillIcon=false
}
