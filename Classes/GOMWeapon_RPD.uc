//=============================================================================
// GOMWeapon_RPD.uc
//=============================================================================
// North Vietnamese Army RPD LMG.
// Modified to only carry drum ammo.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_RPD extends ROWeap_RPD_LMG
	abstract;

defaultproperties
{
	WeaponContentClass.Empty
	WeaponContentClass(0)="GOM3.GOMWeapon_RPD_ActualContent"
	
	AmmoContentClassStart=-1
	AltAmmoLoadouts.Empty
	
	RoleSelectionImage.Empty
	RoleSelectionImage(0)=Texture2D'VN_UI_Textures.WeaponTex.VN_Weap_RPD_LMG_Drum'
}