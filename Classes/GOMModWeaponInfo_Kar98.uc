//=============================================================================
// GOMModWeaponInfo_Kar98.uc
//=============================================================================
// UI information for the Kar98k.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfo_Kar98 extends ROModWeaponInfo;

DefaultProperties
{
	ModWeaponClass=GOMWeapon_Kar98k_ActualContent
	
	InventoryIconTexturePath="GOM3_UI.WP_Select.ui_hud_weaponselect_Kar98"
	
	AmmoIconTexturePath="VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M40"
	
	KillIconTexturePath="GOM3_UI.WP_KillIcon.ui_kill_icon_Kar98"
	bHasSquareKillIcon=false
}