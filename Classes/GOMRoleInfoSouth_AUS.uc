//=============================================================================
// GOMRoleInfoSouth_AUS.uc
//=============================================================================
// Basic info for all Southern Australian Roles.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_AUS extends GOMRoleInfo
	abstract;

DefaultProperties
{
	Items[RORIGM_Default]={(
		SecondaryWeapons=(class'ROWeap_BHP_Pistol'),
		SquadLeaderItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		SecondaryWeapons=(class'ROWeap_BHP_Pistol'),
		SquadLeaderItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		SecondaryWeapons=(class'ROWeap_BHP_Pistol'),
		SquadLeaderItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	RoleRootClass=class'GOMRoleInfoSouth_AUS'
}
