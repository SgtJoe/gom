//=============================================================================
// GOMWeapon_DP28.uc
//=============================================================================
// Viet Cong DP-28 LMG.
// Modified to reduce weight of ammo drums.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_DP28 extends ROWeap_DP28_LMG
	abstract;

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMWeapon_DP28_ActualContent"
	
	AmmoClass=class'GOMAmmo_DP28'
}
