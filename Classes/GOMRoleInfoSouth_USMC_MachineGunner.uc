//=============================================================================
// GOMRoleInfoSouth_USMC_MachineGunner.uc
//=============================================================================
// United States Marine Corps Machine Gunner Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_USMC_MachineGunner extends GOMRoleInfoSouth_US_MachineGunner;

DefaultProperties
{
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M60_GPMG',class'GOMWeapon_M1919A6')
	)}
}
