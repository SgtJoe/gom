//=============================================================================
// GOMDmgType_RoadRage.uc
//=============================================================================
// Damage type for being run over by a ground vehicle.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMDmgType_RoadRage extends RODamageType_RunOver;

DefaultProperties
{
	WeaponShortName="ROADKILL!!"
	
	bAlwaysGibs=false
}
