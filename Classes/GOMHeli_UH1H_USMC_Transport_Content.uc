//=============================================================================
// GOMHeli_UH1H_USMC_Transport_Content.uc
//=============================================================================
// UH-1H Iroquois Transport Helicopter (USMC)
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHeli_UH1H_USMC_Transport_Content extends ROHeli_UH1H_Content
	placeable;

`include(GOM3\Classes\GOMHelicoptersCommon.uci)

defaultproperties
{
	Begin Object Name=ROSVehicleMesh
		Materials(0)=MaterialInstanceConstant'GOM3_VH_US_UH1H_USMC.MIC.VH_US_Huey_UH1H_USMC_Transport'
	End Object
}
