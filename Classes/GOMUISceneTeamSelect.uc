//=============================================================================
// GOMUISceneTeamSelect.uc
//=============================================================================
// In-game menu for selecting teams.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMUISceneTeamSelect extends ROUISceneTeamSelect;

var localized array<string> NorthDescriptions;
var localized array<string> SouthDescriptions;

function InitializeButtonStyle(ROPlayerController ROPC)
{
	switch (`getNF)
	{
		case `VC:
			NorthButtonImageEnabled = NorthEnabledLogos[`VC];
			NorthButtonImageDisabled = NorthDisabledLogos[`VC];
			NorthButtonImageHighlighted = NorthHighlightedLogos[`VC];
			TeamLabelButtons[0].SetCaption(class'GOM3'.default.NorthArmyNames[`VC]);
			break;
		
		case `NVA:
			NorthButtonImageEnabled = NorthEnabledLogos[`NVA];
			NorthButtonImageDisabled = NorthDisabledLogos[`NVA];
			NorthButtonImageHighlighted = NorthHighlightedLogos[`NVA];
			TeamLabelButtons[0].SetCaption(class'GOM3'.default.NorthArmyNames[`NVA]);
			break;
		
		case `NVAU:
			NorthButtonImageEnabled = NorthEnabledLogos[`NVAU];
			NorthButtonImageDisabled = NorthDisabledLogos[`NVAU];
			NorthButtonImageHighlighted = NorthHighlightedLogos[`NVAU];
			TeamLabelButtons[0].SetCaption(class'GOM3'.default.NorthArmyNames[`NVAU]);
			break;
		
		default:
			TeamLabelButtons[0].SetCaption("ERROR");
	}
	
	switch (`getSF)
	{
		case `USMC:
			SouthButtonImageEnabled = SouthEnabledLogos[`USMC];
			SouthButtonImageDisabled = SouthDisabledLogos[`USMC];
			SouthButtonImageHighlighted = SouthHighlightedLogos[`USMC];
			TeamLabelButtons[1].SetCaption(class'GOM3'.default.SouthArmyNames[`USMC]);
			break;
		
		case `MACV:
			SouthButtonImageEnabled = SouthEnabledLogos[`MACV];
			SouthButtonImageDisabled = SouthDisabledLogos[`MACV];
			SouthButtonImageHighlighted = SouthHighlightedLogos[`MACV];
			TeamLabelButtons[1].SetCaption(class'GOM3'.default.SouthArmyNames[`MACV]);
			break;
		
		case `AUS:
			SouthButtonImageEnabled = SouthEnabledLogos[`AUS];
			SouthButtonImageDisabled = SouthDisabledLogos[`AUS];
			SouthButtonImageHighlighted = SouthHighlightedLogos[`AUS];
			TeamLabelButtons[1].SetCaption(class'GOM3'.default.SouthArmyNames[`AUS]);
			break;
		
		case `US:
			SouthButtonImageEnabled = SouthEnabledLogos[`US];
			SouthButtonImageDisabled = SouthDisabledLogos[`US];
			SouthButtonImageHighlighted = SouthHighlightedLogos[`US];
			TeamLabelButtons[1].SetCaption(class'GOM3'.default.SouthArmyNames[`US]);
			break;
		
		case `ARVN:
			SouthButtonImageEnabled = SouthEnabledLogos[`ARVN];
			SouthButtonImageDisabled = SouthDisabledLogos[`ARVN];
			SouthButtonImageHighlighted = SouthHighlightedLogos[`ARVN];
			TeamLabelButtons[1].SetCaption(class'GOM3'.default.SouthArmyNames[`ARVN]);
			break;
		
		case `RANGERS:
			SouthButtonImageEnabled = SouthEnabledLogos[`RANGERS];
			SouthButtonImageDisabled = SouthDisabledLogos[`RANGERS];
			SouthButtonImageHighlighted = SouthHighlightedLogos[`RANGERS];
			TeamLabelButtons[1].SetCaption(class'GOM3'.default.SouthArmyNames[`RANGERS]);
			break;
		
		case `ROK:
			SouthButtonImageEnabled = SouthEnabledLogos[`ROK];
			SouthButtonImageDisabled = SouthDisabledLogos[`ROK];
			SouthButtonImageHighlighted = SouthHighlightedLogos[`ROK];
			TeamLabelButtons[1].SetCaption(class'GOM3'.default.SouthArmyNames[`ROK]);
			break;
		
		case `ROKA:
			SouthButtonImageEnabled = SouthEnabledLogos[`ROKA];
			SouthButtonImageDisabled = SouthDisabledLogos[`ROKA];
			SouthButtonImageHighlighted = SouthHighlightedLogos[`ROKA];
			TeamLabelButtons[1].SetCaption(class'GOM3'.default.SouthArmyNames[`ROKA]);
			break;
		
		case `ROKMC:
			SouthButtonImageEnabled = SouthEnabledLogos[`ROKMC];
			SouthButtonImageDisabled = SouthDisabledLogos[`ROKMC];
			SouthButtonImageHighlighted = SouthHighlightedLogos[`ROKMC];
			TeamLabelButtons[1].SetCaption(class'GOM3'.default.SouthArmyNames[`ROKMC]);
			break;
		
		default:
			TeamLabelButtons[1].SetCaption("ERROR");
	}
	
	TeamImages[0].SetValue(NorthButtonImageEnabled);
	TeamImages[1].SetValue(SouthButtonImageEnabled);
}

function ShowTeamInfo()
{
	local UIPanel Container;
	Container = UIPanel(FindChild(TeamInfoContainerName, true));
	
	if( TeamInfoTeamIndex == `AXIS_TEAM_INDEX )
	{
		switch (`getNF)
		{
			case `VC:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(NorthDescriptions[`VC]);
				break;
			
			case `NVAU:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(NorthDescriptions[`NVAU]);
				break;
			
			case `NVA:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(NorthDescriptions[`NVA]);
				break;
			
			default:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue("ERROR");
		}
	}
	else
	{
		switch (`getSF)
		{
			case `USMC:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(SouthDescriptions[`USMC]);
				break;
			
			case `MACV:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(SouthDescriptions[`MACV]);
				break;
			
			case `AUS:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(SouthDescriptions[`AUS]);
				break;
			
			case `US:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(SouthDescriptions[`US]);
				break;
			
			case `ARVN:
			case `RANGERS:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(SouthDescriptions[`ARVN]);
				break;
			
			case `ROK:
			case `ROKA:
			case `ROKMC:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(SouthDescriptions[`ROK]);
				break;
			
			default:
				UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue("ERROR");
		}
	}
	
	Container.SetDockParameters(UIFACE_Top, TeamImages[TeamInfoTeamIndex], UIFACE_Top, 0.5, UIPADDINGEVAL_PercentTarget);
	Container.SetDockParameters(UIFACE_Left, TeamImages[TeamInfoTeamIndex], UIFACE_Left, 0.0);
	Container.SetDockParameters(UIFACE_Right, TeamImages[TeamInfoTeamIndex], UIFACE_Right, 0.0);
	Container.SetVisibility(TeamInfoTeamIndex < 2);
}

defaultproperties
{
	NorthEnabledLogos(`VC)=			MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_VC_desat'
	NorthDisabledLogos(`VC)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_VC_disabled'
	NorthHighlightedLogos(`VC)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_VC_highlighted'
	NorthEnabledLogos(`NVA)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_NVA_desat'
	NorthDisabledLogos(`NVA)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_NVA_disabled'
	NorthHighlightedLogos(`NVA)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_NVA_highlighted'
	NorthEnabledLogos(`NVAU)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_NVAU_desat'
	NorthDisabledLogos(`NVAU)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_NVAU_disabled'
	NorthHighlightedLogos(`NVAU)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_NVAU_highlighted'
	
	
	SouthEnabledLogos(`US)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_US_desat'
	SouthEnabledLogos(`USMC)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_USMC_desat'
	SouthEnabledLogos(`MACV)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_MACV_desat'
	SouthEnabledLogos(`ARVN)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ARVN_desat'
	SouthEnabledLogos(`RANGERS)=MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ARVN_Rangers_desat'
	SouthEnabledLogos(`AUS)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_AUS_desat'
	SouthEnabledLogos(`ROK)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ROK_desat'
	SouthEnabledLogos(`ROKA)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ROKA_desat'
	SouthEnabledLogos(`ROKMC)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ROKMC_desat'
	
	SouthDisabledLogos(`US)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_US_disabled'
	SouthDisabledLogos(`USMC)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_USMC_disabled'
	SouthDisabledLogos(`MACV)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_MACV_disabled'
	SouthDisabledLogos(`ARVN)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ARVN_disabled'
	SouthDisabledLogos(`RANGERS)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ARVN_Rangers_disabled'
	SouthDisabledLogos(`AUS)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_AUS_disabled'
	SouthDisabledLogos(`ROK)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ROK_disabled'
	SouthDisabledLogos(`ROKA)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ROKA_disabled'
	SouthDisabledLogos(`ROKMC)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ROKMC_disabled'
	
	SouthHighlightedLogos(`US)=		MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_US_highlighted'
	SouthHighlightedLogos(`USMC)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_USMC_highlighted'
	SouthHighlightedLogos(`MACV)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_MACV_highlighted'
	SouthHighlightedLogos(`ARVN)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ARVN_highlighted'
	SouthHighlightedLogos(`RANGERS)=MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ARVN_Rangers_highlighted'
	SouthHighlightedLogos(`AUS)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_AUS_highlighted'
	SouthHighlightedLogos(`ROK)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ROK_highlighted'
	SouthHighlightedLogos(`ROKA)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ROKA_highlighted'
	SouthHighlightedLogos(`ROKMC)=	MaterialInstanceConstant'GOM3_UI.TeamCutouts.Team_Cutout_ROKMC_highlighted'
}
