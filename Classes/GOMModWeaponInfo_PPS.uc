//=============================================================================
// GOMModWeaponInfo_PPS.uc
//=============================================================================
// UI information for the PPS-43.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfo_PPS extends ROModWeaponInfo;

DefaultProperties
{
	ModWeaponClass=GOMWeapon_PPS_ActualContent
	
	InventoryIconTexturePath="GOM3_UI.WP_Select.UI_HUD_WeaponSelect_K50M"
	
	AmmoIconTexturePath="VN_UI_Textures.HUD.Ammo.ui_hud_ammo_ppsh_stick"
	
	KillIconTexturePath="GOM3_UI.WP_KillIcon.UI_Kill_Icon_K50M"
	bHasSquareKillIcon=false
}
