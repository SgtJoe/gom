//=============================================================================
// GOMWeapon_PPS_ActualContent.uc
//=============================================================================
// North Vietnamese Army PPS-43 SMG (Content class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_PPS_ActualContent extends GOMWeapon_PPS;

DefaultProperties
{
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_NVA_PPS.Mesh.Sov_PPS42_UPGD2'
		PhysicsAsset=none
		AnimSets(0)=AnimSet'GOM3_WP_VN_NVA_PPS.Anim.WP_pps42hands_UPGD1'
		AnimTreeTemplate=AnimTree'GOM3_WP_VN_NVA_PPS.Anim.Sov_PPS42_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	ArmsAnimSet=AnimSet'GOM3_WP_VN_NVA_PPS.Anim.WP_pps42hands_UPGD1'
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_NVA_PPS.Mesh.PPS42_3rd_Master_UPGD2'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.M3_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.M3A1_SMG_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'GOMWeapon_PPS_Attach'
}
