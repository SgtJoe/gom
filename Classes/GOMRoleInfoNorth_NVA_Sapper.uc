//=============================================================================
// GOMRoleInfoNorth_NVA_Sapper.uc
//=============================================================================
// North Vietnamese Army Sapper Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_NVA_Sapper extends GOMRoleInfoNorth_NVA;

DefaultProperties
{
	RoleType=RORIT_Engineer
	ClassTier=3
	ClassIndex=`ROCI_ENGINEER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle',class'GOMWeapon_PPS',class'GOMWeapon_PPSH'),
		OtherItems=(class'ROWeap_MD82_Mine',class'GOMWeapon_Satchel'),
		OtherItemsStartIndexForPrimary=(0, 1, 1),
		NumOtherItemsForPrimary=(1, 1, 1)
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle',class'GOMWeapon_PPS',class'GOMWeapon_PPSH'),
		OtherItems=(class'ROWeap_MD82_Mine',class'GOMWeapon_Satchel'),
		OtherItemsStartIndexForPrimary=(0, 1, 1),
		NumOtherItemsForPrimary=(1, 1, 1)
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle',class'GOMWeapon_PPS',class'GOMWeapon_PPSH'),
		OtherItems=(class'ROWeap_MD82_Mine',class'GOMWeapon_Satchel'),
		OtherItemsStartIndexForPrimary=(0, 1, 1),
		NumOtherItemsForPrimary=(1, 1, 1)
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle',class'ROWeap_K50M_SMG',class'GOMWeapon_PPSH'),
		OtherItems=(class'ROWeap_MD82_Mine',class'GOMWeapon_Satchel'),
		OtherItemsStartIndexForPrimary=(0, 1, 1),
		NumOtherItemsForPrimary=(1, 1, 1)
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sapper'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_sapper'
}
