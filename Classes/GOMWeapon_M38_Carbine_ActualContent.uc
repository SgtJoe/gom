//=============================================================================
// GOMWeapon_M38_Carbine_ActualContent.uc
//=============================================================================
// Viet Cong M38 Carbine bolt-action rifle (Content class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M38_Carbine_ActualContent extends GOMWeapon_M38_Carbine;

defaultproperties
{
	ArmsAnimSet=AnimSet'WP_VN_VC_MN9130_rifle.animation.WP_MN9130Hands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_VC_M44.Mesh.M44_Carbine'
		AnimSets(0)=AnimSet'WP_VN_VC_MN9130_rifle.animation.WP_MN9130Hands'
		AnimTreeTemplate=AnimTree'WP_VN_VC_MN9130_rifle.animation.Sov_MN9130_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_VC_M44.Mesh.M44_Carbine_3rd'
		PhysicsAsset=PhysicsAsset'GOM3_WP_VN_VC_M44.Phy.M44_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.MN9130_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'GOMWeapon_M38_Carbine_Attach'
}