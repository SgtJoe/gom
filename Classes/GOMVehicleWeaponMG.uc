//=============================================================================
// GOMVehicleWeaponMG.uc
//=============================================================================
// Base class for vehicle-mounted machine guns.
// Default setup is for an M2 Browning.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleWeaponMG extends ROHelicopterDoorWeapon
	abstract;

simulated function bool ShouldFireFromMuzzleLocation()
{
	return (Instigator.IsHumanControlled() && (MyVehicle.SeatPositionIndex(SeatIndex,,true) != MyVehicle.Seats[SeatIndex].FiringPositionIndex));
}

function ConsumeAmmo( byte FireModeNum )
{
	if( FireModeNum == DEFAULT_FIREMODE )
		AddAmmo(Min(-RoundsPerShot[FireModeNum],-1));
	else
		AddAltAmmo(Min(-RoundsPerShot[FireModeNum],-1));
	
	if (!HasAmmo(FireModeNum))
	{
		MyVehicle.HandleBattleChatterEvent(`BATTLECHATTER_OutOfAmmo);
	}
	else if (GetHeloAmmoPercentage(FireModeNum) < 0.30)
	{
		MyVehicle.HandleBattleChatterEvent(`BATTLECHATTER_LowOnAmmo);
	}
}

simulated function StartFire(byte FireModeNum)
{
	super.StartFire(FireModeNum);
	
	if (!HasAmmo(DEFAULT_FIREMODE))
	{
		MyVehicle.HandleBattleChatterEvent(`BATTLECHATTER_OutOfAmmo);
		
		WeaponPlaySound(WeaponDryFireSnd);
		if( Role < ROLE_Authority )
		{
			ServerPlayDryFireSound();
		}
	}
}

defaultproperties
{
	VehicleClass=none
	
	SeatIndex=-1
	
	FireTriggerTags=(MG)
	
	ZoomedFOV=55
	
	bResetWhenUnattended=false
	
	FiringStatesArray(0)=WeaponFiring
	WeaponFireTypes(0)=EWFT_Projectile
	WeaponProjectiles(0)=class'M2Bullet'
	FireInterval(0)=+0.12
	FireCameraAnim(0)=CameraAnim'1stperson_Cameras.Anim.Camera_MG34_Shoot'
	Spread(0)=0.00025
	
	FiringStatesArray(ALTERNATE_FIREMODE)=none
	WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_Projectile
	WeaponProjectiles(ALTERNATE_FIREMODE)=none
	FireInterval(ALTERNATE_FIREMODE)=+0.1
	FireCameraAnim(ALTERNATE_FIREMODE)=CameraAnim'1stperson_Cameras.Anim.Camera_MG34_Shoot'
	Spread(ALTERNATE_FIREMODE)=0.0007
	
	AmmoDisplayNames(0)="M2"
	
	MaxAmmoCount=1000
	bShowAmmoAsPercentage=true
	
	PenetrationDepth=23.5
	MaxPenetrationTests=3
	MaxNumPenetrations=2
	
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_USA_M2'
	ShellEjectSocket=MG_ShellEject
	
	SuppressionPower=25.0
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M2_Browning.Play_WEP_M2_Browning_Loop_3P',FirstPersonCue=AkEvent'WW_WEP_M2_Browning.Play_WEP_M2_Browning_Auto_LP')
	WeaponFireLoopEndSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M2_Browning.Play_WEP_M2_Browning_Tail_3P',FirstPersonCue=AkEvent'WW_WEP_M2_Browning.Play_WEP_M2_Browning_Auto_Tail')
}