//=============================================================================
// GOMRoleInfoSouth_USMC_Pointman.uc
//=============================================================================
// United States Marine Corps Pointman Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_USMC_Pointman extends GOMRoleInfoSouth_US_Pointman;

DefaultProperties
{
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M1A1_SMG',class'ROWeap_M37_Shotgun',class'ROWeap_M2_Carbine')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M1A1_SMG',class'ROWeap_M37_Shotgun',class'ROWeap_M2_Carbine')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M3A1_SMG',class'ROWeap_M37_Shotgun',class'ROWeap_M2_Carbine')
	)}
}
