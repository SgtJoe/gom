//=============================================================================
// GOMModWeaponInfo_Bowie.uc
//=============================================================================
// UI information for the Bowie.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfo_Bowie extends ROModWeaponInfo;

DefaultProperties
{
	ModWeaponClass=GOMWeapon_BowieKnife_ActualContent
	
	InventoryIconTexturePath="VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_PickMattock"
	
	AmmoIconTexturePath="VN_UI_Textures.HUD.Ammo.HUD_UI_Ammo_PickMattock"
	
	KillIconTexturePath="VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_Bayonet"
	bHasSquareKillIcon=true
}