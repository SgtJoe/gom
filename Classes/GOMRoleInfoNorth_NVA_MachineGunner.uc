//=============================================================================
// GOMRoleInfoNorth_NVA_MachineGunner.uc
//=============================================================================
// North Vietnamese Army Machine Gunner Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_NVA_MachineGunner extends GOMRoleInfoNorth_NVA;

DefaultProperties
{
	RoleType=RORIT_MachineGunner
	ClassTier=2
	ClassIndex=`ROCI_MACHINEGUNNER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'GOMWeapon_RPD',class'ROWeap_RP46_LMG')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'GOMWeapon_RPD',class'ROWeap_RP46_LMG')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'GOMWeapon_RPD',class'ROWeap_RP46_LMG')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'GOMWeapon_RPD',class'ROWeap_RP46_LMG')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_mg'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_mg'
}
