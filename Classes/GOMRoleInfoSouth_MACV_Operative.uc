//=============================================================================
// GOMRoleInfoSouth_MACV_Operative.uc
//=============================================================================
// MACV-SOG Operative Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_MACV_Operative extends GOMRoleInfoSouth_MACV;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=1
	ClassIndex=`ROCI_RIFLEMAN
	
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'ROWeap_XM177E1_Carbine'),
	
	OtherItems=(class'ROWeap_M61_Grenade')
	
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_grunt'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_guerilla'
}
