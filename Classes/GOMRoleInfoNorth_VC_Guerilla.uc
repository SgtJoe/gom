//=============================================================================
// GOMRoleInfoNorth_VC_Guerilla.uc
//=============================================================================
// Viet Cong Guerilla Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_VC_Guerilla extends GOMRoleInfoNorth_VC;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=1
	ClassIndex=`ROCI_RIFLEMAN
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'GOMWeapon_M1_Carbine',class'GOMWeapon_M38_Carbine',class'GOMWeapon_Kar98k'),
		OtherItems=(class'ROWeap_PunjiTrap',class'ROWeap_Type67_Grenade')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'GOMWeapon_M1_Carbine',class'GOMWeapon_M38_Carbine',class'GOMWeapon_Kar98k'),
		OtherItems=(class'ROWeap_PunjiTrap',class'ROWeap_Type67_Grenade')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle',class'GOMWeapon_M38_Carbine',class'GOMWeapon_Kar98k'),
		OtherItems=(class'ROWeap_PunjiTrap',class'ROWeap_Type67_Grenade')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle'),
		OtherItems=(class'ROWeap_Type67_Grenade')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'GOM3_UI.RoleIcons.class_icon_guerilla'
	ClassIconLarge=Texture2D'GOM3_UI.RoleIcons.class_icon_large_guerilla'
}
