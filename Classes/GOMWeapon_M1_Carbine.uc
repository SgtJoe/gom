//=============================================================================
// GOMWeapon_M1_Carbine.uc
//=============================================================================
// Viet Cong M1 Carbine.
// Modified viewmodel mesh.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M1_Carbine extends ROWeap_M1_Carbine
	abstract;

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMWeapon_M1_Carbine_ActualContent"
}