//=============================================================================
// GOMRoleInfoSouth_AUS_Engineer.uc
//=============================================================================
// Australian Army Combat Engineer Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_AUS_Engineer extends GOMRoleInfoSouth_AUS;

DefaultProperties
{
	RoleType=RORIT_Engineer
	ClassTier=3
	ClassIndex=`ROCI_ENGINEER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_L1A1_Rifle',class'ROWeap_F1_SMG',class'ROWeap_M9_Flamethrower'),
		DisableSecondaryForPrimary=(true, true, false),
		OtherItems=(class'ROWeap_M61_GrenadeQuad',class'ROWeap_C4_Explosive'),
		OtherItemsStartIndexForPrimary=(0, 0, 0),
		NumOtherItemsForPrimary=(0, 0, 255)
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_L1A1_Rifle',class'ROWeap_F1_SMG',class'ROWeap_M9_Flamethrower'),
		DisableSecondaryForPrimary=(true, true, false),
		OtherItems=(class'ROWeap_M61_GrenadeQuad',class'ROWeap_C4_Explosive'),
		OtherItemsStartIndexForPrimary=(0, 0, 0),
		NumOtherItemsForPrimary=(0, 0, 255)
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_L1A1_Rifle',class'ROWeap_F1_SMG',class'ROWeap_M9_Flamethrower'),
		DisableSecondaryForPrimary=(true, true, false),
		OtherItems=(class'ROWeap_M61_GrenadeQuad',class'ROWeap_C4_Explosive'),
		OtherItemsStartIndexForPrimary=(0, 0, 0),
		NumOtherItemsForPrimary=(0, 0, 255)
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sapper'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_sapper'
}
