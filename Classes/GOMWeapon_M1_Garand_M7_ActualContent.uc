//=============================================================================
// GOMWeapon_M1_Garand_M7_ActualContent.uc
//=============================================================================
// ARVN M1 Garand Rifle (Content class).
// Modified for M7 Rifle Grenade functionality.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M1_Garand_M7_ActualContent extends GOMWeapon_M1_Garand_M7;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WP_VN_ARVN_M1Garand_Rifle.Animation.WP_M1GarHands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WP_VN_ARVN_M1Garand_Rifle.Mesh.ARVn_M1Gar'
		PhysicsAsset=PhysicsAsset'WP_VN_ARVN_M1Garand_Rifle.Phys.ARVN_M1Gar_Physics'
		AnimSets(0)=AnimSet'WP_VN_ARVN_M1Garand_Rifle.Animation.WP_M1GarHands'
		AnimTreeTemplate=AnimTree'WP_VN_ARVN_M1Garand_Rifle.Animation.WP_M1GarandHands_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WP_VN_ARVN_3rd_Master.Mesh.M1Garand_3rd_Master'
		PhysicsAsset=PhysicsAsset'WP_VN_ARVN_3rd_Master.Phys.M1Garand_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_ARVN_3rd_Master.AnimTree.M1Garand_Rifle_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'GOMWeapon_M1_Garand_M7_Attach'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M1_Garand.Play_WEP_M1_Garand_Single_3P', FirstPersonCue=AkEvent'WW_WEP_M1_Garand.Play_WEP_M1_Garand_Fire_Single')
	WeaponFirePingSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M1_Garand.Play_WEP_M1_Garand_Ping_3P',FirstPersonCue=AkEvent'WW_WEP_M1_Garand.Play_WEP_M1_Garand_Ping')
}
