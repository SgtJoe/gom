//=============================================================================
// GOMRoleInfoSouth_MACV_Radioman.uc
//=============================================================================
// MACV-SOG Radio Operator Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_MACV_Radioman extends GOMRoleInfoSouth_MACV;

DefaultProperties
{
	RoleType=RORIT_Radioman
	ClassTier=3
	ClassIndex=`ROCI_RADIOMAN
	bIsRadioman=true
	
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'ROWeap_XM177E1_Carbine')
	
	)}
	
	bAllowPistolsInRealism=false
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_radioman'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_radioman'
}
