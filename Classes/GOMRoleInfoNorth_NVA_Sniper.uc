//=============================================================================
// GOMRoleInfoNorth_NVA_Sniper.uc
//=============================================================================
// North Vietnamese Army Sniper Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_NVA_Sniper extends GOMRoleInfoNorth_NVA;

DefaultProperties
{
	RoleType=RORIT_Marksman
	ClassTier=3
	ClassIndex=`ROCI_SNIPER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_SVDScoped_Rifle',class'ROWeap_MN9130Scoped_Rifle')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_MN9130Scoped_Rifle')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_SVDScoped_Rifle',class'ROWeap_MN9130Scoped_Rifle')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_SVDScoped_Rifle')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sniper'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_sniper'
}
