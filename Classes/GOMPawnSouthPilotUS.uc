//=============================================================================
// GOMPawnSouthPilotUS.uc
//=============================================================================
// Southern Pilot Pawn (United States).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawnSouthPilotUS extends GOMPawnSouthUS;

DefaultProperties
{
	bIsPilot=true
	
	TunicMesh=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Pilot_Mesh'
	BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Pilot_A_INST'
	
	HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_04_Pilot_INST'
	
	FieldgearMesh=SkeletalMesh'CHR_VN_US_Army.GearMesh.US_Gear_Pilot'
	
	PawnMesh_SV=SkeletalMesh'CHR_VN_US_Army.Mesh_Low.US_Tunic_Pilot_Low_Mesh'
	
	HeadgearMesh=SkeletalMesh'CHR_VN_US_Headgear.PilotMesh.US_Headgear_Pilot_Base'
	HeadgearMICTemplate=MaterialInstanceConstant'CHR_VN_US_Headgear.Materials.M_US_Headgear_Pilot_INST'
	
	Begin Object name=ThirdPersonHeadgear0
		PhysicsAsset=PhysicsAsset'CHR_VN_Playeranim_Master.Phys.Headgear_Physics_Pilot'
	EndObject
}
