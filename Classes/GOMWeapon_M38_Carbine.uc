//=============================================================================
// GOMWeapon_M38_Carbine.uc
//=============================================================================
// Viet Cong M38 Carbine bolt-action rifle.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M38_Carbine extends ROWeap_MN9130_Rifle
	abstract;

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMWeapon_M38_Carbine_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'GOM3_UI.WP_Render.VN_Weap_M44_Carbine'
	
	InvIndex=`WI_M38
	bIsModWeapon=true
	
	AltFireModeType=ROAFT_None
	
	bHasBayonet=false
	
	bShortShotty=true
	
	CollisionCheckLength=45.5
	
	EquipTime=+0.44
	PutDownTime=+0.35
}