//=============================================================================
// GOMRoleInfoSouth_ARVN_Pointman.uc
//=============================================================================
// Republic of Vietnam Army Pointman Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ARVN_Pointman extends GOMRoleInfoSouth_ARVN;

DefaultProperties
{
	RoleType=RORIT_Scout
	ClassTier=2
	ClassIndex=`ROCI_SCOUT
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M1A1_SMG',class'ROWeap_M37_Shotgun',class'ROWeap_M2_Carbine'),
		OtherItems=(class'ROWeap_M8_Smoke'),
		SquadLeaderItems=(class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M1A1_SMG',class'ROWeap_M37_Shotgun',class'ROWeap_M2_Carbine'),
		OtherItems=(class'ROWeap_M8_Smoke'),
		SquadLeaderItems=(class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M1A1_SMG',class'ROWeap_M37_Shotgun',class'ROWeap_M16A1_AssaultRifle'),
		OtherItems=(class'ROWeap_M8_Smoke'),
		SquadLeaderItems=(class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_scout'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_scout'
}
