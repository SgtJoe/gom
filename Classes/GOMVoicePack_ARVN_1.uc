//=============================================================================
// GOMVoicePack_ARVN_1.uc
//=============================================================================
// ARVN Voice Pack.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVoicePack_ARVN_1 extends ROVoicePackSVietTeam01;

defaultproperties
{
	VoiceComs[`VOICECOM_TakeCover]=(Sound="AKEvent'WW_VOX_ARVN_Voice1.Play_ARVN_Soldier_1_TakingFireGeneric'")
	VoiceComs[`VOICECOM_AIConfirm]=(Sound="AKEvent'WW_VOX_ARVN_Voice1.Play_ARVN_Soldier_1_Confirm'")
	VoiceComs[`VOICECOM_HeloIdleSituation]=(Sound="AKEvent'WW_VOX_ARVN_Voice1.Play_ARVN_Pilot_1_HeloIdleGround'")
}
