//=============================================================================
// GOMHeli_OH6_AUS_Content.uc
//=============================================================================
// OH-6 "Loach" Observation Helicopter (RAAF)
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHeli_OH6_AUS_Content extends ROHeli_OH6_Aus_Content
	placeable;

`include(GOM3\Classes\GOMHelicoptersCommon.uci)
