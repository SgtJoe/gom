//=============================================================================
// GOMWeapon_M7RifleGrenade.uc
//=============================================================================
// M7 Rifle Grenade attachment for the M1 Garand.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M7RifleGrenade extends ROWeap_M1Garand_Rifle
	abstract;

function DestroyFrom(vector StartLocation, vector StartVelocity)
{
	ForceEndFire();
	DetachWeapon();
	ClientWeaponThrown();
	Destroy();
}

reliable client function ClientGrenadeWasThrown()
{
	GrenadewasThrown();
}

simulated function GrenadeWasThrown()
{
	if( CurrentMagCount < 1 )
	{
		if ( Instigator != none )
		{
			DestroyFrom(Instigator.Location, vect(0,0,1));
		}
	}
}

function ConsumeAmmo( byte FireModeNum )
{
	AddAmmo(-1);
	
	if( AmmoArray.Length > 0 && (ROInventoryManager(InvManager) == none || !ROInventoryManager(InvManager).bInfiniteAmmo) )
	{
		AmmoArray.Remove((AmmoArray.Length - 1), 1);
		CurrentMagCount = AmmoArray.Length;
		
		if ( ROPawn(Instigator) != none )
		{
			ROPawn(Instigator).ModifyEncumbrance(-1 * AmmoClass.default.Weight, self);
		}
		
		if ( ROInventoryManager(Instigator.InvManager) != none )
		{
			ROInventoryManager(Instigator.InvManager).CategoryCounts[ROIC_Ammo] -= 1.0 / AmmoClass.default.ClipsPerSlot;
		}
	}
}

function PerformMagazineReload()
{
	if( AmmoArray.Length == 0 || CurrentReloadStatus == RORS_OpeningBolt || CurrentReloadStatus == RORS_ClosingBolt )
	{
		return;
	}
	
	CurrentReloadStatus = RORS_Complete;
	
	if( AmmoArray.Length > 0 )
	{
		CurrentMagIndex = GetNextMagIndex(true);
		SetAmmo(AmmoArray[CurrentMagIndex]);
		CurrentMagCount = AmmoArray.Length;
	}
}

simulated function FireAmmunition()
{
	local float AnimDuration;
	local name AnimName;
	
	Super(ROProjectileWeapon).FireAmmunition();
	
	bLastShot = false;
	
	InstantDetachBayonet();
	
	if ( bUsingSights )
	{
		AnimName = WeaponFireSightedAnim[0];
	}
	else if ( bShouldered )
	{
		AnimName = WeaponFireShoulderedAnim[0];
	}
	else
	{
		AnimName = WeaponFireAnim[0];
	}
	
	if( AnimName != '' )
	{
		AnimDuration = SkeletalMeshComponent(Mesh).GetAnimLength(AnimName);
	}
	
	AnimDuration *= 0.5f;
	
	SetTimer(AnimDuration, false, 'GrenadewasThrown');
}

simulated function PlayFireEffects(byte FireModeNum, optional vector HitLocation)
{
	Super(ROProjectileWeapon).PlayFireEffects(FireModeNum, HitLocation);
}

simulated function bool ShouldFireFromMuzzleLocation()
{
	return Instigator.IsHumanControlled() ? true : super.ShouldFireFromMuzzleLocation();
}

simulated event PostInitAnimTree(SkeletalMeshComponent SkelComp)
{
	super.PostInitAnimTree(SkelComp);
	
	SkelComp.ForceSkelUpdate();
	
	InstantAttachBayonet();
}

simulated exec function SwitchFireMode() {}

simulated function CauseClipEject(){}

simulated state Active
{
	simulated function bool AllowCheckAmmo()
	{
		return false;
	}
}

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMWeapon_M7RifleGrenade_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'GOM3_UI.WP_Render.US_Weap_M7_RifleGrenade'
	
	WeaponClassType=ROWCT_Grenade
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	Category=ROIC_Grenade
	
	Weight=4.3 // kg
	
	InvIndex=`WI_M7
	bIsModWeapon=true
	
	InventoryWeight=1
	
	bCanThrow=false
	
	bCanSaveConfigs=false
	
	PlayerIronSightFOV=45.0//50.0
	
	// MAIN FIREMODE
	FiringStatesArray(0)=WeaponSingleFiring
	WeaponFireTypes(0)=EWFT_Projectile
	WeaponProjectiles(0)=class'M79GrenadeProjectile'
	FireInterval(0)=0.15
	DelayedRecoilTime(0)=0.0
	Spread(0)=0.01 // 36 MOA

	// ALT FIREMODE
	FiringStatesArray(ALTERNATE_FIREMODE)=none
	WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_Custom
	WeaponProjectiles(ALTERNATE_FIREMODE)=none
	FireInterval(ALTERNATE_FIREMODE)=0.15
	DelayedRecoilTime(ALTERNATE_FIREMODE)=0.01
	Spread(ALTERNATE_FIREMODE)=0.01

	PreFireTraceLength=2500 //50 Meters
	FireTweenTime=0.02

	//ShoulderedSpreadMod=3.0//6.0
	//HippedSpreadMod=5.0//10.0

	// AI
	MinBurstAmount=1
	MaxBurstAmount=4
	//BurstWaitTime=1.0

	// Recoil
	maxRecoilPitch=450//550//700
	minRecoilPitch=450//650
	maxRecoilYaw=40//280  //250
	minRecoilYaw=-40//-280 //200
	minRecoilYawAbsolute=100
	RecoilRate=0.07//0.09
	RecoilMaxYawLimit=500
	RecoilMinYawLimit=65035
	RecoilMaxPitchLimit=2000
	RecoilMinPitchLimit=63535
	RecoilISMaxYawLimit=500
	RecoilISMinYawLimit=65035
	RecoilISMaxPitchLimit=350
	RecoilISMinPitchLimit=65035
	RecoilBlendOutRatio=0.45//0.75//0.45
	RecoilViewRotationScale=0.5
   	//PostureHippedRecoilModifer=1.65
   	//PostureShoulderRecoilModifer=1.4

	InstantHitDamage(0)=0
	InstantHitDamage(1)=0

	InstantHitDamageTypes(0)=none
	InstantHitDamageTypes(1)=none

	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=none
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'

	// Shell eject FX
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons_Two.ShellEjects.FX_Wep_ShellEject_M1Garand'

	//Equip and putdown
	WeaponPutDownAnim=M1Garand_putaway
	WeaponEquipAnim=M1Garand_pullout
	WeaponDownAnim=M1Garand_Down
	WeaponUpAnim=M1Garand_Up

	// Fire Anims
	//Hip fire
	WeaponFireAnim(0)=M1Garand_shoot
	WeaponFireAnim(1)=M1Garand_Shoot
	WeaponFireLastAnim=M1Garand_Shoot
	//Shouldered fire
	WeaponFireShoulderedAnim(0)=M1Garand_shoot
	WeaponFireShoulderedAnim(1)=M1Garand_Shoot
	WeaponFireLastShoulderedAnim=M1Garand_Shoot
	//Fire using iron sights
	WeaponFireSightedAnim(0)=M1Garand_shoot
	WeaponFireSightedAnim(1)=M1Garand_shoot
	WeaponFireLastSightedAnim=M1Garand_shoot

	// Idle Anims
	//Hip Idle
	WeaponIdleAnims(0)=M1Garand_shoulder_idle
	WeaponIdleAnims(1)=M1Garand_shoulder_idle
	// Shouldered idle
	WeaponIdleShoulderedAnims(0)=M1Garand_shoulder_idle
	WeaponIdleShoulderedAnims(1)=M1Garand_shoulder_idle
	//Sighted Idle
	WeaponIdleSightedAnims(0)=M1Garand_shoulder_idle
	WeaponIdleSightedAnims(1)=M1Garand_shoulder_idle

	// Prone Crawl
	WeaponCrawlingAnims(0)=M1Garand_CrawlF
	WeaponCrawlStartAnim=M1Garand_Crawl_into
	WeaponCrawlEndAnim=M1Garand_Crawl_out

	//Reloading
	WeaponReloadEmptyMagAnim=M1Garand_Bayonet_Attach
	WeaponReloadNonEmptyMagAnim=M1Garand_Bayonet_Attach

	// Ammo check
	WeaponAmmoCheckAnim=M1Garand_ammocheck
	WeaponRestAmmoCheckAnim=M1Garand_ammocheck

	// Sprinting
	WeaponSprintStartAnim=M1Garand_sprint_into
	WeaponSprintLoopAnim=M1Garand_Sprint
	WeaponSprintEndAnim=M1Garand_sprint_out
	Weapon1HSprintStartAnim=M1Garand_1H_sprint_into
	Weapon1HSprintLoopAnim=M1Garand_1H_Sprint
	Weapon1HSprintEndAnim=M1Garand_1H_sprint_out

	// Mantling
	WeaponMantleOverAnim=M1Garand_Mantle

	// Cover/Blind Fire Anims
	WeaponRestAnim=M1Garand_shoulder_idle
	WeaponEquipRestAnim=M1Garand_shoulder_idle
	WeaponPutDownRestAnim=M1Garand_shoulder_idle
	WeaponBlindFireRightAnim=M1Garand_shoulder_idle
	WeaponBlindFireLeftAnim=M1Garand_shoulder_idle
	WeaponBlindFireUpAnim=M1Garand_shoulder_idle
	WeaponIdleToRestAnim=M1Garand_shoulder_idle
	WeaponRestToIdleAnim=M1Garand_shoulder_idle
	WeaponRestToBlindFireRightAnim=M1Garand_shoulder_idle
	WeaponRestToBlindFireLeftAnim=M1Garand_shoulder_idle
	WeaponRestToBlindFireUpAnim=M1Garand_shoulder_idle
	WeaponBlindFireRightToRestAnim=M1Garand_shoulder_idle
	WeaponBlindFireLeftToRestAnim=M1Garand_shoulder_idle
	WeaponBlindFireUpToRestAnim=M1Garand_shoulder_idle
	WeaponBFLeftToUpTransAnim=M1Garand_shoulder_idle
	WeaponBFRightToUpTransAnim=M1Garand_shoulder_idle
	WeaponBFUpToLeftTransAnim=M1Garand_shoulder_idle
	WeaponBFUpToRightTransAnim=M1Garand_shoulder_idle
	// Blind Fire ready
	WeaponBF_Rest2LeftReady=M1Garand_shoulder_idle
	WeaponBF_LeftReady2LeftFire=M1Garand_shoulder_idle
	WeaponBF_LeftFire2LeftReady=M1Garand_shoulder_idle
	WeaponBF_LeftReady2Rest=M1Garand_shoulder_idle
	WeaponBF_Rest2RightReady=M1Garand_shoulder_idle
	WeaponBF_RightReady2RightFire=M1Garand_shoulder_idle
	WeaponBF_RightFire2RightReady=M1Garand_shoulder_idle
	WeaponBF_RightReady2Rest=M1Garand_shoulder_idle
	WeaponBF_Rest2UpReady=M1Garand_shoulder_idle
	WeaponBF_UpReady2UpFire=M1Garand_shoulder_idle
	WeaponBF_UpFire2UpReady=M1Garand_shoulder_idle
	WeaponBF_UpReady2Rest=M1Garand_shoulder_idle
	WeaponBF_LeftReady2Up=M1Garand_shoulder_idle
	WeaponBF_LeftReady2Right=M1Garand_shoulder_idle
	WeaponBF_UpReady2Left=M1Garand_shoulder_idle
	WeaponBF_UpReady2Right=M1Garand_shoulder_idle
	WeaponBF_RightReady2Up=M1Garand_shoulder_idle
	WeaponBF_RightReady2Left=M1Garand_shoulder_idle
	WeaponBF_LeftReady2Idle=M1Garand_shoulder_idle
	WeaponBF_RightReady2Idle=M1Garand_shoulder_idle
	WeaponBF_UpReady2IdleM1Garand_shoulder_idle
	WeaponBF_Idle2UpReady=M1Garand_shoulder_idle
	WeaponBF_Idle2LeftReady=M1Garand_shoulder_idle
	WeaponBF_Idle2RightReady=M1Garand_shoulder_idle

	// Enemy Spotting
	WeaponSpotEnemyAnim=M1Garand_Spotting
	WeaponSpotEnemySightedAnim=M1Garand_Spotting

	// Melee anims
	WeaponMeleeAnims(0)=M1Garand_Bash
	WeaponMeleeHardAnim=M1Garand_BashHard
	WeaponBayonetMeleeAnims(0)=M1Garand_Stab
	WeaponBayonetMeleeHardAnim=M1Garand_StabHard
	MeleePullbackAnim=M1Garand_Pullback
	MeleeHoldAnim=M1Garand_Pullback_Hold

	EquipTime=+0.66//1.00
	PutDownTime=+0.66//0.75

	// bDebugWeapon = true

  	BoltControllerNames[0]=BoltSlide_M1Garand
  	BoltControllerNames[1]=BoltSlide_M1Garand_2
  	BoltControllerNames[2]=BoltLock_M1Garand
	BayonetSkelControlName=Bayonet_M1Garand

	ISFocusDepth=37.5
	ISFocusBlendRadius=0.5

	// Ammo
	MaxAmmoCount=1
	AmmoClass=class'ROAmmo_40x46_M79Grenade'
	bUsesMagazines=true
	InitialNumPrimaryMags=4
	MaxNumPrimaryMags=4
	bLosesRoundOnReload=false
	bPlusOneLoading=false
	bCanReloadNonEmptyMag=false
	bCanLoadStripperClip=false
	bCanLoadSingleBullet=false
	PenetrationDepth=10
	MaxPenetrationTests=3
	MaxNumPenetrations=1
	
	PlayerViewOffset=(X=0.75,Y=4.5,Z=-2.0)
	ZoomInRotation=(Pitch=-910,Yaw=0,Roll=2910)
	
	ShoulderedPosition=(X=-0.5,Y=3.4,Z=-3.75)
	
	// IronSightPosition=(X=-3,Y=0,Z=-0.03)
	IronSightPosition=(X=-0.5,Y=2,Z=-2.8)
	
	bHasIronSights=true
	
	RestDeployCollisionSize=(X=4,Y=6,Z=13)
	
	ZoomInTime=0.25//0.32
	ZoomOutTime=0.25//0.3

	Begin Object Class=ForceFeedbackWaveform Name=ForceFeedbackWaveformShooting1
		Samples(0)=(LeftAmplitude=30,RightAmplitude=50,LeftFunction=WF_Constant,RightFunction=WF_Constant,Duration=0.150)
	End Object
	WeaponFireWaveForm=ForceFeedbackWaveformShooting1

	CollisionCheckLength=55.25

	FireCameraAnim[0]=CameraAnim'1stperson_Cameras.Anim.Camera_SVT40_Shoot'
	FireCameraAnim[1]=CameraAnim'1stperson_Cameras.Anim.Camera_SVT40_Shoot'
	ShakeScaleSighted=1.75
	ShakeScaleControlled=1.25
	ShakeScaleStandard=1.75

	SightSlideControlName=Sight_Slide
	SightRotControlName=Sight_Rotation

	SightRanges.Empty
	SightRanges[0]=(SightRange=100,SightPitch=0,SightPositionOffset=0.02,SightSlideOffset=0.0,AddedPitch=70)

	SuppressionPower=25

	//SwayOffsetMod=2000.f
	RecoilOffsetModY = 1300.f
	RecoilOffsetModZ = 1300.f

	FireModeCanUseClientSideHitDetection[DEFAULT_FIREMODE]=false

	bNoAccuracyStat=true

	bHasBayonet=false

	SwayScale=1.29

	LagLimit=0.7
}

