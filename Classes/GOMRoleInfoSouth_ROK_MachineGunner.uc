//=============================================================================
// GOMRoleInfoSouth_ROK_MachineGunner.uc
//=============================================================================
// Republic of Korea Machine Gunner Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ROK_MachineGunner extends GOMRoleInfoSouth_ROK;

DefaultProperties
{
	RoleType=RORIT_MachineGunner
	ClassTier=2
	ClassIndex=`ROCI_MACHINEGUNNER
	
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'GOMWeapon_M1919A6',class'ROWeap_M60_GPMG',class'ROWeap_M1918_BAR')
	
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_mg'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_mg'
}
