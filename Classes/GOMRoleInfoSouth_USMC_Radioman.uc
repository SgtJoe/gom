//=============================================================================
// GOMRoleInfoSouth_USMC_Radioman.uc
//=============================================================================
// United States Marine Corps Radio Operator Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_USMC_Radioman extends GOMRoleInfoSouth_US_Radioman;

DefaultProperties
{
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M14_Rifle',class'ROWeap_M2_Carbine')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle',class'ROWeap_M14_Rifle')
	)}
}
