//=============================================================================
// GOMRoleInfoSouth_AUS_Pilot_Transport.uc
//=============================================================================
// Australian Army Transport Pilot Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_AUS_Pilot_Transport extends GOMRoleInfoSouth_AUS_Pilot_Combat;

DefaultProperties
{
	ClassIndex=`ROCI_TRANSPORTPILOT
	
	bIsTransportPilot=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_transportpilot'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_transportpilot'
}
