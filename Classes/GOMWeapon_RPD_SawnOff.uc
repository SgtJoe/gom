//=============================================================================
// GOMWeapon_RPD_SawnOff.uc
//=============================================================================
// MACV-SOG Sawn-Off RPD LMG.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_RPD_SawnOff extends ROWeap_RPD_LMG
	abstract;

defaultproperties
{
	WeaponContentClass.Empty
	WeaponContentClass(0)="GOM3.GOMWeapon_RPD_SawnOff_ActualContent"
	
	AmmoContentClassStart=-1
	AltAmmoLoadouts.Empty
	
	RoleSelectionImage.Empty
	RoleSelectionImage(0)=Texture2D'GOM3_UI.WP_Render.VN_Weap_RPD_SawnOff'
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	InvIndex=-1
	bIsModWeapon=true
	
	AltFireModeType=ROAFT_None
	
	bHasBipod=false
}
