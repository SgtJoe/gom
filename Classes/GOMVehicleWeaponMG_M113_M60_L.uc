//=============================================================================
// GOMVehicleWeaponMG_M113_M60_L.uc
//=============================================================================
// M113 ACAV Left Side M60.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleWeaponMG_M113_M60_L extends GOMVehicleWeaponMG_M113_M60_R
	abstract;

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMVehicleWeaponMG_M113_M60_L_ActualContent"
	
	SeatIndex=`M113_MGL
	
	FireTriggerTags=(MG_L)
	
	ShellEjectSocket=MG_L_ShellEject
}