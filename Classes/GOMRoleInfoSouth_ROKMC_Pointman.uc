//=============================================================================
// GOMRoleInfoSouth_ROKMC_Pointman.uc
//=============================================================================
// Republic of Korea Marine Corps Pointman Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ROKMC_Pointman extends GOMRoleInfoSouth_ROK_Pointman;

DefaultProperties
{
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'ROWeap_M3A1_SMG'),
	
	)}
}
