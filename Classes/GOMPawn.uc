//=============================================================================
// GOMPawn.uc
//=============================================================================
// Generic properties for all Pawns.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawn extends ROPawn
	abstract;

var SkeletalMesh HairMesh;
var ROSkeletalMeshComponent ThirdPersonHairMeshComponent;

var float GrimePct;

var MaterialInstanceConstant BodyMICTemplate2;
var MaterialInstanceConstant BodyMIC2;
var MaterialInstanceConstant GearMIC2;
var MaterialInstanceConstant GearMIC3;

var bool BandagedOnce, AltGear, needsGlove;

simulated event PreBeginPlay()
{
	PawnHandlerClass = class'GOMPawnHandler';
	
	super.PreBeginPlay();
}

simulated function SetPawnElementsByConfig(bool bViaReplication, optional ROPlayerReplicationInfo OverrideROPRI)
{
	local int TeamNum, ArmyIndex, ClassIndex, HonorLevel, VoiceIndex;
	local ROPlayerReplicationInfo ROPRI;
	local byte IsHelmet, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, SkinID, FaceItemID, FacialHairID, TattooID, bPilot, bNoFacialHair;
	local Texture2D ShirtD, ShirtN, ShirtS, TattooTex;
	local float TattooUOffset, TattooVOffset, TattooDrawScale;
	local bool isMP, isRegular, isARVN;
	
	PawnHandlerClass = class'GOMPawnHandler';
	
	TeamNum = GetTeamNum();
	
	if( OverrideROPRI != none )
		ROPRI = OverrideROPRI;
	else
		ROPRI = GOMPlayerReplicationInfo(PlayerReplicationInfo);
	
	`gom(self @ "PRI" @ ROPRI, 'Verification');
	
	if( ROPRI != none )
	{
		if( ROPRI.RoleInfo != none )
		{
			ClassIndex = ROPRI.RoleInfo.ClassIndex;
		}
		
		CheckGlove(ROPRI);
		
		if( !ROPRI.bBot )
		{
			if( bViaReplication )
			{
				HonorLevel = int(ROPRI.HonorLevel);
			}
			else if( ROPlayerController(Controller) != none )
			{
				ROPlayerController(Controller).StatsWrite.UpdateHonorLevel();
				HonorLevel = ROPlayerController(Controller).StatsWrite.HonorLevel;
			}
		}
	}
	
	if( TeamNum == 255 )
	{
		`gom(self @ "TeamNum is 255!", 'Pawns');
		if (self.IsA('GOMPawnNorth'))
		{
			TeamNum = `AXIS_TEAM_INDEX;
		}
		else if (self.IsA('GOMPawnSouth'))
		{
			TeamNum = `ALLIES_TEAM_INDEX;
		}
		`gom(self @ "TeamNum is now" @ TeamNum, 'Pawns');
	}
	
	if (TeamNum == `AXIS_TEAM_INDEX)
	{
		ArmyIndex = `getNF;
	}
	else
	{
		ArmyIndex = `getSF;
	}
	
	isMP = false;
	isRegular = false;
	isARVN = false;
	ROPRI.bUsesAltVoicePacks = false;
	
	// NVA regulars with Viet Cong
	if (
		(TeamNum == `AXIS_TEAM_INDEX && (`getNF <= `VC) && ClassIndex == `ROCI_RADIOMAN)
		||
		(TeamNum == `AXIS_TEAM_INDEX && (`getNF <= `VC) && ClassIndex == `ROCI_COMMANDER)
		)
	{
		isRegular = true;
		ArmyIndex = `NVAU;
	}
	
	// Military Police
	if (TeamNum == `ALLIES_TEAM_INDEX && `getSF <= `USMC && `getMF == MF_MilitaryPolice && ClassIndex == `ROCI_SCOUT)
	{
		isMP = true;
		ArmyIndex = `MP;
	}
	
	// ARVN Support
	if (TeamNum == `ALLIES_TEAM_INDEX && `getSF < `ARVN && `getMF == MF_ARVNSupport && (ClassIndex == `ROCI_RADIOMAN || ClassIndex == `ROCI_MACHINEGUNNER /*|| ClassIndex == `ROCI_SCOUT*/))
	{
		isARVN = true;
		ArmyIndex = `ARVN;
		ROPRI.bUsesAltVoicePacks = true;
	}
	
	if (ClassIndex == `ROCI_TANKCREW)
	{
		bPilot = 2;
	}
	else if (bIsPilot)
	{
		bPilot = 1;
	}
	else
	{
		bPilot = 0;
	}
	
	if (bUseSingleCharacterVariant)
	{
		isMP = false;
		isRegular = false;
		isARVN = false;
		ROPRI.bUsesAltVoicePacks = false;
		ArmyIndex = (TeamNum == `AXIS_TEAM_INDEX) ? `getNF : `getSF;
	}
	
	if (isMP || isRegular || isARVN)
	{
		bSetFinalMesh = false;
	}
	
	`gom(self @ "Team:" @ TeamNum @ "Army:" @ ArmyIndex @ GOMPlayerController(Controller).GetRoleNameFromIndex(ClassIndex), 'PawnsGear');
	`gom("Pilot?" @ bPilot @ "MP?" @ isMP @ "NVA Regular?" @ isRegular @ "ARVN Support?" @ isARVN, 'PawnsGear');
	
	if (!bViaReplication && IsHumanControlled())
	{
		PawnHandlerClass.static.GetCharConfig(TeamNum, ArmyIndex, bPilot, ClassIndex, HonorLevel, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI);
	}
	else if (ROPRI != none)
	{
		TunicID = ROPRI.CurrentCharConfig.TunicMesh;
		TunicMatID = ROPRI.CurrentCharConfig.TunicMaterial;
		ShirtID = ROPRI.CurrentCharConfig.ShirtTexture;
		HeadID = ROPRI.CurrentCharConfig.HeadMesh;
		HairID = ROPRI.CurrentCharConfig.HairMaterial;
		HeadgearID = ROPRI.CurrentCharConfig.HeadgearMesh;
		HeadgearMatID = ROPRI.CurrentCharConfig.HeadgearMaterial;
		FaceItemID = ROPRI.CurrentCharConfig.FaceItemMesh;
		FacialHairID = ROPRI.CurrentCharConfig.FacialHairMesh;
		TattooID = ROPRI.CurrentCharConfig.TattooTex;
		
		PawnHandlerClass.static.ValidateCharConfig(TeamNum, ArmyIndex, bPilot, HonorLevel, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI);
	}
	
	if (TunicID == 0 && TunicMatID == 0 && ShirtID == 0 && HeadID == 0 && HairID == 0 && HeadgearID == 0 && FaceItemID == 0 && TattooID == 0)
	{
		// Client hasn't saved their config yet, randomize it for them to avoid a bunch of 0-everything clones
		PawnHandlerClass.static.GetCharConfig(TeamNum, ArmyIndex, bPilot, ClassIndex, HonorLevel, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI, true);
		PawnHandlerClass.static.ValidateCharConfig(TeamNum, ArmyIndex, bPilot, HonorLevel, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI);
	}
	
	if (ROPRI.bBot)
	{
		TattooID = (TeamNum == `ALLIES_TEAM_INDEX && ArmyIndex == `MACV && rand(3) > 1) ? 3 : 0;
	}
	
	if (TeamNum == `AXIS_TEAM_INDEX && ClassIndex == `ROCI_COMMANDER)
	{
		TunicID = 0;
		TunicMatID = 0;
		HeadgearID = 0;
		HeadgearMatID = 0;
		TattooID = 0;
		
		TunicMesh = PawnHandlerClass.static.GetTunicMeshes(TeamNum, ArmyIndex, `ROCI_COMMANDER, 0);
		BodyMICTemplate = PawnHandlerClass.static.GetBodyMIC(TeamNum, ArmyIndex, `ROCI_COMMANDER, 0, 0);
	}
	else
	{
		TunicMesh = PawnHandlerClass.static.GetTunicMeshes(TeamNum, ArmyIndex, bPilot, TunicID);
		BodyMICTemplate = PawnHandlerClass.static.GetBodyMIC(TeamNum, ArmyIndex, bPilot, TunicID, TunicMatID);
	}
	
	BodyMICTemplate2 = class'GOMPawnHandler'.static.GetSecondaryBodyMIC(TeamNum, ArmyIndex, TunicID, TunicMatID);
	
	PawnMesh_SV = PawnHandlerClass.static.GetTunicMeshSV(TeamNum, ArmyIndex, ClassIndex, bPilot, bHasFlamethrower, BodyMICTemplate_SV);
	
	if( TeamNum == `AXIS_TEAM_INDEX )
	{
		FieldgearMesh = PawnHandlerClass.static.GetFieldgearMesh(TeamNum, ArmyIndex, TunicID, ClassIndex, TunicMatID);
	}
	else if (self.needsGlove)
	{
		FieldgearMesh = class'GOMPawnHandler'.static.GetAltFieldgearMesh(TeamNum, ArmyIndex, TunicID);
		
		if (FieldgearMesh == none)
		{
			`gom(self @ "could not get FieldgearMesh for Team" @ TeamNum @ "Army" @ ArmyIndex @ "TunicID" @ TunicID, 'PawnsGear');
			FieldgearMesh = PawnHandlerClass.static.GetFieldgearMesh(TeamNum, ArmyIndex, TunicID, ClassIndex, byte(bHasFlamethrower));
		}
	}
	else
	{
		FieldgearMesh = PawnHandlerClass.static.GetFieldgearMesh(TeamNum, ArmyIndex, TunicID, ClassIndex, byte(bHasFlamethrower));
	}
	
	if( bUseSingleCharacterVariant )
	{
		HeadID = 0;
		TunicID = 0;
	}
	
	HeadAndArmsMesh = PawnHandlerClass.static.GetHeadAndArmsMesh(TeamNum, ArmyIndex, bPilot, HeadID, SkinID);
	HeadAndArmsMICTemplate = PawnHandlerClass.static.GetHeadMIC(TeamNum, ArmyIndex, HeadID, TunicID, bPilot);
	
	if( HeadAndArmsMIC != none && PawnHandlerClass.static.GetShirtTextures(TeamNum, ArmyIndex, bPilot, TunicID, ShirtID, ShirtD, ShirtN, ShirtS) && !bUseSingleCharacterVariant)
	{
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtDiffuseParam,ShirtD);
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtNormalParam,ShirtN);
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtSpecParam,ShirtS);
	}
	
	TattooTex = PawnHandlerClass.static.GetTattooTexture(TeamNum, ArmyIndex, bPilot, TattooID, TattooUOffset, TattooVOffset, TattooDrawScale);
	
	if( TattooTex != none && HeadAndArmsMIC != none && !bUseSingleCharacterVariant)
	{
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.TattooParam,TattooTex);
		HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.TattooUOffsetParam,TattooUOffset);
		HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.TattooVOffsetParam,TattooVOffset);
		HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.TattooDrawScaleParam,TattooDrawScale);
	}
	
	if( !bUseSingleCharacterVariant )
	{
		UberGoreMesh = PawnHandlerClass.static.GetGoreMeshes(TeamNum, ArmyIndex, TunicID, SkinID, GoreMIC, Gore_LeftHand.GibClass, Gore_RightHand.GibClass, Gore_LeftLeg.GibClass, Gore_RightLeg.GibClass);
	}
	
	if (self.needsGlove)
	{
		ArmsOnlyMeshFP = class'GOMPawnHandler'.static.GetAltFPArmsMesh(TeamNum, ArmyIndex, TunicID, TunicMatID, SkinID, FPArmsSkinMaterialTemplate, FPArmsSleeveMaterialTemplate);
		
		if (ArmsOnlyMeshFP == none)
		{
			`gom(self @ "could not get ArmsOnlyMeshFP for Team" @ TeamNum @ "Army" @ ArmyIndex @ "TunicID" @ TunicID, 'PawnsGear');
			ArmsOnlyMeshFP = PawnHandlerClass.static.GetFPArmsMesh(TeamNum, ArmyIndex, bPilot, TunicID, TunicMatID, SkinID, FPArmsSkinMaterialTemplate, FPArmsSleeveMaterialTemplate);
		}
	}
	else
	{
		ArmsOnlyMeshFP = PawnHandlerClass.static.GetFPArmsMesh(TeamNum, ArmyIndex, bPilot, TunicID, TunicMatID, SkinID, FPArmsSkinMaterialTemplate, FPArmsSleeveMaterialTemplate);
	}
	
	if (TeamNum == `AXIS_TEAM_INDEX && ClassIndex == `ROCI_COMMANDER && !bUseSingleCharacterVariant)
	{
		HeadgearMesh = PawnHandlerClass.static.GetHeadgearMesh(TeamNum, ArmyIndex, `ROCI_COMMANDER, HeadID, HairID, 0, 0, HeadgearMICTemplate, HairMICTemplate, HeadgearAttachSocket, IsHelmet);
	}
	else if (bUseSingleCharacterVariant)
	{
		HeadgearMesh = PawnHandlerClass.static.GetHeadgearMesh(TeamNum, ArmyIndex, bPilot, 0, 0, 0, 0, HeadgearMICTemplate, HairMICTemplate, HeadgearAttachSocket, IsHelmet);
	}
	else
	{
		HeadgearMesh = PawnHandlerClass.static.GetHeadgearMesh(TeamNum, ArmyIndex, bPilot, HeadID, HairID, HeadgearID, HeadgearMatID, HeadgearMICTemplate, HairMICTemplate, HeadgearAttachSocket, IsHelmet);
	}
	
	HairMesh = class'GOMPawnHandler'.static.GetHairMesh(TeamNum, ArmyIndex, bPilot, HeadID, HairID, HeadgearID, HeadgearMICTemplate, HairMICTemplate);
	
	bHeadGearIsHelmet = false;
	
	FaceItemMesh = PawnHandlerClass.static.GetFaceItemMesh(TeamNum, ArmyIndex, bPilot, HeadgearID, FaceItemID, FaceItemAttachSocket, bNoFacialHair);
	FacialHairMesh = (bNoFacialHair == 1) ? none : PawnHandlerClass.static.GetFacialHairMesh(TeamNum, ArmyIndex, FacialHairID, FacialHairAttachSocket);
	
	if (GOMPlayerController(Controller) != none)
	{
		GOMPlayerController(Controller).SetSuitableVoicePack(TeamNum, ArmyIndex, SkinID);
	}
	else if (ROPRI.bBot)
	{
		if (TeamNum == `AXIS_TEAM_INDEX)
		{
			VoiceIndex = rand(3);
		}
		else
		{
			switch (`getSF)
			{
				case `ARVN:
				case `RANGERS:
					VoiceIndex = rand(3);
					break;
				
				default:
				VoiceIndex = (SkinID == 1) ? 0 : rand(2) + 1;
			}
		}
		ROPRI.VoicePackIndex = VoiceIndex;
	}
}

simulated function SetPawnElementsForPosedPawn(int TeamNum, int ArmyIndex, int ClassIndex, int PawnLevel, ROPlayerReplicationInfo ROPRI)
{
	local byte IsHelmet, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, SkinID, FaceItemID, FacialHairID, TattooID, bPilot, bNoFacialHair;
	local Texture2D ShirtD, ShirtN, ShirtS, TattooTex;
	local float TattooUOffset, TattooVOffset, TattooDrawScale;
	
	PawnHandlerClass = class'GOMPawnHandler';
	
	CheckGlove(ROPRI);
	
	if (ClassIndex == `ROCI_TANKCREW)
	{
		bPilot = 2;
	}
	else if (ClassIndex == `ROCI_COMBATPILOT || ClassIndex == `ROCI_TRANSPORTPILOT)
	{
		bPilot = 1;
	}
	else
	{
		bPilot = 0;
	}
	
	PawnHandlerClass.static.GetCharConfig(TeamNum, ArmyIndex, bPilot, ClassIndex, PawnLevel, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI, true);
	PawnHandlerClass.static.ValidateCharConfig(TeamNum, ArmyIndex, bPilot, PawnLevel, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI);
	
	HeadgearMatID =	0;
	FaceItemID =	0;
	FacialHairID =	0;
	TattooID =		0;
	
	if (TeamNum == `AXIS_TEAM_INDEX && ClassIndex == `ROCI_COMMANDER)
	{
		TunicID = 0;
		TunicMatID = 0;
		HeadgearID = 0;
		HeadgearMatID = 0;
		TattooID = 0;
		
		TunicMesh = PawnHandlerClass.static.GetTunicMeshes(TeamNum, ArmyIndex, `ROCI_COMMANDER, 0);
		BodyMICTemplate = PawnHandlerClass.static.GetBodyMIC(TeamNum, ArmyIndex, `ROCI_COMMANDER, 0, 0);
	}
	else
	{
		TunicMesh = PawnHandlerClass.static.GetTunicMeshes(TeamNum, ArmyIndex, bPilot, TunicID);
		BodyMICTemplate = PawnHandlerClass.static.GetBodyMIC(TeamNum, ArmyIndex, bPilot, TunicID, TunicMatID);
	}
	
	BodyMICTemplate2 = class'GOMPawnHandler'.static.GetSecondaryBodyMIC(TeamNum, ArmyIndex, TunicID, TunicMatID);
	
	if( TeamNum == `AXIS_TEAM_INDEX )
	{
		FieldgearMesh = PawnHandlerClass.static.GetFieldgearMesh(TeamNum, ArmyIndex, TunicID, ClassIndex, TunicMatID);
	}
	else if (self.needsGlove)
	{
		FieldgearMesh = class'GOMPawnHandler'.static.GetAltFieldgearMesh(TeamNum, ArmyIndex, TunicID);
		
		if (FieldgearMesh == none)
		{
			`gom(self @ "could not get FieldgearMesh for Team" @ TeamNum @ "Army" @ ArmyIndex @ "TunicID" @ TunicID, 'PawnsGear');
			FieldgearMesh = PawnHandlerClass.static.GetFieldgearMesh(TeamNum, ArmyIndex, TunicID, ClassIndex, byte(bHasFlamethrower));
		}
	}
	else
	{
		FieldgearMesh = PawnHandlerClass.static.GetFieldgearMesh(TeamNum, ArmyIndex, TunicID, ClassIndex, byte(bHasFlamethrower));
	}
	
	HeadAndArmsMesh = PawnHandlerClass.static.GetHeadAndArmsMesh(TeamNum, ArmyIndex, bPilot, HeadID, SkinID);
	HeadAndArmsMICTemplate = PawnHandlerClass.static.GetHeadMIC(TeamNum, ArmyIndex, HeadID, TunicID, bPilot);
	
	if( HeadAndArmsMIC != none && PawnHandlerClass.static.GetShirtTextures(TeamNum, ArmyIndex, bPilot, TunicID, ShirtID, ShirtD, ShirtN, ShirtS) && !bUseSingleCharacterVariant)
	{
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtDiffuseParam,ShirtD);
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtNormalParam,ShirtN);
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtSpecParam,ShirtS);
	}
	
	TattooTex = PawnHandlerClass.static.GetTattooTexture(TeamNum, ArmyIndex, bPilot, TattooID, TattooUOffset, TattooVOffset, TattooDrawScale);
	
	if (TattooTex != none && HeadAndArmsMIC != none)
	{
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.TattooParam,TattooTex);
		HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.TattooUOffsetParam,TattooUOffset);
		HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.TattooVOffsetParam,TattooVOffset);
		HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.TattooDrawScaleParam,TattooDrawScale);
	}
	
	if (TeamNum == `AXIS_TEAM_INDEX && ClassIndex == `ROCI_COMMANDER)
	{
		HeadgearMesh = PawnHandlerClass.static.GetHeadgearMesh(TeamNum, ArmyIndex, `ROCI_COMMANDER, HeadID, HairID, 0, 0, HeadgearMICTemplate, HairMICTemplate, HeadgearAttachSocket, IsHelmet);
	}
	else
	{
		HeadgearMesh = PawnHandlerClass.static.GetHeadgearMesh(TeamNum, ArmyIndex, bPilot, HeadID, HairID, HeadgearID, HeadgearMatID, HeadgearMICTemplate, HairMICTemplate, HeadgearAttachSocket, IsHelmet);
	}
	
	HairMesh = class'GOMPawnHandler'.static.GetHairMesh(TeamNum, ArmyIndex, bPilot, HeadID, HairID, HeadgearID, HeadgearMICTemplate, HairMICTemplate);
	
	bHeadGearIsHelmet = false;
	
	FaceItemMesh = PawnHandlerClass.static.GetFaceItemMesh(TeamNum, ArmyIndex, bPilot, HeadgearID, FaceItemID, FaceItemAttachSocket, bNoFacialHair);
	FacialHairMesh = (bNoFacialHair == 1) ? none : PawnHandlerClass.static.GetFacialHairMesh(TeamNum, ArmyIndex, FacialHairID, FacialHairAttachSocket);
}

simulated function CreatePawnMesh()
{
	local ROMapInfo ROMI;
	
	if (Health <= 0)
	{
		return;
	}
	
	if( HeadAndArmsMIC == none )
		HeadAndArmsMIC = new class'MaterialInstanceConstant';
	if( BodyMIC == none )
		BodyMIC = new class'MaterialInstanceConstant';
	if( BodyMIC2 == none )
		BodyMIC2 = new class'MaterialInstanceConstant';
	if( GearMIC == none )
		GearMIC = new class'MaterialInstanceConstant';
	if( GearMIC2 == none )
		GearMIC2 = new class'MaterialInstanceConstant';
	if( GearMIC3 == none )
		GearMIC3 = new class'MaterialInstanceConstant';
	if( HeadgearMIC == none )
		HeadgearMIC = new class'MaterialInstanceConstant';
	if( HairMIC == none && HairMICTemplate != none )
		HairMIC = new class'MaterialInstanceConstant';
	if( FPArmsSleeveMaterial == none && FPArmsSleeveMaterialTemplate != none )
		FPArmsSleeveMaterial = new class'MaterialInstanceConstant';
	if( FPArmsSkinMaterial == none && FPArmsSkinMaterialTemplate != none )
		FPArmsSkinMaterial = new class'MaterialInstanceConstant';

	if( bUseSingleCharacterVariant && BodyMICTemplate_SV != none )
		BodyMIC.SetParent(BodyMICTemplate_SV);
	else
		BodyMIC.SetParent(BodyMICTemplate);
	
	HeadAndArmsMIC.SetParent(HeadAndArmsMICTemplate);
	HeadgearMIC.SetParent(HeadgearMICTemplate);
	
	if( HairMIC != none )
		HairMIC.SetParent(HairMICTemplate);
	
	if( FPArmsSleeveMaterial != none )
		FPArmsSleeveMaterial.SetParent(FPArmsSleeveMaterialTemplate);
	
	if( FPArmsSkinMaterial != none )
		FPArmsSkinMaterial.SetParent(FPArmsSkinMaterialTemplate);
	
	MeshMICs.Length = 0;
	MeshMICs.AddItem(BodyMIC);
	MeshMICs.AddItem(GearMIC);
	MeshMICs.AddItem(GearMIC2);
	MeshMICs.AddItem(GearMIC3);
	MeshMICs.AddItem(BodyMIC2);
	MeshMICs.AddItem(HeadAndArmsMIC);
	MeshMICs.AddItem(HeadgearMIC);
	
	if( ThirdPersonHeadgearMeshComponent.AttachedToSkelComponent != none )
		mesh.DetachComponent(ThirdPersonHeadgearMeshComponent);
	if( ThirdPersonHairMeshComponent.AttachedToSkelComponent != none )
		mesh.DetachComponent(ThirdPersonHairMeshComponent);
	if( FaceItemMeshComponent.AttachedToSkelComponent != none )
		mesh.DetachComponent(FaceItemMeshComponent);
	if( FacialHairMeshComponent.AttachedToSkelComponent != none )
		mesh.DetachComponent(FacialHairMeshComponent);
	if( ThirdPersonHeadAndArmsMeshComponent.AttachedToSkelComponent != none )
		DetachComponent(ThirdPersonHeadAndArmsMeshComponent);
	if( TrapDisarmToolMeshTP.AttachedToSkelComponent != none )
		mesh.DetachComponent(TrapDisarmToolMeshTP);
	
	ROMI = ROMapInfo(WorldInfo.GetMapInfo());
	
	if( !bUseSingleCharacterVariant && ROMI != none)
	{
		CompositedBodyMesh = ROMI.GetCachedCompositedPawnMesh(TunicMesh, FieldgearMesh);
	}
	else
	{
		CompositedBodyMesh = PawnMesh_SV;
	}
	
	CompositedBodyMesh.Characterization = PlayerHIKCharacterization;
	ROSkeletalMeshComponent(mesh).ReplaceSkeletalMesh(CompositedBodyMesh);
	
	// GetCachedCompositedPawnMesh() CREATES A LIST OF UNIQUE MATERIALS! IT DOES NOT OVERLAP INDEXES!
	// THAT SHIT TOOK ME ALMOST 12 HOURS STRAIGHT TO FIGURE OUT
	// Wait, no it doesn't??? Seems to be a combination of both! Fuck this stupid system!
	// Certainly doesn't help that GetCachedCompositedPawnMesh() is a NATIVE function...
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// OKAY. SO. IT ONLY TOOK 16 HOURS, BUT IT'S WORKING. FOR FUTURE REFERENCE: GetCachedCompositedPawnMesh()
	// TAKES THE MIC VALUES OF THE SECOND ARGUMENT MESH, AND SYNCS THEM TO ANY MATCHING MICS IN THE FIRST ARGUMENT MESH.
	// HOWEVER, IF A MATERIAL SLOT DOES NOT HAVE ANY PART OF THE MESH ASSIGNED TO IT, IT GETS THROWN OUT OF THE LIST!
	// ALL SLOTS MUST HAVE A PIECE OF THE MESH ASSIGNED TO THEM, EVEN IF IT'S JUST A SINGLE HIDDEN TRIANGLE.
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////// DO NOT FORGET ABOUT THESE VALUES WHEN MODIFYING MESH ////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	GearMIC.SetParent(FieldgearMesh.Materials[0]);
	GearMIC2.SetParent(FieldgearMesh.Materials[1]);
	GearMIC3.SetParent(FieldgearMesh.Materials[2]);
	
	if (TunicMesh.Materials[2] != FieldgearMesh.Materials[1])
	{
		GearMIC2.SetParent(TunicMesh.Materials[2]);
		AltGear = true;
	}
	
	if (bIsPilot)
	{
		GearMIC.SetParent(FieldgearMesh.Materials[1]);
	}
	
	if (BodyMICTemplate2 != none)
	{
		BodyMIC2.SetParent(BodyMICTemplate2);
	}
	else
	{
		BodyMIC2.SetParent(TunicMesh.Materials[4]);
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	
	if (bUseSingleCharacterVariant)
	{
		GearMIC.SetParent(PawnMesh_SV.Materials[1]);
	}
	
	// 0 = Main Tunic
	// 1 = Main Gear
	// 2 = Secondary Gear
	// 3 = Alternate Gear
	// 4 = Supporting Tunic
	// 5 = Flamethrower Hose
	
	mesh.SetMaterial(0, BodyMIC);
	mesh.SetMaterial(1, GearMIC);
	mesh.SetMaterial(2, GearMIC2);
	mesh.SetMaterial(3, GearMIC3);
	mesh.SetMaterial(4, BodyMIC2);
	
	GrimePct = bIsPilot ? 0.2 : (FRand() / 2) + 0.5;
	
	`gom(self @ "grime:" @ GrimePct, 'PawnsGear');
	
	BodyMIC.SetScalarParameterValue('Grime_Scaler', GrimePct);
	BodyMIC2.SetScalarParameterValue('Grime_Scaler', GrimePct);
	GearMIC.SetScalarParameterValue('Grime_Scaler', GrimePct);
	GearMIC2.SetScalarParameterValue('Grime_Scaler', GrimePct);
	GearMIC3.SetScalarParameterValue('Grime_Scaler', GrimePct);
	HeadgearMIC.SetScalarParameterValue('Grime_Scaler', GrimePct);
	HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.HeadGrimeParam, GrimePct);
	
	if (GetTeamNum() == `ALLIES_TEAM_INDEX && `getSF == `MACV && !bIsPilot)
	{
		BodyMIC.SetScalarParameterValue('Mud_Scaler', GrimePct * 5);
		BodyMIC2.SetScalarParameterValue('Mud_Scaler', GrimePct * 5);
		GearMIC.SetScalarParameterValue('Mud_Scaler', GrimePct * 5);
		GearMIC2.SetScalarParameterValue('Mud_Scaler', GrimePct * 5);
		GearMIC3.SetScalarParameterValue('Mud_Scaler', GrimePct * 5);
		HeadgearMIC.SetScalarParameterValue('Mud_Scaler', GrimePct * 5);
	}
	else
	{
		BodyMIC.SetScalarParameterValue('Mud_Scaler', 0.0);
		BodyMIC2.SetScalarParameterValue('Mud_Scaler', 0.0);
		GearMIC.SetScalarParameterValue('Mud_Scaler', 0.0);
		GearMIC2.SetScalarParameterValue('Mud_Scaler', 0.0);
		GearMIC3.SetScalarParameterValue('Mud_Scaler', 0.0);
		HeadgearMIC.SetScalarParameterValue('Mud_Scaler', 0.0);
	}
	
	ROSkeletalMeshComponent(mesh).GenerateAnimationOverrideBones(HeadAndArmsMesh);
	ThirdPersonHeadAndArmsMeshComponent.SetSkeletalMesh(HeadAndArmsMesh);
	ThirdPersonHeadAndArmsMeshComponent.SetMaterial(0, HeadAndArmsMIC);
	ThirdPersonHeadAndArmsMeshComponent.SetParentAnimComponent(mesh);
	ThirdPersonHeadAndArmsMeshComponent.SetShadowParent(mesh);
	ThirdPersonHeadAndArmsMeshComponent.SetLODParent(mesh);
	AttachComponent(ThirdPersonHeadAndArmsMeshComponent);
	
	if( HeadgearMesh != none )
	{
		AttachNewHeadgear(HeadgearMesh);
	}
	
	if( FaceItemMesh != none && !bUseSingleCharacterVariant )
	{
		AttachNewFaceItem(FaceItemMesh);
	}
	
	if( FacialHairMesh != none )
	{
		AttachNewFacialHair(FacialHairMesh);
	}
	
	if ( ArmsMesh != None )
	{
		ArmsMesh.SetSkeletalMesh(ArmsOnlyMeshFP);
		ArmsMesh.SetMaterial(0, FPArmsSkinMaterial);
		ArmsMesh.SetMaterial(1, FPArmsSleeveMaterial);
	}
	
	if ( BandageMesh != none )
	{
		BandageMesh.SetSkeletalMesh(BandageMeshFP);
		BandageMesh.SetHidden(true);
	}
	
	if ( TrapDisarmToolMesh != none )
	{
		TrapDisarmToolMesh.SetSkeletalMesh(GetTrapDisarmToolMesh(true));
	}
	
	if ( TrapDisarmToolMeshTP != none )
	{
		TrapDisarmToolMeshTP.SetSkeletalMesh(GetTrapDisarmToolMesh(false));
	}
	
	if ( TrapDisarmToolMesh != none )
	{
		TrapDisarmToolMesh.SetHidden(true);
	}
	
	if ( TrapDisarmToolMeshTP != none )
	{
		Mesh.AttachComponentToSocket(TrapDisarmToolMeshTP, GrenadeSocket);
		TrapDisarmToolMeshTP.SetHidden(true);
	}
	
	if ( bOverrideLighting )
	{
		ThirdPersonHeadAndArmsMeshComponent.SetLightingChannels(LightingOverride);
		ThirdPersonHeadgearMeshComponent.SetLightingChannels(LightingOverride);
		ThirdPersonHairMeshComponent.SetLightingChannels(LightingOverride);
	}
	
	if( WorldInfo.NetMode == NM_DedicatedServer )
	{
		mesh.ForcedLODModel = 1000;
		ThirdPersonHeadAndArmsMeshComponent.ForcedLodModel = 1000;
		ThirdPersonHeadgearMeshComponent.ForcedLodModel = 1000;
		ThirdPersonHairMeshComponent.ForcedLodModel = 1000;
		FaceItemMeshComponent.ForcedLodModel = 1000;
		FacialHairMeshComponent.ForcedLodModel = 1000;
	}
}

simulated function CheckGlove(ROPlayerReplicationInfo ROPRI)
{
	local int i, ItemsIndex, PrimaryIndex;
	
	ItemsIndex = ROGameReplicationInfo(WorldInfo.GRI).RoleInfoItemsIdx;
	PrimaryIndex = ROPRI.RoleInfo.ActiveWeaponLoadout.PrimaryWeaponIndex;
	
	needsGlove = false;
	
	`gom(self @ "RoleInfo:" @ ROPRI.RoleInfo.class @ "ItemsIndex:" @ ItemsIndex @ "Primary:" @ PrimaryIndex, 'PawnsGear');
	
	if (ROPRI.RoleInfo.default.Items[ItemsIndex].PrimaryWeapons[PrimaryIndex] == class'GOMWeapon_M1919A6')
	{
		`gom(self @ "has an M1919A6, giving them a glove", 'PawnsGear');
		needsGlove = true;
	}
	
	// For some reason PrimaryWeaponIndex is always 0 in Campaign
	// Don't have time to spend hours trying to fix, so here's just a hack
	// It'll be a bit weird to have a glove with an M60, but whatever
	if (`Campaign)
	{
		for (i = 0; i < ROPRI.RoleInfo.default.Items[ItemsIndex].PrimaryWeapons.length; i++)
		{
			if (ROPRI.RoleInfo.default.Items[ItemsIndex].PrimaryWeapons[i] == class'GOMWeapon_M1919A6')
			{
				`gom(self @ "can possibly have an M1919A6, giving them a glove just in case", 'PawnsGear');
				needsGlove = true;
			}
		}
	}
}

simulated function AttachNewHeadgear(SkeletalMesh NewHeadgearMesh)
{
	local SkeletalMeshSocket HeadSocket;
	
	ThirdPersonHeadgearMeshComponent.SetSkeletalMesh(NewHeadgearMesh);
	ThirdPersonHeadgearMeshComponent.SetMaterial(0, HeadgearMIC);
	
	if (!bIsPilot && HairMIC != none)
	{
		ThirdPersonHairMeshComponent.SetSkeletalMesh(HairMesh);
		ThirdPersonHairMeshComponent.SetMaterial(0, HairMIC);
		
		if (ThirdPersonHeadgearMeshComponent.GetNumElements() > 1)
		{
			ThirdPersonHeadgearMeshComponent.SetMaterial(1, HairMIC);
		}
	}
	
	HeadSocket = ThirdPersonHeadAndArmsMeshComponent.GetSocketByName(HeadgearAttachSocket);
	
	if (mesh.MatchRefBone(HeadSocket.BoneName) != INDEX_NONE)
	{
		ThirdPersonHeadgearMeshComponent.SetShadowParent(mesh);
		ThirdPersonHeadgearMeshComponent.SetLODParent(mesh);
		mesh.AttachComponent( ThirdPersonHeadgearMeshComponent, HeadSocket.BoneName, HeadSocket.RelativeLocation, HeadSocket.RelativeRotation, HeadSocket.RelativeScale);
		
		ThirdPersonHairMeshComponent.SetShadowParent(mesh);
		ThirdPersonHairMeshComponent.SetLODParent(mesh);
		mesh.AttachComponent( ThirdPersonHairMeshComponent, HeadSocket.BoneName, HeadSocket.RelativeLocation, HeadSocket.RelativeRotation, HeadSocket.RelativeScale);
	}
}

simulated function HideHead(PlayerController LocalPC)
{
	super.HideHead(LocalPC);
	
	ThirdPersonHairMeshComponent.SetHidden(true);
}

simulated function UnHideHead(PlayerController LocalPC)
{
	super.UnHideHead(LocalPC);
	
	ThirdPersonHairMeshComponent.SetHidden(false);
}

simulated function SetMeshVisibility(bool bVisible)
{
	super.SetMeshVisibility(bVisible);
	
	if ( !default.ThirdPersonHairMeshComponent.bOwnerNoSee )
	{
		ThirdPersonHairMeshComponent.SetOwnerNoSee(!bVisible);
	}
	
	if( !bVisible || WorldInfo.NetMode == NM_DedicatedServer || WorldInfo.NetMode == NM_ListenServer )
	{
		ThirdPersonHairMeshComponent.bUpdateSkelWhenNotRendered = true;
	}
	else
	{
		ThirdPersonHairMeshComponent.AnimatingIntoView(2.0);
	}
}

simulated event StartDriving(Vehicle V)
{
	Super.StartDriving(V);
	
	if ( V != none && V.Mesh.DepthPriorityGroup == SDPG_Foreground && Mesh.DepthPriorityGroup == SDPG_World )
	{
		ThirdPersonHairMeshComponent.SetDepthPriorityGroup(SDPG_Foreground);
	}
	
	`gomdebug("HIDING COMPONENT",'Pawns');
	
	ThirdPersonHairMeshComponent.SetHidden(true);
}

simulated event StopDriving(Vehicle V)
{
	Super.StopDriving(V);
	
	if ( V != none && V.Mesh.DepthPriorityGroup == SDPG_World && Mesh.DepthPriorityGroup == SDPG_Foreground )
	{
		ThirdPersonHairMeshComponent.SetDepthPriorityGroup(SDPG_World);
	}
	
	`gomdebug("SHOWING COMPONENT",'Pawns');
	
	ThirdPersonHairMeshComponent.SetHidden(false);
}

simulated function UpdateWetnessValue(float DeltaTime)
{
	local bool bWetnessChanged;
	
	if( WetnessValue > TargetWetnessValue )
	{
		WetnessValue = FInterpConstantTo(WetnessValue, TargetWetnessValue, DeltaTime, 0.02);
		bWetnessChanged = true;
	}
	else if( WetnessValue < TargetWetnessValue )
	{
		WetnessValue = FInterpConstantTo(WetnessValue, TargetWetnessValue, DeltaTime, WetnessSpeedModifier);
		bWetnessChanged = true;
	}
	
	if( bWetnessChanged )
	{
		BodyMIC.SetScalarParameterValue(PawnHandlerClass.default.TunicWetnessParam, WetnessValue);
		BodyMIC2.SetScalarParameterValue(PawnHandlerClass.default.TunicWetnessParam, WetnessValue);
		HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.HeadWetnessParam, WetnessValue);
		GearMIC.SetScalarParameterValue(PawnHandlerClass.default.TunicWetnessParam, WetnessValue);
		GearMIC2.SetScalarParameterValue(PawnHandlerClass.default.TunicWetnessParam, WetnessValue);
		GearMIC3.SetScalarParameterValue(PawnHandlerClass.default.TunicWetnessParam, WetnessValue);
		
		if( HeadgearMIC != none )
		{
			HeadgearMIC.SetScalarParameterValue(PawnHandlerClass.default.TunicWetnessParam, WetnessValue);
		}
	}
}

simulated function SwitchFlamethrowerHose(optional bool bTurnOffHose)
{
	if( bHasFlamethrower )
	{
		if (FlamethrowerHoseMIC == none)
		{
			FlamethrowerHoseMIC = new class'MaterialInstanceConstant';
			FlamethrowerHoseMIC.SetParent(FlamethrowerHoseMICTemplate);
			
			if (AltGear)
			{
				mesh.SetMaterial(6, FlamethrowerHoseMIC);
			}
			else
			{
				mesh.SetMaterial(5, FlamethrowerHoseMIC);
			}
		}
		
		if (bTurnOffHose || CurrentWeaponAttachment == none)
		{
			FlamethrowerHoseMIC.SetScalarParameterValue('Hose', 0.0);
		}
		else if (CurrentWeaponAttachment != none)
		{
			if (CurrentWeaponAttachment.bHasFieldgearAttachment)
			{
				FlamethrowerHoseMIC.SetScalarParameterValue('Hose', 1.0);
			}
			else
			{
				FlamethrowerHoseMIC.SetScalarParameterValue('Hose', 0.0);
			}
		}
	}
}

simulated function BandagingComplete(optional bool bConsumeBandage = false, optional bool bCancelled)
{
	super.BandagingComplete(bConsumeBandage, bCancelled);
	
	if (!bCancelled && HeadAndArmsMIC != none)
	{
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtDiffuseParam, Texture2D'GOM3_CHR_ALL.Texture.NONE_BANDAGE_D1');
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtNormalParam, Texture2D'GOM3_CHR_ALL.Texture.NONE_BANDAGE_N');
		HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtSpecParam, Texture2D'GOM3_CHR_ALL.Texture.NONE_BANDAGE_S');
		
		if (BandagedOnce)
			HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtDiffuseParam, Texture2D'GOM3_CHR_ALL.Texture.NONE_BANDAGE_D2');
	}
	
	BandagedOnce = true;
}

function bool Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
	BandagedOnce = false;
	
	return super.Died(Killer, damageType, HitLocation);
}

function JumpOffVehicle() {}

function CheckInvSizeOverride(ROPlayerReplicationInfo ROPRI)
{
	local ROInventoryManager MyInvManager;
	MyInvManager = ROInventoryManager(InvManager);
	
	if (ROPRI != none && ROPRI.RoleInfo != none && MyInvManager != none)
	{
		if (ROPRI.RoleInfo.default.Items[ROGameReplicationInfo(WorldInfo.GRI).RoleInfoItemsIdx].SecondaryWeapons[0].default.Category == ROIC_Primary)
		{
			MyInvManager.CategoryLimits[ROIC_Primary] = 2.0;
		}
	}
}

defaultproperties
{
	ArmsOnlyMeshFP=SkeletalMesh'CHR_VN_1stP_Hands_Master.Mesh.VN_1stP_ALL_Rolled_Mesh'
	
	TunicMesh=SkeletalMesh'GOM3_CHR_ALL.CharRef_Full' // Reference this so it gets autoloaded in the editor
	
	BulletHitHelmetSound=none
	BulletHitMyHeadSound=none
	BulletHitMyHelmetSound=none
	MyBulletHitHelmetSound=none
	
	BandagedOnce=false
	AltGear=false
	needsGlove=false
	
	Begin Object class=ROSkeletalMeshComponent name=ThirdPersonHair0
		PhysicsAsset=PhysicsAsset'CHR_VN_Playeranim_Master.Phys.Headgear_Physics_Infantry'
		LightEnvironment = MyLightEnvironment
		bUpdateSkelWhenNotRendered=false
		bNoSkeletonUpdate=true
		CastShadow=FALSE
		MaxDrawDistance=1000 // 20m, beyond that they're barely visible so avoid the extra drawcall
		CollideActors=false
		BlockActors=false
		BlockZeroExtent=false
		BlockNonZeroExtent=false
		bHasPhysicsAssetInstance=true
	End Object
	ThirdPersonHairMeshComponent=ThirdPersonHair0
	
	Begin Object Name=ROPawnSkeletalMeshComponent
		AnimSets(0)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Stand_anim'
		AnimSets(1)=AnimSet'CHR_Playeranim_Master.Anim.CHR_ChestCover_anim'
		AnimSets(2)=AnimSet'CHR_Playeranim_Master.Anim.CHR_WaistCover_anim'
		AnimSets(3)=AnimSet'CHR_Playeranim_Master.Anim.CHR_StandCover_anim'
		AnimSets(4)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Crouch_anim'
		AnimSets(5)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Prone_anim'
		AnimSets(6)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Hand_Poses_Master'
		AnimSets(7)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Death_anim'
		AnimSets(8)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Tripod_anim'
		AnimSets(9)=AnimSet'CHR_Playeranim_Master.Anim.Special_Actions'
		AnimSets(10)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Melee'
		AnimSets(11)=none // Team specific sprinting animations
		AnimSets(12)=none // Reserved for weapon specific animations
		AnimSets(13)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_Tripod_anim'
		AnimSets(14)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_Stand_anim'
		AnimSets(15)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_ChestCover_anim'
		AnimSets(16)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_WaistCover_anim'
		AnimSets(17)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_StandCover_anim'
		AnimSets(18)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_Crouch_anim'
		AnimSets(19)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_Prone_anim'
		AnimSets(20)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_Hand_Poses_Master'
		AnimSets(21)=AnimSet'CHR_VN_Playeranim_Master.Anim.VN_Special_Actions'
		AnimSets(22)=AnimSet'GOM3_CHR_ALL.Anim.CHR_RS_Hand_Poses_Master'
		AnimSets(23)=AnimSet'GOM3_CHR_ALL.Anim.CHR_RS_Tripod_Anim'
	End Object
	
	Gore_LeftHand=(GibClass=class'ROGameContent.ROGib_HumanArm_Gore_BareArm')
	Gore_RightHand=(GibClass=class'ROGameContent.ROGib_HumanArm_Gore_BareArm')
	Gore_LeftLeg=(GibClass=class'ROGameContent.ROGib_HumanLeg_Gore_BareLeg')
	Gore_RightLeg=(GibClass=class'ROGameContent.ROGib_HumanLeg_Gore_BareLeg')
}
