//=============================================================================
// GOMRoleInfoSouth_US_Sniper.uc
//=============================================================================
// United States Army Sniper Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_US_Sniper extends GOMRoleInfoSouth_US;

DefaultProperties
{
	RoleType=RORIT_Marksman
	ClassTier=3
	ClassIndex=`ROCI_SNIPER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_XM21Scoped_Rifle')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M1DGarand_SniperRifle')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_XM21Scoped_Rifle')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sniper'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_sniper'
}
