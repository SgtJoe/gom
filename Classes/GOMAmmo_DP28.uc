//=============================================================================
// GOMAmmo_DP28.uc
//=============================================================================
// Ammunition info for the DP-28.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMAmmo_DP28 extends ROAmmo_762x54R_DP28Drum
	abstract;

defaultproperties
{
	Weight=0.7
}
