//=============================================================================
// GOMWeapon_RPD_SawnOff_Attach.uc
//=============================================================================
// MACV-SOG Sawn-Off RPD LMG (Attachment class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_RPD_SawnOff_Attach extends ROWeaponAttachment;

defaultproperties
{
	TriggerHoldDuration=0.2
	
	CarrySocketName=WeaponSling
	ThirdPersonHandsAnim=RPD_Handpose
	IKProfileName=MG34
	
	bHasBipod=false
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'GOM3_WP_US_MACV_RPD_SAWNOFF.Mesh.RPD_SawnOff_3rd_Master'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy_Bounds.RPD_3rd_Bounds_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.RPD_Bipod_3rd_Tree'
		AnimSets(0)=AnimSet'WP_VN_3rd_Master.Anim.RPD_3rd_Anim'
		CullDistance=5000
	End Object
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_3rdP_Rifles_round'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	WeaponClass=class'GOMWeapon_RPD_SawnOff'
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_AK47'
	
	TracerClass=class'RPDBulletTracer'
	TracerFrequency=5
	
	CHR_AnimSet=AnimSet'CHR_VN_Playeranim_Master.Weapons.CHR_RPD'
	
	FireAnim=Shoot
	FireLastAnim=Shoot_Last
	IdleAnim=Idle
	IdleEmptyAnim=Idle_Empty
}
