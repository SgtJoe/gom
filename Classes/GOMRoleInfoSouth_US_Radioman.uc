//=============================================================================
// GOMRoleInfoSouth_US_Radioman.uc
//=============================================================================
// United States Army Radio Operator Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_US_Radioman extends GOMRoleInfoSouth_US;

DefaultProperties
{
	RoleType=RORIT_Radioman
	ClassTier=3
	ClassIndex=`ROCI_RADIOMAN
	bIsRadioman=true
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle',class'ROWeap_M14_Rifle')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M14_Rifle')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_radioman'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_radioman'
}
