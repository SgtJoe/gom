//=============================================================================
// GOMVehicle_M113_ACAV.uc
//=============================================================================
// South Korean M113 ACAV.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicle_M113_ACAV extends GOMVehicleTransport
	abstract;

var repnotify	vector	DoorMGRightFlashLocation, DoorMGLeftFlashLocation;
var repnotify	byte	DoorMGRightFlashCount, DoorMGLeftFlashCount;
var repnotify	rotator	DoorMGRightWeaponRotation, DoorMGLeftWeaponRotation;
var 			byte	DoorMGRightFiringMode, DoorMGLeftFiringMode;

var repnotify byte DoorMGLeftCurrentPositionIndex, DoorMGRightCurrentPositionIndex;
var repnotify byte PassengerOneCurrentPositionIndex, PassengerTwoCurrentPositionIndex, PassengerThreeCurrentPositionIndex, PassengerFourCurrentPositionIndex;
var repnotify bool bDrivingDoorMGLeft, bDrivingDoorMGRight;
var repnotify bool bDrivingPassengerOne, bDrivingPassengerTwo, bDrivingPassengerThree, bDrivingPassengerFour;

var int SeatIndexDoorMGLeft, SeatIndexDoorMGRight;

var repnotify TakeHitInfo DeathHitInfo_ProxyDriver,
DeathHitInfo_ProxyHullMG, DeathHitInfo_ProxyHullMGR, DeathHitInfo_ProxyHullMGL,
DeathHitInfo_ProxyPassOne, DeathHitInfo_ProxyPassTwo, DeathHitInfo_ProxyPassThree, DeathHitInfo_ProxyPassFour;

replication
{
	if (SeatIndexDoorMGLeft >= 0 && !IsSeatControllerReplicationViewer(SeatIndexDoorMGLeft))
		DoorMGLeftFlashCount, DoorMGLeftWeaponRotation;
	
	if (SeatIndexDoorMGRight >= 0 && !IsSeatControllerReplicationViewer(SeatIndexDoorMGRight))
		DoorMGRightFlashCount, DoorMGRightWeaponRotation;
	
	if (bNetDirty)
		DoorMGLeftFlashLocation, DoorMGRightFlashLocation;
	
	if (bNetDirty)
		DoorMGLeftCurrentPositionIndex, DoorMGRightCurrentPositionIndex,
		PassengerOneCurrentPositionIndex, PassengerTwoCurrentPositionIndex,
		PassengerThreeCurrentPositionIndex, PassengerFourCurrentPositionIndex,
		bDrivingDoorMGLeft, bDrivingDoorMGRight, bDrivingPassengerOne,
		bDrivingPassengerTwo, bDrivingPassengerThree, bDrivingPassengerFour;
	
	if (bNetDirty)
		DeathHitInfo_ProxyDriver, DeathHitInfo_ProxyHullMG, DeathHitInfo_ProxyHullMGR, DeathHitInfo_ProxyHullMGL,
		DeathHitInfo_ProxyPassOne, DeathHitInfo_ProxyPassTwo, DeathHitInfo_ProxyPassThree, DeathHitInfo_ProxyPassFour;
}

simulated event PostBeginPlay()
{
	super.PostBeginPlay();
	
	SeatIndexDoorMGLeft = GetDoorMGLeftSeatIndex();
	SeatIndexDoorMGRight = GetDoorMGRightSeatIndex();
}

simulated function int GetDoorMGLeftSeatIndex()
{
	return GetSeatIndexFromPrefix("DoorMGLeft");
}

simulated function int GetDoorMGRightSeatIndex()
{
	return GetSeatIndexFromPrefix("DoorMGRight");
}

simulated function int GetFreePassengerSeatIndex()
{
	local int i;
	
	for (i = 0; i < Seats.Length; i++)
	{
		if (!Seats[i].bNonEnterable && (Seats[i].SeatPawn == none || Seats[i].SeatPawn.Controller == none))
		{
			if (Left(Seats[i].TurretVarPrefix,9) ~= "Passenger" || Left(Seats[i].TurretVarPrefix,6) ~= "DoorMG" || Left(Seats[i].TurretVarPrefix,6) ~= "HullMG")
				return i;
		}
	}
	
	return -1;
}

simulated event ReplicatedEvent(name VarName)
{
	if (VarName == 'DeathHitInfo_ProxyDriver')
	{
		PlaySeatProxyDeathHitEffects(`M113_Driver, DeathHitInfo_ProxyDriver);
	}
	else if (VarName == 'DeathHitInfo_ProxyHullMG')
	{
		PlaySeatProxyDeathHitEffects(`M113_MG, DeathHitInfo_ProxyHullMG);
	}
	else if (VarName == 'DeathHitInfo_ProxyHullMGR')
	{
		PlaySeatProxyDeathHitEffects(`M113_MGR, DeathHitInfo_ProxyHullMGR);
	}
	else if (VarName == 'DeathHitInfo_ProxyHullMGL')
	{
		PlaySeatProxyDeathHitEffects(`M113_MGL, DeathHitInfo_ProxyHullMGL);
	}
	else if (VarName == 'DeathHitInfo_ProxyPassOne')
	{
		PlaySeatProxyDeathHitEffects(`M113_PASS1, DeathHitInfo_ProxyPassOne);
	}
	else if (VarName == 'DeathHitInfo_ProxyPassTwo')
	{
		PlaySeatProxyDeathHitEffects(`M113_PASS2, DeathHitInfo_ProxyPassTwo);
	}
	else if (VarName == 'DeathHitInfo_ProxyPassThree')
	{
		PlaySeatProxyDeathHitEffects(`M113_PASS3, DeathHitInfo_ProxyPassThree);
	}
	else if (VarName == 'DeathHitInfo_ProxyPassFour')
	{
		PlaySeatProxyDeathHitEffects(`M113_PASS4, DeathHitInfo_ProxyPassFour);
	}
	else
	{
		super.ReplicatedEvent(VarName);
	}
}

function DamageSeatProxy(int SeatProxyIndex, int Damage, Controller InstigatedBy, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional Actor DamageCauser)
{
	switch( SeatProxyIndex )
	{
		case `M113_Driver:
			DeathHitInfo_ProxyDriver.Damage = Damage;
			DeathHitInfo_ProxyDriver.HitLocation = HitLocation;
			DeathHitInfo_ProxyDriver.Momentum = Momentum;
			DeathHitInfo_ProxyDriver.DamageType = DamageType;
			break;
		
		case `M113_MG:
			DeathHitInfo_ProxyHullMG.Damage = Damage;
			DeathHitInfo_ProxyHullMG.HitLocation = HitLocation;
			DeathHitInfo_ProxyHullMG.Momentum = Momentum;
			DeathHitInfo_ProxyHullMG.DamageType = DamageType;
			break;
		
		case `M113_MGR:
			DeathHitInfo_ProxyHullMGR.Damage = Damage;
			DeathHitInfo_ProxyHullMGR.HitLocation = HitLocation;
			DeathHitInfo_ProxyHullMGR.Momentum = Momentum;
			DeathHitInfo_ProxyHullMGR.DamageType = DamageType;
			break;
		
		case `M113_MGL:
			DeathHitInfo_ProxyHullMGL.Damage = Damage;
			DeathHitInfo_ProxyHullMGL.HitLocation = HitLocation;
			DeathHitInfo_ProxyHullMGL.Momentum = Momentum;
			DeathHitInfo_ProxyHullMGL.DamageType = DamageType;
			break;
		
		case `M113_PASS1:
			DeathHitInfo_ProxyPassOne.Damage = Damage;
			DeathHitInfo_ProxyPassOne.HitLocation = HitLocation;
			DeathHitInfo_ProxyPassOne.Momentum = Momentum;
			DeathHitInfo_ProxyPassOne.DamageType = DamageType;
			break;
		
		case `M113_PASS2:
			DeathHitInfo_ProxyPassTwo.Damage = Damage;
			DeathHitInfo_ProxyPassTwo.HitLocation = HitLocation;
			DeathHitInfo_ProxyPassTwo.Momentum = Momentum;
			DeathHitInfo_ProxyPassTwo.DamageType = DamageType;
			break;
		
		case `M113_PASS3:
			DeathHitInfo_ProxyPassThree.Damage = Damage;
			DeathHitInfo_ProxyPassThree.HitLocation = HitLocation;
			DeathHitInfo_ProxyPassThree.Momentum = Momentum;
			DeathHitInfo_ProxyPassThree.DamageType = DamageType;
			break;
		
		case `M113_PASS4:
			DeathHitInfo_ProxyPassFour.Damage = Damage;
			DeathHitInfo_ProxyPassFour.HitLocation = HitLocation;
			DeathHitInfo_ProxyPassFour.Momentum = Momentum;
			DeathHitInfo_ProxyPassFour.DamageType = DamageType;
			break;
	}
	
	Super.DamageSeatProxy(SeatProxyIndex, Damage, InstigatedBy, HitLocation, Momentum, DamageType, DamageCauser);
}

DefaultProperties
{
	Team=`ALLIES_TEAM_INDEX
	
	Seats(`M113_Driver)={(
		CameraTag=none,
		CameraOffset=-420,
		SeatAnimBlendName=DriverPositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=None,
				ViewFOV=70.0,
				PositionUpAnim=Driver_idle,
				PositionIdleAnim=Driver_idle,
				DriverIdleAnim=Driver_idle,
				AlternateIdleAnim=Driver_idle,
				SeatProxyIndex=`M113_Driver,
				bIsExterior=true,
				LeftHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_DriverLeftBrake,DefaultEffectorRotationTargetName=IK_DriverLeftBrake),
				RightHandIKInfo=(
					IKEnabled=true,
					DefaultEffectorLocationTargetName=IK_DriverRightBrake,
					DefaultEffectorRotationTargetName=IK_DriverRightBrake/*,
					AlternateEffectorTargets=(
						(
							Action=DAct_ShiftGears,
							IKEnabled=true,
							EffectorLocationTargetName=IK_DriverClutchLever,
							EffectorRotationTargetName=IK_DriverClutchLever
						)
					)*/
				),
				LeftFootIKInfo=(PinEnabled=true),
				RightFootIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_DriverPedal,DefaultEffectorRotationTargetName=IK_DriverPedal),
				HipsIKInfo=(PinEnabled=true),
				PositionFlinchAnims=(Driver_idle),
				PositionDeathAnims=(Driver_idle)
			),
			(
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=None,
				ViewFOV=70.0,
				PositionDownAnim=Driver_LookOut_Idle,
				PositionIdleAnim=Driver_LookOut_Idle,
				DriverIdleAnim=Driver_LookOut_Idle,
				AlternateIdleAnim=Driver_LookOut_Idle,
				SeatProxyIndex=`M113_Driver,
				bIsExterior=true,
				LeftHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_DriverLeftBrake,DefaultEffectorRotationTargetName=IK_DriverLeftBrake),
				RightHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_DriverRightBrake,DefaultEffectorRotationTargetName=IK_DriverRightBrake),
				LeftFootIKInfo=(PinEnabled=true),
				RightFootIKInfo=(PinEnabled=true),
				HipsIKInfo=(PinEnabled=true),
				PositionFlinchAnims=(Driver_LookOut_Idle),
				PositionDeathAnims=(Driver_idle)
			)
		),
		bSeatVisible=true,
		SeatBone=Root_Driver,
		DriverDamageMult=1.0,
		InitialPositionIndex=0
	)}
	
	Seats(`M113_MG)={(
		GunClass=class'GOMVehicleWeaponMG_M113_M2',
		GunSocket=(MG_Barrel),
		GunPivotPoints=(MG_Pitch),
		TurretControls=(MG_Rot_Yaw,MG_Rot_Pitch),
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=HullMGPositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=0.0,
				bIsExterior=true,
				bIgnoreWeapon=true,
				bRotateGunOnCommand=true,
				PositionUpAnim=MG_Hull_Idle,
				PositionDownAnim=MG_Hull_Idle,
				PositionIdleAnim=MG_Hull_Idle,
				DriverIdleAnim=MG_Hull_Idle,
				AlternateIdleAnim=MG_Hull_Idle,
				SeatProxyIndex=`M113_MG,
				LeftHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_HullMGLeftHand,DefaultEffectorRotationTargetName=IK_HullMGLeftHand),
				RightHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_HullMGRightHand,DefaultEffectorRotationTargetName=IK_HullMGRightHand),
				ChestIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_HullMGChest,DefaultEffectorRotationTargetName=IK_HullMGChest),
				HipsIKInfo=(PinEnabled=true),
				LeftFootIKInfo=(PinEnabled=true),
				RightFootIKInfo=(PinEnabled=true),
				PositionFlinchAnims=(MG_Hull_Idle),
				PositionDeathAnims=(MG_Hull_Duck_Idle)
			),
			(
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=MG_Camera,
				ViewFOV=45,
				bCamRotationFollowSocket=true,
				bViewFromCameraTag=true,
				bUseDOF=true,
				bIsExterior=true,
				PositionUpAnim=MG_Hull_Idle,
				PositionDownAnim=MG_Hull_Idle,
				PositionIdleAnim=MG_Hull_Idle,
				bConstrainRotation=false,
				YawContraintIndex=0,
				PitchContraintIndex=1,
				DriverIdleAnim=MG_Hull_Idle,
				AlternateIdleAnim=MG_Hull_Idle,
				SeatProxyIndex=`M113_MG,
				LeftHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_HullMGLeftHand,DefaultEffectorRotationTargetName=IK_HullMGLeftHand),
				RightHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_HullMGRightHand,DefaultEffectorRotationTargetName=IK_HullMGRightHand),
				ChestIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_HullMGChest,DefaultEffectorRotationTargetName=IK_HullMGChest),
				LookAtInfo=(LookAtEnabled=true,DefaultLookAtTargetName=MG_Barrel,HeadInfluence=1.0,BodyInfluence=0.0),
				HipsIKInfo=(PinEnabled=true),
				LeftFootIKInfo=(PinEnabled=true),
				RightFootIKInfo=(PinEnabled=true),
				PositionFlinchAnims=(MG_Hull_Idle),
				PositionDeathAnims=(MG_Hull_Duck_Idle)
			),
			(
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=0.0,
				bIsExterior=true,
				bIgnoreWeapon=true,
				bRotateGunOnCommand=true,
				PositionUpAnim=MG_Hull_Duck_Idle,
				PositionDownAnim=MG_Hull_Duck_Idle,
				PositionIdleAnim=MG_Hull_Duck_Idle,
				DriverIdleAnim=MG_Hull_Duck_Idle,
				AlternateIdleAnim=MG_Hull_Duck_Idle,
				SeatProxyIndex=`M113_MG,
				LeftHandIKInfo=(PinEnabled=true),
				RightHandIKInfo=(PinEnabled=true),
				ChestIKInfo=(PinEnabled=true),
				HipsIKInfo=(PinEnabled=true),
				LeftFootIKInfo=(PinEnabled=true),
				RightFootIKInfo=(PinEnabled=true),
				PositionFlinchAnims=(MG_Hull_Duck_Idle),
				PositionDeathAnims=(MG_Hull_Duck_Idle)
			)
		),
		TurretVarPrefix="HullMG",
		bSeatVisible=true,
		SeatBone=MG_Attach,
		DriverDamageMult=1.0,
		InitialPositionIndex=1,
		FiringPositionIndex=1,
		WeaponRotation=(Pitch=0,Yaw=16384,Roll=0),
		TracerFrequency=5,
		WeaponTracerClass=(class'M2BulletTracer')
	)}
	
	Seats(`M113_MGR)={(
		GunClass=class'GOMVehicleWeaponMG_M113_M60_R',
		GunSocket=(MG_R_Barrel),
		GunPivotPoints=(R_M60_MG_Pitch),
		TurretControls=(MGR_Rot_Yaw,MGR_Rot_Pitch),
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=RightMGPositionNode,
		SeatPositions=(
			(
				SeatProxyIndex=`M113_MGR,
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=0.0,
				bIsExterior=true,
				bIgnoreWeapon=true,
				bRotateGunOnCommand=true,
				PositionUpAnim=Pass01_Idle,
				PositionDownAnim=Pass01_Idle,
				PositionIdleAnim=Pass01_Idle,
				DriverIdleAnim=Pass01_Idle,
				AlternateIdleAnim=Pass01_Idle,
				LeftHandIKInfo=(IKEnabled=false),
				RightHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGR_RH,DefaultEffectorRotationTargetName=IK_MGR_RH),
				ChestIKInfo=(PinEnabled=true),
				HipsIKInfo=(PinEnabled=true),
				LeftFootIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGR_LF,DefaultEffectorRotationTargetName=IK_MGR_LF),
				RightFootIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGR_RF,DefaultEffectorRotationTargetName=IK_MGR_RF),
				PositionFlinchAnims=(Pass01_Idle),
				PositionDeathAnims=(MG_Hull_Duck_Idle)
			),
			(
				SeatProxyIndex=`M113_MGR,
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=MG_R_Camera,
				ViewFOV=45,
				bCamRotationFollowSocket=true,
				bViewFromCameraTag=true,
				bUseDOF=true,
				bIsExterior=true,
				PositionUpAnim=MG_Right_Idle,
				PositionDownAnim=MG_Right_Idle,
				PositionIdleAnim=MG_Right_Idle,
				bConstrainRotation=false,
				YawContraintIndex=0,
				PitchContraintIndex=1,
				DriverIdleAnim=MG_Right_Idle,
				AlternateIdleAnim=MG_Right_Idle,
				LeftHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGR_LH,DefaultEffectorRotationTargetName=IK_MGR_LH),
				RightHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGR_RH,DefaultEffectorRotationTargetName=IK_MGR_RH),
				ChestIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGR_C,DefaultEffectorRotationTargetName=IK_MGR_C),
				HipsIKInfo=(PinEnabled=false),
				LeftFootIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGR_LF,DefaultEffectorRotationTargetName=IK_MGR_LF),
				RightFootIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGR_RF,DefaultEffectorRotationTargetName=IK_MGR_RF),
				LookAtInfo=(LookAtEnabled=true,DefaultLookAtTargetName=MG_R_Barrel,HeadInfluence=1.0,BodyInfluence=0.0),
				PositionFlinchAnims=(MG_Right_Idle),
				PositionDeathAnims=(MG_Hull_Duck_Idle)
			)
		),
		TurretVarPrefix="DoorMGRight",
		bSeatVisible=true,
		SeatBone=Root_R_M60,
		DriverDamageMult=1.0,
		InitialPositionIndex=1,
		FiringPositionIndex=1,
		WeaponRotation=(Pitch=0,Yaw=16384,Roll=0),
		TracerFrequency=5,
		WeaponTracerClass=(class'M60BulletTracer')
	)}
	
	Seats(`M113_MGL)={(
		GunClass=class'GOMVehicleWeaponMG_M113_M60_L',
		GunSocket=(MG_L_Barrel),
		GunPivotPoints=(L_M60_MG_Pitch),
		TurretControls=(MGL_Rot_Yaw,MGL_Rot_Pitch),
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=LeftMGPositionNode,
		SeatPositions=(
			(
				SeatProxyIndex=`M113_MGL,
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=0.0,
				bIsExterior=true,
				bIgnoreWeapon=true,
				bRotateGunOnCommand=true,
				PositionUpAnim=Pass01_Idle,
				PositionDownAnim=Pass01_Idle,
				PositionIdleAnim=Pass01_Idle,
				DriverIdleAnim=Pass01_Idle,
				AlternateIdleAnim=Pass01_Idle,
				LeftHandIKInfo=(IKEnabled=false),
				RightHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGL_RH,DefaultEffectorRotationTargetName=IK_MGL_RH),
				ChestIKInfo=(PinEnabled=true),
				HipsIKInfo=(PinEnabled=true),
				LeftFootIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGL_LF,DefaultEffectorRotationTargetName=IK_MGL_LF),
				RightFootIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGL_RF,DefaultEffectorRotationTargetName=IK_MGL_RF),
				PositionFlinchAnims=(Pass01_Idle),
				PositionDeathAnims=(MG_Hull_Duck_Idle)
			),
			(
				SeatProxyIndex=`M113_MGL,
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=MG_L_Camera,
				ViewFOV=45,
				bCamRotationFollowSocket=true,
				bViewFromCameraTag=true,
				bUseDOF=true,
				bIsExterior=true,
				PositionUpAnim=MG_Left_Idle,
				PositionDownAnim=MG_Left_Idle,
				PositionIdleAnim=MG_Left_Idle,
				bConstrainRotation=false,
				YawContraintIndex=0,
				PitchContraintIndex=1,
				DriverIdleAnim=MG_Left_Idle,
				AlternateIdleAnim=MG_Left_Idle,
				LeftHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGL_LH,DefaultEffectorRotationTargetName=IK_MGL_LH),
				RightHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGL_RH,DefaultEffectorRotationTargetName=IK_MGL_RH),
				ChestIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGL_C,DefaultEffectorRotationTargetName=IK_MGL_C),
				HipsIKInfo=(PinEnabled=false),
				LeftFootIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGL_LF,DefaultEffectorRotationTargetName=IK_MGL_LF),
				RightFootIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_MGL_RF,DefaultEffectorRotationTargetName=IK_MGL_RF),
				LookAtInfo=(LookAtEnabled=true,DefaultLookAtTargetName=MG_L_Barrel,HeadInfluence=1.0,BodyInfluence=0.0),
				PositionFlinchAnims=(MG_Left_Idle),
				PositionDeathAnims=(MG_Hull_Duck_Idle)
			)
		),
		TurretVarPrefix="DoorMGLeft",
		bSeatVisible=true,
		SeatBone=Root_L_M60,
		DriverDamageMult=1.0,
		InitialPositionIndex=1,
		FiringPositionIndex=1,
		WeaponRotation=(Pitch=0,Yaw=16384,Roll=0),
		TracerFrequency=5,
		WeaponTracerClass=(class'M60BulletTracer')
	)}
	
	Seats(`M113_PASS1)={(
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=Pass1PositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bIsExterior=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=0.0,
				PositionIdleAnim=Pass01_Idle,
				DriverIdleAnim=Pass01_Idle,
				AlternateIdleAnim=Pass01_Idle,
				SeatProxyIndex=`M113_PASS1,
				PositionFlinchAnims=(Pass01_Idle),
				PositionDeathAnims=(Pass01_Idle)
			)
		),
		TurretVarPrefix="PassengerOne",
		bSeatVisible=true,
		DriverDamageMult=1.0,
		SeatBone=Root_Pass01
	)}
	
	Seats(`M113_PASS2)={(
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=Pass2PositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bIsExterior=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=0.0,
				PositionIdleAnim=Pass02_Idle,
				DriverIdleAnim=Pass02_Idle,
				AlternateIdleAnim=Pass02_Idle,
				SeatProxyIndex=`M113_PASS2,
				PositionFlinchAnims=(Pass02_Idle),
				PositionDeathAnims=(Pass02_Idle)
			)
		),
		TurretVarPrefix="PassengerTwo",
		bSeatVisible=true,
		DriverDamageMult=1.0,
		SeatBone=Root_Pass02
	)}
	
	Seats(`M113_PASS3)={(
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=Pass3PositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bIsExterior=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=0.0,
				PositionIdleAnim=Pass03_Idle,
				DriverIdleAnim=Pass03_Idle,
				AlternateIdleAnim=Pass03_Idle,
				SeatProxyIndex=`M113_PASS3,
				PositionFlinchAnims=(Pass03_Idle),
				PositionDeathAnims=(Pass03_Idle)
			)
		),
		TurretVarPrefix="PassengerThree",
		bSeatVisible=true,
		DriverDamageMult=1.0,
		SeatBone=Root_Pass03
	)}
	
	Seats(`M113_PASS4)={(
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=Pass4PositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bIsExterior=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=0.0,
				PositionIdleAnim=Pass04_Idle,
				DriverIdleAnim=Pass04_Idle,
				AlternateIdleAnim=Pass04_Idle,
				SeatProxyIndex=`M113_PASS4,
				PositionFlinchAnims=(Pass04_Idle),
				PositionDeathAnims=(Pass04_Idle)
			)
		),
		TurretVarPrefix="PassengerFour",
		bSeatVisible=true,
		DriverDamageMult=1.0,
		SeatBone=Root_Pass04
	)}
	
	PassengerAnimTree=AnimTree'CHR_Playeranimtree_Master.CHR_Tanker_animtree'
	CrewAnimSet=AnimSet'VH_VN_ARVN_M113_APC.Anim.CHR_M113_Anim_Master'
	
	LeftWheels(0)="L1_Wheel_Static"
	LeftWheels(1)="L2_Wheel"
	LeftWheels(2)="L3_Wheel"
	LeftWheels(3)="L4_Wheel"
	LeftWheels(4)="L5_Wheel"
	LeftWheels(5)="L6_Wheel"
	LeftWheels(6)="L7_Wheel_Static"
	
	RightWheels(0)="R1_Wheel_Static"
	RightWheels(1)="R2_Wheel"
	RightWheels(2)="R3_Wheel"
	RightWheels(3)="R4_Wheel"
	RightWheels(4)="R5_Wheel"
	RightWheels(5)="R6_Wheel"
	RightWheels(6)="R7_Wheel_Static"
	
	Begin Object Name=RRWheel
		BoneName="R_Wheel_06"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=RMWheel
		BoneName="R_Wheel_04"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=RFWheel
		BoneName="R_Wheel_02"
		BoneOffset=(X=0.0,Y=0,Z=2.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LRWheel
		BoneName="L_Wheel_06"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LMWheel
		BoneName="L_Wheel_04"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LFWheel
		BoneName="L_Wheel_02"
		BoneOffset=(X=0.0,Y=0,Z=2.0)
		WheelRadius=20
	End Object
	
	`include(GOM3\Classes\GOMVehicleGearData_APC.uci)
	
	VehicleEffects(VFX_Firing1)=(EffectStartTag=MG,EffectTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_DShK',EffectSocket=MG_Barrel)
	VehicleEffects(VFX_Firing2)=(EffectStartTag=MG,EffectTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_USA_M2',EffectSocket=MG_ShellEject,bInteriorEffect=true,bNoKillOnRestart=true)
	
	VehicleEffects(VFX_Firing3)=(EffectStartTag=MG_R,EffectTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_split',EffectSocket=MG_R_Barrel)
	VehicleEffects(VFX_Firing4)=(EffectStartTag=MG_R,EffectTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_USA_M60',EffectSocket=MG_R_ShellEject,bInteriorEffect=true,bNoKillOnRestart=true)
	
	VehicleEffects(VFX_Firing5)=(EffectStartTag=MG_L,EffectTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_split',EffectSocket=MG_L_Barrel)
	VehicleEffects(VFX_Firing6)=(EffectStartTag=MG_L,EffectTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_USA_M60',EffectSocket=MG_L_ShellEject,bInteriorEffect=true,bNoKillOnRestart=true)
	
	VehicleEffects(VFX_Exhaust)=(EffectStartTag=EngineStart,EffectEndTag=EngineStop,EffectTemplate=ParticleSystem'GOM3_FX.ParticleSystems.FX_VEH_exhaust',EffectSocket=Exhaust)
	// VehicleEffects(VFX_TreadWing)=(EffectStartTag=EngineStart,EffectEndTag=EngineStop,bStayActive=true,EffectTemplate=ParticleSystem'FX_VEH_Tank_Three.FX_VEH_Tank_A_Wing_Dirt_T34',EffectSocket=attachments_body_ground)
	
	// VehicleEffects(VFX_DmgSmoke)=(EffectStartTag=DamageSmoke,EffectEndTag=NoDamageSmoke,bRestartRunning=false,EffectTemplate=ParticleSystem'FX_Vehicles_Two.UniversalCarrier.FX_UnivCarrier_damaged_burning',EffectSocket=attachments_body)
	// VehicleEffects(VFX_DmgInterior)=(EffectStartTag=DamageInterior,EffectEndTag=NoInternalSmoke,bRestartRunning=false,bInteriorEffect=true,EffectTemplate=ParticleSystem'FX_VEH_Tank_Two.FX_VEH_Tank_Interior_Penetrate',EffectSocket=attachments_body)
	
	// VehicleEffects(VFX_DeathSmoke1)=(EffectStartTag=Destroyed,EffectEndTag=NoDeathSmoke,EffectTemplate=ParticleSystem'FX_VN_Smoke.Emitter.FX_VN_BurningLong',EffectSocket=FX_Smoke_1)
	// VehicleEffects(VFX_DeathSmoke2)=(EffectStartTag=Destroyed,EffectEndTag=NoDeathSmoke,EffectTemplate=ParticleSystem'FX_VN_Smoke.Emitter.FX_VN_BurningLong',EffectSocket=FX_Smoke_2)
	// VehicleEffects(VFX_DeathSmoke3)=(EffectStartTag=Destroyed,EffectEndTag=NoDeathSmoke,EffectTemplate=ParticleSystem'FX_VN_Smoke.Emitter.FX_VN_BurningLong',EffectSocket=FX_Smoke_3)
	
	Exhaust_FXSocket=Exhaust
	
	BigExplosionSocket=FX_Fire
	ExplosionTemplate=ParticleSystem'GOM3_FX.ParticleSystems.FX_VEH_Tank_C_Explosion'
	
	SeatTextureOffsets(`M113_DRIVER)=(PositionOffSet=(X=10,Y=-22,Z=0),bTurretPosition=0)
	SeatTextureOffsets(`M113_MG)=(PositionOffSet=(X=-9,Y=-22,Z=0),bTurretPosition=0)
	SeatTextureOffsets(`M113_MGR)=(PositionOffSet=(X=10,Y=10,Z=0),bTurretPosition=0)
	SeatTextureOffsets(`M113_MGL)=(PositionOffSet=(X=-10,Y=10,Z=0),bTurretPosition=0)
	SeatTextureOffsets(`M113_PASS1)=(PositionOffSet=(X=11,Y=-1,Z=0),bTurretPosition=0)
	SeatTextureOffsets(`M113_PASS2)=(PositionOffSet=(X=-11,Y=-1,Z=0),bTurretPosition=0)
	SeatTextureOffsets(`M113_PASS3)=(PositionOffSet=(X=11,Y=23,Z=0),bTurretPosition=0)
	SeatTextureOffsets(`M113_PASS4)=(PositionOffSet=(X=-11,Y=23,Z=0),bTurretPosition=0)
	
	TreadTextureOffsets(0)=(PositionOffset=(X=37,Y=35,Z=0),MySizeX=8,MYSizeY=80)
	TreadTextureOffsets(1)=(PositionOffset=(X=95,Y=35,Z=0),MySizeX=8,MYSizeY=80)
	
	EngineTextureOffset=(PositionOffset=(X=58,Y=73,Z=0),MySizeX=24,MYSizeY=24)
	
	VehHitZones(0) =(ZoneName=BODY,		DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,PhysBodyBoneName=Chassis)
	VehHitZones(1) =(ZoneName=INTERIOR,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_Fuel,		PhysBodyBoneName=SMAttach)
	VehHitZones(2) =(ZoneName=ENGINE,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_Engine,		PhysBodyBoneName=PHAT_ENGINE)
	VehHitZones(3) =(ZoneName=TRACKS_R,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,		PhysBodyBoneName=PHAT_TRACKS_R)
	VehHitZones(4) =(ZoneName=TRACKS_L,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,		PhysBodyBoneName=PHAT_TRACKS_L)
	
	VehHitZones(5) =(ZoneName=DRIVER,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,CrewSeatIndex=`M113_Driver,	PhysBodyBoneName=Root_Driver)
	VehHitZones(6) =(ZoneName=MG_A,		DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,CrewSeatIndex=`M113_MG,		PhysBodyBoneName=MG_Root)
	VehHitZones(7) =(ZoneName=MG_B,		DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,CrewSeatIndex=`M113_MG,		PhysBodyBoneName=MG_Yaw)
	VehHitZones(8) =(ZoneName=MG_C,		DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,CrewSeatIndex=`M113_MG,		PhysBodyBoneName=MG_Hatch)
	VehHitZones(9) =(ZoneName=PASS1,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,CrewSeatIndex=`M113_PASS1,	PhysBodyBoneName=Root_Pass01)
	VehHitZones(10)=(ZoneName=PASS2,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,CrewSeatIndex=`M113_PASS2,	PhysBodyBoneName=Root_Pass02)
	VehHitZones(11)=(ZoneName=PASS3,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,CrewSeatIndex=`M113_PASS3,	PhysBodyBoneName=Root_Pass03)
	VehHitZones(12)=(ZoneName=PASS4,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,CrewSeatIndex=`M113_PASS4,	PhysBodyBoneName=Root_Pass04)
	
	CrewHitZoneStart=13
	VehHitZones(13)=(ZoneName=MGHEAD,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=`M113_MG,	CrewBoneName=Root_MG_head_HITBOX)
	VehHitZones(14)=(ZoneName=MGBODY,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=`M113_MG,	CrewBoneName=Root_MG_HITBOX)
	VehHitZones(15)=(ZoneName=MGRHEAD,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=`M113_MGR,	CrewBoneName=Root_R_M60_head_HITBOX)
	VehHitZones(16)=(ZoneName=MGRBODY,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=`M113_MGR,	CrewBoneName=Root_R_M60_HITBOX)
	VehHitZones(17)=(ZoneName=MGLHEAD,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=`M113_MGL,	CrewBoneName=Root_L_M60_head_HITBOX)
	VehHitZones(18)=(ZoneName=MGLBODY,	DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=`M113_MGL,	CrewBoneName=Root_L_M60_HITBOX)
	VehHitZones(19)=(ZoneName=DRIVERHEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=`M113_Driver,CrewBoneName=Root_Driver_head_HITBOX)
	CrewHitZoneEnd=19
}
