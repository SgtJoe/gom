//=============================================================================
// GOMPawnNorthNVAU.uc
//=============================================================================
// Northern Pawn (North Vietnamese Army Urban).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawnNorthNVAU extends GOMPawnNorthNVA;

defaultproperties
{
	TunicMesh=SkeletalMesh'GOM3_CHR_VN_NVA.Mesh.Tunic.GOM_CHR_NVA_Tunic_Long_Urban'
	BodyMICTemplate=MaterialInstanceConstant'GOM3_CHR_VN_NVA.MIC.NVA_Tunic_Long_Urban'
	
	FieldgearMesh=SkeletalMesh'GOM3_CHR_VN_NVA.Mesh.Gear.GOM_CHR_NVA_Gear_Scout_Urban'
	
	PawnMesh_SV=SkeletalMesh'GOM3_CHR_VN_NVA.Mesh.Tunic.GOM_CHR_NVA_Tunic_Long_Urban'
}
