//=============================================================================
// GOMRoleInfoSouth_ROKMC_Grunt.uc
//=============================================================================
// Republic of Korea Marine Corps Grunt Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ROKMC_Grunt extends GOMRoleInfoSouth_ROK_Grunt;
