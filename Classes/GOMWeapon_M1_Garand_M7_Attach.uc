//=============================================================================
// GOMWeapon_M1_Garand_M7_Attach.uc
//=============================================================================
// ARVN M1 Garand Rifle (Attachment class).
// Modified for M7 Rifle Grenade functionality.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M1_Garand_M7_Attach extends ROWeapAttach_M1Garand_Rifle;

defaultproperties
{
	WeaponClass=class'GOMWeapon_M1_Garand_M7'
	
	CarrySocketName=none
}
