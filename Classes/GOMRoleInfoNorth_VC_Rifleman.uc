//=============================================================================
// GOMRoleInfoNorth_VC_Rifleman.uc
//=============================================================================
// Viet Cong Rifleman Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_VC_Rifleman extends GOMRoleInfoNorth_VC;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=1
	ClassIndex=`ROCI_HEAVY
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_AK47_AssaultRifle_NLF'),
		SecondaryWeapons=(class'ROWeap_TT33_Pistol'),
		OtherItems=(class'ROWeap_Type67_GrenadeSingle')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_MAS49_Rifle'),
		OtherItems=(class'ROWeap_Type67_Grenade')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_AK47_AssaultRifle_NLF'),
		SecondaryWeapons=(class'ROWeap_TT33_Pistol'),
		OtherItems=(class'ROWeap_Type67_GrenadeSingle')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_AK47_AssaultRifle'),
		OtherItems=(class'ROWeap_Type67_GrenadeSingle')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_guerilla'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_guerilla'
}
