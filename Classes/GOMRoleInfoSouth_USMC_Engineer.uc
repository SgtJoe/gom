//=============================================================================
// GOMRoleInfoSouth_USMC_Engineer.uc
//=============================================================================
// United States Marine Corps Combat Engineer Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_USMC_Engineer extends GOMRoleInfoSouth_US_Engineer;

DefaultProperties
{
	Items[RORIGM_Default]={(
		OtherItems=(class'ROWeap_M61_Grenade',class'ROWeap_M18_Claymore',class'ROWeap_C4_Explosive')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		OtherItems=(class'ROWeap_M61_Grenade',class'ROWeap_M18_Claymore',class'ROWeap_C4_Explosive')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle',class'ROWeap_M37_Shotgun',class'ROWeap_M9_Flamethrower'),
		OtherItems=(class'ROWeap_M61_Grenade',class'ROWeap_M18_Claymore',class'ROWeap_C4_Explosive')
	)}
}
