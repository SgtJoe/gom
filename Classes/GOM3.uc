//=============================================================================
// GOM3.uc
//=============================================================================
// Master mutator class for GOM 3.0 update.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOM3 extends ROMutator
	config(Mutator_GOM_Server);

var config int LastRefreshBuild;

enum ENorthForce
{
	NF_VietCong,
	NF_NorthVietnameseArmy,
	NF_NorthVietnameseArmyUrban
};

enum ESouthForce
{
	SF_UnitedStates,
	SF_UnitedStatesMarines,
	SF_MACV,
	SF_SouthVietnam,
	SF_SouthVietnamRangers,
	SF_Australia,
	SF_SouthKorea,
	SF_SouthKoreaArmoured,
	SF_SouthKoreaMarines
};

enum EMiniFactions
{
	MF_MilitaryPolice,
	MF_ARVNSupport,
	MF_None
};

struct MapData
{
	var string MapName;
	var ENorthForce NorthForce;
	var ESouthForce SouthForce;
	var EMiniFactions MiniFaction;
	var bool Locked;
	
	structdefaultproperties
	{
		Locked = false;
	}
};

var localized array<string> NorthArmyNames;
var localized array<string> SouthArmyNames;
var localized array<string> NorthArmyShortNames;
var localized array<string> SouthArmyShortNames;

// Collection of data for maps
var config array<MapData> MapOptions;

// Single map options object for offline games
var MapData CurrentMap;

// What map we're currently on in the map array
var int MapIndex;

// Pawn ContentClasses
var RORoleInfoClasses ccVC, ccNVA, ccNVAU, ccUS, ccMACV, ccARVN, ccAUS, ccROK;
var string ccUSF, ccAUSF, ccROKF, ccUSP, ccARVNP, ccAUSP, ccROKP;

//=============================================================================
// WARNING: PreBeginPlay() on a Mutator can only modify the GameInfo, any
// changes to MapInfo from here will NOT be replicated. So we have to 
// set a custom PlayerController and make all MapInfo changes there.
// Also on a server this function is only run once on startup. If the 
// server changes maps these modifications won't happen. 
//=============================================================================
function PreBeginPlay()
{
	if (class'GOMDefaultSettings'.default.Version != `BUILDVER)
	{
		class'GOMDefaultSettings'.static.UpdateBuildVer();
	}
	
	`gom(self @ "starting up Gameplay Overhaul Mutator" @ `BUILDVER, 'Init');
	
	CheckConfig();
	
	ROGameInfo(WorldInfo.Game).PlayerControllerClass = class'GOMPlayerController';
	ROGameInfo(WorldInfo.Game).PlayerReplicationInfoClass = class'GOMPlayerReplicationInfo';
	ROGameInfo(WorldInfo.Game).GameReplicationInfoClass = class'GOMGameReplicationInfo';
	ROGameInfo(WorldInfo.Game).HUDType = class'GOMHUD';
	
	if (WorldInfo.NetMode == NM_Standalone)
	{
		VerifyMapList();
	}
	
	super.PreBeginPlay();
}

simulated function ReplacePawns()
{
	`gom("Replacing Pawns", 'Pawns');
	
	switch (`getNF)
	{
		case `VC:
			`gom("Replacing North pawns with VC", 'Pawns');
			ROGameInfo(WorldInfo.Game).NorthRoleContentClasses = ccVC;
			break;
		
		case `NVA:
			`gom("Replacing North pawns with NVA", 'Pawns');
			ROGameInfo(WorldInfo.Game).NorthRoleContentClasses = ccNVA;
			break;
		
		case `NVAU:
			`gom("Replacing North pawns with NVAU", 'Pawns');
			ROGameInfo(WorldInfo.Game).NorthRoleContentClasses = ccNVAU;
			break;
	}
	
	switch (`getSF)
	{
		case `US:
		case `USMC:
			`gom("Replacing South pawns with US", 'Pawns');
			ROGameInfo(WorldInfo.Game).SouthRoleContentClasses = ccUS;
			ROGameInfo(WorldInfo.Game).SouthRoleFlamerContentClass = ccUSF;
			ROGameInfo(WorldInfo.Game).SouthRolePilotContentClass = ccUSP;
			break;
		
		case `MACV:
			`gom("Replacing South pawns with MACV", 'Pawns');
			ROGameInfo(WorldInfo.Game).SouthRoleContentClasses = ccMACV;
			ROGameInfo(WorldInfo.Game).SouthRoleFlamerContentClass = ccUSF;
			ROGameInfo(WorldInfo.Game).SouthRolePilotContentClass = ccUSP;
			break;
		
		case `ARVN:
		case `RANGERS:
			`gom("Replacing South pawns with ARVN", 'Pawns');
			ROGameInfo(WorldInfo.Game).SouthRoleContentClasses = ccARVN;
			ROGameInfo(WorldInfo.Game).SouthRoleFlamerContentClass = ccUSF;
			ROGameInfo(WorldInfo.Game).SouthRolePilotContentClass = ccARVNP;
			break;
		
		case `AUS:
			`gom("Replacing South pawns with AUS", 'Pawns');
			ROGameInfo(WorldInfo.Game).SouthRoleContentClasses = ccAUS;
			ROGameInfo(WorldInfo.Game).SouthRoleFlamerContentClass = ccAUSF;
			ROGameInfo(WorldInfo.Game).SouthRolePilotContentClass = ccAUSP;
			break;
		
		case `ROK:
		case `ROKA:
		case `ROKMC:
			`gom("Replacing South pawns with ROK", 'Pawns');
			ROGameInfo(WorldInfo.Game).SouthRoleContentClasses = ccROK;
			ROGameInfo(WorldInfo.Game).SouthRoleFlamerContentClass = ccROKF;
			ROGameInfo(WorldInfo.Game).SouthRolePilotContentClass = ccROKP;
			break;
	}
}

function CheckConfig()
{
	local int i;
	local MapData NewMapData;
	
	`gom("Checking config", 'Init');
	
	if (default.LastRefreshBuild < `CURRENTID)
	{
		MapOptions.length = 0;
		default.MapOptions.length = 0;
		SaveConfig();
		
		LastRefreshBuild = `CURRENTID;
		default.LastRefreshBuild = `CURRENTID;
	}
	
	SaveConfig();
	
	if (default.MapOptions.length == 0 || MapOptions.length == 0)
	{
		`gom("Server config has not been initialized, creating config file from default template", 'Init');
		
		MapOptions.length = class'GOMDefaultSettings'.default.DefaultMapOptions.length;
		
		for (i = 0; i < class'GOMDefaultSettings'.default.DefaultMapOptions.length; i++)
		{
			NewMapData.MapName = class'GOMDefaultSettings'.default.DefaultMapOptions[i].MapName;
			NewMapData.NorthForce = class'GOMDefaultSettings'.default.DefaultMapOptions[i].NorthForce;
			NewMapData.SouthForce = class'GOMDefaultSettings'.default.DefaultMapOptions[i].SouthForce;
			NewMapData.MiniFaction = class'GOMDefaultSettings'.default.DefaultMapOptions[i].MiniFaction;
			NewMapData.Locked = class'GOMDefaultSettings'.default.DefaultMapOptions[i].Locked;
			
			MapOptions[i] = NewMapData;
			
			SaveConfig();
		}
	}
	
	for (i = 0; i < MapOptions.length; i++)
	{
		`gom(""$ MapOptions[i].MapName @ MapOptions[i].NorthForce @ MapOptions[i].SouthForce @ MapOptions[i].MiniFaction, 'Init-MapCheck');
	}
}

// Have to hijack this function because GRI does not seem to exist in PreBeginPlay OR PostBeginPlay even though it's created in PreBeginPlay
// This means at least one person has to connect for anything to happen
// WARNING: this function does NOT run in offline games, use PreBeginPlay() instead
function ModifyPreLogin( string Options, string Address, out string ErrorMessage )
{
	`gom("GRI class is" @ ROGameInfo(WorldInfo.Game).GameReplicationInfoClass, 'Replication');
	`gom("GRI is" @ WorldInfo.GRI, 'Replication');
	
	VerifyMapList();
	
	`gom("Map:" @ WorldInfo.GetMapName(true) @ "ID" @ GOMGameReplicationInfo(WorldInfo.GRI).RMapIndex @ "Data:" @ `getNF @ `getSF @ `getMF, 'Init');
	`gom("NVA on map?" @ `NVAMap, 'Init');
	
	ReplacePawns();
	
	if (!`Campaign)
	{
		// Factions in MapInfo need to be synced on both client and server, otherwise commander abilities will not change
		
		if (`getSF == `USMC)
		{
			ROMapInfo(WorldInfo.GetMapInfo()).SouthernForce = SFOR_USMC;
		}
		else if (`getSF == SF_Australia)
		{
			ROMapInfo(WorldInfo.GetMapInfo()).SouthernForce = SFOR_AusArmy;
		}
		else if (`getSF == SF_SouthVietnam || `getSF == SF_SouthVietnamRangers)
		{
			ROMapInfo(WorldInfo.GetMapInfo()).SouthernForce = SFOR_ARVN;
		}
		else
		{
			ROMapInfo(WorldInfo.GetMapInfo()).SouthernForce = SFOR_USArmy;
		}
		
		ROMapInfo(WorldInfo.GetMapInfo()).NorthernForce = (`NVAMap) ? NFOR_NVA : NFOR_NLF;
	}
	
	ReplaceHelicopters();
	
	if (`NVAMap)
	{
		// Make the NVA spawn on SL, only the VC use tunnels
		ROGameInfo(WorldInfo.Game).SquadSpawnMethod[`AXIS_TEAM_INDEX] = 1 /*ROSSM_SquadLeader*/;
		ROGameReplicationInfo(WorldInfo.GRI).SquadSpawnMethod[`AXIS_TEAM_INDEX] = 1 /*ROSSM_SquadLeader*/;
		
		`gom("Spawn method:" @ ROGameInfo(WorldInfo.Game).SquadSpawnMethod[`AXIS_TEAM_INDEX], 'Spawning');
	}
	
`ifndef(RELEASE)
	ROMapInfo(WorldInfo.GetMapInfo()).MinimumTimeDead = 0;
	ROMapInfo(WorldInfo.GetMapInfo()).AxisReinforcementDelay16 = 3;
	ROMapInfo(WorldInfo.GetMapInfo()).AxisReinforcementDelay32 = 3;
	ROMapInfo(WorldInfo.GetMapInfo()).AxisReinforcementDelay64 = 3;
	ROMapInfo(WorldInfo.GetMapInfo()).AlliesReinforcementDelay16 = 3;
	ROMapInfo(WorldInfo.GetMapInfo()).AlliesReinforcementDelay32 = 3;
	ROMapInfo(WorldInfo.GetMapInfo()).AlliesReinforcementDelay64 = 3;
`endif
}

simulated function ReplaceHelicopters()
{
	local ROVehicleFactory_OH6 LoachSpawn;
	local ROVehicleFactory_AH1G CobraSpawn;
	local ROVehicleFactory_UH1H THueySpawn;
	local ROVehicleFactory_UH1H_Gunship GHueySpawn;
	local GOMVehicleFactory testfactory;
	local class<ROVehicleHelicopter> NewHeliClass;
	local bool DeletThis;
	DeletThis = false;
	
	if (ROGameReplicationInfo(WorldInfo.GRI).bNoVehicleRoles)
	{
		`gom("No helicopters on map", 'Helis');
		return;
	}
	
	`gom("WARNING - CAMPAIGNPHASE:" @ `CampaignPhase,'Campaign');
	
	foreach BasedActors(class'GOMVehicleFactory', testfactory)
	{
		if (testfactory.TankFactory)
		{
			`gom("Map has Tanks, removing helicopters", 'Helis');
			DeletThis = true;
			break;
		}
	}
	
	`gom("Replacing Helicopters", 'Helis');
	
	foreach AllActors(class'ROVehicleFactory_OH6', LoachSpawn)
	{
		switch (`getSF)
		{
			case `USMC:
				NewHeliClass = class'GOMHeli_UH1H_USMC_Gunship_Content';
				break;
			
			case `AUS:
				NewHeliClass = class'GOMHeli_OH6_AUS_Content';
				break;
			
			case `ROK:
			case `ROKMC:
				NewHeliClass = class'GOMHeli_OH6_ROK_Content';
				break;
			
			default:
				NewHeliClass = class'GOMHeli_OH6_US_Content';
		}
		
		if (DeletThis || (`Campaign && `CampaignPhase != `CP_MID))
		{
			NewHeliClass = none;
			LoachSpawn.bDisabled = true;
		}
		
		`gom("Replacing" @ LoachSpawn @ "with" @ NewHeliClass, 'Helis');
		
		LoachSpawn.VehicleClass = NewHeliClass;
		LoachSpawn.USLoachClass = NewHeliClass;
		LoachSpawn.AusLoachClass = NewHeliClass;
	}
	
	foreach AllActors(class'ROVehicleFactory_AH1G', CobraSpawn)
	{
		switch (`getSF)
		{
			case `USMC:
				NewHeliClass = class'GOMHeli_AH1G_USMC_Content';
				break;
			
			case `AUS:
				NewHeliClass = class'GOMHeli_UH1H_AUS_Gunship_Content';
				break;
			
			case `ARVN:
			case `RANGERS:
				NewHeliClass = class'GOMHeli_UH1H_VNAF_Gunship_Content';
				break;
			
			case `ROK:
			case `ROKMC:
				NewHeliClass = class'GOMHeli_AH1G_ROK_Content';
				break;
			
			default:
				NewHeliClass = class'GOMHeli_AH1G_US_Content';
		}
		
		if (DeletThis || (`Campaign && `CampaignPhase != `CP_MID))
		{
			NewHeliClass = none;
			CobraSpawn.bDisabled = true;
		}
		
		`gom("Replacing" @ CobraSpawn @ "with" @ NewHeliClass, 'Helis');
		
		CobraSpawn.VehicleClass = NewHeliClass;
	}
	
	foreach AllActors(class'ROVehicleFactory_UH1H', THueySpawn)
	{
		switch (`getSF)
		{
			case `USMC:
				NewHeliClass = class'GOMHeli_UH1H_USMC_Transport_Content';
				break;
			
			case `AUS:
				NewHeliClass = class'GOMHeli_UH1H_AUS_Transport_Content';
				break;
			
			case `ARVN:
			case `RANGERS:
				NewHeliClass = class'GOMHeli_UH1H_VNAF_Transport_Content';
				break;
			
			case `ROK:
			case `ROKMC:
				NewHeliClass = class'GOMHeli_UH1H_ROK_Transport_Content';
				break;
			
			default:
				NewHeliClass = class'GOMHeli_UH1H_US_Transport_Content';
		}
		
		if (DeletThis)
		{
			NewHeliClass = none;
			THueySpawn.bDisabled = true;
		}
		
		`gom("Replacing" @ THueySpawn @ "with" @ NewHeliClass, 'Helis');
		
		THueySpawn.VehicleClass = NewHeliClass;
		THueySpawn.AusHueyClass = NewHeliClass;
		THueySpawn.USHueyClass = NewHeliClass;
	}
	
	foreach AllActors(class'ROVehicleFactory_UH1H_Gunship', GHueySpawn)
	{
		switch (`getSF)
		{
			case `USMC:
				NewHeliClass = class'GOMHeli_AH1G_USMC_Content';
				break;
			
			case `AUS:
				NewHeliClass = class'GOMHeli_UH1H_AUS_Gunship_Content';
				break;
			
			case `ARVN:
			case `RANGERS:
				NewHeliClass = class'GOMHeli_UH1H_VNAF_Gunship_Content';
				break;
			
			case `ROK:
			case `ROKMC:
				NewHeliClass = class'GOMHeli_AH1G_ROK_Content';
				break;
			
			default:
				NewHeliClass = class'GOMHeli_AH1G_US_Content';
		}
		
		if (DeletThis || (`Campaign && `CampaignPhase != `CP_MID))
		{
			NewHeliClass = none;
			GHueySpawn.bDisabled = true;
		}
		
		`gom("Replacing" @ GHueySpawn @ "with" @ NewHeliClass, 'Helis');
		
		GHueySpawn.VehicleClass = NewHeliClass;
	}
}

static function int GetMapIndex()
{
	if (class'Engine'.static.GetCurrentWorldInfo().NetMode == NM_Standalone)
	{
		return GOM3(class'Engine'.static.GetCurrentWorldInfo().Game.BaseMutator).MapIndex;
	}
	
	return GOMGameReplicationInfo(class'Engine'.static.GetCurrentWorldInfo().GRI).RMapIndex;
}

// Checks to see if we have the current map in our map list and adds it if we don't
// NEW: Pass data to GameReplication so that it can send everything to clients
simulated function VerifyMapList()
{
	local int i, NewMapIndex;
	local bool FoundMap, MapLocked;
	local MapData NewMapData;
	local string CurrentMapName;
	
	CurrentMapName = class'Engine'.static.GetCurrentWorldInfo().GetMapName(true);
	FoundMap = false;
	
	if (class'Engine'.static.IsEditor())
	{
		ReplaceText(CurrentMapName, "UEDPIE", "");
	}
	
	for (i = 0; i < MapOptions.length; i++)
	{
		if (CurrentMapName == MapOptions[i].MapName)
		{
			FoundMap = true;
			NewMapIndex = i;
			
			`gom("Found map, index is" @ NewMapIndex, 'Replication');
			GOMGameReplicationInfo(WorldInfo.GRI).RMapIndex = NewMapIndex;
			
			NewMapData.MapName = CurrentMapName;
			NewMapData.NorthForce = MapOptions[i].NorthForce;
			NewMapData.SouthForce = MapOptions[i].SouthForce;
			NewMapData.MiniFaction = MapOptions[i].MiniFaction;
		}
	}
	
	// Don't add non-maps to the maplist
	if (!FoundMap && CurrentMapName != "ROEntry" && CurrentMapName != "VNTE-CampaignStart")
	{
		`gom("Could not find map" @ CurrentMapName$", creating new entry", 'Init');
		NewMapData.MapName = CurrentMapName;
		NewMapData.NorthForce = NF_VietCong;
		NewMapData.SouthForce = SF_UnitedStates;
		NewMapData.MiniFaction = MF_None;
		
		MapOptions[MapOptions.length] = NewMapData;
		NewMapIndex = MapOptions.length - 1;
		
		`gom("Map index is" @ NewMapIndex, 'Replication');
		GOMGameReplicationInfo(WorldInfo.GRI).RMapIndex = NewMapIndex;
		
		SaveConfig();
	}
	
	for (i = 0; i < class'GOMDefaultSettings'.default.LockedMaps.length; i++)
	{
		if (CurrentMapName == class'GOMDefaultSettings'.default.LockedMaps[i].MapName)
		{
			MapLocked = true;
			
			`gom("Map" @ CurrentMapName @ "is locked, making sure it hasn't been modified", 'Init');
			
			NewMapData.NorthForce = class'GOMDefaultSettings'.default.LockedMaps[i].NorthForce;
			NewMapData.SouthForce = class'GOMDefaultSettings'.default.LockedMaps[i].SouthForce;
			NewMapData.MiniFaction = class'GOMDefaultSettings'.default.LockedMaps[i].MiniFaction;
			NewMapData.Locked = true;
			
			MapOptions[NewMapIndex] = NewMapData;
			
			SaveConfig();
		}
	}
	
	if (!MapLocked && NewMapData.SouthForce >= `ROK)
	{
		// `gom("ERROR - cannot dynamically assign ROK factions, resetting SouthForce to default for map" @ CurrentMapName, 'Init');
		// NewMapData.SouthForce = SF_UnitedStates;
	}
	
	GOMGameReplicationInfo(WorldInfo.GRI).RNorthForce = NewMapData.NorthForce;
	GOMGameReplicationInfo(WorldInfo.GRI).RSouthForce = NewMapData.SouthForce;
	GOMGameReplicationInfo(WorldInfo.GRI).RMiniFaction = NewMapData.MiniFaction;
	
	CurrentMap.NorthForce = NewMapData.NorthForce;
	CurrentMap.SouthForce = NewMapData.SouthForce;
	CurrentMap.MiniFaction = NewMapData.MiniFaction;
	
	if (`Campaign)
	{
		`gom("WARNING - WE ARE IN CAMPAIGN - MANUALLY SETTING PROPER FACTIONS", 'Campaign');
		
		if (ROMapInfo(WorldInfo.GetMapInfo()).NorthernForce == NFOR_NVA)
		{
			GOMGameReplicationInfo(WorldInfo.GRI).RNorthForce = (`CampaignPhase == `CP_LATE) ? `NVA : `NVAU;
		}
		else
		{
			GOMGameReplicationInfo(WorldInfo.GRI).RNorthForce = `VC;
		}
		
		`gom("WARNING - NORTH FACTION:" @ GOMGameReplicationInfo(WorldInfo.GRI).RNorthForce, 'Campaign');
		
		switch (ROMapInfo(class'Engine'.static.GetCurrentWorldInfo().GetMapInfo()).SouthernForce)
		{
			case SFOR_USMC:
				GOMGameReplicationInfo(WorldInfo.GRI).RSouthForce = `USMC;
				break;
				
			case SFOR_AusArmy:
				GOMGameReplicationInfo(WorldInfo.GRI).RSouthForce = `AUS;
				break;
				
			case SFOR_ARVN:
				GOMGameReplicationInfo(WorldInfo.GRI).RSouthForce = (`CampaignPhase == `CP_LATE) ? `RANGERS : `ARVN;
				break;
			
			default:
				GOMGameReplicationInfo(WorldInfo.GRI).RSouthForce = `US;
		}
		
		`gom("WARNING - SOUTH FACTION:" @ GOMGameReplicationInfo(WorldInfo.GRI).RSouthForce, 'Campaign');
	}
	
	`gom("Mapindex is" @ NewMapIndex @ "Replicated:" @ GOMGameReplicationInfo(WorldInfo.GRI).RMapIndex, 'Replication');
	
	`gom(""$ WorldInfo.GRI @ "Data:" @ GOMGameReplicationInfo(WorldInfo.GRI).RNorthForce @ GOMGameReplicationInfo(WorldInfo.GRI).RSouthForce @ GOMGameReplicationInfo(WorldInfo.GRI).RMiniFaction, 'Init');
	`gom(""$ self @ "Data:" @ `getNF @ `getSF @ `getMF, 'Init');
}

static function ENorthForce GetNorthernForce()
{
	if (class'Engine'.static.GetCurrentWorldInfo().NetMode == NM_Standalone)
	{
		return GOM3(class'Engine'.static.GetCurrentWorldInfo().Game.BaseMutator).CurrentMap.NorthForce;
	}
	
	return GOMGameReplicationInfo(class'Engine'.static.GetCurrentWorldInfo().GRI).RNorthForce;
}

static function ESouthForce GetSouthernForce()
{
	if (class'Engine'.static.GetCurrentWorldInfo().NetMode == NM_Standalone)
	{
		return GOM3(class'Engine'.static.GetCurrentWorldInfo().Game.BaseMutator).CurrentMap.SouthForce;
	}
	
	return GOMGameReplicationInfo(class'Engine'.static.GetCurrentWorldInfo().GRI).RSouthForce;
}

static function EMiniFactions GetMiniFaction()
{
	if (class'Engine'.static.GetCurrentWorldInfo().NetMode == NM_Standalone)
	{
		return GOM3(class'Engine'.static.GetCurrentWorldInfo().Game.BaseMutator).CurrentMap.MiniFaction;
	}
	
	return GOMGameReplicationInfo(class'Engine'.static.GetCurrentWorldInfo().GRI).RMiniFaction;
}

static function bool NVAPresentOnMap()
{
	return (`getNF == `NVA) || (`getNF == `NVAU);
}

function ModifyPlayer(Pawn Other)
{
	local ROPlayerController c;
	local ROPawn p;
	
	c = ROPlayerController(Other.Controller);
	p = ROPawn(Other);
	
	if (p != none)
	{
		if (c != none)
		{
			`gomdebug("DEBUG MODE ENABLED");
			
			if (p.bUseSingleCharacterVariant)
			{
				c.ClientMessage("WARNING! Your Character Detail setting is Low, you will not see new cosmetics.");
			}
		}
		
		// This causes log spam, so let's keep it offline only
		// Plus it really only becomes a problem when bots are spawned
		if (class'Engine'.static.GetCurrentWorldInfo().NetMode == NM_Standalone)
		{
			p.BreathInSound = none;
			p.BreathOutSound = none;
			p.BreathHoldSound = none;
			p.BreathLowStaminaSound = none;
			p.BreathNoStaminaSound = none;
			p.BreathStopSound = none;
			p.BreathStopQuickSound = none;
		}
	}
}

defaultproperties
{
	ccNVAU=(LevelContentClasses=("GOM3.GOMPawnNorthNVAU"))
	ccNVA=(LevelContentClasses=("GOM3.GOMPawnNorthNVA"))
	ccVC=(LevelContentClasses=("GOM3.GOMPawnNorthVC"))
	
	ccUS=(LevelContentClasses=("GOM3.GOMPawnSouthUS"))
	ccUSF="GOM3.GOMPawnSouthFlamethrowerUS"
	ccUSP="GOM3.GOMPawnSouthPilotUS"
	
	ccMACV=(LevelContentClasses=("GOM3.GOMPawnSouthMACV"))
	
	ccARVN=(LevelContentClasses=("GOM3.GOMPawnSouthARVN"))
	ccARVNP="GOM3.GOMPawnSouthPilotARVN"
	
	ccAUS=(LevelContentClasses=("GOM3.GOMPawnSouthAUS"))
	ccAUSF="GOM3.GOMPawnSouthFlamethrowerAUS"
	ccAUSP="GOM3.GOMPawnSouthPilotAUS"
	
	ccROK=(LevelContentClasses=("GOM3.GOMPawnSouthROK"))
	ccROKF="GOM3.GOMPawnSouthFlamethrowerROK"
	ccROKP="GOM3.GOMPawnSouthPilotROK"
}