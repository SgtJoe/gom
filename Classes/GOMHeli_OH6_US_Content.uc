//=============================================================================
// GOMHeli_OH6_US_Content.uc
//=============================================================================
// OH-6 "Loach" Observation Helicopter (US Army)
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHeli_OH6_US_Content extends ROHeli_OH6_Content
	placeable;

`include(GOM3\Classes\GOMHelicoptersCommon.uci)
