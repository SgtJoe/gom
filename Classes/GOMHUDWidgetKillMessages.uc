//=============================================================================
// GOMHUDWidgetWeapon.uc
//=============================================================================
// HUD Widget that displays Kill/Death messages.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHUDWidgetKillMessages extends ROHUDWidgetKillMessages;

function Texture2D GetNonWeaponKillTextureFromDamageType(class<DamageType> InDamageType, byte InKillType, out string WeaponShortName, out int KillIconX, out int KillIconY, optional out byte WasSpecificHitZoneKill)
{
	if (InDamageType == class'GOMDmgType_RoadRage')
	{
		KillIconX = 128;
		KillIconY = 64;
		
		WeaponShortName = GetWeaponShortNameFromDamageClass(InDamageType, InKillType);
		return M113RanOverKillTexture;
	}
	
	return super.GetNonWeaponKillTextureFromDamageType(InDamageType, InKillType, WeaponShortName, KillIconX, KillIconY, WasSpecificHitZoneKill);
}

defaultproperties
{
	M113RanOverKillTexture=Texture2D'GOM3_UI.Vehicle.UI_Kill_Icon_RoadRage'
}
