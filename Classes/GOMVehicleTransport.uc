//=============================================================================
// GOMVehicleTransport.uc
//=============================================================================
// Base class for ground vehicles.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleTransport extends ROVehicleTransport
	abstract;

var bool SoftSkinnedVehicle; // For vehicles that have no armor like cars & trucks

var float EngineStartupSeconds; // How long we have to wait before the vehicle can start moving

var array<MaterialInstanceConstant> DestroyedMats;

// Note: this and the original enum are purely for reference purposes
// Why they used an enum instead of macros, I don't know
enum ExtendedVehicleEffectIDs
{
	VFX_Firing1,		// MG 1 Muzzleflash
	VFX_Firing2,		// MG 1 Shell Eject
	VFX_Firing3,		// MG 2 Muzzleflash
	VFX_Firing4,		// MG 2 Shell Eject
	VFX_Exhaust,		// Exhaust Smoke
	VFX_TreadWing,		// Tread Debris
	VFX_DmgSmoke,		// Running Damaged Smoke
	VFX_DmgInterior,	// Penetration Smoke
	VFX_DeathSmoke1,	// Dead Smoke 1
	VFX_DeathSmoke2,	// Dead Smoke 2
	VFX_DeathSmoke3,	// Dead Smoke 3
	// The original enum ends here, add any extras below
	VFX_Firing5,		// MG 3 Muzzleflash
	VFX_Firing6,		// MG 3 Shell Eject
	VFX_Firing7,		// MG 4 Muzzleflash
	VFX_Firing8			// MG 4 Shell Eject
};

function EvaluateBackSeatDrivers()
{
`ifndef(RELEASE)
	super(ROVehicle).EvaluateBackSeatDrivers();
`endif
}

function HandleMomentum( vector Momentum, Vector HitLocation, class<DamageType> DamageType, optional TraceHitInfo HitInfo )
{
	if (class<RODmgType_SmallArmsBullet>(DamageType) != none)
	{
		AddVelocity( Momentum, HitLocation, DamageType, HitInfo );
	}
}

simulated function StartEngineSound()
{
	super.StartEngineSound();
	
	if (EngineSound != None)
	{
		EngineSound.StopEvents();
	}
	
	SetTimer(EngineStartupSeconds, false, 'EngineStartDelay');
	
	// if(EngineStartLeftSound != None)
	// {
		// EngineStartLeftSound.PlayEvent(EngineStartLeftSoundEvent);
	// }
	if(EngineStartRightSound != None && !EngineStartRightSound.IsPlaying())
	{
		EngineStartRightSound.PlayEvent(EngineStartRightSoundEvent);
	}
	// if(EngineStartExhaustSound != None)
	// {
		// EngineStartExhaustSound.StopEvents();
		// EngineStartExhaustSound.PlayEvent(EngineStartExhaustSoundEvent);
	// }
}

simulated function EngineStartDelay()
{
	if (EngineSound != None)
	{
		EngineSound.PlayEvent(EngineSoundEvent);
	}
	
	ClearTimer('EngineStartDelay');
}

simulated function StopEngineSound()
{
	Super.StopEngineSound();
	
	if( EngineStopSound != none )
	{
		EngineStopSound.StopEvents();
		EngineStopSound.PlayEvent(EngineStopSoundEvent);
	}
}

simulated function SetInputs(float InForward, float InStrafe, float InUp)
{
	if( IsTimerActive('EngineStartDelay') )
	{
		InForward = 0;
		InStrafe = 0;
	}
	
	Super(ROVehicleTreaded).SetInputs(InForward, InStrafe, InUp);
	
	if (EngineSound != None && EngineIdleDamagedSound != none)
	{
		if (InForward > 0 || InStrafe > 0)
		{
			ReplaceLoopingAudio(EngineSound, EngineIdleDamagedSound);
		}
		else
		{
			ReplaceLoopingAudio(EngineSound, EngineSoundEvent);
		}
	}
}

simulated event TakeDamage(int Damage, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	local int i;
	local bool bHasAssisted;
	local int HitIndex, HitSeatIndex; //, HitZoneDamage, VehicleDamage;
	local ROPawn ROP;
	local TraceHitInfo DriverHitInfo;
	
	// First we process battlechatter events and last combat times
	if (Role == ROLE_Authority)
	{
		CheckDamageSmoke();
		
		LastCombatEventTime = WorldInfo.TimeSeconds;
		
		if (EventInstigator != none)
		{
			// Friendly fire: Vehicle - Vehicle
			if (ROVehicle(EventInstigator.Pawn) != none && ROVehicle(EventInstigator.Pawn).Team == Team)
			{
				LastEnemyEncounterTime = WorldInfo.TimeSeconds;
				
				ROVehicle(EventInstigator.Pawn).LastEnemyEncounterTime = WorldInfo.TimeSeconds;
				ROVehicle(EventInstigator.Pawn).LastCombatEventTime = WorldInfo.TimeSeconds;
				
				HandleBattleChatterEvent(`BATTLECHATTER_FriendlyFire);
				
				if( ROVehicleHelicopter(EventInstigator.Pawn) != none )
					ROVehicle(EventInstigator.Pawn).HandleBattleChatterEvent(`BATTLECHATTER_HeloFriendlyFire);
				else
					ROVehicle(EventInstigator.Pawn).HandleBattleChatterEvent(`BATTLECHATTER_TankFriendlyFire);
			}
			// Friendly fire: Vehicle Gun - Vehicle
			else if (ROWeaponPawn(EventInstigator.Pawn) != none && ROWeaponPawn(EventInstigator.Pawn).MyVehicle.Team == Team)
			{
				LastEnemyEncounterTime = WorldInfo.TimeSeconds;
				
				HandleBattleChatterEvent(`BATTLECHATTER_FriendlyFire);
				
				if( ROVehicleHelicopter(ROWeaponPawn(EventInstigator.Pawn).MyVehicle) != none )
					ROWeaponPawn(EventInstigator.Pawn).MyVehicle.HandleBattleChatterEvent(`BATTLECHATTER_HeloFriendlyFire);
				else
					ROWeaponPawn(EventInstigator.Pawn).MyVehicle.HandleBattleChatterEvent(`BATTLECHATTER_TankFriendlyFire);
			}
			// Friendly fire: Infantry - Vehicle
			else if (ROPawn(EventInstigator.Pawn) != none && ROPawn(EventInstigator.Pawn).GetTeamNum() == Team)
			{
				LastEnemyEncounterTime = WorldInfo.TimeSeconds;
				
				HandleBattleChatterEvent(`BATTLECHATTER_FriendlyFire);
				ROGameInfo(WorldInfo.Game).HandleBattleChatterEvent(EventInstigator.Pawn, `BATTLECHATTER_SawFriendlyFire);
			}
			// Enemy fire: Infantry - Vehicle
			else if (ROPawn(EventInstigator.Pawn) != none)
			{
				LastEnemyEncounterTime = WorldInfo.TimeSeconds;
				
				HandleBattleChatterEvent(`BATTLECHATTER_TakingFireInfantry);
			}
			
			for ( i = 0; i < DamageCausers.Length; i++)
			{
				if ( DamageCausers[i] == EventInstigator.PlayerReplicationInfo )
				{
					bHasAssisted = true;
				}
			}
			
			if ( EventInstigator != none && (LastDamageCauser != EventInstigator || WorldInfo.TimeSeconds > LastDamageTime + 0.5) &&
				 !bHasAssisted && EventInstigator != Controller && Damage * DamageType.static.VehicleDamageScalingFor(self) * DamageType.static.VehicleTreadDamageScalingFor(self) > 0 )
			{
				DamageCausers.AddItem(EventInstigator.PlayerReplicationInfo);
				LastDamageCauser = EventInstigator;
				LastDamageTime = WorldInfo.TimeSeconds;
			}
		}
	}
	
	`gomdebug("taking" @ DamageType @ "damage", 'VehicleDamage');
	
	if (ShouldInstakill(DamageType, SoftSkinnedVehicle))
	{
		`gomdebug("instakilled by a" @ DamageType, 'VehicleDamage');
		Died(EventInstigator, DamageType, HitLocation);
		return;
	}
	else if (IsAntiArmor(DamageType))
	{
		// for some weird reason this function gets called twice for one RPG hit, so divide this by two
		Health -= 50; //100; 
		
		`gomdebug("health:" @ Health, 'VehicleDamage');
	}
	
	if (Health <= 0)
	{
		`gomdebug("ran out of health", 'VehicleDamage');
		Died(EventInstigator, DamageType, HitLocation);
		return;
	}
	
	if (HitInfo.BoneName != '')
	{
		HitIndex = VehHitZones.Find('PhysBodyBoneName',HitInfo.BoneName);
		
		if (HitIndex < 0)
		{
			HitIndex = VehHitZones.Find('CrewBoneName',HitInfo.BoneName);
		}
		
		if (HitIndex < 0)
		{
			`gomdebug("HitIndex is negative, what do?", 'VehicleDamage');
			return;
		}
		
		/* if (VehHitZones[HitIndex].VehicleHitZoneType == VHT_Fuel && ShouldInstakill(DamageType, true))
		{
			`gomdebug("took a hit straight inside!", 'VehicleDamage');
			
			Died(EventInstigator, DamageType, HitLocation);
			return;
		} */
		
		if (!IsAntiArmor(DamageType))
		{
			if (VehHitZones[HitIndex].VehicleHitZoneType == VHT_CrewBody || VehHitZones[HitIndex].VehicleHitZoneType == VHT_CrewHead)
			{
				`gomdebug("HitIndex" @ HitIndex @ "is a crew zone", 'VehicleDamage');
				
				// Note: the bLocationalHit check means that crew can only be killed by bullets and etc, not explosions.
				if (DamageType.default.bLocationalHit || class<RODmgType_Fire>(DamageType) != none)
				{
					HitSeatIndex = VehHitZones[HitIndex].CrewSeatIndex;
					
					if (Seats[HitSeatIndex].SeatPositions[SeatPositionIndex(HitSeatIndex,,true)].bDriverVisible)
					{
						if( HitSeatIndex >= 0 )
						{
							ROP = ROPawn(Seats[HitSeatIndex].SeatPawn.Driver);
						}
						
						if (ROP != none)
						{
							DriverHitInfo = HitInfo;
							
							if (VehHitZones[HitIndex].VehicleHitZoneType == VHT_CrewBody )
							{
								DriverHitInfo.BoneName = ROP.PlayerHitZones[2].ZoneName; // Chest
							}
							else
							{
								DriverHitInfo.BoneName = ROP.PlayerHitZones[0].ZoneName; // Head
							}
							
							if (Damage > 0)
							{
								`gomdebug("Seat" @ HitSeatIndex @ "taking" @ Damage @ "from" @ DamageType, 'VehicleDamage');
								
								ROP.TakeDamage(Damage, EventInstigator, HitLocation, Momentum, DamageType, DriverHitInfo, DamageCauser);
							}
						}
						else
						{
							`gomdebug("Nobody in Seat" @ HitSeatIndex, 'VehicleDamage');
						}
					}
					else
					{
						`gomdebug("Seat" @ HitSeatIndex @ "is not visible, not hitting.", 'VehicleDamage');
					}
				}
				else
				{
					`gomdebug(DamageType @ "is not locational", 'VehicleDamage');
				}
			}
		}
		else if (IsAntiArmor(DamageType))
		{
			if (VehHitZones[HitIndex].VehicleHitZoneType == VHT_Ammo)
			{
				`gomdebug("instakilled by a critical hit", 'VehicleDamage');
				
				bHitAmmo = true;
				Died(EventInstigator, DamageType, HitLocation);
			}
			else if (VehHitZones[HitIndex].VehicleHitZoneType == VHT_Track)
			{
				`gomdebug("took a hit to the tracks", 'VehicleDamage');
				
				if (Right(VehHitZones[HitIndex].ZoneName, 1) == "R")
				{
					`gomdebug("RIGHT tracks destroyed", 'VehicleDamage');
					bRightTrackDestroyed = true;
				}
				else
				{
					`gomdebug("LEFT tracks destroyed", 'VehicleDamage');
					bLeftTrackDestroyed = true;
				}
			}
			else if (VehHitZones[HitIndex].VehicleHitZoneType == VHT_Engine)
			{
				`gomdebug("took a hit to the engine", 'VehicleDamage');
				
				// ROVehicleSimTreaded(SimObj).bLimitHighGear = true;
				bEngineDestroyed = true;
			}
			else if (VehHitZones[HitIndex].VehicleHitZoneType == VHT_Mechanicals && VehHitZones[HitIndex].CrewSeatIndex >= 0)
			{
				`gomdebug("took a hit to the crew compartment", 'VehicleDamage');
				
				HitSeatIndex = VehHitZones[HitIndex].CrewSeatIndex;
				
				ROP = ROPawn(Seats[HitSeatIndex].SeatPawn.Driver);
				
				if (ROP != none)
				{
					DriverHitInfo = HitInfo;
					DriverHitInfo.BoneName = ROP.PlayerHitZones[2].ZoneName;
					
					`gomdebug("Seat" @ HitSeatIndex @ "taking" @ Damage @ "from" @ DamageType, 'VehicleDamage');
					
					ROP.TakeDamage(Damage, EventInstigator, HitLocation, Momentum, DamageType, DriverHitInfo, DamageCauser);
				}
				else
				{
					`gomdebug("Nobody in Seat" @ HitSeatIndex, 'VehicleDamage');
				}
			}
			else if (VehHitZones[HitIndex].VehicleHitZoneType == VHT_Mechanicals)
			{
				`gomdebug("took a generic hit", 'VehicleDamage');
			}
			else
			{
				`gomdebug("took a hit in zone" @ VehHitZones[HitIndex].VehicleHitZoneType, 'VehicleDamage');
			}
		}
	}
	else
	{
		`gomdebug("HitInfo has a bad value, what do?", 'VehicleDamage');
	}
}

simulated state DyingVehicle
{
	simulated function PlayDeathEffects()
	{
		SwapToDestroyedMesh();
		
		PlayVehicleExplosion(false);
		
		VehicleEvent('Destroyed');
	}
	
	simulated function SwapToDestroyedMesh()
	{
		local int i;
		
		Mesh.SetSkeletalMesh(DestroyedSkeletalMesh);
		Mesh.SetPhysicsAsset(DestroyedPhysicsAsset);
		
		if (DestroyedMats.length > 0)
		{
			for (i = 0; i < DestroyedMats.length; i++)
			{
				Mesh.SetMaterial(i, DestroyedMats[i]);
			}
		}
	}
}

function bool IsAntiArmor(class<DamageType> DamageType)
{
	return (class<RODamageType_CannonShell>(DamageType) != none);
}

function bool ShouldInstakill(class<DamageType> DamageType, bool IgnoreArmor)
{
	if (IgnoreArmor)
	{
		return (class<RODmgType_AntiVehicleGeneral>(DamageType) != none ||
				class<RODamageType_CannonShell>(DamageType) != none ||
				class<RODamageType_Grenades>(DamageType) != none ||
				class<RODmgType_Satchel>(DamageType) != none ||
				class<RODmgTypeArtillery>(DamageType) != none ||
				class<RODmgTypeMineField>(DamageType) != none);
	}
	else
	{
		return (class<RODmgType_Satchel>(DamageType) != none ||
				class<RODmgTypeArtillery>(DamageType) != none ||
				class<RODmgTypeMineField>(DamageType) != none);
	}
}

simulated event PostBeginPlay()
{
	super.PostBeginPlay();
	
	if( WorldInfo.NetMode != NM_DedicatedServer )
	{
		mesh.MinLodModel = 1;
	}
}

simulated function PositionIndexUpdated(int SeatIndex, byte NewPositionIndex)
{
	local int i;
	
	if (Seats[SeatIndex].Gun != none )
	{
		for (i = 0; i < Seats[SeatIndex].TurretControllers.Length; i++)
		{
			if( NewPositionIndex != Seats[SeatIndex].FiringPositionIndex )
			{
				Seats[SeatIndex].TurretControllers[i].SetSkelControlActive(false);
				Seats[SeatIndex].Gun.ForceEndFire();
			}
			else if( NewPositionIndex == Seats[SeatIndex].PreviousPositionIndex )
			{
				EnableMGSkelControls(SeatIndex);
			}
		}
	}
	
	super(ROVehicle).PositionIndexUpdated(SeatIndex, NewPositionIndex);
}

simulated state PositionTransitioning
{
	simulated event EndState(Name NextStateName)
	{
		super.EndState(NextStateName);
		
		if (Seats[0].Gun != none && SeatPositionIndex(0,, true) == Seats[0].FiringPositionIndex )
		{
			EnableMGSkelControls(0);
		}
	}
}

simulated function HandleFinishRemoteSeatPawnPositionTransitioningByIndex(int SeatIndex)
{
	super.HandleFinishRemoteSeatPawnPositionTransitioningByIndex(SeatIndex);
	
	if (Seats[SeatIndex].Gun != none && SeatPositionIndex(SeatIndex,, true) == Seats[SeatIndex].FiringPositionIndex)
	{
		EnableMGSkelControls(SeatIndex);
	}
}

simulated function EnableMGSkelControls(int SeatIndex)
{
	local int i;
	
	for (i = 0; i < Seats[SeatIndex].TurretControllers.Length; i++)
	{
		Seats[SeatIndex].TurretControllers[i].SetSkelControlActive(true);
	}
}

simulated function LeaveBloodSplats(int InSeatIndex) {}

simulated exec function SwitchFireMode() {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// Temp spawn protection code until official ROVolumeSpawnProtection class is updated ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
event Touch(Actor Other, PrimitiveComponent OtherComp, vector HitLocation, vector HitNormal)
{
	super.Touch(Other, OtherComp, HitLocation, HitNormal);
	
	if(ROVolumeSpawnProtection(Other) != none)
	{
		if (ROVolumeSpawnProtection(Other).bEnabled && ROVolumeSpawnProtection(Other).OwningTeam != self.GetTeamNum())
		{
			OnTouchEnemySpawnVolume(ROVolumeSpawnProtection(Other));
		}
	}
}

event UnTouch(Actor Other)
{
	super.UnTouch(Other);
	
	if(ROVolumeSpawnProtection(Other) != none)
	{
		if (ROVolumeSpawnProtection(Other).OwningTeam != self.GetTeamNum())
		{
			OnUnTouchEnemySpawnVolume(ROVolumeSpawnProtection(Other));
		}
	}
}

simulated function OnTouchEnemySpawnVolume(ROVolumeSpawnProtection vol)
{
	local int i;
	
	for (i = 0; i < Seats.Length; i++)
	{
		if (Seats[i].Gun != none)
		{
			`gomdebug("Stopping" @ Seats[i].Gun, 'Vehicle');
			
			// This function stops the actual firing on the client and server
			Seats[i].Gun.StopFire(0);
			Seats[i].Gun.StopFire(1); // we don't have alt firemodes, but just in case
			
			// This function stops the visual effects on the client
			Seats[i].Gun.ClientEndFire(0);
			Seats[i].Gun.ClientEndFire(1);
			
			// and just to make sure
			Seats[i].Gun.ForceEndFire();
			
			Seats[i].Gun.bPrimaryFireDisabled = true;
			Seats[i].Gun.bAlternateFireDisabled = true;
		}
		
		if (ROPlayerController(Seats[i].SeatPawn.Controller) != none)
		{
			ROPlayerController(Seats[i].SeatPawn.Controller).ReceiveLocalizedMessage(class'ROLocalMessageGameRedAlert', RORAMSG_SpawnProtectionWarning,,, vol);
		}
	}
}

simulated function OnUnTouchEnemySpawnVolume(ROVolumeSpawnProtection vol)
{
	local int i;
	
	for (i = 0; i < Seats.Length; i++)
	{
		if (Seats[i].Gun != none)
		{
			Seats[i].Gun.bPrimaryFireDisabled = false;
			Seats[i].Gun.bAlternateFireDisabled = false;
		}
		
		if (ROPlayerController(Seats[i].SeatPawn.Controller) != none)
		{
			ROPlayerController(Seats[i].SeatPawn.Controller).ReceiveLocalizedMessage(class'ROLocalMessageGameRedAlert', RORAMSG_SpawnProtectionWarningUnTouch,,, vol);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DefaultProperties
{
	Health=300 // 1 RPG hit = -100 HP
	
	SoftSkinnedVehicle=false
	
	WeaponPawnClass=class'GOMTransportWeaponPawn'
	
	bOpenVehicle=true
	bUseLoopedMGSound=true
	
	ExitRadius=200
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=60.0
		CollisionRadius=200.0
		Translation=(X=0.0,Y=0.0,Z=0.0)
	End Object
	CylinderComponent=CollisionCylinder
	
	bDontUseCollCylinderForRelevancyCheck=true
	RelevancyHeight=70.0
	RelevancyRadius=130.0
	
	bInfantryCanUse=true
	
	DrivingPhysicalMaterial=PhysicalMaterial'VH_VN_ARVN_M113_APC.Phys.M113_PhysMat_Moving'
	DefaultPhysicalMaterial=PhysicalMaterial'VH_VN_ARVN_M113_APC.Phys.M113_PhysMat'
	
	EngineStartOffsetSecs=0.2
	EngineStopOffsetSecs=0.1
	
	SpeedoMinDegree=5461
	SpeedoMaxDegree=56000
	SpeedoMaxSpeed=1365 //100 km/h
	
	RPM3DGaugeMaxAngle=-43690
	EngineIdleRPM=100
	EngineNormalRPM=2000
	EngineMaxRPM=4000
	Speedo3DGaugeMaxAngle=-33607
	EngineOil3DGaugeMinAngle=-16384
	EngineOil3DGaugeMaxAngle=-43690
	EngineOil3DGaugeDamageAngle=-8192
	GearboxOil3DGaugeMinAngle=-15000
	GearboxOil3DGaugeNormalAngle=-22500
	GearboxOil3DGaugeMaxAngle=-43690
	GearboxOil3DGaugeDamageAngle=-5000
	EngineTemp3DGaugeNormalAngle=-16384
	EngineTemp3DGaugeEngineDamagedAngle=-22500
	EngineTemp3DGaugeFireDamagedAngle=-28250
	EngineTemp3DGaugeFireDestroyedAngle=-32580
	
	TreadSpeedParameterName=Tank_Tread_Speed
	TrackSoundParamScale=0.000033
	TreadSpeedScale=2.5
	
	ExplosionDamageType=class'RODmgType_VehicleExplosion'
	ExplosionDamage=100.0
	ExplosionRadius=300.0
	ExplosionMomentum=60000
	ExplosionInAirAngVel=1.5
	InnerExplosionShakeRadius=400.0
	OuterExplosionShakeRadius=1000.0
	ExplosionLightClass=none
	MaxExplosionLightDistance=4000.0
	TimeTilSecondaryVehicleExplosion=0//2.0f
	SecondaryExplosion=none
	bHasTurretExplosion=false
	
	ExplosionSound=AkEvent'GOM3_AUD_SFX_VH.Play_ALL_Explode_Light'
	
	// Begin Object Class=AkComponent Name=BrakeLeftSnd
		// bStopWhenOwnerDestroyed=true
	// End Object
	// Begin Object Class=AkComponent Name=BrakeRightSnd
		// bStopWhenOwnerDestroyed=true
	// End Object
	// BrakeLeftSound=BrakeLeftSnd
	// BrakeRightSound=BrakeRightSnd
	// BrakeLeftSoundEvent=AkEvent'GOM3_AUD_SFX_VH.Play_ALL_Treads_Brake'
	// BrakeRightSoundEvent=AkEvent'GOM3_AUD_SFX_VH.Play_ALL_Treads_Brake'
	
	// TrackTakeDamageSound=
	// TrackDamagedSound=
	// TrackDestroyedSound=
	
	ShiftLeverSound=AkEvent'GOM3_AUD_SFX_VH.Play_ALL_Lever_GearShift'
	
	RanOverDamageType=GOMDmgType_RoadRage
}
