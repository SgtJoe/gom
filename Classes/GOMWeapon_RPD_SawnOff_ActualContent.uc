//=============================================================================
// GOMWeapon_RPD_SawnOff_ActualContent.uc
//=============================================================================
// MACV-SOG Sawn-Off RPD LMG (Content class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_RPD_SawnOff_ActualContent extends GOMWeapon_RPD_SawnOff;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WP_VN_VC_RPD.Animation.WP_RPDbipodhands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'GOM3_WP_US_MACV_RPD_SAWNOFF.Mesh.US_MACV_RPD_SawnOff'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_RPD.Phy.VC_RPD_Physics'
		AnimSets(0)=AnimSet'WP_VN_VC_RPD.Animation.WP_RPDbipodhands'
		AnimTreeTemplate=AnimTree'WP_VN_VC_RPD.Animation.VC_RPDBipod_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'GOM3_WP_US_MACV_RPD_SAWNOFF.Mesh.RPD_SawnOff_3rd_Master'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.RPD_3rd_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.RPD_Bipod_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	Begin Object Class=ROAmmoBeltMesh Name=AmmoBelt0
		SkeletalMesh=SkeletalMesh'WP_VN_VC_RPD.Mesh.VC_RPD_BELT'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_RPD.Phy.VC_RPD_BELT_Physics'
		AnimSets.Add(AnimSet'WP_VN_VC_RPD.Animation.WP_RPD_BELT')
		DepthPriorityGroup=SDPG_Foreground
		bOnlyOwnerSee=true
		MaxAmmoShown=13
	End Object
	AmmoBeltMesh=AmmoBelt0
	
	AmmoBeltSocket=AmmoBeltSocket
	
	AttachmentClass=class'GOMWeapon_RPD_SawnOff_Attach'
}
