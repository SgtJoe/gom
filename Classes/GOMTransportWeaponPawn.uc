//=============================================================================
// GOMTransportWeaponPawn.uc
//=============================================================================
// Extended to mirror new functionality in GOMVehicleTransport.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMTransportWeaponPawn extends ROTransportWeaponPawn
	notplaceable;

simulated exec function SwitchFireMode() {}

simulated state PositionTransitioning
{
	simulated event EndState(Name NextStateName)
	{
		local GOMVehicleTransport MyTransport;
		
		Super.EndState(NextStateName);
		
		MyTransport = GOMVehicleTransport(MyVehicle);
		
		if (MyTransport != none && MyVehicle.Seats[MySeatIndex].Gun != none && MyVehicle.SeatPositionIndex(MySeatIndex,, true) == MyVehicle.Seats[MySeatIndex].FiringPositionIndex)
		{
			MyTransport.EnableMGSkelControls(MySeatIndex);
		}
	}
}
