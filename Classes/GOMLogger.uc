//=============================================================================
// GOMLogger.uc
//=============================================================================
// Writes to the game's log file with a customizable header.
// Individual headers can be enabled/disabled in the mutator config file.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMLogger extends Object
	config (Mutator_GOM);

struct LogStruct
{
	var string LogType;
	var bool Enabled;
};

var config array<LogStruct> LogOptions;

static function GOMLog(string Message, optional name Type)
{
	local int i, index;
	local bool LogExists;
	local LogStruct inData;
	
	Type = (Type == 'None') ? 'GOM' : name('GOM-'$Type);
	LogExists = false;
	index = 0;
	
	for (i = 0; i < default.LogOptions.length; i++)
	{
		if (string(Type) ~= default.LogOptions[i].LogType)
		{
			LogExists = true;
			index = i;
			break;
		}
	}
	
	if (LogExists && default.LogOptions[index].Enabled)
	{
		`log(Message,,Type);
	}
	else if (!LogExists)
	{
		inData.LogType = string(Type);
		inData.Enabled = true;
		
		default.LogOptions[default.LogOptions.length] = inData;
		
		StaticSaveConfig();
		
		`log("Creating new log" @ Type,,'GOM');
		`log(Message,,Type);
	}
}

static function GOMDebugLog(string Message, string FuncTrace, optional name Type)
{
`ifndef(RELEASE)
	local GOMPlayerController PC;
	
	Type = (Type == 'None') ? 'DEBUG' : Type;
	
	foreach class'Engine'.static.GetCurrentWorldInfo().AllControllers(class'GOMPlayerController', PC)
	{
		PC.ClientMessage("("$Type$")" @ FuncTrace @ Message);
	}
`endif
}