//=============================================================================
// GOMVehicleFactory.uc
//=============================================================================
// Base class for Vehicle Factories.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleFactory extends ROTransportVehicleFactory
	abstract;

var bool TankFactory;

defaultproperties
{
	Begin Object Name=CollisionCylinder
		CollisionHeight=+60.0
		CollisionRadius=+260.0
		Translation=(X=0.0,Y=0.0,Z=0.0)
	End Object
	
	EnemyVehicleClass=none
	DrawScale=1.0
	
	TankFactory=false
}
