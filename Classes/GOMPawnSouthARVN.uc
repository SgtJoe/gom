//=============================================================================
// GOMPawnSouthARVN.uc
//=============================================================================
// Southern Pawn (South Vietnam).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawnSouthARVN extends GOMPawnSouth;

DefaultProperties
{
	TunicMesh=SkeletalMesh'GOM3_CHR_US_ARVN.Mesh.Tunic.GOM_CHR_ARVN_Tunic_Long'
	BodyMICTemplate=MaterialInstanceConstant'GOM3_CHR_US_ARVN.MIC.ARVN_Tunic_Long'
	
	HeadAndArmsMesh=SkeletalMesh'CHR_VN_ARVN_Heads.Mesh.ARVN_Head7_Mesh'
	HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_ARVN_Heads.Materials.M_ARVN_Head_07_Long_INST'
	
	FieldgearMesh=SkeletalMesh'GOM3_CHR_US_ARVN.Mesh.Gear.GOM_CHR_ARVN_Gear_Rifleman'
}
