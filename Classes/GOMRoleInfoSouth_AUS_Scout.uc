//=============================================================================
// GOMRoleInfoSouth_AUS_Scout.uc
//=============================================================================
// Australian Army Scout Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_AUS_Scout extends GOMRoleInfoSouth_AUS;

DefaultProperties
{
	RoleType=RORIT_Scout
	ClassTier=2
	ClassIndex=`ROCI_SCOUT
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_F1_SMG',class'ROWeap_Owen_SMG'),
		OtherItems=(class'ROWeap_M8_Smoke'),
		SquadLeaderItems=(class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_Owen_SMG'),
		OtherItems=(class'ROWeap_M8_Smoke'),
		SquadLeaderItems=(class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle',class'ROWeap_F1_SMG',class'ROWeap_Owen_SMG'),
		OtherItems=(class'ROWeap_M8_Smoke'),
		SquadLeaderItems=(class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_scout'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_scout'
}
