//=============================================================================
// GOMVoicePack_US_1.uc
//=============================================================================
// US Voice Pack.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVoicePack_US_1 extends ROVoicePackUSATeam01;

defaultproperties
{
	VoiceComs[`VOICECOM_TakeCover]=(Sound="AKEvent'WW_VOX_US_Voice1.Play_US_Soldier_1_TakingFireGeneric'")
	VoiceComs[`VOICECOM_AIConfirm]=(Sound="AKEvent'WW_VOX_US_Voice1.Play_US_Soldier_1_Confirm'")
	VoiceComs[`VOICECOM_HeloIdleSituation]=(Sound="AKEvent'WW_VOX_US_Voice1.Play_US_Pilot_1_HeloIdleGround'")
}
