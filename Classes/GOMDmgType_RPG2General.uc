//=============================================================================
// GOMDmgType_RPG2General.uc
//=============================================================================
// Damage type for the RPG-2 rocket.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMDmgType_RPG2General extends RODmgType_RPG7RocketGeneral
	abstract;

DefaultProperties
{
	WeaponClass=GOMWeapon_RPG2_ActualContent
	WeaponShortName="RPG2"
}
