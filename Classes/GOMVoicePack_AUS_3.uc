//=============================================================================
// GOMVoicePack_AUS_3.uc
//=============================================================================
// AUS Voice Pack.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVoicePack_AUS_3 extends ROVoicePackAusTeam03;

defaultproperties
{
	VoiceComs[`VOICECOM_TakeCover]=(Sound="AKEvent'WW_VOX_ANZAC_Voice3.Play_ANZAC_Soldier_3_TakingFireGeneric'")
	VoiceComs[`VOICECOM_AIConfirm]=(Sound="AKEvent'WW_VOX_ANZAC_Voice3.Play_ANZAC_Soldier_3_Confirm'")
	VoiceComs[`VOICECOM_HeloIdleSituation]=(Sound="AKEvent'WW_VOX_ANZAC_Voice3.Play_ANZAC_Pilot_3_HeloIdleGround'")
}
