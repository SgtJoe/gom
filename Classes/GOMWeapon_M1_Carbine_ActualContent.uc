//=============================================================================
// GOMWeapon_M1_Carbine_ActualContent.uc
//=============================================================================
// Viet Cong M1 Carbine (Content class).
// Modified viewmodel mesh.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M1_Carbine_ActualContent extends GOMWeapon_M1_Carbine;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WP_VN_ARVN_M1Carbine_Rifle.Animation.WP_M2CarbineHands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_VC_M1.Mesh.M1_Carbine'
		PhysicsAsset=PhysicsAsset'WP_VN_ARVN_M1Carbine_Rifle.Phys.ARVN_M1Carbine_Physics'
		AnimSets(0)=AnimSet'WP_VN_ARVN_M1Carbine_Rifle.Animation.WP_M2CarbineHands'
		AnimTreeTemplate=AnimTree'WP_VN_ARVN_M1Carbine_Rifle.Animation.WP_M1CarbineHands_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WP_VN_ARVN_3rd_Master.Mesh.M1Carbine_3rd_Master'
		PhysicsAsset=PhysicsAsset'WP_VN_ARVN_3rd_Master.Phys.M1Carbine_3rd_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_ARVN_3rd_Master.AnimTree.M1Carbine_Rifle_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'ROWeapAttach_M1_Carbine'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue= AkEvent'WW_WEP_M1_M2.Play_WEP_M1_M2_Single_3P', FirstPersonCue=AkEvent'WW_WEP_M1_M2.Play_WEP_M1_M2_Fire_Single')
	bLoopingFireSnd(DEFAULT_FIREMODE)=false
	bLoopHighROFSounds(DEFAULT_FIREMODE)=false
}
