//=============================================================================
// GOMWeapon_M38_Carbine_Attach.uc
//=============================================================================
// Viet Cong M38 Carbine bolt-action rifle (Attachment class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M38_Carbine_Attach extends ROWeaponAttachment;

defaultproperties
{
	CarrySocketName=WeaponSling
	ThirdPersonHandsAnim=MN9130_Handpose
	IKProfileName=MN9130
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_VC_M44.Mesh.M44_Carbine_3rd'
		AnimSets(0)=AnimSet'WP_VN_3rd_Master.Anim.MN9130_3rd_anim'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.MN9130_3rd_Tree'
		Animations=NONE
		PhysicsAsset=PhysicsAsset'GOM3_WP_VN_VC_M44.Phy.M44_3rd_Bounds_Physics'
		CullDistance=5000
	End Object
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_3rdP_Rifles_round'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	WeaponClass=class'GOMWeapon_M38_Carbine'
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_MN9130'
	bNoShellEjectOnFire=true
	
	CHR_AnimSet=AnimSet'CHR_VN_Playeranim_Master.Weapons.CHR_MN9130'
	
	FireAnim=Shoot
	FireLastAnim=Shoot_Last
	IdleAnim=Idle
	IdleEmptyAnim=Idle_Empty
}
