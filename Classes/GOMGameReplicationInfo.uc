//=============================================================================
// GOMGameReplicationInfo.uc
//=============================================================================
// Game Replication Info, hijacked in an attempt 
// to properly replicate mutator data.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMGameReplicationInfo extends ROGameReplicationInfo;

var ENorthForce RNorthForce;
var ESouthForce RSouthForce;
var EMiniFactions RMiniFaction;
var int RMapIndex;

replication
{
	if (Role == ROLE_Authority)
		RNorthForce, RSouthForce, RMiniFaction, RMapIndex;
}

simulated event ReplicatedEvent(name VarName)
{
	local ROPlayerController ROPC;
	
	if (VarName != 'CampaignFactionOverrides')
	{
		super.ReplicatedEvent(VarName);
	}
	else if (VarName == 'CampaignFactionOverrides' && WorldInfo.NetMode == NM_Client)
	{
		if (bIsCampaignGame && ROMapInfo(WorldInfo.GetMapInfo()) != none )
		{
			`gom("WARNING - RECIEVING CAMPAIGNFACTIONOVERRIDES",'Campaign');
			
			if (CampaignFactionOverrides[0] < NFOR_Max)
			{
				ROMapInfo(WorldInfo.GetMapInfo()).NorthernForce = ENorthernForces(CampaignFactionOverrides[0]);
			}
			
			if (CampaignFactionOverrides[1] < SFOR_Max)
			{
				ROMapInfo(WorldInfo.GetMapInfo()).SouthernForce = ESouthernForces(CampaignFactionOverrides[1]);
			}
			
			// Originally this function called ROMapInfo::InitRolesForGametype
			// but we don't want that since we're already replacing the roles ourselves
			
			foreach LocalPlayerControllers(class'ROPlayerController', ROPC)
			{
				ROPC.MyROHUD.SetTeamColoursForWidgets(ROPC.GetTeamNum());
			}
		}
	}
}