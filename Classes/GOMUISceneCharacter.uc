//=============================================================================
// GOMUISceneCharacter.uc
//=============================================================================
// In-game menu for customization.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMUISceneCharacter extends ROUISceneCharacter;

function InitPlayerConfig()
{
	local LocalPlayer Player;
	local ROMapInfo ROMI;
	local bool bMainMenu;
	
	Player = GetPlayerOwner();
	if ( Player != none && Player.Actor != none )
	{
		ROPC = ROPlayerController(Player.Actor);
		if(ROPC != none)
		{
			ROMI = ROMapInfo(ROPC.WorldInfo.GetMapInfo());
			
			bMainMenu = ROPC.WorldInfo.GRI.GameClass.static.GetGameType() == ROGT_Default;
			
			if( bMainMenu )
			{
				TeamIndexActual = ROPC.LastDisplayedTeam;
				ArmyIndexActual = ROPC.LastDisplayedArmy;
			}
			else
			{
				TeamIndexActual = ROPC.GetTeamNum();
				
				if( ROMI != none && TeamIndexActual == `ALLIES_TEAM_INDEX )
					ArmyIndexActual = `getSF;
				else if( ROMI != none && TeamIndexActual == `AXIS_TEAM_INDEX )
					ArmyIndexActual = `getNF;
				else
					ArmyIndexActual = 0;
			}
			
			ROPRI = ROPlayerReplicationInfo(ROPC.PlayerReplicationInfo);
		}
		
		if(ROPRI != none && ROPRI.RoleInfo != none)
		{
			ClassIndexActual = ROPRI.RoleInfo.ClassIndex;
			bPilotActual = ROPRI.RoleInfo.bIsPilot;
			bCombatPilotActual = bPilotActual && !ROPRI.RoleInfo.bIsTransportPilot;
		}
		else
		{
			if( bMainMenu )
			{
				bPilotActual = ROPC.bLastDisplayedPilot;
				ClassIndexActual = ROPC.LastDisplayedClass;
			}
			else
			{
				bPilotActual = false;
				ClassIndexActual = -1;
			}
		}
		
		ROPC.StatsWrite.UpdateHonorLevel();
		HonorLevel = byte(ROPC.StatsWrite.HonorLevel);
	}
}

function PopulateArmyList()
{
	local int i;
	local ROMapInfo ROMI;
	
	if( ROPC != none )
		ROMI = ROMapInfo(ROPC.WorldInfo.GetMapInfo());
	
	if( ROMI != none )
	{
		ROCharCustStringsDataStore.Empty('ROCharCustArmyType');
		
		for(i = 0; i < class'GOM3'.default.NorthArmyShortNames.length; i++)
		{
			ROCharCustStringsDataStore.AddStr('ROCharCustArmyType', class'GOM3'.default.NorthArmyShortNames[i]);
		}
		
		FirstSouthIndex = class'GOM3'.default.NorthArmyShortNames.length;
		
		for(i = 0; i < class'GOM3'.default.SouthArmyShortNames.length; i++)
		{
			ROCharCustStringsDataStore.AddStr('ROCharCustArmyType', class'GOM3'.default.SouthArmyShortNames[i]);
		}
		
		ArmyComboBox.ComboList.RefreshSubscriberValue();
		ArmyComboBox.ComboList.SetRowCount(class'GOM3'.default.NorthArmyShortNames.length + class'GOM3'.default.SouthArmyShortNames.length);
		ArmyComboBox.SetSelection(TeamIndexActual * FirstSouthIndex + ArmyIndexActual);
	}
}

function OnArmyComboBoxChanged(UIObject Sender, int PlayerIndex)
{
	ActiveComboBox = UIComboBox(Sender);
	
	if (bConfigChanged)
	{
		bUnsavedTriggeredByArmy = true;
		UnsavedDialog.SetVisibility(true);
	}
	else if (bPostFirstRender)
	{
		ArmyComboUpdated();
		PopulateRoleList();
	}
}

function SetPawnHandler()
{
	local int NorthArmyCount;
	
	PawnHandlerClass = class'GOMPawnHandler';
	
	if (ROCCM != none)
	{
		TunicSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		TunicMatSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		ShirtSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		HeadSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		HairColourSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		HeadgearSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		HeadgearMatSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		FaceItemSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		FacialHairSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		TattooSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		
		NorthArmyCount = class'GOM3'.default.NorthArmyShortNames.length;
		
		TunicSelectionWidget.TeamSplitValue = NorthArmyCount;
		TunicMatSelectionWidget.TeamSplitValue = NorthArmyCount;
		ShirtSelectionWidget.TeamSplitValue = NorthArmyCount;
		HeadSelectionWidget.TeamSplitValue = NorthArmyCount;
		HairColourSelectionWidget.TeamSplitValue = NorthArmyCount;
		HeadgearSelectionWidget.TeamSplitValue = NorthArmyCount;
		HeadgearMatSelectionWidget.TeamSplitValue = NorthArmyCount;
		FaceItemSelectionWidget.TeamSplitValue = NorthArmyCount;
		FacialHairSelectionWidget.TeamSplitValue = NorthArmyCount;
		TattooSelectionWidget.TeamSplitValue = NorthArmyCount;
	}
}

event bool CloseScene(optional UIScene SceneToClose=self, optional bool bCloseChildScenes=true, optional bool bForceCloseImmediately)
{
	local CharacterConfig LastPreviewConfig;
	local bool bMainMenu;
	
	if (ROPC != none)
	{
		bMainMenu = ROPC.WorldInfo.GRI.GameClass.static.GetGameType() == ROGT_Default;
		
		if( bMainMenu && ROPC.ROCCC != none )
		{
			ROPC.ROCCC.SetTargetLocation(ROPC.Location);
			ROPC.ROCCC.SetTargetRotation(ROPC.Rotation);
			ROPC.ROCCC.bDisableOnComplete = true;
		}
		
		ROPC.CloseCharacterMenu();
	}
	
	GOMPlayerReplicationInfo(ROPC.PlayerReplicationInfo).ClientSetCustomCharConfig();
	
	if (bMainMenu)
	{
		ROPC.LastDisplayedTeam = TeamIndex;
		ROPC.LastDisplayedArmy = ArmyIndex;
		ROPC.LastDisplayedClass = ClassIndex;
		ROPC.bLastDisplayedPilot = bPilot;
		ROPC.SaveConfig();
		
		LastPreviewConfig = PreviewConfig;
		
		GetCurrentConfig();
		
		if( CurrentCharConfig.TunicMesh == 255 )
			PreviewConfig.TunicMesh = LastPreviewConfig.TunicMesh;
		if( CurrentCharConfig.TunicMaterial == 255 )
			PreviewConfig.TunicMaterial = LastPreviewConfig.TunicMaterial;
		if( CurrentCharConfig.ShirtTexture == 255 )
			PreviewConfig.ShirtTexture = LastPreviewConfig.ShirtTexture;
		if( CurrentCharConfig.HeadMesh == 255 )
			PreviewConfig.HeadMesh = LastPreviewConfig.HeadMesh;
		if( CurrentCharConfig.HairMaterial == 255 )
			PreviewConfig.HairMaterial = LastPreviewConfig.HairMaterial;
		if( CurrentCharConfig.HeadgearMesh == 255 )
			PreviewConfig.HeadgearMesh = LastPreviewConfig.HeadgearMesh;
		if( CurrentCharConfig.FaceItemMesh == 255 )
			PreviewConfig.FaceItemMesh = LastPreviewConfig.FaceItemMesh;
		if( CurrentCharConfig.TattooTex == 255 )
			PreviewConfig.TattooTex = LastPreviewConfig.TattooTex;
		
		UpdatePreviewMesh(bMainMenu);
		ROCCM.PlayTransitionAnim();
		
		if( ROUISceneMainMenu(ROGameViewportClient(GetPlayerOwner().ViewportClient).MainMenuScene) != none && ROPC.bUseMenuMovieBG )
			ROUISceneMainMenu(ROGameViewportClient(GetPlayerOwner().ViewportClient).MainMenuScene).BackgroundMovie.Fade(0.0, 1.0, 0.5);
	}
	
	ROPC = none;
	ROCCM = none;
	ROPRI = none;
	
	return super(ROUIScene).CloseScene(SceneToClose, bCloseChildScenes, bForceCloseImmediately);
}
