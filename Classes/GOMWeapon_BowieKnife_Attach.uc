//=============================================================================
// GOMWeapon_BowieKnife_Attach.uc
//=============================================================================
// US Army Bowie Combat Knife (Attachment class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_BowieKnife_Attach extends ROMeleeWeaponAttachment;

defaultproperties
{
	ThirdPersonHandsAnim=M1939_Grenade_Handpose
	IKProfileName=F1
	
	CarrySocketName=none
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'GOM3_WP_US_BOWIE.Mesh.BowieKnife_3rd'
		PhysicsAsset=PhysicsAsset'GOM3_WP_GEN.Phy.CombatKnife_3rd_Physics'
		CullDistance=5000
	End Object
	
	WeaponClass=class'GOMWeapon_BowieKnife'
	
	CHR_AnimSet=AnimSet'CHR_VN_Playeranim_Master.Weapons.CHR_Katana'
	
	HolsterWeaponAnim=Katana_Putaway
	EquipWeaponAnim=Katana_pullout
	EquipWeaponIronAnim=Katana_pullout
	
	MeleeBash1Anim=Katana_Attack_2
	MeleeBash1Anim_CH=CH_Katana_Attack_2
	MeleeStabAnim=Katana_Attack_2
	MeleeStabAnim_CH=CH_Katana_Attack_2
	MeleeStabAnim_Prone=Prone_Katana_Attack_Stab
	
	MeleeBashChargeAnim=Katana_Attack_Hard
	MeleeBashChargeAnim_CH=CH_Katana_Attack_Hard
	MeleeStabChargeAnim=Katana_Attack_Hard
	MeleeStabChargeAnim_CH=CH_Katana_Attack_Hard
}
