//=============================================================================
// GOMWeapon_PPSH_ActualContent.uc
//=============================================================================
// North Vietnamese Army PPSH SMG (Content class).
// Modified to only carry drum ammo.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_PPSH_ActualContent extends GOMWeapon_PPSH;

defaultproperties
{
	maxRecoilPitch=170//150
	minRecoilPitch=170//150//115
	maxRecoilYaw=80//95
	minRecoilYaw=-80//-35
	
	RecoilModWhenEmpty = 1.2f
	
	AttachmentClass=class'ROWeapAttach_PPSH41_SMG_Drum'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WP_VN_VC_ppsh.Mesh.Sov_ppsh'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_ppsh.Phys.Sov_ppsh_Physics'
		AnimSets(0)=AnimSet'WP_VN_VC_ppsh.animation.WP_PpshHands'
		AnimTreeTemplate=AnimTree'WP_VN_VC_ppsh.animation.Sov_PPSH41_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	ArmsAnimSet=AnimSet'WP_VN_VC_ppsh.animation.WP_PpshHands'
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh.PPSH_3rd_Master'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.PPSH_3rd_Master_Physics'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AmmoClass=class'ROAmmo_762x25_PPSHDrum'
	MaxAmmoCount=65 //71
	InitialNumPrimaryMags=2
	
	ZoomInTime=0.5//0.35//0.45
	ZoomOutTime=0.35//0.25//0.4
	
	EquipTime=+0.8//1.2
	PutDownTime=+0.66//1.00
	
	LagLimit=3.0
	
	SwayScale=1.35//1.25
	
	PlayerIronSightFOV=60.0
}