//=============================================================================
// GOMModWeaponInfo_M7.uc
//=============================================================================
// UI information for the M7 Rifle Grenade.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfo_M7 extends ROModWeaponInfo;

DefaultProperties
{
	ModWeaponClass=GOMWeapon_M7RifleGrenade_ActualContent
	
	InventoryIconTexturePath="VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_M61_Grenade"
	
	AmmoIconTexturePath="VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M61_Grenade"
	
	KillIconTexturePath="VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_M61Grenade"
	bHasSquareKillIcon=true
}
