//=============================================================================
// GOMRoleInfoNorth_NVA_Grunt.uc
//=============================================================================
// North Vietnamese Army Grunt Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_NVA_Grunt extends GOMRoleInfoNorth_NVA;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=1
	ClassIndex=`ROCI_RIFLEMAN
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_AK47_AssaultRifle'),
		OtherItems=(class'ROWeap_Type67_Grenade')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_AK47_AssaultRifle_NLF'),
		OtherItems=(class'ROWeap_Type67_Grenade')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_AK47_AssaultRifle'),
		OtherItems=(class'ROWeap_Type67_Grenade')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_AK47_AssaultRifle'),
		OtherItems=(class'ROWeap_Type67_Grenade')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_guerilla'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_guerilla'
}
