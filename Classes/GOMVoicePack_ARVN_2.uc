//=============================================================================
// GOMVoicePack_ARVN_2.uc
//=============================================================================
// ARVN Voice Pack.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVoicePack_ARVN_2 extends ROVoicePackSVietTeam02;

defaultproperties
{
	VoiceComs[`VOICECOM_TakeCover]=(Sound="AKEvent'WW_VOX_ARVN_Voice2.Play_ARVN_Soldier_2_TakingFireGeneric'")
	VoiceComs[`VOICECOM_AIConfirm]=(Sound="AKEvent'WW_VOX_ARVN_Voice2.Play_ARVN_Soldier_2_Confirm'")
	VoiceComs[`VOICECOM_HeloIdleSituation]=(Sound="AKEvent'WW_VOX_ARVN_Voice2.Play_ARVN_Pilot_2_HeloIdleGround'")
}
