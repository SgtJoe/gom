//=============================================================================
// GOMVehicleWeaponMG_M113_M60_R.uc
//=============================================================================
// M113 ACAV Right Side M60.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleWeaponMG_M113_M60_R extends GOMVehicleWeaponMG
	abstract;

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMVehicleWeaponMG_M113_M60_R_ActualContent"
	
	VehicleClass=class'GOMVehicle_M113_ACAV'
	
	SeatIndex=`M113_MGR
	
	FireTriggerTags=(MG_R)
	
	FiringStatesArray(0)=WeaponFiring
	WeaponFireTypes(0)=EWFT_Projectile
	WeaponProjectiles(0)=class'M60Bullet'
	FireInterval(0)=+0.1
	FireCameraAnim(0)=CameraAnim'1stperson_Cameras.Anim.Camera_MG34_Shoot'
	Spread(0)=0.004
	
	AmmoDisplayNames(0)="M60"
	
	MaxAmmoCount=600
	
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_USA_M60'
	ShellEjectSocket=MG_R_ShellEject
	
	SuppressionPower=20.0
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M60.Play_WEP_M60_Fire_Loop_3P',FirstPersonCue=AkEvent'WW_WEP_M60.Play_WEP_M60_Fire_Stereo_Loop')
	WeaponFireLoopEndSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M60.Play_WEP_M60_Tail_3P',FirstPersonCue=AkEvent'WW_WEP_M60.Play_WEP_M60_Stereo_Tail')
}