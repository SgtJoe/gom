//=============================================================================
// GOMWeapon_M2A17_Flamethrower.uc
//=============================================================================
// South Korean M2A1-7 Flamethrower.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M2A17_Flamethrower extends ROWeap_M9_Flamethrower
	abstract;

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMWeapon_M2A17_Flamethrower_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'VN_UI_Textures.WeaponTex.US_Weap_M9A1_Flamethrower'
	
	InvIndex=`WI_M2A17
	bIsModWeapon=true
	
	AmmoClass=class'ROAmmo_182L_M2Tank'
	MaxAmmoCount=105
}
