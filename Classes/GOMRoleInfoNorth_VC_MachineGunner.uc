//=============================================================================
// GOMRoleInfoNorth_VC_MachineGunner.uc
//=============================================================================
// Viet Cong Machine Gunner Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_VC_MachineGunner extends GOMRoleInfoNorth_VC;

DefaultProperties
{
	RoleType=RORIT_MachineGunner
	ClassTier=2
	ClassIndex=`ROCI_MACHINEGUNNER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'GOMWeapon_DP28',class'ROWeap_M1918_BAR'),
		OtherItems=(class'ROWeap_PunjiTrap')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'GOMWeapon_DP28',class'ROWeap_M1918_BAR'),
		OtherItems=(class'ROWeap_PunjiTrap')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'GOMWeapon_DP28',class'ROWeap_M1918_BAR'),
		OtherItems=(class'ROWeap_PunjiTrap')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'GOMWeapon_RPD',class'ROWeap_RP46_LMG')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_mg'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_mg'
}
