//=============================================================================
// GOMRoleInfoNorth_NVA_Commander.uc
//=============================================================================
// North Vietnamese Army Commander Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_NVA_Commander extends GOMRoleInfoNorth_NVA;

DefaultProperties
{
	RoleType=RORIT_Commander
	ClassTier=4
	ClassIndex=`ROCI_COMMANDER
	bIsTeamLeader=true
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_K50M_SMG',class'ROWeap_SKS_Rifle'),
		SecondaryWeapons=(class'ROWeap_PM_Pistol'),
		OtherItems=(class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle',class'GOMWeapon_PPS'),
		OtherItems=(class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle',class'ROWeap_K50M_SMG'),
		SecondaryWeapons=(class'ROWeap_PM_Pistol'),
		OtherItems=(class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_AK47_AssaultRifle',class'ROWeap_K50M_SMG'),
		SecondaryWeapons=(class'ROWeap_PM_Pistol'),
		OtherItems=(class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_commander'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_commander'
}
