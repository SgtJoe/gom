//=============================================================================
// GOMPawnHandler.uc
//=============================================================================
// Handles Pawn appearance.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMPawnHandler extends ROPawnHandler
	config(Mutator_GOM_Client);

var config array<CharacterConfig> CFGA_NVA; // North Vietnamese Army
var config array<CharacterConfig> CFGA_NVAU; // North Vietnamese Army (Urban Tan)
var config array<CharacterConfig> CFGA_VC; // Viet Cong

var config array<CharacterConfig> CFGA_US; // United States Army
var config array<CharacterConfig> CFGA_USMC; // United States Marine Corps
var config array<CharacterConfig> CFGA_MACV; // Military Assistance Command, Vietnam – Studies and Observations Group
var config array<CharacterConfig> CFGA_AUS; // Australian Army
var config array<CharacterConfig> CFGA_ARVN; // Army of the Republic of Vietnam
var config array<CharacterConfig> CFGA_RANGERS; // Army of the Republic of Vietnam Rangers
var config array<CharacterConfig> CFGA_ROK; // Republic of Korea Infantry Division
var config array<CharacterConfig> CFGA_ROKA; // Republic of Korea Armoured Division
var config array<CharacterConfig> CFGA_ROKMC; // Republic of Korea Marine Corps

// North Vietnamese Army
var array<TunicInfo>		NVA_Tunics;
var array<TunicInfo>		NVA_Tunics_Commander;
var TunicInfo				NVA_SecondaryBodyMICTemplates;
var array<ShirtInfo>		NVA_Shirts;
var array<HeadgearInfo>		NVA_Headgear;
var array<HeadgearInfo>		NVA_Headgear_Commander;
var array<SkeletalMesh>		NVA_FieldgearByRole;
var	array<PlayerHeadInfo>	NVA_Heads;

// North Vietnamese Army (Urban Tan)
var array<TunicInfo>	NVAU_Tunics;
var array<TunicInfo>	NVAU_Tunics_Commander;
var array<HeadgearInfo>	NVAU_Headgear;
var array<HeadgearInfo>	NVAU_Headgear_Commander;
var array<SkeletalMesh>	NVAU_FieldgearByRole;
var TunicSVInfo			NVAU_TunicSV;
var TunicSVInfo			NVAU_TunicSVRadioman;

// Viet Cong
var array<TunicInfo>		VC_Tunics;
var int						VC_TunicMatsFoliageCutoff;
var TunicInfo				VC_SecondaryBodyMICTemplates;
var TunicInfo				VC_SecondaryBodyMICTemplates2;
var array<HeadgearInfo>		VC_Headgear;
var array<SkeletalMesh>		VC_FieldgearByRole;
var array<ShirtInfo>		VC_Shirts;
var TunicSVInfo				VC_TunicSV;
var TunicSVInfo				VC_TunicSVRadioman;
var array<PlayerHeadInfo>	VC_Heads;

// United States Army
var array<TunicInfo>		US_Tunics;
var array<HeadgearInfo>		US_Headgear;
var array<HeadgearInfo>		US_Headgear_Shared;
var array<FieldgearMeshes>	US_FieldgearByRole;
var	FieldgearMeshes			US_FlamerFieldgear;
var array<ShirtInfo>		US_Shirts;
var	array<PlayerHeadInfo>	US_Heads;
var TunicSVInfo				US_TunicSV;
var TunicSVInfo				US_TunicSVRadioman;
var TunicSVInfo				US_TunicSVFlamer;

// United States Marine Corps
var array<TunicInfo>		USMC_Tunics;
var TunicInfo				USMC_SecondaryBodyMICTemplates;
var array<HeadgearInfo>		USMC_Headgear;
var array<HeadgearMICInfo>	USMC_HeadgearMICs;
var array<FaceItemInfo>		USMC_FaceItems;

// Military Assistance Command, Vietnam – Studies and Observations Group
var array<TunicInfo>		MACV_Tunics;
var array<HeadgearInfo>		MACV_Headgear;
var array<FieldgearMeshes>	MACV_FieldgearByRole;

// Australian Army
var array<TunicInfo>		AUS_Tunics;
var array<SkeletalMesh>		AUS_FieldgearByRole;
var	SkeletalMesh			AUS_FlamerFieldgear;
var	array<PlayerHeadInfo>	AUS_Heads;

// Army of the Republic of Vietnam
var array<TunicInfo>		ARVN_Tunics;
var TunicInfo				ARVN_SecondaryBodyMICTemplates;
var array<SkeletalMesh>		ARVN_AltFPArmsMeshes;
var array<FieldgearMeshes>	ARVN_FieldgearByRole;
var FieldgearMeshes			ARVN_AltFieldgear;
var array<HeadgearInfo>		ARVN_Headgear;
var	array<PlayerHeadInfo>	ARVN_Heads;

// Army of the Republic of Vietnam Rangers
var array<TunicInfo>		RANGERS_Tunics;
var TunicInfo				RANGERS_SecondaryBodyMICTemplates;
var array<FieldgearMeshes>	RANGERS_FieldgearByRole;
var array<HeadgearInfo>		RANGERS_Headgear;
var array<FaceItemInfo>		RANGERS_Aviators;

// Republic of Korea Army
var array<TunicInfo>		ROK_Tunics;
var array<TunicInfo>		ROKA_Tunics;
var array<SkeletalMesh>		ROK_AltFPArmsMeshes;
var array<SkeletalMesh>		ROKA_AltFPArmsMeshes;
var array<HeadgearInfo>		ROK_Headgear;
var array<FieldgearMeshes>	ROK_FieldgearByRole;
var FieldgearMeshes			ROK_AltFieldgear;
var	FieldgearMeshes			ROK_FlamerFieldgear;
var array<ShirtInfo>		ROK_Shirts;
var	array<PlayerHeadInfo>	ROK_Heads;

// Republic of Korea Marines
var array<TunicInfo>	ROKMC_Tunics;
var array<SkeletalMesh>	ROKMC_AltFPArmsMeshes;
var array<HeadgearInfo>	ROKMC_Headgear;

// Multi-faction arrays
var array<HeadgearMICInfo> North_HeadgearMICs;
var array<HeadgearMICInfo> South_HeadgearMICs;
var array<FaceItemInfo> FaceItems_Viet;
var array<FaceItemInfo> FaceItems_US;
var array<FaceItemInfo> FaceItems_Pilot;
var array<FaceItemInfo> FaceItems_None;
var array<SkeletalMesh> HairMeshes;
var array<TattooInfo> All_BodyOverlays;

// United States Military Police
var array<TunicInfo>	MP_Tunics;
var array<HeadgearInfo>	MP_Headgear;
var array<FaceItemInfo>	MP_FaceItems;
var SkeletalMesh		MP_Fieldgear;

static function SkeletalMesh GetTunicMeshSV(int Team, int ArmyIndex, int ClassIndex, byte bPilot, bool bFlamer, out MaterialInstanceConstant TunicMatSV)
{
	if( Team == `AXIS_TEAM_INDEX )
	{
		switch (ArmyIndex)
		{
			case `VC:
				return super.GetTunicMeshSV(Team, NFOR_NLF, ClassIndex, bPilot, bFlamer, TunicMatSV);
			
			default:
				return super.GetTunicMeshSV(Team, NFOR_NVA, ClassIndex, bPilot, bFlamer, TunicMatSV);
		}
	}
	else
	{
		switch (ArmyIndex)
		{
			case `AUS:
				return super.GetTunicMeshSV(Team, SFOR_AusArmy, ClassIndex, bPilot, bFlamer, TunicMatSV);
			
			case `ARVN:
			case `RANGERS:
				return super.GetTunicMeshSV(Team, SFOR_ARVN, ClassIndex, bPilot, bFlamer, TunicMatSV);
			
			case `USMC:
				return super.GetTunicMeshSV(Team, SFOR_USMC, ClassIndex, bPilot, bFlamer, TunicMatSV);
			
			default:
				return super.GetTunicMeshSV(Team, SFOR_USArmy, ClassIndex, bPilot, bFlamer, TunicMatSV);
		}
	}
}

static function SkeletalMesh GetFieldgearMesh(int Team, int ArmyIndex, int TunicMeshID, int ClassIndex, byte BodyMIC)
{
	local SkeletalMesh ChosenMesh;
	
	if (Team == `AXIS_TEAM_INDEX)
	{
		if (ArmyIndex == `VC)
		{
			// NVA Riflemen use an AK harness, but we don't want that for VC Guerilla, so swap them
			if (ClassIndex == `ROCI_RIFLEMAN)
			{
				ClassIndex = `ROCI_HEAVY;
			}
			else if (ClassIndex == `ROCI_HEAVY)
			{
				ClassIndex = `ROCI_RIFLEMAN;
			}
			
			if (default.VC_Tunics[TunicMeshID].AltGearMeshID > 0 && (ClassIndex == `ROCI_ENGINEER || ClassIndex == `ROCI_ANTITANK))
			{
				ChosenMesh = default.NVAU_FieldgearByRole[`ROCI_SCOUT];
			}
			else
			{
				ChosenMesh = (BodyMIC > default.VC_TunicMatsFoliageCutoff) ? default.VC_FieldgearByRole[ClassIndex] : default.NVAU_FieldgearByRole[ClassIndex];
			}
		}
		else if (ArmyIndex == `NVA)
		{
			if (default.NVA_Tunics[TunicMeshID].AltGearMeshID > 0)
			{
				ChosenMesh = (ClassIndex == `ROCI_RADIOMAN) ? SkeletalMesh'GOM3_CHR_VN_NVA.Mesh.Gear.GOM_CHR_NVA_Gear_Radio_Alt' : default.MP_Fieldgear;
			}
			else
			{
				ChosenMesh = default.NVA_FieldgearByRole[ClassIndex];
			}
		}
		else
		{
			ChosenMesh = default.NVAU_FieldgearByRole[ClassIndex];
		}
	}
	else
	{
		// First we see if BodyMIC is anything but 0, which means we have a flamethrower (if the faction has them)
		// Then we check TunicInfo.bUseAltGear to see if we use the Flak Jacket mesh (if the faction has them)
		// If TunicInfo.SkinToShow == STS_AboveWaistAndFeet then we have an Open variant of the Flak Jacket
		// Then we check if TunicInfo.SkinToShow is higher than STS_AboveWaist to see if we use the shirtless mesh
		// Otherwise we just use the normal mesh
		
		if (ArmyIndex == `MACV)
		{
			if (BodyMIC > 0)
			{
				if (default.MACV_Tunics[TunicMeshID].SkinToShow >= STS_AboveWaist)
				{
					ChosenMesh = default.US_FlamerFieldgear.FieldgearByTunicType[2];
				}
				else
				{
					ChosenMesh = default.US_FlamerFieldgear.FieldgearByTunicType[0];
				}
			}
			else
			{
				if (default.MACV_Tunics[TunicMeshID].SkinToShow >= STS_AboveWaist)
				{
					ChosenMesh = default.US_FieldgearByRole[ClassIndex].FieldgearByTunicType[2];
				}
				else
				{
					ChosenMesh = default.US_FieldgearByRole[ClassIndex].FieldgearByTunicType[0];
				}
			}
		}
		else if (ArmyIndex == `AUS)
		{
			ChosenMesh = (BodyMIC > 0) ? default.AUS_FlamerFieldgear : default.AUS_FieldgearByRole[ClassIndex];
		}
		else if (ArmyIndex == `ARVN)
		{
			if (default.ARVN_Tunics[TunicMeshID].AltGearMeshID > 0 && default.ARVN_Tunics[TunicMeshID].SkinToShow == STS_AboveWaistAndFeet)
			{
				ChosenMesh = default.ARVN_FieldgearByRole[ClassIndex].FieldgearByTunicType[2];
			}
			else if (default.ARVN_Tunics[TunicMeshID].AltGearMeshID > 0)
			{
				ChosenMesh = default.ARVN_FieldgearByRole[ClassIndex].FieldgearByTunicType[1];
			}
			else
			{
				ChosenMesh = default.ARVN_FieldgearByRole[ClassIndex].FieldgearByTunicType[0];
			}
		}
		else if (ArmyIndex == `RANGERS)
		{
			if (ClassIndex == `ROCI_RIFLEMAN || ClassIndex == `ROCI_ENGINEER)
			{
				ChosenMesh = (default.RANGERS_Tunics[TunicMeshID].AltGearMeshID > 0) ? default.RANGERS_FieldgearByRole[ClassIndex].FieldgearByTunicType[1] : default.RANGERS_FieldgearByRole[ClassIndex].FieldgearByTunicType[0];
			}
			else if (default.RANGERS_Tunics[TunicMeshID].AltGearMeshID > 0)
			{
				ChosenMesh = default.ARVN_FieldgearByRole[ClassIndex].FieldgearByTunicType[1];
			}
			else
			{
				ChosenMesh = default.ARVN_FieldgearByRole[ClassIndex].FieldgearByTunicType[0];
			}
		}
		else if (ArmyIndex == `ROK)
		{
			// South Korea uses a different system.
			// Their tunics use Shirtless Gear for Long, Rolled, and Pants. Only the ROKMC use Long Gear.
			// Vest Gear is unaffected.
			
			if (BodyMIC > 0)
			{
				if (default.ROK_Tunics[TunicMeshID].AltGearMeshID > 0)
				{
					ChosenMesh = default.ROK_FlamerFieldgear.FieldgearByTunicType[1];
				}
				else
				{
					ChosenMesh = default.ROK_FlamerFieldgear.FieldgearByTunicType[2];
				}
			}
			else
			{
				if (default.ROK_Tunics[TunicMeshID].AltGearMeshID > 0)
				{
					ChosenMesh = default.ROK_FieldgearByRole[ClassIndex].FieldgearByTunicType[1];
				}
				else
				{
					ChosenMesh = default.ROK_FieldgearByRole[ClassIndex].FieldgearByTunicType[2];
				}
			}
		}
		else if (ArmyIndex == `ROKA)
		{
			if (BodyMIC > 0)
			{
				if (default.ROKA_Tunics[TunicMeshID].AltGearMeshID > 0)
				{
					ChosenMesh = default.ROK_FlamerFieldgear.FieldgearByTunicType[1];
				}
				else
				{
					ChosenMesh = default.ROK_FlamerFieldgear.FieldgearByTunicType[0];
				}
			}
			else
			{
				if (default.ROKA_Tunics[TunicMeshID].AltGearMeshID > 0)
				{
					ChosenMesh = default.ROK_FieldgearByRole[ClassIndex].FieldgearByTunicType[1];
				}
				else
				{
					ChosenMesh = default.ROK_FieldgearByRole[ClassIndex].FieldgearByTunicType[0];
				}
			}
		}
		else if (ArmyIndex == `ROKMC)
		{
			if (BodyMIC > 0)
			{
				if (default.ROKMC_Tunics[TunicMeshID].AltGearMeshID > 0)
				{
					ChosenMesh = default.ROK_FlamerFieldgear.FieldgearByTunicType[1];
				}
				else
				{
					ChosenMesh = default.ROK_FlamerFieldgear.FieldgearByTunicType[0];
				}
			}
			else
			{
				if (default.ROKMC_Tunics[TunicMeshID].AltGearMeshID > 0)
				{
					ChosenMesh = default.ROK_FieldgearByRole[ClassIndex].FieldgearByTunicType[1];
				}
				else
				{
					ChosenMesh = default.ROK_FieldgearByRole[ClassIndex].FieldgearByTunicType[0];
				}
			}
		}
		else if (ArmyIndex == `MP)
		{
			ChosenMesh = default.MP_Fieldgear;
		}
		else
		{
			if (BodyMIC > 0)
			{
				if (default.US_Tunics[TunicMeshID].AltGearMeshID > 0)
				{
					ChosenMesh = default.US_FlamerFieldgear.FieldgearByTunicType[1];
				}
				else if (default.US_Tunics[TunicMeshID].SkinToShow >= STS_AboveWaist)
				{
					ChosenMesh = default.US_FlamerFieldgear.FieldgearByTunicType[2];
				}
				else
				{
					ChosenMesh = default.US_FlamerFieldgear.FieldgearByTunicType[0];
				}
			}
			else
			{
				if (default.US_Tunics[TunicMeshID].AltGearMeshID > 0)
				{
					ChosenMesh = default.US_FieldgearByRole[ClassIndex].FieldgearByTunicType[1];
				}
				else if (default.US_Tunics[TunicMeshID].SkinToShow >= STS_AboveWaist)
				{
					ChosenMesh = default.US_FieldgearByRole[ClassIndex].FieldgearByTunicType[2];
				}
				else
				{
					ChosenMesh = default.US_FieldgearByRole[ClassIndex].FieldgearByTunicType[0];
				}
			}
		}
	}
	
	if (ClassIndex == `ROCI_COMBATPILOT || ClassIndex == `ROCI_TRANSPORTPILOT)
	{
		ChosenMesh = default.US_FieldgearByRole[ClassIndex].FieldgearByTunicType[0];
	}
	
	`gom("Pawn is a" @ class'GOMPlayerController'.static.GetArmyNameFromIndex(ArmyIndex, Team) @ class'GOMPlayerController'.static.GetRoleNameFromIndex(ClassIndex) @ "Tunic" @ TunicMeshID @ "returning" @ ChosenMesh, 'PawnsGear');
	return ChosenMesh;
}

static function SkeletalMesh GetAltFieldgearMesh(int Team, int ArmyIndex, int TunicMeshID)
{
	local SkeletalMesh ChosenMesh;
	
	if (Team == `ALLIES_TEAM_INDEX)
	{
		if (ArmyIndex == `ARVN)
		{
			if (default.ARVN_Tunics[TunicMeshID].AltGearMeshID > 0 && default.ARVN_Tunics[TunicMeshID].SkinToShow == STS_AboveWaistAndFeet)
			{
				ChosenMesh = default.ARVN_AltFieldgear.FieldgearByTunicType[2];
			}
			else if (default.ARVN_Tunics[TunicMeshID].AltGearMeshID > 0)
			{
				ChosenMesh = default.ARVN_AltFieldgear.FieldgearByTunicType[1];
			}
			else
			{
				ChosenMesh = default.ARVN_AltFieldgear.FieldgearByTunicType[0];
			}
		}
		else if (ArmyIndex == `ROK)
		{
			if (default.ROK_Tunics[TunicMeshID].AltGearMeshID > 0)
			{
				ChosenMesh = default.ROK_AltFieldgear.FieldgearByTunicType[1];
			}
			else
			{
				ChosenMesh = default.ROK_AltFieldgear.FieldgearByTunicType[2];
			}
		}
		else if (ArmyIndex == `ROKA)
		{
			if (default.ROKA_Tunics[TunicMeshID].AltGearMeshID > 0)
			{
				ChosenMesh = default.ROK_AltFieldgear.FieldgearByTunicType[1];
			}
			else
			{
				ChosenMesh = default.ROK_AltFieldgear.FieldgearByTunicType[0];
			}
		}
		else if (ArmyIndex == `ROKMC)
		{
			if (default.ROKMC_Tunics[TunicMeshID].AltGearMeshID > 0)
			{
				ChosenMesh = default.ROK_AltFieldgear.FieldgearByTunicType[1];
			}
			else
			{
				ChosenMesh = default.ROK_AltFieldgear.FieldgearByTunicType[0];
			}
		}
	}
	
	`gom("GetAltFieldgearMesh Team" @ Team @ "Army" @ ArmyIndex @ "Tunic" @ TunicMeshID @ "returning" @ ChosenMesh, 'PawnHandler');
	return ChosenMesh;
}

static function bool GetShirtTextures(int Team, int ArmyIndex, byte bPilot, byte TunicMeshID, byte ShirtID, out Texture2D ShirtD, out Texture2D ShirtN, out Texture2D ShirtS)
{
	local array<ShirtInfo> TempShirts;
	TempShirts = GetShirtArray(Team, ArmyIndex, bPilot);
	ShirtD = TempShirts[ShirtID].ShirtD;
	ShirtN = TempShirts[ShirtID].ShirtN;
	ShirtS = TempShirts[ShirtID].ShirtS;
	return true;
}

static function SkeletalMesh GetAltFPArmsMesh(int Team, int ArmyIndex, byte TunicMeshID, byte TunicMaterialID, byte SkinToneID, out MaterialInstanceConstant SkinMIC, out MaterialInstanceConstant SleeveMIC)
{
	local array<TunicInfo> Tunics;
	local array<SkeletalMesh> AltFPArmsMeshes;
	
	`gom("GetAltFPArmsMesh Team" @ Team @ "Army" @ ArmyIndex @ "Tunic" @ TunicMeshID, 'PawnHandler');
	
	if (Team == `ALLIES_TEAM_INDEX )
	{
		switch (ArmyIndex)
		{
			case `ARVN:
				Tunics = default.ARVN_Tunics;
				AltFPArmsMeshes = default.ARVN_AltFPArmsMeshes;
				break;
			
			case `ROK:
				Tunics = default.ROK_Tunics;
				AltFPArmsMeshes = default.ROK_AltFPArmsMeshes;
				break;
			
			case `ROKA:
				Tunics = default.ROKA_Tunics;
				AltFPArmsMeshes = default.ROKA_AltFPArmsMeshes;
				break;
			
			case `ROKMC:
				Tunics = default.ROKMC_Tunics;
				AltFPArmsMeshes = default.ROKMC_AltFPArmsMeshes;
				break;
		}
	}
	
	SkinMIC = default.FPSkinToneMICs[SkinToneID];
	SleeveMIC = Tunics[TunicMeshID].BodyMICS[TunicMaterialID].SleeveMICFP;
	
	return AltFPArmsMeshes[TunicMeshID];
}

static function array<TattooInfo> GetTattooArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	local array<TattooInfo> nonMACVoverlays;
	local int i;
	
	if (TeamIndex == `ALLIES_TEAM_INDEX && ArmyIndex == `MACV && bPilot <= 0)
	{
		return default.All_BodyOverlays;
	}
	else if (bPilot <= 0 && ArmyIndex != `MP)
	{
		for (i = 0; i < default.All_BodyOverlays.length - 1; i++)
		{
			nonMACVoverlays.AddItem(default.All_BodyOverlays[i]);
		}
		
		return nonMACVoverlays;
	}
	
	return default.USPilotTattoos;
}

static function array<TunicInfo> GetTunicArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	`gom("GetTunicArray Team" @ TeamIndex @ "Army" @ ArmyIndex @ "Pilot" @ bPilot, 'PawnHandler');
	
	if (TeamIndex == `AXIS_TEAM_INDEX )
	{
		if (bPilot == `ROCI_COMMANDER)
		{
			switch (ArmyIndex)
			{
				case `VC:
				case `NVAU:
					return default.NVAU_Tunics_Commander;
				
				default:
					return default.NVA_Tunics_Commander;
			}
		}
		
		switch (ArmyIndex)
		{
			case `VC:
				return default.VC_Tunics;
			
			case `NVAU:
				return default.NVAU_Tunics;
			
			default:
				return default.NVA_Tunics;
		}
	}
	else
	{
		if (bPilot > 0)
		{
			switch (ArmyIndex)
			{
				case `AUS:
					return default.AusPilotTunics;
				
				case `ARVN:
				case `RANGERS:
				case `ROK:
				case `ROKMC:
					return default.ARVNPilotTunics;
				
				default:
					return default.USPilotTunics;
			}
		}
		else
		{
			switch (ArmyIndex)
			{
				case `USMC: return default.USMC_Tunics;
				case `MACV: return default.MACV_Tunics;
				case `AUS: return default.AUS_Tunics;
				case `ARVN: return default.ARVN_Tunics;
				case `RANGERS: return default.RANGERS_Tunics;
				case `ROK: return default.ROK_Tunics;
				case `ROKA: return default.ROKA_Tunics;
				case `ROKMC: return default.ROKMC_Tunics;
				case `MP: return default.MP_Tunics;
				default: return default.US_Tunics;
			}
		}
	}
}

static function MaterialInstanceConstant GetSecondaryBodyMIC(int TeamIndex, int ArmyIndex, byte TunicMeshID, byte TunicMaterialID)
{
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		switch (ArmyIndex)
		{
			case `VC:
				return (default.VC_Tunics[TunicMeshID].AltGearMeshID > 0) ? default.VC_SecondaryBodyMICTemplates2.BodyMICs[0].BodyMICTemplate : default.VC_SecondaryBodyMICTemplates.BodyMICs[TunicMaterialID].BodyMICTemplate;
			
			case `NVA:
				return (default.NVA_Tunics[TunicMeshID].AltGearMeshID > 0) ? default.NVA_SecondaryBodyMICTemplates.BodyMICs[0].BodyMICTemplate : none;
		}
	}
	else
	{
		switch (ArmyIndex)
		{
			case `USMC: return default.USMC_SecondaryBodyMICTemplates.BodyMICs[TunicMaterialID].BodyMICTemplate;
			case `ARVN: return (default.ARVN_Tunics[TunicMeshID].SkinToShow == STS_AboveWaistAndFeet) ? default.ARVN_SecondaryBodyMICTemplates.BodyMICs[TunicMaterialID].BodyMICTemplate : none;
			case `RANGERS: return default.RANGERS_SecondaryBodyMICTemplates.BodyMICs[TunicMaterialID].BodyMICTemplate;
		}
	}
	
	return none;
}

static function array<ShirtInfo> GetShirtArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		switch (ArmyIndex)
		{
			case `VC:
				return default.VC_Shirts;
			
			case `NVA:
			case `NVAU:
				return default.NVA_Shirts;
		}
	}
	else if (bPilot <= 0 && (ArmyIndex == `ROK || ArmyIndex == `ROKA || ArmyIndex == `ROKMC))
	{
		return default.ROK_Shirts;
	}
	else if (bPilot <= 0 && ArmyIndex != `MP)
	{
		return default.US_Shirts;
	}
	
	return default.USPilotShirts;
}

static function array<PlayerHeadInfo> GetHeadArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	if (TeamIndex == `AXIS_TEAM_INDEX )
	{
		switch (ArmyIndex)
		{
			case `NVA:
			case `NVAU:
				return default.NVA_Heads;
			
			case `VC:
				return default.VC_Heads;
			
			default:
				return default.VietnameseHeads;
		}
	}
	else
	{
		switch (ArmyIndex)
		{
			case `AUS:
				return default.AUS_Heads;
			
			case `ARVN:
			case `RANGERS:
				return default.ARVN_Heads;
			
			case `ROK:
			case `ROKA:
			case `ROKMC:
				return default.ROK_Heads;
			
			default:
				return default.US_Heads;
		}
	}
}

static function array<HeadgearInfo> GetHeadgearArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	local int i;
	local array<HeadgearInfo> AUS_Headgear, USA_Headgear, ROKP_Headgear;
	
	USA_Headgear = default.US_Headgear_Shared;
	
	if (TeamIndex == `AXIS_TEAM_INDEX )
	{
		switch (ArmyIndex)
		{
			case `VC:
				return default.VC_Headgear;
			
			case `NVAU:
				return default.NVAU_Headgear;
			
			default:
				return default.NVA_Headgear;
		}
	}
	else
	{
		if (bPilot > 0)
		{
			switch (ArmyIndex)
			{
				case `AUS:
					return super.GetHeadgearArray(TeamIndex, SFOR_AusArmy, bPilot);
				
				case `ARVN:
				case `RANGERS:
					return super.GetHeadgearArray(TeamIndex, SFOR_ARVN, bPilot);
				
				case `ROK:
				case `ROKMC:
					ROKP_Headgear.AddItem(default.ARVNPilotHeadgear[0]);
					return ROKP_Headgear;
				
				default:
					return super.GetHeadgearArray(TeamIndex, SFOR_USArmy, bPilot);
			}
		}
		
		if (ArmyIndex == `MACV)
		{
			return default.MACV_Headgear;
		}
		else if (ArmyIndex == `AUS)
		{
			for (i = 0; i < default.AusArmyHeadgear.length - 3; i++)
			{
				AUS_Headgear.AddItem(default.AusArmyHeadgear[i]);
			}
			
			return AUS_Headgear;
		}
		else if (ArmyIndex == `ARVN)
		{
			return default.ARVN_Headgear;
		}
		else if (ArmyIndex == `RANGERS)
		{
			return default.RANGERS_Headgear;
		}
		else if (ArmyIndex == `MP)
		{
			return default.MP_Headgear;
		}
		else if (ArmyIndex == `USMC)
		{
			for (i = 0; i < default.USMC_Headgear.length; i++)
			{
				USA_Headgear.AddItem(default.USMC_Headgear[i]);
			}
			
			return USA_Headgear;
		}
		else if (ArmyIndex == `ROK || ArmyIndex == `ROKA)
		{
			return default.ROK_Headgear;
		}
		else if (ArmyIndex == `ROKMC)
		{
			return default.ROKMC_Headgear;
		}
		else
		{
			for (i = 0; i < default.US_Headgear.length; i++)
			{
				USA_Headgear.AddItem(default.US_Headgear[i]);
			}
			
			return USA_Headgear;
		}
	}
}

static function array<HeadgearMICInfo> GetHeadgearMICArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	if (TeamIndex == `ALLIES_TEAM_INDEX && (bPilot > 0 || ArmyIndex == `AUS))
	{
		switch (ArmyIndex)
		{
			case `AUS:
				return super.GetHeadgearMICArray(TeamIndex, SFOR_AusArmy, bPilot);
			
			case `ARVN:
			case `RANGERS:
			case `ROK:
			case `ROKMC:
				return super.GetHeadgearMICArray(TeamIndex, SFOR_ARVN, bPilot);
			
			case `ROKA:
				return default.North_HeadgearMICs;
			
			default:
				return super.GetHeadgearMICArray(TeamIndex, SFOR_USArmy, bPilot);
		}
	}
	
	if (TeamIndex == `ALLIES_TEAM_INDEX && ArmyIndex == `USMC)
	{
		return default.USMC_HeadgearMICs;
	}
	
	return (TeamIndex == `AXIS_TEAM_INDEX) ? default.North_HeadgearMICs : default.South_HeadgearMICs;
}

static function SkeletalMesh GetHeadgearMesh(int Team, int ArmyIndex, byte bPilot, byte HeadID, byte HairID, byte HeadgearID, byte HeadgearMatID, out MaterialInstanceConstant HeadgearMIC, out MaterialInstanceConstant HairMIC, out name SocketName, out byte bIsHelmet)
{
	local array<PlayerHeadInfo> Heads;
	local array<HeadgearInfo> Headgear;
	local array<HeadgearMICInfo> HeadgearMICs;
	local byte HeadgearSubIndex;
	
	if (Team == `AXIS_TEAM_INDEX && bPilot == `ROCI_COMMANDER)
	{
		switch (ArmyIndex)
		{
			case `VC:
			case `NVAU:
				Headgear = default.NVAU_Headgear_Commander;
				break;
			
			default:
				Headgear = default.NVA_Headgear_Commander;
		}
		
		bPilot = 0;
		bIsHelmet = 1;
	}
	else
	{
		Headgear = GetHeadgearArray(Team, ArmyIndex, bPilot);
	}
	
	HeadgearMICs = GetHeadgearMICArray(Team, ArmyIndex, bPilot);
	Heads = GetHeadArray(Team, ArmyIndex, bPilot);
	HairMIC = default.HairMICs[HairID].HairMIC;
	
	HeadgearSubIndex = (bPilot > 0) ? 0 : Heads[HeadID].HeadgearSubIndex;
	
	SocketName = Headgear[HeadgearID].HeadgearSocket;
	bIsHelmet = Headgear[HeadgearID].bIsHelmet;
	HeadgearMIC = HeadgearMICs[Headgear[HeadgearID].HeadgearMICs[HeadgearMatID]].HeadgearMICTemplate;
	
	if( HeadgearSubIndex > 0 && Headgear[HeadgearID].HeadgearMeshes[HeadgearSubIndex] != none )
		return Headgear[HeadgearID].HeadgearMeshes[HeadgearSubIndex];
	else
		return Headgear[HeadgearID].HeadgearMeshes[0];
}

static function SkeletalMesh GetHairMesh(int Team, int ArmyIndex, byte bPilot, byte HeadID, byte HairID, byte HeadgearID, out MaterialInstanceConstant HeadgearMIC, out MaterialInstanceConstant HairMIC)
{
	local array<PlayerHeadInfo> Heads;
	local array<HeadgearInfo> Headgear;
	
	Headgear = GetHeadgearArray(Team, ArmyIndex, bPilot);
	
	HairMIC = default.HairMICs[HairID].HairMIC;
	
	// We use the bIsHelmet flag to distinguish between headgear that needs hair underneath
	// or headgear that is already hair, in which case we don't need to add extra hair
	if (Headgear[HeadgearID].bIsHelmet > 0)
	{
		Heads = GetHeadArray(Team, ArmyIndex, bPilot);
		return default.HairMeshes[Heads[HeadID].HeadgearSubIndex];
	}
	
	return default.HairMeshes[default.HairMeshes.length - 1];
}

static function array<FaceItemInfo> GetFaceItemArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		if (bPilot > 0)
		{
			return default.FaceItems_None;
		}
		
		return default.FaceItems_Viet;
	}
	else
	{
		if (bPilot > 0)
		{
			return default.FaceItems_Pilot;
		}
		
		switch (ArmyIndex)
		{
			case `MP:
				return default.MP_FaceItems;
			
			case `RANGERS:
				return default.RANGERS_Aviators;
			
			case `USMC:
				return default.USMC_FaceItems;
			
			default:
				return default.FaceItems_US;
		}
	}
}

static function array<FacialHairInfo> GetFacialHairArray(byte TeamIndex, byte ArmyIndex)
{
	if (TeamIndex == `AXIS_TEAM_INDEX )
	{
		return default.VietFacialHair;
	}
	else
	{
		switch (ArmyIndex)
		{
			case `AUS:
				return default.AusArmyFacialHair;
			
			case `ARVN:
			case `RANGERS:
			case `ROK:
			case `ROKA:
			case `ROKMC:
				return default.ARVNFacialHair;
			
			default:
				return default.USAFacialHair;
		}
	}
}

static function GetCharConfig(int Team, int ArmyIndex, byte bPilot, int ClassIndex, int HonorLevel, out byte TunicID, out byte TunicMaterialID, out byte ShirtID, out byte HeadID, out byte HairID, out byte HeadgearID, out byte HeadgearMatID, out byte FaceItemID, out byte FacialHairID, out byte TattooID, ROPlayerReplicationInfo ROPRI, optional bool bRandomiseAll, optional bool bInitByMenu, optional out byte bUseBase)
{
	// For some FUCKING reason setting TempCharConfig to a CharacterConfig throws a "Type mismatch" error.
	// TempCharConfig = default.RandomConfig;
	// TempCharConfig = default.NVAConfig;
	// TempCharConfig = default.USArmyConfig;
	// None of the above lines compile, even though that's literally the original code. wtf?
	// So now we gotta do it the super annoying way.
	
	`gom("Getting Team" @ Team @ "ArmyIndex" @ ArmyIndex @ "Pilot" @ bPilot, 'PawnHandler');
	
	if (bRandomiseAll)
	{
		`gom("Getting Random", 'PawnHandler');
		TunicID = default.RandomConfig.TunicMesh;
		TunicMaterialID = default.RandomConfig.TunicMaterial;
		ShirtID = default.RandomConfig.ShirtTexture;
		HeadID = default.RandomConfig.HeadMesh;
		HairID = default.RandomConfig.HairMaterial;
		HeadgearID = default.RandomConfig.HeadgearMesh;
		HeadgearMatID = default.RandomConfig.HeadgearMaterial;
		FaceItemID = default.RandomConfig.FaceItemMesh;
		FacialHairID = default.RandomConfig.FacialHairMesh;
		TattooID = default.RandomConfig.TattooTex;
		
	}
	else if (Team == `AXIS_TEAM_INDEX)
	{
		switch (ArmyIndex)
		{
			case `VC:
				`gom("Getting VC", 'PawnHandler');
				TunicID = default.CFGA_VC[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_VC[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_VC[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_VC[ClassIndex].HeadMesh;
				HairID = default.CFGA_VC[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_VC[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_VC[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_VC[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_VC[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_VC[ClassIndex].TattooTex;
				break;
			
			case `NVA:
				`gom("Getting NVA", 'PawnHandler');
				TunicID = default.CFGA_NVA[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_NVA[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_NVA[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_NVA[ClassIndex].HeadMesh;
				HairID = default.CFGA_NVA[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_NVA[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_NVA[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_NVA[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_NVA[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_NVA[ClassIndex].TattooTex;
				break;
			
			case `NVAU:
				`gom("Getting NVAU", 'PawnHandler');
				TunicID = default.CFGA_NVAU[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_NVAU[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_NVAU[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_NVAU[ClassIndex].HeadMesh;
				HairID = default.CFGA_NVAU[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_NVAU[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_NVAU[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_NVAU[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_NVAU[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_NVAU[ClassIndex].TattooTex;
				break;
			
			default:
				`gom("WARNING - could not get Axis config" @ ArmyIndex, 'PawnHandler');
				TunicID = 0;
				TunicMaterialID = 0;
				ShirtID = 0;
				HeadID = 0;
				HairID = 0;
				HeadgearID = 0;
				HeadgearMatID = 0;
				FaceItemID = 0;
				FacialHairID = 0;
				TattooID = 0;
		}
	}
	else
	{
		switch (ArmyIndex)
		{
			case `US:
				`gom("Getting US", 'PawnHandler');
				TunicID = default.CFGA_US[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_US[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_US[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_US[ClassIndex].HeadMesh;
				HairID = default.CFGA_US[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_US[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_US[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_US[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_US[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_US[ClassIndex].TattooTex;
				break;
			
			case `USMC:
				`gom("Getting USMC", 'PawnHandler');
				TunicID = default.CFGA_USMC[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_USMC[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_USMC[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_USMC[ClassIndex].HeadMesh;
				HairID = default.CFGA_USMC[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_USMC[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_USMC[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_USMC[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_USMC[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_USMC[ClassIndex].TattooTex;
				break;
			
			case `MACV:
				`gom("Getting MACV", 'PawnHandler');
				TunicID = default.CFGA_MACV[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_MACV[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_MACV[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_MACV[ClassIndex].HeadMesh;
				HairID = default.CFGA_MACV[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_MACV[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_MACV[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_MACV[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_MACV[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_MACV[ClassIndex].TattooTex;
				break;
			
			case `AUS:
				`gom("Getting AUS", 'PawnHandler');
				TunicID = default.CFGA_AUS[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_AUS[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_AUS[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_AUS[ClassIndex].HeadMesh;
				HairID = default.CFGA_AUS[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_AUS[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_AUS[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_AUS[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_AUS[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_AUS[ClassIndex].TattooTex;
				break;
			
			case `ARVN:
				`gom("Getting ARVN", 'PawnHandler');
				TunicID = default.CFGA_ARVN[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_ARVN[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_ARVN[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_ARVN[ClassIndex].HeadMesh;
				HairID = default.CFGA_ARVN[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_ARVN[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_ARVN[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_ARVN[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_ARVN[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_ARVN[ClassIndex].TattooTex;
				break;
			
			case `RANGERS:
				`gom("Getting ARVN Rangers", 'PawnHandler');
				TunicID = default.CFGA_RANGERS[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_RANGERS[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_RANGERS[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_RANGERS[ClassIndex].HeadMesh;
				HairID = default.CFGA_RANGERS[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_RANGERS[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_RANGERS[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_RANGERS[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_RANGERS[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_RANGERS[ClassIndex].TattooTex;
				break;
			
			/* case `MP:
				`gom("Getting MP", 'PawnHandler');
				TunicID = default.CFG_MP.TunicMesh;
				TunicMaterialID = default.CFG_MP.TunicMaterial;
				ShirtID = default.CFG_MP.ShirtTexture;
				HeadID = default.CFG_MP.HeadMesh;
				HairID = default.CFG_MP.HairMaterial;
				HeadgearID = default.CFG_MP.HeadgearMesh;
				HeadgearMatID = default.CFG_MP.HeadgearMaterial;
				FaceItemID = default.CFG_MP.FaceItemMesh;
				FacialHairID = default.CFG_MP.FacialHairMesh;
				TattooID = default.CFG_MP.TattooTex;
				break; */
			
			case `ROK:
				`gom("Getting ROK", 'PawnHandler');
				TunicID = default.CFGA_ROK[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_ROK[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_ROK[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_ROK[ClassIndex].HeadMesh;
				HairID = default.CFGA_ROK[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_ROK[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_ROK[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_ROK[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_ROK[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_ROK[ClassIndex].TattooTex;
				break;
			
			case `ROKA:
				`gom("Getting ROK Armored", 'PawnHandler');
				TunicID = default.CFGA_ROKA[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_ROKA[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_ROKA[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_ROKA[ClassIndex].HeadMesh;
				HairID = default.CFGA_ROKA[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_ROKA[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_ROKA[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_ROKA[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_ROKA[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_ROKA[ClassIndex].TattooTex;
				break;
			
			case `ROKMC:
				`gom("Getting ROK Marines", 'PawnHandler');
				TunicID = default.CFGA_ROKMC[ClassIndex].TunicMesh;
				TunicMaterialID = default.CFGA_ROKMC[ClassIndex].TunicMaterial;
				ShirtID = default.CFGA_ROKMC[ClassIndex].ShirtTexture;
				HeadID = default.CFGA_ROKMC[ClassIndex].HeadMesh;
				HairID = default.CFGA_ROKMC[ClassIndex].HairMaterial;
				HeadgearID = default.CFGA_ROKMC[ClassIndex].HeadgearMesh;
				HeadgearMatID = default.CFGA_ROKMC[ClassIndex].HeadgearMaterial;
				FaceItemID = default.CFGA_ROKMC[ClassIndex].FaceItemMesh;
				FacialHairID = default.CFGA_ROKMC[ClassIndex].FacialHairMesh;
				TattooID = default.CFGA_ROKMC[ClassIndex].TattooTex;
				break;
			
			default:
				`gom("WARNING - could not get Allies config" @ ArmyIndex, 'PawnHandler');
				TunicID = 0;
				TunicMaterialID = 0;
				ShirtID = 0;
				HeadID = 0;
				HairID = 0;
				HeadgearID = 0;
				HeadgearMatID = 0;
				FaceItemID = 0;
				FacialHairID = 0;
				TattooID = 0;
		}
	}
	
	ValidateCharConfig(Team, ArmyIndex, bPilot, HonorLevel, TunicID, TunicMaterialID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI);
}

static function SaveCharConfig(int Team, int ArmyIndex, byte bPilot, int ClassIndex, int HonorLevel, out byte TunicID, out byte TunicMaterialID, out byte ShirtID, out byte HeadID, out byte HairID, out byte HeadgearID, out byte HeadgearMatID, out byte FaceItemID, out byte FacialHairID, out byte TattooID, byte bUseBase)
{
	// Same damn problem as in GetCharConfig().
	
	`gom("Saving Team" @ Team @ "ArmyIndex" @ ArmyIndex @ "Pilot" @ bPilot, 'PawnHandler');
	
	default.CFGA_NVA.length = `TOTALROLECOUNT_NORTH;
	default.CFGA_NVAU.length = `TOTALROLECOUNT_NORTH;
	default.CFGA_VC.length = `TOTALROLECOUNT_NORTH;
	
	default.CFGA_US.length = `TOTALROLECOUNT_SOUTH;
	default.CFGA_USMC.length = `TOTALROLECOUNT_SOUTH;
	default.CFGA_MACV.length = `TOTALROLECOUNT_SOUTH;
	default.CFGA_AUS.length = `TOTALROLECOUNT_SOUTH;
	default.CFGA_ARVN.length = `TOTALROLECOUNT_SOUTH;
	default.CFGA_RANGERS.length = `TOTALROLECOUNT_SOUTH;
	default.CFGA_ROK.length = `TOTALROLECOUNT_SOUTH;
	default.CFGA_ROKA.length = `TOTALROLECOUNT_SOUTH;
	default.CFGA_ROKMC.length = `TOTALROLECOUNT_SOUTH;
	
	StaticSaveConfig();
	
	if (Team == `AXIS_TEAM_INDEX)
	{
		switch (ArmyIndex)
		{
			case `VC:
				`gom("Saving VC", 'PawnHandler');
				default.CFGA_VC[ClassIndex].TunicMesh = TunicID;
				default.CFGA_VC[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_VC[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_VC[ClassIndex].HeadMesh = HeadID;
				default.CFGA_VC[ClassIndex].HairMaterial = HairID;
				default.CFGA_VC[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_VC[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_VC[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_VC[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_VC[ClassIndex].TattooTex = TattooID;
				break;
			
			case `NVAU:
				`gom("Saving NVAU", 'PawnHandler');
				default.CFGA_NVAU[ClassIndex].TunicMesh = TunicID;
				default.CFGA_NVAU[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_NVAU[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_NVAU[ClassIndex].HeadMesh = HeadID;
				default.CFGA_NVAU[ClassIndex].HairMaterial = HairID;
				default.CFGA_NVAU[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_NVAU[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_NVAU[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_NVAU[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_NVAU[ClassIndex].TattooTex = TattooID;
				break;
			
			case `NVA:
				`gom("Saving NVA", 'PawnHandler');
				default.CFGA_NVA[ClassIndex].TunicMesh = TunicID;
				default.CFGA_NVA[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_NVA[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_NVA[ClassIndex].HeadMesh = HeadID;
				default.CFGA_NVA[ClassIndex].HairMaterial = HairID;
				default.CFGA_NVA[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_NVA[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_NVA[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_NVA[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_NVA[ClassIndex].TattooTex = TattooID;
				break;
			
			default:
				`gom("WARNING - Could not save Axis ArmyIndex" @ ArmyIndex, 'PawnHandler');
		}
	}
	else
	{
		switch (ArmyIndex)
		{
			case `USMC:
				`gom("Saving USMC", 'PawnHandler');
				default.CFGA_USMC[ClassIndex].TunicMesh = TunicID;
				default.CFGA_USMC[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_USMC[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_USMC[ClassIndex].HeadMesh = HeadID;
				default.CFGA_USMC[ClassIndex].HairMaterial = HairID;
				default.CFGA_USMC[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_USMC[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_USMC[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_USMC[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_USMC[ClassIndex].TattooTex = TattooID;
				break;
			
			case `MACV:
				`gom("Saving MACV", 'PawnHandler');
				default.CFGA_MACV[ClassIndex].TunicMesh = TunicID;
				default.CFGA_MACV[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_MACV[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_MACV[ClassIndex].HeadMesh = HeadID;
				default.CFGA_MACV[ClassIndex].HairMaterial = HairID;
				default.CFGA_MACV[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_MACV[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_MACV[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_MACV[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_MACV[ClassIndex].TattooTex = TattooID;
				break;
			
			case `AUS:
				`gom("Saving AUS", 'PawnHandler');
				default.CFGA_AUS[ClassIndex].TunicMesh = TunicID;
				default.CFGA_AUS[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_AUS[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_AUS[ClassIndex].HeadMesh = HeadID;
				default.CFGA_AUS[ClassIndex].HairMaterial = HairID;
				default.CFGA_AUS[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_AUS[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_AUS[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_AUS[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_AUS[ClassIndex].TattooTex = TattooID;
				break;
			
			case `US:
				`gom("Saving US", 'PawnHandler');
				default.CFGA_US[ClassIndex].TunicMesh = TunicID;
				default.CFGA_US[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_US[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_US[ClassIndex].HeadMesh = HeadID;
				default.CFGA_US[ClassIndex].HairMaterial = HairID;
				default.CFGA_US[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_US[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_US[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_US[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_US[ClassIndex].TattooTex = TattooID;
				break;
			
			case `ARVN:
				`gom("Saving ARVN", 'PawnHandler');
				default.CFGA_ARVN[ClassIndex].TunicMesh = TunicID;
				default.CFGA_ARVN[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_ARVN[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_ARVN[ClassIndex].HeadMesh = HeadID;
				default.CFGA_ARVN[ClassIndex].HairMaterial = HairID;
				default.CFGA_ARVN[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_ARVN[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_ARVN[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_ARVN[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_ARVN[ClassIndex].TattooTex = TattooID;
				break;
			
			case `RANGERS:
				`gom("Saving ARVN Rangers", 'PawnHandler');
				default.CFGA_RANGERS[ClassIndex].TunicMesh = TunicID;
				default.CFGA_RANGERS[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_RANGERS[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_RANGERS[ClassIndex].HeadMesh = HeadID;
				default.CFGA_RANGERS[ClassIndex].HairMaterial = HairID;
				default.CFGA_RANGERS[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_RANGERS[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_RANGERS[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_RANGERS[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_RANGERS[ClassIndex].TattooTex = TattooID;
				break;
			
			/* case `MP:
				`gom("Saving MP", 'PawnHandler');
				default.CFG_MP.TunicMesh = TunicID;
				default.CFG_MP.TunicMaterial = TunicMaterialID;
				default.CFG_MP.ShirtTexture = ShirtID;
				default.CFG_MP.HeadMesh = HeadID;
				default.CFG_MP.HairMaterial = HairID;
				default.CFG_MP.HeadgearMesh = HeadgearID;
				default.CFG_MP.HeadgearMaterial = HeadgearMatID;
				default.CFG_MP.FaceItemMesh = FaceItemID;
				default.CFG_MP.FacialHairMesh = FacialHairID;
				default.CFG_MP.TattooTex = TattooID;
				break; */
			
			case `ROK:
				`gom("Saving ROK", 'PawnHandler');
				default.CFGA_ROK[ClassIndex].TunicMesh = TunicID;
				default.CFGA_ROK[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_ROK[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_ROK[ClassIndex].HeadMesh = HeadID;
				default.CFGA_ROK[ClassIndex].HairMaterial = HairID;
				default.CFGA_ROK[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_ROK[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_ROK[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_ROK[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_ROK[ClassIndex].TattooTex = TattooID;
				break;
			
			case `ROKA:
				`gom("Saving ROK Armored", 'PawnHandler');
				default.CFGA_ROKA[ClassIndex].TunicMesh = TunicID;
				default.CFGA_ROKA[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_ROKA[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_ROKA[ClassIndex].HeadMesh = HeadID;
				default.CFGA_ROKA[ClassIndex].HairMaterial = HairID;
				default.CFGA_ROKA[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_ROKA[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_ROKA[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_ROKA[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_ROKA[ClassIndex].TattooTex = TattooID;
				break;
			
			case `ROKMC:
				`gom("Saving ROK Marines", 'PawnHandler');
				default.CFGA_ROKMC[ClassIndex].TunicMesh = TunicID;
				default.CFGA_ROKMC[ClassIndex].TunicMaterial = TunicMaterialID;
				default.CFGA_ROKMC[ClassIndex].ShirtTexture = ShirtID;
				default.CFGA_ROKMC[ClassIndex].HeadMesh = HeadID;
				default.CFGA_ROKMC[ClassIndex].HairMaterial = HairID;
				default.CFGA_ROKMC[ClassIndex].HeadgearMesh = HeadgearID;
				default.CFGA_ROKMC[ClassIndex].HeadgearMaterial = HeadgearMatID;
				default.CFGA_ROKMC[ClassIndex].FaceItemMesh = FaceItemID;
				default.CFGA_ROKMC[ClassIndex].FacialHairMesh = FacialHairID;
				default.CFGA_ROKMC[ClassIndex].TattooTex = TattooID;
				break;
			
			default:
				`gom("WARNING - Could not save Allies ArmyIndex" @ ArmyIndex, 'PawnHandler');
		}
	}
	
	StaticSaveConfig();
}

static function bool CopyConfigToClass(int Team, int ArmyIndex, int SourceClassIndex, int TargetClassIndex)
{
	local int i;
	
	if (Team == `AXIS_TEAM_INDEX)
	{
		switch (ArmyIndex)
		{
			case `VC:
			for (i = 0; i < default.CFGA_VC.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_VC[i].TunicMesh		= default.CFGA_VC[SourceClassIndex].TunicMesh;
					default.CFGA_VC[i].TunicMaterial	= default.CFGA_VC[SourceClassIndex].TunicMaterial;
					default.CFGA_VC[i].ShirtTexture		= default.CFGA_VC[SourceClassIndex].ShirtTexture;
					default.CFGA_VC[i].HeadMesh			= default.CFGA_VC[SourceClassIndex].HeadMesh;
					default.CFGA_VC[i].HairMaterial		= default.CFGA_VC[SourceClassIndex].HairMaterial;
					default.CFGA_VC[i].HeadgearMesh		= default.CFGA_VC[SourceClassIndex].HeadgearMesh;
					default.CFGA_VC[i].HeadgearMaterial	= default.CFGA_VC[SourceClassIndex].HeadgearMaterial;
					default.CFGA_VC[i].FaceItemMesh		= default.CFGA_VC[SourceClassIndex].FaceItemMesh;
					default.CFGA_VC[i].FacialHairMesh	= default.CFGA_VC[SourceClassIndex].FacialHairMesh;
					default.CFGA_VC[i].TattooTex		= default.CFGA_VC[SourceClassIndex].TattooTex;
				}
			}
			break;
			
			case `NVA:
			for (i = 0; i < default.CFGA_NVA.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_NVA[i].TunicMesh		= default.CFGA_NVA[SourceClassIndex].TunicMesh;
					default.CFGA_NVA[i].TunicMaterial	= default.CFGA_NVA[SourceClassIndex].TunicMaterial;
					default.CFGA_NVA[i].ShirtTexture	= default.CFGA_NVA[SourceClassIndex].ShirtTexture;
					default.CFGA_NVA[i].HeadMesh		= default.CFGA_NVA[SourceClassIndex].HeadMesh;
					default.CFGA_NVA[i].HairMaterial	= default.CFGA_NVA[SourceClassIndex].HairMaterial;
					default.CFGA_NVA[i].HeadgearMesh	= default.CFGA_NVA[SourceClassIndex].HeadgearMesh;
					default.CFGA_NVA[i].HeadgearMaterial= default.CFGA_NVA[SourceClassIndex].HeadgearMaterial;
					default.CFGA_NVA[i].FaceItemMesh	= default.CFGA_NVA[SourceClassIndex].FaceItemMesh;
					default.CFGA_NVA[i].FacialHairMesh	= default.CFGA_NVA[SourceClassIndex].FacialHairMesh;
					default.CFGA_NVA[i].TattooTex		= default.CFGA_NVA[SourceClassIndex].TattooTex;
				}
			}
			break;
			
			case `NVAU:
			for (i = 0; i < default.CFGA_NVAU.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_NVAU[i].TunicMesh			= default.CFGA_NVAU[SourceClassIndex].TunicMesh;
					default.CFGA_NVAU[i].TunicMaterial		= default.CFGA_NVAU[SourceClassIndex].TunicMaterial;
					default.CFGA_NVAU[i].ShirtTexture		= default.CFGA_NVAU[SourceClassIndex].ShirtTexture;
					default.CFGA_NVAU[i].HeadMesh			= default.CFGA_NVAU[SourceClassIndex].HeadMesh;
					default.CFGA_NVAU[i].HairMaterial		= default.CFGA_NVAU[SourceClassIndex].HairMaterial;
					default.CFGA_NVAU[i].HeadgearMesh		= default.CFGA_NVAU[SourceClassIndex].HeadgearMesh;
					default.CFGA_NVAU[i].HeadgearMaterial	= default.CFGA_NVAU[SourceClassIndex].HeadgearMaterial;
					default.CFGA_NVAU[i].FaceItemMesh		= default.CFGA_NVAU[SourceClassIndex].FaceItemMesh;
					default.CFGA_NVAU[i].FacialHairMesh		= default.CFGA_NVAU[SourceClassIndex].FacialHairMesh;
					default.CFGA_NVAU[i].TattooTex			= default.CFGA_NVAU[SourceClassIndex].TattooTex;
				}
			}
			break;
		}
	}
	else if (SourceClassIndex < `ROCI_COMBATPILOT)
	{
		switch (ArmyIndex)
		{
			case `US:
			for (i = 0; i < default.CFGA_US.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_US[i].TunicMesh		= default.CFGA_US[SourceClassIndex].TunicMesh;
					default.CFGA_US[i].TunicMaterial	= default.CFGA_US[SourceClassIndex].TunicMaterial;
					default.CFGA_US[i].ShirtTexture		= default.CFGA_US[SourceClassIndex].ShirtTexture;
					default.CFGA_US[i].HeadMesh			= default.CFGA_US[SourceClassIndex].HeadMesh;
					default.CFGA_US[i].HairMaterial		= default.CFGA_US[SourceClassIndex].HairMaterial;
					default.CFGA_US[i].HeadgearMesh		= default.CFGA_US[SourceClassIndex].HeadgearMesh;
					default.CFGA_US[i].HeadgearMaterial	= default.CFGA_US[SourceClassIndex].HeadgearMaterial;
					default.CFGA_US[i].FaceItemMesh		= default.CFGA_US[SourceClassIndex].FaceItemMesh;
					default.CFGA_US[i].FacialHairMesh	= default.CFGA_US[SourceClassIndex].FacialHairMesh;
					default.CFGA_US[i].TattooTex		= default.CFGA_US[SourceClassIndex].TattooTex;
				}
			}
			break;
			
			case `USMC:
			for (i = 0; i < default.CFGA_USMC.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_USMC[i].TunicMesh			= default.CFGA_USMC[SourceClassIndex].TunicMesh;
					default.CFGA_USMC[i].TunicMaterial		= default.CFGA_USMC[SourceClassIndex].TunicMaterial;
					default.CFGA_USMC[i].ShirtTexture		= default.CFGA_USMC[SourceClassIndex].ShirtTexture;
					default.CFGA_USMC[i].HeadMesh			= default.CFGA_USMC[SourceClassIndex].HeadMesh;
					default.CFGA_USMC[i].HairMaterial		= default.CFGA_USMC[SourceClassIndex].HairMaterial;
					default.CFGA_USMC[i].HeadgearMesh		= default.CFGA_USMC[SourceClassIndex].HeadgearMesh;
					default.CFGA_USMC[i].HeadgearMaterial	= default.CFGA_USMC[SourceClassIndex].HeadgearMaterial;
					default.CFGA_USMC[i].FaceItemMesh		= default.CFGA_USMC[SourceClassIndex].FaceItemMesh;
					default.CFGA_USMC[i].FacialHairMesh		= default.CFGA_USMC[SourceClassIndex].FacialHairMesh;
					default.CFGA_USMC[i].TattooTex			= default.CFGA_USMC[SourceClassIndex].TattooTex;
				}
			}
			break;
			
			case `MACV:
			for (i = 0; i < default.CFGA_MACV.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_MACV[i].TunicMesh			= default.CFGA_MACV[SourceClassIndex].TunicMesh;
					default.CFGA_MACV[i].TunicMaterial		= default.CFGA_MACV[SourceClassIndex].TunicMaterial;
					default.CFGA_MACV[i].ShirtTexture		= default.CFGA_MACV[SourceClassIndex].ShirtTexture;
					default.CFGA_MACV[i].HeadMesh			= default.CFGA_MACV[SourceClassIndex].HeadMesh;
					default.CFGA_MACV[i].HairMaterial		= default.CFGA_MACV[SourceClassIndex].HairMaterial;
					default.CFGA_MACV[i].HeadgearMesh		= default.CFGA_MACV[SourceClassIndex].HeadgearMesh;
					default.CFGA_MACV[i].HeadgearMaterial	= default.CFGA_MACV[SourceClassIndex].HeadgearMaterial;
					default.CFGA_MACV[i].FaceItemMesh		= default.CFGA_MACV[SourceClassIndex].FaceItemMesh;
					default.CFGA_MACV[i].FacialHairMesh		= default.CFGA_MACV[SourceClassIndex].FacialHairMesh;
					default.CFGA_MACV[i].TattooTex			= default.CFGA_MACV[SourceClassIndex].TattooTex;
				}
			}
			break;
			
			case `ARVN:
			for (i = 0; i < default.CFGA_ARVN.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_ARVN[i].TunicMesh			= default.CFGA_ARVN[SourceClassIndex].TunicMesh;
					default.CFGA_ARVN[i].TunicMaterial		= default.CFGA_ARVN[SourceClassIndex].TunicMaterial;
					default.CFGA_ARVN[i].ShirtTexture		= default.CFGA_ARVN[SourceClassIndex].ShirtTexture;
					default.CFGA_ARVN[i].HeadMesh			= default.CFGA_ARVN[SourceClassIndex].HeadMesh;
					default.CFGA_ARVN[i].HairMaterial		= default.CFGA_ARVN[SourceClassIndex].HairMaterial;
					default.CFGA_ARVN[i].HeadgearMesh		= default.CFGA_ARVN[SourceClassIndex].HeadgearMesh;
					default.CFGA_ARVN[i].HeadgearMaterial	= default.CFGA_ARVN[SourceClassIndex].HeadgearMaterial;
					default.CFGA_ARVN[i].FaceItemMesh		= default.CFGA_ARVN[SourceClassIndex].FaceItemMesh;
					default.CFGA_ARVN[i].FacialHairMesh		= default.CFGA_ARVN[SourceClassIndex].FacialHairMesh;
					default.CFGA_ARVN[i].TattooTex			= default.CFGA_ARVN[SourceClassIndex].TattooTex;
				}
			}
			break;
			
			case `RANGERS:
			for (i = 0; i < default.CFGA_RANGERS.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_RANGERS[i].TunicMesh		= default.CFGA_RANGERS[SourceClassIndex].TunicMesh;
					default.CFGA_RANGERS[i].TunicMaterial	= default.CFGA_RANGERS[SourceClassIndex].TunicMaterial;
					default.CFGA_RANGERS[i].ShirtTexture	= default.CFGA_RANGERS[SourceClassIndex].ShirtTexture;
					default.CFGA_RANGERS[i].HeadMesh		= default.CFGA_RANGERS[SourceClassIndex].HeadMesh;
					default.CFGA_RANGERS[i].HairMaterial	= default.CFGA_RANGERS[SourceClassIndex].HairMaterial;
					default.CFGA_RANGERS[i].HeadgearMesh	= default.CFGA_RANGERS[SourceClassIndex].HeadgearMesh;
					default.CFGA_RANGERS[i].HeadgearMaterial= default.CFGA_RANGERS[SourceClassIndex].HeadgearMaterial;
					default.CFGA_RANGERS[i].FaceItemMesh	= default.CFGA_RANGERS[SourceClassIndex].FaceItemMesh;
					default.CFGA_RANGERS[i].FacialHairMesh	= default.CFGA_RANGERS[SourceClassIndex].FacialHairMesh;
					default.CFGA_RANGERS[i].TattooTex		= default.CFGA_RANGERS[SourceClassIndex].TattooTex;
				}
			}
			break;
			
			case `AUS:
			for (i = 0; i < default.CFGA_AUS.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_AUS[i].TunicMesh		= default.CFGA_AUS[SourceClassIndex].TunicMesh;
					default.CFGA_AUS[i].TunicMaterial	= default.CFGA_AUS[SourceClassIndex].TunicMaterial;
					default.CFGA_AUS[i].ShirtTexture	= default.CFGA_AUS[SourceClassIndex].ShirtTexture;
					default.CFGA_AUS[i].HeadMesh		= default.CFGA_AUS[SourceClassIndex].HeadMesh;
					default.CFGA_AUS[i].HairMaterial	= default.CFGA_AUS[SourceClassIndex].HairMaterial;
					default.CFGA_AUS[i].HeadgearMesh	= default.CFGA_AUS[SourceClassIndex].HeadgearMesh;
					default.CFGA_AUS[i].HeadgearMaterial= default.CFGA_AUS[SourceClassIndex].HeadgearMaterial;
					default.CFGA_AUS[i].FaceItemMesh	= default.CFGA_AUS[SourceClassIndex].FaceItemMesh;
					default.CFGA_AUS[i].FacialHairMesh	= default.CFGA_AUS[SourceClassIndex].FacialHairMesh;
					default.CFGA_AUS[i].TattooTex		= default.CFGA_AUS[SourceClassIndex].TattooTex;
				}
			}
			break;
			
			case `ROK:
			for (i = 0; i < default.CFGA_ROK.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_ROK[i].TunicMesh		= default.CFGA_ROK[SourceClassIndex].TunicMesh;
					default.CFGA_ROK[i].TunicMaterial	= default.CFGA_ROK[SourceClassIndex].TunicMaterial;
					default.CFGA_ROK[i].ShirtTexture	= default.CFGA_ROK[SourceClassIndex].ShirtTexture;
					default.CFGA_ROK[i].HeadMesh		= default.CFGA_ROK[SourceClassIndex].HeadMesh;
					default.CFGA_ROK[i].HairMaterial	= default.CFGA_ROK[SourceClassIndex].HairMaterial;
					default.CFGA_ROK[i].HeadgearMesh	= default.CFGA_ROK[SourceClassIndex].HeadgearMesh;
					default.CFGA_ROK[i].HeadgearMaterial= default.CFGA_ROK[SourceClassIndex].HeadgearMaterial;
					default.CFGA_ROK[i].FaceItemMesh	= default.CFGA_ROK[SourceClassIndex].FaceItemMesh;
					default.CFGA_ROK[i].FacialHairMesh	= default.CFGA_ROK[SourceClassIndex].FacialHairMesh;
					default.CFGA_ROK[i].TattooTex		= default.CFGA_ROK[SourceClassIndex].TattooTex;
				}
			}
			break;
			
			case `ROKA:
			for (i = 0; i < default.CFGA_ROKA.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_ROKA[i].TunicMesh			= default.CFGA_ROKA[SourceClassIndex].TunicMesh;
					default.CFGA_ROKA[i].TunicMaterial		= default.CFGA_ROKA[SourceClassIndex].TunicMaterial;
					default.CFGA_ROKA[i].ShirtTexture		= default.CFGA_ROKA[SourceClassIndex].ShirtTexture;
					default.CFGA_ROKA[i].HeadMesh			= default.CFGA_ROKA[SourceClassIndex].HeadMesh;
					default.CFGA_ROKA[i].HairMaterial		= default.CFGA_ROKA[SourceClassIndex].HairMaterial;
					default.CFGA_ROKA[i].HeadgearMesh		= default.CFGA_ROKA[SourceClassIndex].HeadgearMesh;
					default.CFGA_ROKA[i].HeadgearMaterial	= default.CFGA_ROKA[SourceClassIndex].HeadgearMaterial;
					default.CFGA_ROKA[i].FaceItemMesh		= default.CFGA_ROKA[SourceClassIndex].FaceItemMesh;
					default.CFGA_ROKA[i].FacialHairMesh		= default.CFGA_ROKA[SourceClassIndex].FacialHairMesh;
					default.CFGA_ROKA[i].TattooTex			= default.CFGA_ROKA[SourceClassIndex].TattooTex;
				}
			}
			break;
			
			case `ROKMC:
			for (i = 0; i < default.CFGA_ROKMC.length; i++)
			{
				if (i <= `ROCI_COMMANDER)
				{
					default.CFGA_ROKMC[i].TunicMesh			= default.CFGA_ROKMC[SourceClassIndex].TunicMesh;
					default.CFGA_ROKMC[i].TunicMaterial		= default.CFGA_ROKMC[SourceClassIndex].TunicMaterial;
					default.CFGA_ROKMC[i].ShirtTexture		= default.CFGA_ROKMC[SourceClassIndex].ShirtTexture;
					default.CFGA_ROKMC[i].HeadMesh			= default.CFGA_ROKMC[SourceClassIndex].HeadMesh;
					default.CFGA_ROKMC[i].HairMaterial		= default.CFGA_ROKMC[SourceClassIndex].HairMaterial;
					default.CFGA_ROKMC[i].HeadgearMesh		= default.CFGA_ROKMC[SourceClassIndex].HeadgearMesh;
					default.CFGA_ROKMC[i].HeadgearMaterial	= default.CFGA_ROKMC[SourceClassIndex].HeadgearMaterial;
					default.CFGA_ROKMC[i].FaceItemMesh		= default.CFGA_ROKMC[SourceClassIndex].FaceItemMesh;
					default.CFGA_ROKMC[i].FacialHairMesh	= default.CFGA_ROKMC[SourceClassIndex].FacialHairMesh;
					default.CFGA_ROKMC[i].TattooTex			= default.CFGA_ROKMC[SourceClassIndex].TattooTex;
				}
			}
			break;
		}
	}
	
	StaticSaveConfig();
	
	return true;
}

defaultproperties
{
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_ALL.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_ARVN.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_ARVN_Rangers.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_AUS.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_MACV.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_MP.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_NVA.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_NVAU.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_ROK.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_ROKA.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_ROKMC.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_US.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_USMC.uci)
	`include(GOM3\Classes\GOMPawnHandlerDefaultProperties_VC.uci)
}