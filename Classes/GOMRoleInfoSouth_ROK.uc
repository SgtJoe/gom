//=============================================================================
// GOMRoleInfoSouth_ROK.uc
//=============================================================================
// Basic info for all Southern Republic of Korea Roles.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ROK extends GOMRoleInfo
	abstract;

DefaultProperties
{
	Items[RORIGM_Default]={(
	
	SecondaryWeapons=(class'ROWeap_M1911_Pistol'),
	
	SquadLeaderItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	
	)}
	
	RoleRootClass=class'GOMRoleInfoSouth_ROK'
}
