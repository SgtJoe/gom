//=============================================================================
// GOMWeapon_M1919A6_ActualContent.uc
//=============================================================================
// ARVN M1919A6 Browning LMG (Content class).
// Modified to add barrel change functionality.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M1919A6_ActualContent extends GOMWeapon_M1919A6;

DefaultProperties
{
	ArmsAnimSet=AnimSet'GOM3_WP_ARVN_M1919A6.Anim.WP_M1919'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'GOM3_WP_ARVN_M1919A6.Mesh.M1919A6'
		PhysicsAsset=none
		AnimSets(0)=AnimSet'GOM3_WP_ARVN_M1919A6.Anim.WP_M1919'
		AnimTreeTemplate=AnimTree'GOM3_WP_ARVN_M1919A6.Anim.M1919A6_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'GOM3_WP_ARVN_M1919A6.Mesh.M1919A6_3rd'
		PhysicsAsset=PhysicsAsset'GOM3_WP_ARVN_M1919A6.Phy.M1919A6_3rd_Physics'
		AnimTreeTemplate=AnimTree'GOM3_WP_ARVN_M1919A6.Anim.M1919A6_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	Begin Object Class=ROAmmoBeltMesh Name=AmmoBelt0
		SkeletalMesh=SkeletalMesh'GOM3_WP_ARVN_M1919A6.Mesh.M1919A6_BELT'
		PhysicsAsset=PhysicsAsset'GOM3_WP_ARVN_M1919A6.Phy.M1919_BELT_Physics'
		AnimSets.Add(AnimSet'GOM3_WP_ARVN_M1919A6.Anim.WP_M1919_BELT')
		DepthPriorityGroup=SDPG_Foreground
		bOnlyOwnerSee=true
		MaxAmmoShown=22
	End Object
	AmmoBeltMesh=AmmoBelt0
	
	AmmoBeltSocket=AmmoBeltSocket

	AttachmentClass=class'GOMWeapon_M1919A6_Attach'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M1919_A6.Play_WEP_M1919A6_Loop_3P', FirstPersonCue=AkEvent'WW_WEP_M1919_A6.Play_WEP_M1919A6_Auto_LP')
	WeaponFireLoopEndSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M1919_A6.Play_WEP_M1919A6_Tail_3P', FirstPersonCue=AkEvent'WW_WEP_M1919_A6.Play_WEP_M1919A6_Auto_Tail')
	bLoopingFireSnd(DEFAULT_FIREMODE)=true
	bLoopHighROFSounds(DEFAULT_FIREMODE)=true
}
