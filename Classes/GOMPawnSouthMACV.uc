//=============================================================================
// GOMPawnSouthMACV.uc
//=============================================================================
// Southern Pawn (MACV-SOG).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawnSouthMACV extends GOMPawnSouthUS;

DefaultProperties
{
	BodyMICTemplate=MaterialInstanceConstant'GOM3_CHR_US_MACV.MIC.MACV_Tunic_Long'
}
