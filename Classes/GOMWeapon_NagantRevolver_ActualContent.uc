//=============================================================================
// GOMWeapon_NagantRevolver_ActualContent.uc
//=============================================================================
// Viet Cong Nagant Revolver (Content Class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_NagantRevolver_ActualContent extends GOMWeapon_NagantRevolver;

simulated function SetupArmsAnim()
{
	super.SetupArmsAnim();
	
	// ArmsMesh.AnimSets has slots 0-2-3 filled, so we need to back fill slot 1 and then move to slot 4
	ROPawn(Instigator).ArmsMesh.AnimSets[1] = SkeletalMeshComponent(Mesh).AnimSets[0];
	ROPawn(Instigator).ArmsMesh.AnimSets[4] = SkeletalMeshComponent(Mesh).AnimSets[1];
}

DefaultProperties
{
	ArmsAnimSet=AnimSet'GOM3_WP_VN_VC_REVOLVER.Anim.NagantRevolver_Hands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_VC_REVOLVER.Mesh.Nagant_Revolver'
		PhysicsAsset=None
		AnimSets(0)=AnimSet'GOM3_WP_VN_VC_REVOLVER.Anim.NagantRevolver_Hands'
		AnimSets(1)=AnimSet'GOM3_WP_VN_VC_REVOLVER.Anim.NagantRevolver_Hands_NewReload'
		AnimTreeTemplate=AnimTree'GOM3_WP_VN_VC_REVOLVER.Anim.Sov_M1895_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_VC_REVOLVER.Mesh.Nagant_Revolver_3rd_Master'
		PhysicsAsset=PhysicsAsset'GOM3_WP_VN_VC_REVOLVER.Phy.Nagant_Revolver_3rd_phy'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'GOMWeapon_NagantRevolver_Attach'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M1917.Play_WEP_M1917_Single_3P', FirstPersonCue=AkEvent'WW_WEP_M1917.Play_WEP_M1917_Fire_Single')
	WeaponFireSnd(ALTERNATE_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M1917.Play_WEP_M1917_Single_3P', FirstPersonCue=AkEvent'WW_WEP_M1917.Play_WEP_M1917_Fire_Single')
}
