//=============================================================================
// GOMVoicePack_AUS_2.uc
//=============================================================================
// AUS Voice Pack.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVoicePack_AUS_2 extends ROVoicePackAusTeam02;

defaultproperties
{
	VoiceComs[`VOICECOM_TakeCover]=(Sound="AKEvent'WW_VOX_ANZAC_Voice2.Play_ANZAC_Soldier_2_TakingFireGeneric'")
	VoiceComs[`VOICECOM_AIConfirm]=(Sound="AKEvent'WW_VOX_ANZAC_Voice2.Play_ANZAC_Soldier_2_Confirm'")
	VoiceComs[`VOICECOM_HeloIdleSituation]=(Sound="AKEvent'WW_VOX_ANZAC_Voice2.Play_ANZAC_Pilot_2_HeloIdleGround'")
}
