//=============================================================================
// GOMHUDWidgetWeapon.uc
//=============================================================================
// HUD Widget that shows the current Weapon/Ammo/Tunnel status.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHUDWidgetWeapon extends ROHUDWidgetWeapon;

var bool NoTunnels;

function Initialize(PlayerController HUDPlayerOwner)
{
	super.Initialize(HUDPlayerOwner);
	
	// Grab this value once so we're not constantly pestering ReplicationInfo
	NoTunnels = `NVAmap;
	
	`gom("Display tunnel info?" @ !NoTunnels, 'HUD');
}

function UpdateWidget()
{
	super.UpdateWidget();
	
	if (NoTunnels)
	{
		HUDComponents[ROWC_TunnelSpawnIcon].bVisible = false;
		HUDComponents[ROWC_TunnelSpawnCount].bVisible = false;
		HUDComponents[ROWC_DigTunnelIcon].bVisible = false;
		HUDComponents[ROWC_DigTunnelTextP1].bVisible = false;
		HUDComponents[ROWC_DigTunnelTextP2].bVisible = false;
		HUDComponents[ROWC_DigTunnelTextP3].bVisible = false;
	}
}

defaultproperties
{
	NoTunnels=false
}
