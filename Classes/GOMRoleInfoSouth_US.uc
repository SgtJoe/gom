//=============================================================================
// GOMRoleInfoSouth_US.uc
//=============================================================================
// Basic info for all Southern United States Roles.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_US extends GOMRoleInfo
	abstract;

DefaultProperties
{
	Items[RORIGM_Default]={(
		SecondaryWeapons=(class'ROWeap_M1911_Pistol',class'GOMWeapon_BowieKnife'),
		SquadLeaderItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		SecondaryWeapons=(class'ROWeap_M1911_Pistol',class'ROWeap_M1917_Pistol',class'GOMWeapon_BowieKnife'),
		SquadLeaderItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		SecondaryWeapons=(class'ROWeap_M1911_Pistol',class'GOMWeapon_BowieKnife'),
		SquadLeaderItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		SecondaryWeapons=(class'ROWeap_M1911_Pistol',class'GOMWeapon_BowieKnife'),
		SquadLeaderItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	RoleRootClass=class'GOMRoleInfoSouth_US'
}
