//=============================================================================
// GOMHeli_AH1G_US_Content.uc
//=============================================================================
// AH-1G Cobra Gunship Helicopter (US Army)
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHeli_AH1G_US_Content extends ROHeli_AH1G_Content
	placeable;

`include(GOM3\Classes\GOMHelicoptersCommon.uci)
