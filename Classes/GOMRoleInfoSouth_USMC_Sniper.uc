//=============================================================================
// GOMRoleInfoSouth_USMC_Sniper.uc
//=============================================================================
// United States Marine Corps Sniper Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_USMC_Sniper extends GOMRoleInfoSouth_US_Sniper;

DefaultProperties
{
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M40Scoped_Rifle')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M1DGarand_SniperRifle')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M40Scoped_Rifle')
	)}
}
