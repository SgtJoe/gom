//=============================================================================
// GOMDmgType_RPG2Impact.uc
//=============================================================================
// Damage type for the RPG-2 rocket.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMDmgType_RPG2Impact extends RODmgType_RPG7RocketImpact
	abstract;

DefaultProperties
{
	WeaponClass=GOMWeapon_RPG2_ActualContent
	WeaponShortName="RPG2"
}
