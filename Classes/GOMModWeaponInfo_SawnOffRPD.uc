//=============================================================================
// GOMModWeaponInfo_SawnOffRPD.uc
//=============================================================================
// UI information for the Sawn-Off RPD.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfo_SawnOffRPD extends ROModWeaponInfo;

DefaultProperties
{
	ModWeaponClass=GOMWeapon_RPD_SawnOff_ActualContent
	
	InventoryIconTexturePath="GOM3_UI.WP_Select.UI_HUD_WeaponSelect_RPD_SawnOff"
	
	AmmoIconTexturePath="VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_RPD_DRUM"
	
	KillIconTexturePath="GOM3_UI.WP_Select.UI_HUD_WeaponSelect_RPD_SawnOff"
	bHasSquareKillIcon=false
}
