//=============================================================================
// GOMWeapon_PPS_Attach.uc
//=============================================================================
// North Vietnamese Army PPS-43 SMG (Attachment class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_PPS_Attach extends ROWeaponAttachment;

defaultproperties
{
	TriggerHoldDuration=0.2
	
	CarrySocketName=WeaponSling
	ThirdPersonHandsAnim=M3_Handpose
	IKProfileName=mp40
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_NVA_PPS.Mesh.PPS42_3rd_Master_UPGD2'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy_Bounds.M3_3rd_Bounds_Physics'
		AnimSets(0)=AnimSet'WP_VN_3rd_Master.Anim.M3_3rd_Anim'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.M3A1_SMG_3rd_Tree'
		CullDistance=5000
	End Object

	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_3rdP_SMG'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	WeaponClass=class'GOMWeapon_PPS'
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_PPSH'
	
	CHR_AnimSet=AnimSet'CHR_VN_Playeranim_Master.Weapons.CHR_M3'
	
	FireAnim=Shoot
	FireLastAnim=Shoot_Last
	IdleAnim=Idle
	IdleEmptyAnim=Idle_Empty
}
