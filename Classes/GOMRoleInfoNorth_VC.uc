//=============================================================================
// GOMRoleInfoNorth_VC.uc
//=============================================================================
// Basic info for all Viet Cong Roles.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_VC extends GOMRoleInfo
	abstract;

DefaultProperties
{
	Items[RORIGM_Default]={(
		SecondaryWeapons=(class'GOMWeapon_NagantRevolver'),
		SquadLeaderItems=(class'ROItem_TunnelTool',class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		SecondaryWeapons=(class'GOMWeapon_NagantRevolver'),
		SquadLeaderItems=(class'ROItem_TunnelTool',class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		SecondaryWeapons=(class'GOMWeapon_NagantRevolver',class'ROWeap_TT33_Pistol'),
		SquadLeaderItems=(class'ROItem_TunnelTool',class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		SecondaryWeapons=(class'ROWeap_TT33_Pistol'),
		SquadLeaderItems=(class'ROItem_TunnelTool',class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	RoleRootClass=class'GOMRoleInfoNorth_VC'
}
