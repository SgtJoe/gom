//=============================================================================
// GOMVehicle_M113_ACAV_ActualContent.uc
//=============================================================================
// South Korean M113 ACAV (Content class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicle_M113_ACAV_ActualContent extends GOMVehicle_M113_ACAV
	placeable;

DefaultProperties
{
	Begin Object Name=ROSVehicleMesh
		SkeletalMesh=SkeletalMesh'GOM3_VH_ROK_M113.Mesh.ROK_M113_ACAV_Rig'
		LightingChannels=(Dynamic=TRUE,Unnamed_1=TRUE,bInitialized=TRUE)
		AnimTreeTemplate=AnimTree'GOM3_VH_ROK_M113.Anim.AT_VH_ROK_M113_ACAV'
		PhysicsAsset=PhysicsAsset'GOM3_VH_ROK_M113.Phy.ROK_M113_Physics'
	End Object
	
	DestroyedSkeletalMesh=SkeletalMesh'GOM3_VH_ROK_M113.Mesh.ROK_M113_ACAV_Rig_WRECK'
	DestroyedPhysicsAsset=PhysicsAsset'GOM3_VH_ROK_M113.Phy.ROK_M113_ACAV_Rig_WRECK_Physics'
	
	DestroyedMats(0)=MaterialInstanceConstant'GOM3_VH_ROK_M113.MIC.ROKA_M113_WRECK_Hull'
	DestroyedMats(1)=MaterialInstanceConstant'GOM3_VH_ROK_M113.MIC.ROKA_M113_WRECK_Tracks'
	
	HUDBodyTexture=Texture2D'GOM3_UI.Vehicle.UI_HUD_VH_M113_Base'
	
	SeatProxies(`M113_Driver)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`M113_Driver,
		PositionIndex=1
	)}
	
	SeatProxies(`M113_MG)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`M113_MG,
		PositionIndex=1
	)}
	
	SeatProxies(`M113_MGR)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`M113_MGR,
		PositionIndex=1
	)}
	
	SeatProxies(`M113_MGL)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`M113_MGL,
		PositionIndex=1
	)}
	
	SeatProxies(`M113_PASS1)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`M113_PASS1,
		PositionIndex=0
	)}
	
	SeatProxies(`M113_PASS2)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`M113_PASS2,
		PositionIndex=0
	)}
	
	SeatProxies(`M113_PASS3)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`M113_PASS3,
		PositionIndex=0
	)}
	
	SeatProxies(`M113_PASS4)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`M113_PASS4,
		PositionIndex=0
	)}
	
	SeatProxyAnimSet=AnimSet'VH_VN_ARVN_M113_APC.Anim.CHR_M113_Anim_Master'
	
	Begin Object class=StaticMeshComponent name=ExtBodyAttachment0
		StaticMesh=StaticMesh'GOM3_VH_ROK_M113.Mesh.ROK_M113_ACAV_SM_Exterior'
		LightingChannels=(Dynamic=TRUE,Unnamed_1=FALSE,bInitialized=TRUE)
		LightEnvironment = MyLightEnvironment
		CastShadow=true
		DepthPriorityGroup=SDPG_Foreground
		HiddenGame=true
		CollideActors=false
		BlockActors=false
		BlockZeroExtent=false
		BlockNonZeroExtent=false
	End Object
	
	Begin Object class=StaticMeshComponent name=IntBodyAttachment0
		StaticMesh=StaticMesh'GOM3_VH_ROK_M113.Mesh.ROK_M113_ACAV_SM_Interior'
		LightingChannels=(Dynamic=TRUE,Unnamed_1=TRUE,bInitialized=TRUE)
		LightEnvironment = MyInteriorLightEnvironment
		CastShadow=false
		DepthPriorityGroup=SDPG_Foreground
		HiddenGame=true
		CollideActors=false
		BlockActors=false
		BlockZeroExtent=false
		BlockNonZeroExtent=false
	End Object
	
	Begin Object class=StaticMeshComponent name=IntBodyAttachment1
		StaticMesh=StaticMesh'GOM3_VH_ROK_M113.Mesh.ROK_M113_ACAV_SM_Interior2'
		LightingChannels=(Dynamic=TRUE,Unnamed_1=TRUE,bInitialized=TRUE)
		LightEnvironment = MyInteriorLightEnvironment
		CastShadow=false
		DepthPriorityGroup=SDPG_Foreground
		HiddenGame=true
		CollideActors=false
		BlockActors=false
		BlockZeroExtent=false
		BlockNonZeroExtent=false
	End Object
	
	MeshAttachments(0)={(AttachmentName=ExtBodyComponent, Component=ExtBodyAttachment0,AttachmentTargetName=SMattach)}
	MeshAttachments(1)={(AttachmentName=IntBodyComponent, Component=IntBodyAttachment0,AttachmentTargetName=SMattach)}
	MeshAttachments(2)={(AttachmentName=IntBodyComponent2,Component=IntBodyAttachment1,AttachmentTargetName=SMattach)}
	
	Exhaust_FXSocket=Exhaust
	CabinL_FXSocket=Sound_L
	CabinR_FXSocket=Sound_R
	TreadL_FXSocket=Sound_L
	TreadR_FXSocket=Sound_R
	
	EngineStartupSeconds=1.6
	
	Begin Object Class=AkComponent Name=StartEngineLSound
		bStopWhenOwnerDestroyed=true
	End Object
	Begin Object Class=AkComponent Name=StartEngineRSound
		bStopWhenOwnerDestroyed=true
	End Object
	EngineStartLeftSound=StartEngineLSound
	EngineStartRightSound=StartEngineRSound
	EngineStartLeftSoundEvent=AkEvent'GOM3_AUD_SFX_VH.Play_APC_Engine_Start'
	EngineStartRightSoundEvent=AkEvent'GOM3_AUD_SFX_VH.Play_APC_Engine_Start'
	
	Begin Object Class=AkComponent Name=StopEngineSound
		bStopWhenOwnerDestroyed=true
	End Object
	EngineStopSound=StopEngineSound
	EngineStopSoundEvent=AkEvent'GOM3_AUD_SFX_VH.Play_APC_Engine_Stop'
	
	// Begin Object Class=AkComponent Name=IdleEngineLeftSound
		// bStopWhenOwnerDestroyed=true
	// End Object
	// Begin Object Class=AkComponent Name=IdleEngineRighSound
		// bStopWhenOwnerDestroyed=true
	// End Object
	// EngineIntLeftSound=IdleEngineLeftSound
	// EngineIntRightSound=IdleEngineRighSound
	// EngineIntLeftSoundEvent=AkEvent'GOM3_AUD_SFX_VH.Play_APC_Engine_Idle'
	// EngineIntRightSoundEvent=AkEvent'GOM3_AUD_SFX_VH.Play_APC_Engine_Idle'
	
	Begin Object Class=AkComponent Name=IdleEngineExhaustSound
		bStopWhenOwnerDestroyed=true
	End Object
	EngineSound=IdleEngineExhaustSound
	EngineSoundEvent=AkEvent'GOM3_AUD_SFX_VH.Play_APC_Engine_Idle'
	
	EngineIdleDamagedSound=AkEvent'GOM3_AUD_SFX_VH.Play_APC_Engine_Run'
	
	// Begin Object Class=AkComponent Name=TrackLSound
		// bStopWhenOwnerDestroyed=true
	// End Object
	// Begin Object Class=AkComponent Name=TrackRSound
		// bStopWhenOwnerDestroyed=true
	// End Object
	// TrackLeftSound=TrackLSound
	// TrackRightSound=TrackRSound
	// TrackLeftSoundEvent=AkEvent'GOM3_AUD_SFX_VH.Play_APC_Tracks'
	// TrackRightSoundEvent=AkEvent'GOM3_AUD_SFX_VH.Play_APC_Tracks'
	
	ShiftUpSound=AkEvent'GOM3_AUD_SFX_VH.Play_APC_Engine_Shift'
}
