//=============================================================================
// GOMRoleInfo.uc
//=============================================================================
// Basic info for all Roles.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfo extends RORoleInfo
	abstract;

DefaultProperties
{
	bAllowPistolsInRealism=false
}
