//=============================================================================
// GOMRoleInfoSouth_ARVNRanger_Grunt.uc
//=============================================================================
// Republic of Vietnam Rangers Grunt Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ARVNRanger_Grunt extends GOMRoleInfoSouth_ARVN;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=1
	ClassIndex=`ROCI_RIFLEMAN
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle',class'ROWeap_M1Garand_Rifle'),
		OtherItems=(class'ROWeap_M61_Grenade')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle_Late',class'ROWeap_M1Garand_Rifle'),
		OtherItems=(class'ROWeap_M61_Grenade')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_grunt'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_guerilla'
}
