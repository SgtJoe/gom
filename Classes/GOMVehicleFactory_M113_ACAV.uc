//=============================================================================
// GOMVehicleFactory_M113_ACAV.uc
//=============================================================================
// South Korean M113 APC ACAV (Factory class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleFactory_M113_ACAV extends GOMVehicleFactory;

defaultproperties
{
	Begin Object Name=SVehicleMesh
		SkeletalMesh=SkeletalMesh'GOM3_VH_ROK_M113.Mesh.ROK_M113_PHAT'
	End Object
	
	VehicleClass=class'GOMVehicle_M113_ACAV_ActualContent'
}
