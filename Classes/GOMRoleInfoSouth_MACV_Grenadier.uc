//=============================================================================
// GOMRoleInfoSouth_MACV_Grenadier.uc
//=============================================================================
// MACV-SOG Grenadier Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_MACV_Grenadier extends GOMRoleInfoSouth_MACV;

DefaultProperties
{
	RoleType=RORIT_Support
	ClassTier=2
	ClassIndex=`ROCI_HEAVY
	
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'ROWeap_M79_GrenadeLauncher',class'ROWeap_XM177E1_Carbine'),
	
	DisableSecondaryForPrimary=(false, true)
	
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_grenadier'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_grenadier'
}
