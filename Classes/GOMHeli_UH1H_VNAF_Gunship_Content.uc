//=============================================================================
// GOMHeli_UH1H_VNAF_Gunship_Content.uc
//=============================================================================
// UH-1H Iroquois Gunship Helicopter (VNAF)
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHeli_UH1H_VNAF_Gunship_Content extends ROHeli_UH1H_Gunship_Content
	placeable;

`include(GOM3\Classes\GOMHelicoptersCommon.uci)

defaultproperties
{
	Begin Object Name=ROSVehicleMesh
		Materials(0)=MaterialInstanceConstant'VH_VN_ARVN_UH1H.Materials.M_ARVN_UH1H_Huey'
	End Object
}
