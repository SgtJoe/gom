//=============================================================================
// GOMRoleInfoSouth_USMC_Grenadier.uc
//=============================================================================
// United States Marine Corps Grenadier Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_USMC_Grenadier extends GOMRoleInfoSouth_US_Grenadier;

DefaultProperties
{
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M79_GrenadeLauncherSmoke',class'GOMWeapon_M1_Garand_M7'),
		DisableSecondaryForPrimary=(false, true),
		OtherItems=(class'GOMWeapon_M7RifleGrenade'),
		OtherItemsStartIndexForPrimary=(0, 0),
		NumOtherItemsForPrimary=(255, 0)
	)}
}
