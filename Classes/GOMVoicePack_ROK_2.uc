//=============================================================================
// GOMVoicePack_ROK_2.uc
//=============================================================================
// Korean Voice Pack.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVoicePack_ROK_2 extends ROVoicePack;

defaultproperties
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////// INFANTRY /////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// INF_Attack
	VoiceComs[`VOICECOM_Attack]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Attack'")
	VoiceComs[`VOICECOM_SLSupressiveFire]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Attack'")
	VoiceComs[`VOICECOM_SLAttackTank]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Attack'")
	VoiceComs[`VOICECOM_SLAttackHelo]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Attack'")
	VoiceComs[`VOICECOM_TLSupressiveFire]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Attack'")
	VoiceComs[`VOICECOM_TLAttackTank]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Attack'")
	VoiceComs[`VOICECOM_TLAttackHelo]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Attack'")
	
	// INF_AttackObjective
	VoiceComs[`VOICECOM_SLAttack]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_AttackObjective'")
	VoiceComs[`VOICECOM_TLAttack]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_AttackObjective'")
	
	// INF_Charging
	VoiceComs[`VOICECOM_Charging]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Charging'")
	
	// INF_Confirm
	VoiceComs[`VOICECOM_Confirm]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Confirm'")
	VoiceComs[`VOICECOM_SLConfirm]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Confirm'")
	VoiceComs[`VOICECOM_TLConfirm]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Confirm'")
	VoiceComs[`VOICECOM_AIConfirm]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Confirm'")
	
	// INF_DefendObjective
	VoiceComs[`VOICECOM_SLDefend]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_DefendObjective'")
	VoiceComs[`VOICECOM_TLDefend]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_DefendObjective'")
	
	// INF_EnemyDeath
	VoiceComs[`VOICECOM_EnemyDeath]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemyDeath'")
	VoiceComs[`VOICECOM_EnemyDeath_Hero]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemyDeath'")
	VoiceComs[`VOICECOM_EnemyDeath_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemyDeath'")
	VoiceComs[`VOICECOM_EnemyDeath_Suppressed]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemyDeath'")
	VoiceComs[`VOICECOM_EnemyHeloDeath]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemyDeath'")
	
	// INF_EnemyDeathUnknown
	VoiceComs[`VOICECOM_EnemyDeathUnknown]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemyDeathUnknown'")
	
	// INF_EnemySpotted_Engineer
	VoiceComs[`VOICECOM_EnemySpottedEngineer]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Engineer'")
	VoiceComs[`VOICECOM_EnemySpottedEngineer_Special]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Engineer'")
	
	// INF_EnemySpotted_Generic
	VoiceComs[`VOICECOM_InfantrySpotted]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Generic'")
	VoiceComs[`VOICECOM_EnemySpottedInfantry]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Generic'")
	
	// INF_EnemySpotted_MG
	VoiceComs[`VOICECOM_TakingFireMachineGunner]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_MG'")
	VoiceComs[`VOICECOM_EnemySpottedMGer]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_MG'")
	
	// INF_EnemySpotted_RPG
	VoiceComs[`VOICECOM_EnemySpottedAntiTank]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_RPG'")
	
	// INF_EnemySpotted_Sniper
	VoiceComs[`VOICECOM_TakingFireSniper]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Sniper'")
	VoiceComs[`VOICECOM_EnemySpottedSniper]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Sniper'")
	
	// INF_EnemySpotted_Tank
	VoiceComs[`VOICECOM_TakingFireTank]=(Type=ROVCT_TeamRadius,  Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Tank'")
	VoiceComs[`VOICECOM_EnemySpottedTank]=						(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Tank'")
	
	// INF_EnemySpotted_Transport
	VoiceComs[`VOICECOM_EnemySpottedTransport]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Transport'")
	
	// INF_EnemySpotted_Trap
	VoiceComs[`VOICECOM_SpottedExplosive]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Trap'")
	
	// INF_EnemySpotted_Tunnel
	VoiceComs[`VOICECOM_EnemySpottedTunnel]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_EnemySpotted_Tunnel'")
	
	// INF_Follow
	VoiceComs[`VOICECOM_SLFollow]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Follow'")
	VoiceComs[`VOICECOM_TLFollow]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Follow'")
	
	// INF_FriendlyDeath
	VoiceComs[`VOICECOM_FriendlyDeath]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_FriendlyDeath'")
	VoiceComs[`VOICECOM_FriendlyDeath_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_FriendlyDeath'")
	VoiceComs[`VOICECOM_FriendlyDeath_Hero]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_FriendlyDeath'")
	VoiceComs[`VOICECOM_FriendlyHeloDeathInf]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_FriendlyDeath'")
	
	// INF_FriendlyFire
	VoiceComs[`VOICECOM_FriendlyFire]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_FriendlyFire'")
	VoiceComs[`VOICECOM_SawFriendlyFire]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_FriendlyFire'")
	
	// INF_IdleChatter
	VoiceComs[`VOICECOM_IdleSituation1]=					(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation1_LowMorale]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation2]=					(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation2_LowMorale]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation3]=					(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation3_LowMorale]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation3_HighMorale]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituationOfficer]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituationOfficer_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituationOfficer_HighMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleEnemyLocation]=					(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleEnemyLocation_LowMorale]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleEnemyLocationOfficer]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_IdleEnemyLocationOfficer_LowMorale]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	VoiceComs[`VOICECOM_SoldierHurt]=						(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_IdleChatter'")
	
	// INF_LosingObjective
	VoiceComs[`VOICECOM_LosingObjective]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_LosingObjective'")
	VoiceComs[`VOICECOM_LosingObjectiveOfficer]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_LosingObjective'")
	VoiceComs[`VOICECOM_LosingObjective_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_LosingObjective'")
	
	// INF_MoveOut
	VoiceComs[`VOICECOM_SLMove]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_MoveOut'")
	VoiceComs[`VOICECOM_TLMove]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_MoveOut'")
	
	// INF_Negative
	VoiceComs[`VOICECOM_Negative]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Negative'")
	VoiceComs[`VOICECOM_SLReject]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Negative'")
	
	// INF_NoAmmo
	VoiceComs[`VOICECOM_NeedAmmo]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_NoAmmo'")
	VoiceComs[`VOICECOM_LowOnAmmo]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_NoAmmo'")
	VoiceComs[`VOICECOM_LowOnAmmo_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_NoAmmo'")
	VoiceComs[`VOICECOM_OutOfAmmo]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_NoAmmo'")
	VoiceComs[`VOICECOM_OutOfAmmoMGer]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_NoAmmo'")
	
	// INF_Reloading
	VoiceComs[`VOICECOM_Reloading]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Reloading'")
	VoiceComs[`VOICECOM_Reloading_Suppressed]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Reloading'")
	
	// INF_RequestArtyCoordinates
	VoiceComs[`VOICECOM_TLRequestArtyCoords]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestArtyCoordinates'")
	
	// INF_RequestOrders
	VoiceComs[`VOICECOM_RequestOrders]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestOrders'")
	VoiceComs[`VOICECOM_SLRequestOrders]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestOrders'")
	
	// INF_RequestSupport_Air
	VoiceComs[`VOICECOM_NeedHelo]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestSupport_Air'")
	
	// INF_RequestSupport_Artillery
	VoiceComs[`VOICECOM_SLRequestArty]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestSupport_Artillery'")
	VoiceComs[`VOICECOM_NeedArtillery]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestSupport_Artillery'")
	
	// INF_RequestSupport_Engineer
	VoiceComs[`VOICECOM_NeedExplosives]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestSupport_Engineer'")
	
	// INF_RequestSupport_Generic
	VoiceComs[`VOICECOM_RequestSupport]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestSupport_Generic'")
	
	// INF_RequestSupport_MG
	VoiceComs[`VOICECOM_NeedMGSupport]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestSupport_MG'")
	
	// INF_RequestSupport_Recon
	VoiceComs[`VOICECOM_NeedRecon]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestSupport_Recon'")
	
	// INF_RequestSupport_Smoke
	VoiceComs[`VOICECOM_NeedSmoke]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_RequestSupport_Smoke'")
	
	// INF_Resume
	VoiceComs[`VOICECOM_SLResume]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Resume'")
	VoiceComs[`VOICECOM_TLResume]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Resume'")
	
	// INF_Retreat
	VoiceComs[`VOICECOM_Retreat]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Retreat'")
	VoiceComs[`VOICECOM_Retreat_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Retreat'")
	
	// INF_Sorry
	VoiceComs[`VOICECOM_Sorry]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Sorry'")
	
	// INF_SpawnSpeech
	VoiceComs[`VOICECOM_SpawnAttacking]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnAttacking_HighMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnAttacking_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnDefending]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnDefending_HighMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnDefending_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnNeutral]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_SpawnSpeech'")
	
	// INF_Suppressed
	VoiceComs[`VOICECOM_Suppressed]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Suppressed'")
	VoiceComs[`VOICECOM_Suppressed_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Suppressed'")
	VoiceComs[`VOICECOM_Suppressed_Hero]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Suppressed'")
	
	// INF_Suppressing
	VoiceComs[`VOICECOM_Suppressing]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Suppressing'")
	VoiceComs[`VOICECOM_Suppressing_Hero]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Suppressing'")
	
	// INF_TakingFire
	VoiceComs[`VOICECOM_TakeCover]=						(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingFire'")
	VoiceComs[`VOICECOM_IncomingArtillery]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingFire'")
	VoiceComs[`VOICECOM_IncomingArtillery_Suppressed]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingFire'")
	VoiceComs[`VOICECOM_IncomingGunship]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingFire'")
	VoiceComs[`VOICECOM_IncomingAirstrike]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingFire'")
	VoiceComs[`VOICECOM_Grenade]=						(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingFire'")
	VoiceComs[`VOICECOM_Satchel]=						(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingFire'")
	VoiceComs[`VOICECOM_TakingFireUnknown]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingFire'")
	VoiceComs[`VOICECOM_TakingFireUnknown_Hero]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingFire'")
	VoiceComs[`VOICECOM_TakingFireInfantry]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingFire'")
	
	// INF_TakingObjective
	VoiceComs[`VOICECOM_TakingObjective]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingObjective'")
	VoiceComs[`VOICECOM_TakingObjectiveOfficer]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingObjective'")
	VoiceComs[`VOICECOM_TakingObjective_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TakingObjective'")
	
	// INF_Taunts
	VoiceComs[`VOICECOM_Taunt]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Taunts'")
	
	// INF_Thanks
	VoiceComs[`VOICECOM_Thanks]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Thanks'")
	
	// INF_ThrowingGrenade
	VoiceComs[`VOICECOM_ThrowingGrenade]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_ThrowingGrenade'")
	
	// INF_ThrowingSatchel
	VoiceComs[`VOICECOM_ThrowingSatchel]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_ThrowingSatchel'")
	
	// INF_ThrowingSmoke
	VoiceComs[`VOICECOM_ThrowingSmoke]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_ThrowingSmoke'")
	
	// INF_TunnelDestroyed
	VoiceComs[`VOICECOM_EnemyTunnelDestroyed]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_TunnelDestroyed'")
	
	// INF_Wounded
	VoiceComs[`VOICECOM_DeathHeart]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Wounded'")
	VoiceComs[`VOICECOM_DeathStomach]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Wounded'")
	VoiceComs[`VOICECOM_DeathNeck]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Wounded'")
	VoiceComs[`VOICECOM_DyingFast]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Wounded'")
	VoiceComs[`VOICECOM_DyingSlow]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Wounded'")
	VoiceComs[`VOICECOM_Wounded]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_INF_2_Wounded'")
	
	VoiceComs[`VOICECOM_Burning]=(Sound="AkEvent'WW_VOX_VC_Voice1.Play_VC_Soldier_1_DeathByFire'")
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////// HELICOPTER ///////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// HEL_Damaged
	VoiceComs[`VOICECOM_HeloEngineDamaged]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Damaged'")
	VoiceComs[`VOICECOM_HeloMainRotorDamaged]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Damaged'")
	VoiceComs[`VOICECOM_HeloTailRotorDamaged]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Damaged'")
	
	// HEL_Dead_Copilot
	VoiceComs[`VOICECOM_HeloCopilotDead]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Dead_Copilot'")
	
	// HEL_Dead_Gunner
	VoiceComs[`VOICECOM_HeloDoorGunnerDead]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Dead_Gunner'")
	
	// HEL_Dead_Passenger
	VoiceComs[`VOICECOM_HeloPassengerDead]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Dead_Passenger'")
	
	// HEL_Dead_Pilot
	VoiceComs[`VOICECOM_HeloPilotDead]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Dead_Pilot'")
	
	// HEL_Destroyed
	VoiceComs[`VOICECOM_HeloTailRotorDestroyed]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Destroyed'")
	VoiceComs[`VOICECOM_HeloLeftSkidDestroyed]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Destroyed'")
	VoiceComs[`VOICECOM_HeloRightSkidDestroyed]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Destroyed'")
	
	// HEL_EnemySpotted
	VoiceComs[`VOICECOM_HeloEnemySpottedInfantry]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_EnemySpotted'")
	
	// HEL_GetOut
	VoiceComs[`VOICECOM_HeloGetOut]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_GetOut'")
	
	// HEL_GoingDown
	VoiceComs[`VOICECOM_HeloBothPilotsDead]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_GoingDown'")
	VoiceComs[`VOICECOM_HeloEngineDestroyed]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_GoingDown'")
	VoiceComs[`VOICECOM_HeloGoingDown]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_GoingDown'")
	
	// HEL_Grounded
	VoiceComs[`VOICECOM_HeloEngineDestroyedGround]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Grounded'")
	VoiceComs[`VOICECOM_HeloMainRotorDestroyedGround]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Grounded'")
	
	// HEL_Hit
	VoiceComs[`VOICECOM_HeloHit]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Hit'")
	
	// HEL_IdleChatter
	VoiceComs[`VOICECOM_HeloIdleVehicleGood]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_IdleChatter'")
	
	// HEL_IdleChatter_Damaged
	VoiceComs[`VOICECOM_HeloIdleVehicleBad]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_IdleChatter_Damaged'")
	
	// HEL_IdleChatter_Destroyed
	VoiceComs[`VOICECOM_HeloIdleVehicleHorrible]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_IdleChatter_Destroyed'")
	
	// HEL_IdleChatter_Ground
	VoiceComs[`VOICECOM_HeloIdleSituation]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_IdleChatter_Ground'")
	
	// HEL_LowAmmo
	VoiceComs[`VOICECOM_HeloLowOnAmmo]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_LowAmmo'")
	
	// HEL_NoAmmo
	VoiceComs[`VOICECOM_HeloOutOfAmmo]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_NoAmmo'")
	
	// HEL_Repairing
	VoiceComs[`VOICECOM_HeloStartRearm]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_Repairing'")
	
	// HEL_RepairsFinished
	VoiceComs[`VOICECOM_HeloFinishRearm]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_RepairsFinished'")
	
	// HEL_SAM_Incoming
	VoiceComs[`VOICECOM_HeloIncomingMissile]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_SAM_Incoming'")
	
	// HEL_SAM_Warning
	VoiceComs[`VOICECOM_HeloAntiAirWarning]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_SAM_Warning'")
	
	// HEL_UnderFire
	VoiceComs[`VOICECOM_HeloUnderFire]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_UnderFire'")
	VoiceComs[`VOICECOM_HeloTakingFriendlyFire]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_UnderFire'")
	
	// HEL_UnderFire_RPG
	VoiceComs[`VOICECOM_HeloIncomingRPG]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_HEL_2_UnderFire_RPG'")
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////// TANK ///////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// TNK_IdleChatter
	VoiceComs[`VOICECOM_TankIdleSituation]=						(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_IdleChatter'")
	VoiceComs[`VOICECOM_TankIdleSituation_LowMorale]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_IdleChatter'")
	VoiceComs[`VOICECOM_TankIdleCommanderSituation]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_IdleChatter'")
	VoiceComs[`VOICECOM_TankIdleCommanderSituation_LowMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_IdleChatter'")
	VoiceComs[`VOICECOM_TankIdleCommanderSituation_HighMorale]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_IdleChatter'")
	VoiceComs[`VOICECOM_TankIdleVehicleGood]=					(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_IdleChatter'")
	
	// TNK_IdleChatter_Damaged
	VoiceComs[`VOICECOM_TankIdleVehicleBad]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_IdleChatter_Damaged'")
	
	// TNK_IdleChatter_Destroyed
	VoiceComs[`VOICECOM_TankIdleVehicleHorrible]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_IdleChatter_Destroyed'")
	
	// TNK_LoadedCannon
	VoiceComs[`VOICECOM_TankCannonReloaded]=(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_LoadedCannon'")
	
	// TNK_UnderTankFire
	VoiceComs[`VOICECOM_TankDriverDead]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankGunnerDead]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankLoaderDead]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankHullGunnerDead]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankEngineDamaged]=				(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankEngineDestroyed]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankMainGunDestroyed]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankHullMGDestroyed]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankLeftTrackDestroyed]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankRightTrackDestroyed]=		(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankBrakesDestroyed]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankGearBoxDestroyed]=			(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankTurretTraverseDestroyed]=	(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankHitFront]=					(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankHitBack]=					(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankHitLeft]=					(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankHitRight]=					(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
	VoiceComs[`VOICECOM_TankUnderFire]=					(Sound="AkEvent'GOM3_AUD_VOX_KOR_2.Play_TNK_2_UnderTankFire'")
}
