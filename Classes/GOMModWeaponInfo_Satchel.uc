//=============================================================================
// GOMModWeaponInfo_Satchel.uc
//=============================================================================
// UI information for the Satchel Charge.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfo_Satchel extends ROModWeaponInfo;

DefaultProperties
{
	ModWeaponClass=GOMWeapon_Satchel_ActualContent
	
	InventoryIconTexturePath="GOM3_UI.WP_Select.WP_Select_Satchel"
	
	AmmoIconTexturePath="VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_C4"
	
	KillIconTexturePath="GOM3_UI.WP_KillIcon.WP_KillIcon_Satchel"
	bHasSquareKillIcon=true
}
