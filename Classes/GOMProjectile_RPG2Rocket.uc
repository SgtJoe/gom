//=============================================================================
// GOMProjectile_RPG2Rocket.uc
//=============================================================================
// Projectile for the Viet Cong RPG-2 Rocket Launcher.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMProjectile_RPG2Rocket extends PG7VRocket;

defaultproperties
{
	MyDamageType=class'GOMDmgType_RPG2'
	ImpactDamageType=class'GOMDmgType_RPG2Impact'
	GeneralDamageType=class'GOMDmgType_RPG2General'
}
