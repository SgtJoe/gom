//=============================================================================
// GOMCharCustMannequin.uc
//=============================================================================
// Essentially an in-game mannequin that shows you what your chosen
// clothes look like before you put them on.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMCharCustMannequin extends ROCharCustMannequin
	notplaceable;

var SkeletalMesh HairMesh;
var ROSkeletalMeshComponent ThirdPersonHairMeshComponent;

var MaterialInstanceConstant BodyMICTemplate2;
var MaterialInstanceConstant BodyMIC2;
var MaterialInstanceConstant GearMIC;
var MaterialInstanceConstant GearMIC2;
var MaterialInstanceConstant GearMIC3;

event PostBeginPlay()
{
	// ROCharCustMannequin::PostBeginPlay() does nothing but set the PawnHandlerClass right back to the value hardcoded in GameInfo, so we just skip it
	super(Actor).PostBeginPlay();
	
	PawnHandlerClass = class'GOMPawnHandler';
}

function UpdateMannequin(byte TeamIndex, byte ArmyIndex, bool bPilot, int ClassIndex, byte HonorLevel, byte TunicID, byte TunicMaterialID, byte ShirtID, byte HeadID, byte HairID, byte HeadgearID, byte HeadgearMatID, byte FaceItemID, byte FacialHairID, byte TattooID, optional bool bMainMenu)
{
	local Texture2D ShirtD, ShirtN, ShirtS, TattooTex;
	local byte byteDisposal, bNoFacialHair, bPilotByte;
	local float TattooUOffset, TattooVOffset, TattooDrawScale, HonourPct;
	local rotator InitialRot;
	
	if (ArmyIndex == 255)
		return;
	
	InitialRot = mesh.Rotation;
	
	DisplayedCharConfig.TunicMesh = TunicID;
	DisplayedCharConfig.TunicMaterial = TunicMaterialID;
	DisplayedCharConfig.ShirtTexture = ShirtID;
	DisplayedCharConfig.HeadMesh = HeadID;
	DisplayedCharConfig.HairMaterial = HairID;
	DisplayedCharConfig.HeadgearMesh = HeadgearID;
	DisplayedCharConfig.HeadgearMat = HeadgearMatID;
	DisplayedCharConfig.FaceItemMesh = FaceItemID;
	DisplayedCharConfig.FacialHairMesh = FacialHairID;
	DisplayedCharConfig.TattooTex = TattooID;
	
	if( HeadAndArmsMIC == none )
		HeadAndArmsMIC = new class'MaterialInstanceConstant';
	if( BodyMIC == none )
		BodyMIC = new class'MaterialInstanceConstant';
	if( BodyMIC2 == none )
		BodyMIC2 = new class'MaterialInstanceConstant';
	if( GearMIC == none )
		GearMIC = new class'MaterialInstanceConstant';
	if( GearMIC2 == none )
		GearMIC2 = new class'MaterialInstanceConstant';
	if( GearMIC3 == none )
		GearMIC3 = new class'MaterialInstanceConstant';
	if( HeadgearMIC == none )
		HeadgearMIC = new class'MaterialInstanceConstant';
	if( HairMIC == none && HairMICTemplate != none )
		HairMIC = new class'MaterialInstanceConstant';
	
	if( ThirdPersonHeadgearMeshComponent.AttachedToSkelComponent != none )
		Mesh.DetachComponent(ThirdPersonHeadgearMeshComponent);
	if( ThirdPersonHairMeshComponent.AttachedToSkelComponent != none )
		Mesh.DetachComponent(ThirdPersonHairMeshComponent);
	if( FaceItemMeshComponent.AttachedToSkelComponent != none )
		Mesh.DetachComponent(FaceItemMeshComponent);
	if( FacialHairMeshComponent.AttachedToSkelComponent != none )
		Mesh.DetachComponent(FacialHairMeshComponent);
	if( ThirdPersonHeadAndArmsMeshComponent.AttachedToSkelComponent != none )
		DetachComponent(ThirdPersonHeadAndArmsMeshComponent);
	
	/* if (TeamIndex == `AXIS_TEAM_INDEX && bPilot)
	{
		bPilotByte = 2;
		bIsPilot = true;
	}
	else */ if (bPilot)
	{
		bPilotByte = 1;
		bIsPilot = true;
	}
	else
	{
		bPilotByte = 0;
		bIsPilot = false;
	}
	
	TunicMesh = class'GOMPawnHandler'.static.GetTunicMeshes(TeamIndex, ArmyIndex, bPilotByte, TunicID);
	BodyMICTemplate = class'GOMPawnHandler'.static.GetBodyMIC(TeamIndex, ArmyIndex, bPilotByte, TunicID, TunicMaterialID);
	BodyMICTemplate2 = class'GOMPawnHandler'.static.GetSecondaryBodyMIC(TeamIndex, ArmyIndex, TunicID, TunicMaterialID);
	
	if (ClassIndex < 0 || bMainMenu)
	{
		ClassIndex = bPilot ? `ROCI_CombatPilot : `ROCI_Scout;
	}
	
	FieldgearMesh = class'GOMPawnHandler'.static.GetFieldgearMesh(TeamIndex, ArmyIndex, TunicID, ClassIndex, TunicMaterialID);
	
	HeadAndArmsMesh = class'GOMPawnHandler'.static.GetHeadAndArmsMesh(TeamIndex, ArmyIndex, bPilotByte, HeadID, byteDisposal);
	HeadAndArmsMICTemplate = class'GOMPawnHandler'.static.GetHeadMIC(TeamIndex, ArmyIndex, HeadID, TunicID, bPilotByte);
	HeadgearMesh = class'GOMPawnHandler'.static.GetHeadgearMesh(TeamIndex, ArmyIndex, bPilotByte, HeadID, HairID, HeadgearID, HeadgearMatID, HeadgearMICTemplate, HairMICTemplate, HeadgearAttachSocket, byteDisposal);
	HairMesh = class'GOMPawnHandler'.static.GetHairMesh(TeamIndex, ArmyIndex, bPilotByte, HeadID, HairID, HeadgearID, HeadgearMICTemplate, HairMICTemplate);
	FaceItemMesh = class'GOMPawnHandler'.static.GetFaceItemMesh(TeamIndex, ArmyIndex, bPilotByte, HeadgearID, FaceItemID, FaceItemAttachSocket, bNoFacialHair);
	FacialHairMesh = class'GOMPawnHandler'.static.GetFacialHairMesh(TeamIndex, ArmyIndex, FacialHairID, FacialHairAttachSocket);
	
	BodyMIC.SetParent(BodyMICTemplate);
	GearMIC.SetParent(TunicMesh.Materials[1]);
	GearMIC2.SetParent(TunicMesh.Materials[2]);
	GearMIC3.SetParent(TunicMesh.Materials[3]);
	
	if (BodyMICTemplate2 != none)
	{
		BodyMIC2.SetParent(BodyMICTemplate2);
	}
	else
	{
		BodyMIC2.SetParent(TunicMesh.Materials[4]);
	}
	
	HeadAndArmsMIC.SetParent(HeadAndArmsMICTemplate);
	HeadgearMIC.SetParent(HeadgearMICTemplate);
	HairMIC.SetParent(HairMICTemplate);
	
	if(HeadAndArmsMIC != none)
	{
		HeadAndArmsMIC.ClearParameterValues();
		
		if( PawnHandlerClass.static.GetShirtTextures(TeamIndex, ArmyIndex, byte(bPilot), TunicID, ShirtID, ShirtD, ShirtN, ShirtS) )
		{
			HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtDiffuseParam,ShirtD);
			HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtNormalParam,ShirtN);
			HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.ShirtSpecParam,ShirtS);
		}
		
		TattooTex = PawnHandlerClass.static.GetTattooTexture(TeamIndex, ArmyIndex, byte(bPilot), TattooID, TattooUOffset, TattooVOffset, TattooDrawScale);
		
		if (TattooTex != none)
		{
			HeadAndArmsMIC.SetTextureParameterValue(PawnHandlerClass.default.TattooParam, TattooTex);
			HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.TattooUOffsetParam,TattooUOffset);
			HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.TattooVOffsetParam,TattooVOffset);
			HeadAndArmsMIC.SetScalarParameterValue(PawnHandlerClass.default.TattooDrawScaleParam,TattooDrawScale);
		}
	}
	
	CompositedBodyMesh = ROMapInfo(WorldInfo.GetMapInfo()).GetCachedCompositedPawnMesh(TunicMesh, FieldgearMesh);
	CompositedBodyMesh.Characterization = PlayerHIKCharacterization;
	
	Mesh.ReplaceSkeletalMesh(CompositedBodyMesh);
	Mesh.GenerateAnimationOverrideBones(HeadAndArmsMesh);
	
	ThirdPersonHeadAndArmsMeshComponent.SetSkeletalMesh(HeadAndArmsMesh);
	ThirdPersonHeadAndArmsMeshComponent.SetMaterial(0, HeadAndArmsMIC);
	ThirdPersonHeadAndArmsMeshComponent.SetParentAnimComponent(Mesh);
	ThirdPersonHeadAndArmsMeshComponent.SetShadowParent(Mesh);
	ThirdPersonHeadAndArmsMeshComponent.SetLODParent(Mesh);
	AttachComponent(ThirdPersonHeadAndArmsMeshComponent);
	
	if( HeadgearMesh != none )
	{
		AttachNewHeadgear(HeadgearMesh);
	}
	
	if( FaceItemID > 0 && FaceItemMesh != none )
	{
		AttachNewFaceItem(FaceItemMesh);
	}
	
	if( FacialHairID > 0 && FacialHairMesh != none && bNoFacialHair == 0 )
	{
		AttachNewFacialHair(FacialHairMesh);
	}
	
	HonourPct = FClamp(HonorLevel / 99.0, 0.0, 1.0);
	
	if (ClassIndex >= `ROCI_COMBATPILOT)
	{
		HonourPct = 0.0;
	}
	
	`gom("Dirt Level:" @ HonourPct, 'PawnHandler');
	
	BodyMIC.SetScalarParameterValue('Grime_Scaler', HonourPct);
	GearMIC.SetScalarParameterValue('Grime_Scaler', HonourPct);
	GearMIC2.SetScalarParameterValue('Grime_Scaler', HonourPct);
	GearMIC3.SetScalarParameterValue('Grime_Scaler', HonourPct);
	BodyMIC2.SetScalarParameterValue('Grime_Scaler', HonourPct);
	HeadAndArmsMIC.SetScalarParameterValue('Grime_Scaler', HonourPct);
	HeadgearMIC.SetScalarParameterValue('Grime_Scaler', HonourPct);
	
	if (TeamIndex == `ALLIES_TEAM_INDEX && ArmyIndex == `MACV)
	{
		HonourPct = 2.5;
	}
	else
	{
		HonourPct = 0.0;
	}
	
	BodyMIC.SetScalarParameterValue('Mud_Scaler', HonourPct);
	GearMIC.SetScalarParameterValue('Mud_Scaler', HonourPct);
	GearMIC2.SetScalarParameterValue('Mud_Scaler', HonourPct);
	GearMIC3.SetScalarParameterValue('Mud_Scaler', HonourPct);
	BodyMIC2.SetScalarParameterValue('Mud_Scaler', HonourPct);
	
	mesh.SetMaterial(0, BodyMIC);
	mesh.SetMaterial(1, GearMIC);
	mesh.SetMaterial(2, GearMIC2);
	mesh.SetMaterial(3, GearMIC3);
	mesh.SetMaterial(4, BodyMIC2);
	
	AttachPreviewWeapon(TeamIndex, ArmyIndex);
	
	mesh.SetRotation(InitialRot);
}

function AttachPreviewWeapon(byte TeamIndex, byte ArmyIndex)
{
	local int PreviewWeaponIndex;
	
	// Main Menu only (for now)
	if (WorldInfo.NetMode != NM_Standalone)
	{
		return;
	}
	
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		PreviewWeaponIndex = ArmyIndex;
	}
	else
	{
		PreviewWeaponIndex = 3 + ArmyIndex;
	}
	
	if (bIsPilot)
	{
		PreviewWeaponType = 0;
		RandomAnimNode = AnimNodeRandom(Mesh.FindAnimNode('RandomIdleNode_None'));
		
		WeaponMeshComponent.SetSkeletalMesh(none);
		
		Mesh.DetachComponent(WeaponMeshComponent);
		
		return;
	}
	
	PreviewWeaponType = WeaponMeshes[PreviewWeaponIndex].WeaponType;
	WeaponMeshComponent.SetSkeletalMesh(WeaponMeshes[PreviewWeaponIndex].WeaponMesh);
	WeaponMeshComponent.SetShadowParent(Mesh);
	WeaponMeshComponent.SetLODParent(Mesh);
	Mesh.AttachComponent(WeaponMeshComponent, 'Righthand_IK', WeaponMeshes[PreviewWeaponIndex].RelativeLocation);
	
	if( WeaponMeshComponent.MatchRefBone('bayonet') != INDEX_NONE )
		WeaponMeshComponent.HideBoneByName('bayonet', PBO_None);
	else if( WeaponMeshComponent.MatchRefBone('Attachment') != INDEX_NONE )
		WeaponMeshComponent.HideBoneByName('Attachment', PBO_None);
	
	if( PreviewWeaponType == 1 )
		RandomAnimNode = AnimNodeRandom(Mesh.FindAnimNode('RandomIdleNode_Rifle'));
	else
		RandomAnimNode = AnimNodeRandom(Mesh.FindAnimNode('RandomIdleNode_PistolGrip'));
	
	if( HandsBlendNode != none )
	{
		HandsBlendNode.SetBlendTarget(1.0, 0.2);
		HandsBlendNode.EnableLeftHandPose(true);
		HandsSequencePlayerNode.SetAnim('AK47_Handpose');
		HandsSequencePlayerNode.SetPosition(0.0f,false);
	}
}

function AttachNewHeadgear(SkeletalMesh NewHeadgearMesh)
{
	local SkeletalMeshSocket HeadSocket;
	
	ThirdPersonHeadgearMeshComponent.SetSkeletalMesh(NewHeadgearMesh);
	ThirdPersonHeadgearMeshComponent.SetMaterial(0, HeadgearMIC);
	
	if (!bIsPilot && HairMIC != none)
	{
		ThirdPersonHairMeshComponent.SetSkeletalMesh(HairMesh);
		ThirdPersonHairMeshComponent.SetMaterial(0, HairMIC);
		
		if (ThirdPersonHeadgearMeshComponent.GetNumElements() > 1)
		{
			ThirdPersonHeadgearMeshComponent.SetMaterial(1, HairMIC);
		}
	}
	
	HeadSocket = ThirdPersonHeadAndArmsMeshComponent.GetSocketByName(HeadgearAttachSocket);
	
	if (mesh.MatchRefBone(HeadSocket.BoneName) != INDEX_NONE)
	{
		ThirdPersonHeadgearMeshComponent.SetShadowParent(mesh);
		ThirdPersonHeadgearMeshComponent.SetLODParent(mesh);
		mesh.AttachComponent( ThirdPersonHeadgearMeshComponent, HeadSocket.BoneName, HeadSocket.RelativeLocation, HeadSocket.RelativeRotation, HeadSocket.RelativeScale);
		
		ThirdPersonHairMeshComponent.SetShadowParent(mesh);
		ThirdPersonHairMeshComponent.SetLODParent(mesh);
		mesh.AttachComponent( ThirdPersonHairMeshComponent, HeadSocket.BoneName, HeadSocket.RelativeLocation, HeadSocket.RelativeRotation, HeadSocket.RelativeScale);
	}
}

defaultproperties
{
	Begin Object class=ROSkeletalMeshComponent name=ThirdPersonHair0
		LightEnvironment = MyLightEnvironment
		bAcceptsDynamicDecals=true
		bUpdateSkelWhenNotRendered=true
		bNoSkeletonUpdate=false
		HiddenGame=false
		ForcedLodModel=0
	End Object
	ThirdPersonHairMeshComponent=ThirdPersonHair0
	
	WeaponMeshes(0)=(WeaponMesh=SkeletalMesh'GOM3_WP_VN_VC_M44.Mesh.M44_Carbine_3rd',RelativeLocation=(X=0,Y=0,Z=0),WeaponType=1)
	WeaponMeshes(1)=(WeaponMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh_UPGD.AK47_3rd_Master_UPGD3',RelativeLocation=(X=0,Y=0,Z=-1),WeaponType=2)
	WeaponMeshes(2)=(WeaponMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh_UPGD.AK47_3rd_Master_UPGD3',RelativeLocation=(X=0,Y=0,Z=-1),WeaponType=2)
	
	WeaponMeshes(3)=(WeaponMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh.M16_3rd_Master',RelativeLocation=(X=0,Y=0,Z=0),WeaponType=2)
	WeaponMeshes(4)=(WeaponMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh.M16_3rd_Master',RelativeLocation=(X=0,Y=0,Z=0),WeaponType=2)
	WeaponMeshes(5)=(WeaponMesh=SkeletalMesh'WP_VN_AUS_3rd_Master.Mesh.XM117_3rd_Master',RelativeLocation=(X=0,Y=0,Z=0),WeaponType=2)
	WeaponMeshes(6)=(WeaponMesh=SkeletalMesh'WP_VN_ARVN_3rd_Master.Mesh.M1Garand_3rd_Master',RelativeLocation=(X=0,Y=0,Z=0),WeaponType=1)
	WeaponMeshes(7)=(WeaponMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh.M16_3rd_Master',RelativeLocation=(X=0,Y=0,Z=0),WeaponType=2)
	WeaponMeshes(8)=(WeaponMesh=SkeletalMesh'WP_VN_AUS_3rd_Master.Mesh.L1A1_3rd_Master',RelativeLocation=(X=-2.1,Y=0.5,Z=-0.4),WeaponType=2)
	WeaponMeshes(9)=(WeaponMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh.M16_3rd_Master',RelativeLocation=(X=0,Y=0,Z=0),WeaponType=2)
	WeaponMeshes(10)=(WeaponMesh=SkeletalMesh'WP_VN_ARVN_3rd_Master.Mesh.M1Garand_3rd_Master',RelativeLocation=(X=0,Y=0,Z=0),WeaponType=1)
	WeaponMeshes(11)=(WeaponMesh=SkeletalMesh'WP_VN_ARVN_3rd_Master.Mesh.M1Garand_3rd_Master',RelativeLocation=(X=0,Y=0,Z=0),WeaponType=1)
	WeaponMeshes(12)=(WeaponMesh=SkeletalMesh'WP_VN_ARVN_3rd_Master.Mesh.M1Garand_3rd_Master',RelativeLocation=(X=0,Y=0,Z=0),WeaponType=1)
}
