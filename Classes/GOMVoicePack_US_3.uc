//=============================================================================
// GOMVoicePack_US_3.uc
//=============================================================================
// US Voice Pack.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVoicePack_US_3 extends ROVoicePackUSATeam03;

defaultproperties
{
	VoiceComs[`VOICECOM_TakeCover]=(Sound="AKEvent'WW_VOX_US_Voice3.Play_US_Soldier_3_TakingFireGeneric'")
	VoiceComs[`VOICECOM_AIConfirm]=(Sound="AKEvent'WW_VOX_US_Voice3.Play_US_Soldier_3_Confirm'")
	VoiceComs[`VOICECOM_HeloIdleSituation]=(Sound="AKEvent'WW_VOX_US_Voice3.Play_US_Pilot_3_HeloIdleGround'")
}
