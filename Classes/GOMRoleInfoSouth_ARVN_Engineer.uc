//=============================================================================
// GOMRoleInfoSouth_ARVN_Engineer.uc
//=============================================================================
// Republic of Vietnam Army Combat Engineer Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ARVN_Engineer extends GOMRoleInfoSouth_ARVN;

DefaultProperties
{
	RoleType=RORIT_Engineer
	ClassTier=3
	ClassIndex=`ROCI_ENGINEER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M1918_BAR'),
		OtherItems=(class'ROWeap_M61_GrenadeQuad',class'ROWeap_C4_Explosive')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M1918_BAR'),
		OtherItems=(class'ROWeap_M61_GrenadeQuad',class'ROWeap_C4_Explosive')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M1918_BAR'),
		OtherItems=(class'ROWeap_M61_GrenadeQuad',class'ROWeap_C4_Explosive')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sapper'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_sapper'
}
