//=============================================================================
// GOMWeapon_M7RifleGrenade_Attach.uc
//=============================================================================
// M7 Rifle Grenade attachment for the M1 Garand (Attachment class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M7RifleGrenade_Attach extends ROWeapAttach_M1Garand_Rifle;

simulated function ThirdPersonFireEffects(vector HitLocation)
{
	super.ThirdPersonFireEffects(HitLocation);
	
	InstantUnSetSpecialFunction();
}

simulated function PlayReloadAnimation(ROPawn ROPOwner, float ProficiencyMod, bool bWeaponIsEmpty)
{
	super.PlayReloadAnimation(ROPOwner, ProficiencyMod, bWeaponIsEmpty);
	
	InstantSetSpecialFunction();
}

defaultproperties
{
	WeaponClass=class'GOMWeapon_M7RifleGrenade'
	
	CarrySocketName=none
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'GOM3_WP_US_ROK_M7.Mesh.M7_RifleGrenade_3rd'
		AnimSets(0)=AnimSet'WP_VN_ARVN_3rd_Master.Anim.M1Garand_3rd_Anims'
		AnimTreeTemplate=AnimTree'WP_VN_ARVN_3rd_Master.AnimTree.M1Garand_Rifle_3rd_Tree'
		Animations=NONE
		PhysicsAsset=PhysicsAsset'WP_VN_ARVN_3rd_Master.Phys_Bounds.M1Garand_3rd_Bounds_Physics'
		CullDistance=5000
	End Object
	
	MuzzleFlashPSCTemplate=none
	ClipEjectPSCTemplate=none
	
	WP_ReloadAnims(0)=Bayonet_Attach
	WP_ReloadAnims(1)=Bayonet_Attach
	WP_Prone_ReloadAnims(0)=Prone_Bayonet_Attach
	WP_Prone_ReloadAnims(1)=Prone_Bayonet_Attach
	
	ReloadAnims(0)=Bayonet_Attach
	ReloadAnims(1)=Bayonet_Attach
	CH_ReloadAnims(0)=CH_Bayonet_Attach
	CH_ReloadAnims(1)=CH_Bayonet_Attach
	Prone_ReloadAnims(0)=Prone_Bayonet_Attach
	Prone_ReloadAnims(1)=Prone_Bayonet_Attach
}
