//=============================================================================
// GOMWeapon_BowieKnife.uc
//=============================================================================
// US Army Bowie Combat Knife.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_BowieKnife extends ROMeleeWeapon
	abstract;

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMWeapon_BowieKnife_ActualContent"
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	RoleSelectionImage(0)=Texture2D'GOM3_UI.WP_Render.VN_Weap_Bowie'
	
	Category=ROIC_Secondary
	Weight=0
	
	bUsesFreeAim=false
	bHasIronSights=false
	
	InvIndex=`WI_BOWIE
	bIsModWeapon=true
	
	InventoryWeight=0
	
	PlayerViewOffset=(X=0.0,Y=1.5,Z=-1.25)
	ShoulderedPosition=(X=0.0,Y=1.0,Z=-0.5)
	
	WeaponPutDownAnim=Katana_Putaway
	WeaponEquipAnim=Katana_Pullout
	WeaponDownAnim=Katana_Down
	WeaponUpAnim=Katana_Up
	
   	WeaponIdleAnims(0)=Katana_idle
	WeaponIdleAnims(1)=Katana_idle
	
	WeaponIdleShoulderedAnims(0)=Katana_idle
	WeaponIdleShoulderedAnims(1)=Katana_idle
	
	WeaponCrawlingAnims(0)=Katana_CrawlF
	WeaponCrawlStartAnim=Katana_Crawl_into
	WeaponCrawlEndAnim=Katana_Crawl_out

	// Sprinting
	WeaponSprintStartAnim=Katana_1H_sprint_into
	WeaponSprintLoopAnim=Katana_1H_Sprint
	WeaponSprintEndAnim=Katana_1H_sprint_out

	// Mantling
	WeaponMantleOverAnim=Katana_Mantle

	// Cover/Blind Fire Anims
	WeaponRestAnim=Katana_rest_idle
	WeaponEquipRestAnim=Katana_pullout_rest
	WeaponPutDownRestAnim=Katana_putaway_rest
	WeaponIdleToRestAnim=Katana_idleTOrest
	WeaponRestToIdleAnim=Katana_restTOidle

	// Swing anims
	WeaponSwingAnims(0)=Katana_Stab
	WeaponSwingAnims(1)=Katana_Stab
	WeaponSwingHardAnim=Katana_StabHard
	SwingPullbackAnims(0)=Katana_Stab_Pullback
	SwingPullbackAnims(1)=Katana_Stab_Pullback
	SwingHoldAnims(0)=Katana_Stab_Pullback_Hold
	SwingHoldAnims(1)=Katana_Stab_Pullback_Hold

	// Stab anims
	WeaponMeleeAnims(0)=Katana_Stab
	WeaponMeleeHardAnim=Katana_StabHard
	MeleePullbackAnim=Katana_Stab_Pullback
	MeleeHoldAnim=Katana_Stab_Pullback_Hold

	// Block anims
	WeaponBlockStartAnim=Katana_Block_into
	WeaponBlockLoopAnim=Katana_BlockHOLD
	WeaponBlockEndAnim=Katana_Block_out
	WeaponBlockDeflectAnim=Katana_Deflect

	// MAIN FIREMODE
	InstantHitDamageTypes(0)=class'GOMDmgType_Bowie'

	// ALT FIREMODE
	InstantHitDamageTypes(ALTERNATE_FIREMODE)=class'GOMDmgType_Bowie'

	// MELEE FIREMODE
	InstantHitDamageTypes(MELEE_ATTACK_FIREMODE)=class'GOMDmgType_Bowie'

	SwingAttackSound=AkEvent'WW_WEP_Shared.Play_WEP_Melee_Katana_Swing'
	SwingHardAttackSound=AkEvent'WW_WEP_Shared.Play_WEP_Melee_Katana_Swing'
	SwingImpactSound=AkEvent'WW_WEP_Shared.Play_WEP_Melee_Bayonet_Hard'
	SwingAttackHitFleshSound=AkEvent'WW_WEP_Shared.Play_WEP_Melee_Bayonet_Flesh'
	MeleeAttackSound=AkEvent'WW_WEP_Shared.Play_WEP_Melee_Rifle_Swing'
	MeleeImpactSound=AkEvent'WW_WEP_TunnelTool.Play_WEP_TunnelTool_Foley_Plant'
	MeleeAttackHitFleshSound=AkEvent'WW_WEP_Shared.Play_WEP_Melee_Rifle_Impact'

	// Swing Settings
	MeleeSwingRange=60
	MeleeSwingDamage=75
	MeleeSwingChargeDamage=200

	// Stab Settings
	MeleeAttackRange=65 //75
	MeleeAttackDamage=75 //90
	MeleeAttackChargeDamage=150

	MeleeInitialImpactDelay=0.15

	EquipTime=+0.5//0.70
	PutDownTime=+0.33//1

	WeaponMeleeCameraAnim=CameraAnim'1stperson_Cameras.Anim.Camera_TunnelTool_Melee'
	WeaponMeleeHardCameraAnim=CameraAnim'1stperson_Cameras.Anim.Camera_TunnelTool_MeleeHard'
}
