//=============================================================================
// GOMWeapon_NagantRevolver_Attach.uc
//=============================================================================
// Viet Cong Nagant Revolver (Attachment Class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_NagantRevolver_Attach extends ROWeaponAttachmentPistol;

defaultproperties
{
	ThirdPersonHandsAnim=M1917_Handpose //Revolver_Handpose
	IKProfileName=C96
	
	Begin Object Name=SkeletalMeshComponent0
		// SkeletalMesh=SkeletalMesh'WP_VN_3rd_Master_02.Mesh.M1917_SW_3rd_Master'
		AnimSets(0)=AnimSet'WP_VN_3rd_Master_02.Anim.M1917_SW_3rd_Anim'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master_02.Phy_Bounds.M1917_SW_3rd_Bounds_Physics'
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_VC_REVOLVER.Mesh.Nagant_Revolver_3rd_Master'
		// AnimSets(0)=AnimSet'GOM3_WP_VN_VC_REVOLVER.Anim.NagantRevolver_3rd_anim'
		// PhysicsAsset=PhysicsAsset'GOM3_WP_VN_VC_REVOLVER.Phy.Nagant_Revolver_3rd_phy'
		CullDistance=5000
	End Object
	
	CHR_AnimSet=AnimSet'CHR_VN_Playeranim_Master_02.Weapons.CHR_M1917_SW' //AnimSet'GOM3_WP_VN_VC_REVOLVER.Anim.CHR_Revolver'
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_3rdP_Pistol'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	WeaponClass=class'GOMWeapon_NagantRevolver'
	
	FireAnim=Shoot
	FireLastAnim=Shoot_Last
	IdleAnim=Idle
	IdleEmptyAnim=Idle_Empty
}
