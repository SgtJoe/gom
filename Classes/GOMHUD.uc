//=============================================================================
// GOMHUD.uc
//=============================================================================
// Main class for the Heads Up Display.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHUD extends ROHUD;

event PostBeginPlay()
{
	super.PostBeginPlay();
	
	if (WatermarkWidget == none)
	{
		WatermarkWidget = Spawn(DefaultWatermarkWidget, PlayerOwner);
		
		HUDWidgetList.AddItem(WatermarkWidget);
	}
}

exec function ToggleHUD()
{
	super.ToggleHUD();
	
	WatermarkWidget.bVisible = !WatermarkWidget.bVisible;
}

defaultproperties
{
	DefaultWeaponWidget=		class'GOMHUDWidgetWeapon'
	DefaultWatermarkWidget=		class'GOMHUDWatermark'
	DefaultKillMessageWidget=	class'GOMHUDWidgetKillMessages'
	
	AmmoUVs(70)=(AmmoClass=GOMAmmo_M18_HE,	MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_RPG')
	AmmoUVs(71)=(AmmoClass=GOMAmmo_M18_HEAT,MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_RPG')
	AmmoUVs(72)=(AmmoClass=GOMAmmo_M18_WP,	MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_RPG')
}
