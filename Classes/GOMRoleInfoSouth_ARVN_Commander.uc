//=============================================================================
// GOMRoleInfoSouth_ARVN_Commander.uc
//=============================================================================
// Republic of Vietnam Army Commander Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ARVN_Commander extends GOMRoleInfoSouth_ARVN;

DefaultProperties
{
	RoleType=RORIT_Commander
	ClassTier=4
	ClassIndex=`ROCI_COMMANDER
	bIsTeamLeader=true
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle',class'ROWeap_M1A1_SMG'),
		OtherItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M1Garand_Rifle',class'ROWeap_M1A1_SMG'),
		OtherItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M1Garand_Rifle',class'ROWeap_M1A1_SMG',class'ROWeap_M16A1_AssaultRifle'),
		OtherItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle_Late',class'ROWeap_M1A1_SMG'),
		OtherItems=(class'ROWeap_M8_Smoke',class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_commander'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_commander'
}
