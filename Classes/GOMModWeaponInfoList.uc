//=============================================================================
// GOMModWeaponInfoList.uc
//=============================================================================
// List of all ROModWeaponInfos for easy access.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfoList extends Object;

var array< class<ROModWeaponInfo> > WeaponInfos;

DefaultProperties
{
	WeaponInfos(0)=class'GOMModWeaponInfo_Kar98'
	WeaponInfos(1)=class'GOMModWeaponInfo_NagantRevolver')
	WeaponInfos(2)=class'GOMModWeaponInfo_RPG2')
	WeaponInfos(3)=class'GOMModWeaponInfo_SawnOffRPD')
	WeaponInfos(4)=class'GOMModWeaponInfo_Bowie')
	WeaponInfos(5)=class'GOMModWeaponInfo_PPS')
	WeaponInfos(6)=class'GOMModWeaponInfo_M1919A6')
	WeaponInfos(7)=class'GOMModWeaponInfo_M7')
	WeaponInfos(8)=class'GOMModWeaponInfo_M2A17')
	WeaponInfos(9)=class'GOMModWeaponInfo_Satchel')
}
