//=============================================================================
// GOMVehicleWeaponMG_M113_M2.uc
//=============================================================================
// M113 ACAV M2 Browning.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleWeaponMG_M113_M2 extends GOMVehicleWeaponMG
	abstract;

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMVehicleWeaponMG_M113_M2_ActualContent"
	
	VehicleClass=class'GOMVehicle_M113_ACAV'
	
	SeatIndex=`M113_MG
}