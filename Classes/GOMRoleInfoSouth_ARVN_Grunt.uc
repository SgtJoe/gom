//=============================================================================
// GOMRoleInfoSouth_ARVN_Grunt.uc
//=============================================================================
// Republic of Vietnam Army Grunt Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ARVN_Grunt extends GOMRoleInfoSouth_ARVN;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=1
	ClassIndex=`ROCI_RIFLEMAN
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M1Garand_Rifle',class'ROWeap_M2_Carbine'),
		OtherItems=(class'ROWeap_M61_Grenade')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M1Garand_Rifle',class'ROWeap_M2_Carbine'),
		OtherItems=(class'ROWeap_M61_Grenade')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M1Garand_Rifle',class'ROWeap_M2_Carbine'),
		OtherItems=(class'ROWeap_M61_Grenade')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_grunt'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_guerilla'
}
