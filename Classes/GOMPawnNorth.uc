//=============================================================================
// GOMPawnNorth.uc
//=============================================================================
// Northern Pawn.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawnNorth extends GOMPawn
	abstract;

simulated event byte ScriptGetTeamNum()
{
	return `AXIS_TEAM_INDEX;
}

defaultproperties
{
	HeadgearMesh=SkeletalMesh'CHR_VN_VN_Headgear.Mesh.VN_Headgear_Hair_Medium'
	HeadgearMICTemplate=none
	
	HeadAndArmsMesh=SkeletalMesh'CHR_VN_VN_Heads.Mesh.VN_Head1_Mesh'
	HeadAndArmsMICTemplate=MaterialInstanceConstant'GOM3_CHR_VN_VC.MIC.VC_Head_01_Long'
	
	Begin Object Name=ROPawnSkeletalMeshComponent
		AnimSets(11)=AnimSet'CHR_Playeranim_Master.Anim.CHR_German_Unique'
	End Object
	
	bSingleHandedSprinting=true
	
	bCanCamouflage=true
}