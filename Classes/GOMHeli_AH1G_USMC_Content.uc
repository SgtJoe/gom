//=============================================================================
// GOMHeli_AH1G_USMC_Content.uc
//=============================================================================
// AH-1G Cobra Gunship Helicopter (USMC)
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMHeli_AH1G_USMC_Content extends ROHeli_AH1G_Content
	placeable;

`include(GOM3\Classes\GOMHelicoptersCommon.uci)

DefaultProperties
{
	Begin Object Name=ROSVehicleMesh
		Materials(3)=MaterialInstanceConstant'GOM3_VH_US_AH1G_USMC.MIC.VH_US_AH1G_USMC'
	End Object
}
