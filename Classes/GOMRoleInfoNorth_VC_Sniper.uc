//=============================================================================
// GOMRoleInfoNorth_VC_Sniper.uc
//=============================================================================
// Viet Cong Sniper Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_VC_Sniper extends GOMRoleInfoNorth_VC;

DefaultProperties
{
	RoleType=RORIT_Marksman
	ClassTier=3
	ClassIndex=`ROCI_SNIPER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_MN9130Scoped_Rifle'),
		OtherItems=(class'ROWeap_PunjiTrap')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_MAS49Scoped_Rifle',class'ROWeap_MN9130Scoped_Rifle'),
		OtherItems=(class'ROWeap_PunjiTrap')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_MN9130Scoped_Rifle'),
		OtherItems=(class'ROWeap_PunjiTrap')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_MN9130Scoped_Rifle')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sniper'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_sniper'
}
