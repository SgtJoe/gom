//=============================================================================
// GOMRoleInfoNorth_NVA.uc
//=============================================================================
// Basic info for all North Vietnamese Army Roles.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_NVA extends GOMRoleInfo
	abstract;

DefaultProperties
{
	Items[RORIGM_Default]={(
		SecondaryWeapons=(class'ROWeap_TT33_Pistol'),
		SquadLeaderItems=(class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		SecondaryWeapons=(class'ROWeap_TT33_Pistol'),
		SquadLeaderItems=(class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		SecondaryWeapons=(class'ROWeap_TT33_Pistol'),
		SquadLeaderItems=(class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		SecondaryWeapons=(class'ROWeap_TT33_Pistol'),
		SquadLeaderItems=(class'ROWeap_RDG1_Smoke',class'ROItem_Binoculars')
	)}
	
	RoleRootClass=class'GOMRoleInfoNorth_NVA'
}
