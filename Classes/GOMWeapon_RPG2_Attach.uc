//=============================================================================
// GOMWeapon_RPG2_Attach.uc
//=============================================================================
// Viet Cong RPG-2 Rocket Launcher (Attachment class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_RPG2_Attach extends ROWeaponAttachmentRocket;

DefaultProperties
{
	ThirdPersonHandsAnim=RPG7_Handpose
	IKProfileName=Kar98
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'GOM3_WP_VN_VC_RPG2.Mesh.RPG2_3rd_Master'
		AnimSets(0)=AnimSet'WP_VN_3rd_Master.Anim.RPG7_3rd_Anim'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy_Bounds.RPG7_3rd_Bounds_Physics'
		CullDistance=5000
	End Object
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_RPG_1stP_3rdP_frontblast'
	MuzzleFlashDuration=2.0
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	BackBlastSocket=BackBlastSocket
	BackBlastPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_RPG_3rdP_blueBackblast'
	BackBlastDuration=3.5
	BackBlastLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	WeaponClass=class'GOMWeapon_RPG2'
	
	bVisibleProj=true
	WeaponProjBone=Warhead
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=none
	bNoShellEjectOnFire=true
	
	CHR_AnimSet=AnimSet'CHR_VN_Playeranim_Master.Weapons.CHR_RPG7'
	
	FireAnim=Shoot
	FireLastAnim=Shoot_Last
	IdleAnim=Idle
	IdleEmptyAnim=Idle_Empty
}
