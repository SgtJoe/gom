//=============================================================================
// GOMPawnSouthFlamethrowerAUS.uc
//=============================================================================
// Southern Pawn w Flamethrower (Australia).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawnSouthFlamethrowerAUS extends GOMPawnSouthAUS;

DefaultProperties
{
	bHasFlamethrower=true
	
	FieldgearMesh=SkeletalMesh'GOM3_CHR_US_AUS.Mesh.GOM_CHR_AUS_Gear_Flamethrower'
}
