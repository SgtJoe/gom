//=============================================================================
// GOMModWeaponInfo_NagantRevolver.uc
//=============================================================================
// UI information for the Nagant Revolver.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfo_NagantRevolver extends ROModWeaponInfo;

DefaultProperties
{
	ModWeaponClass=GOMWeapon_NagantRevolver_ActualContent
	
	InventoryIconTexturePath="GOM3_UI.WP_Select.ui_hud_weaponselect_Revolver"
	
	AmmoIconTexturePath="VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M1917"
	
	KillIconTexturePath="GOM3_UI.WP_KillIcon.ui_kill_icon_Revolver"
	bHasSquareKillIcon=true
}
