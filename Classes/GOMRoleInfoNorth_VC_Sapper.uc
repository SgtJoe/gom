//=============================================================================
// GOMRoleInfoNorth_VC_Sapper.uc
//=============================================================================
// Viet Cong Sapper Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_VC_Sapper extends GOMRoleInfoNorth_VC;

defaultproperties
{
	RoleType=RORIT_Engineer
	ClassTier=3
	ClassIndex=`ROCI_ENGINEER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_MAT49_SMG',class'GOMWeapon_PPSH',class'ROWeap_MAS49_Rifle_Grenade'),
		OtherItems=(class'ROWeap_TripwireTrap',class'ROWeap_Fougasse_Mine'),
		OtherItemsStartIndexForPrimary=(0, 1, 0),
		NumOtherItemsForPrimary=(1, 1, 255)
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_MAT49_SMG',class'ROWeap_MP40_SMG',class'ROWeap_MAS49_Rifle_Grenade'),
		OtherItems=(class'ROWeap_TripwireTrap',class'ROWeap_Fougasse_Mine'),
		OtherItemsStartIndexForPrimary=(0, 1, 0),
		NumOtherItemsForPrimary=(1, 1, 255)
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_MAT49_SMG',class'GOMWeapon_PPS',class'ROWeap_SKS_Rifle'),
		OtherItems=(class'ROWeap_TripwireTrap',class'ROWeap_Fougasse_Mine',class'ROWeap_MD82_MineTriple'),
		OtherItemsStartIndexForPrimary=(0, 1, 2),
		NumOtherItemsForPrimary=(1, 1, 1)
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_K50M_SMG',class'ROWeap_SKS_Rifle'),
		OtherItems=(class'GOMWeapon_Satchel',class'ROWeap_MD82_Mine'),
		OtherItemsStartIndexForPrimary=(0, 1),
		NumOtherItemsForPrimary=(1, 1)
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sapper'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_sapper'
}
