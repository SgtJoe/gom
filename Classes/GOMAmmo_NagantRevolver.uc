//=============================================================================
// GOMAmmo_NagantRevolver.uc
//=============================================================================
// Ammunition info for the Nagant Revolver.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMAmmo_NagantRevolver extends ROAmmunition
	abstract;

defaultproperties
{
	CompatibleWeaponClasses(0)=class'GOMWeapon_NagantRevolver'
	InitialAmount=7
	Weight=0.0896
	ClipsPerSlot=5
}
