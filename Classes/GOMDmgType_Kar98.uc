//=============================================================================
// GOMDmgType_Kar98.uc
//=============================================================================
// Damage type for the Kar98k bullet.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMDmgType_Kar98 extends RODmgType_SmallArmsBullet
	abstract;

DefaultProperties
{
	WeaponClass=GOMWeapon_Kar98k_ActualContent
	KDamageImpulse=625
	BloodSprayTemplate=ParticleSystem'FX_CHAR_Soldier_Two.Emitters.FX_CHAR_Soldier_BloodSpray_Medium'
}
