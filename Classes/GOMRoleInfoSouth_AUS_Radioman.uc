//=============================================================================
// GOMRoleInfoSouth_AUS_Radioman.uc
//=============================================================================
// Australian Army Radio Operator Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_AUS_Radioman extends GOMRoleInfoSouth_AUS;

DefaultProperties
{
	RoleType=RORIT_Radioman
	ClassTier=3
	ClassIndex=`ROCI_RADIOMAN
	bIsRadioman=true
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_L1A1_Rifle',class'ROWeap_F1_SMG',class'ROWeap_Owen_SMG')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_L1A1_Rifle',class'ROWeap_F1_SMG',class'ROWeap_Owen_SMG')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle',class'ROWeap_L1A1_Rifle',class'ROWeap_F1_SMG')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_radioman'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_radioman'
}
