//=============================================================================
// GOMWeapon_BowieKnife_ActualContent.uc
//=============================================================================
// US Army Bowie Combat Knife (Content class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_BowieKnife_ActualContent extends GOMWeapon_BowieKnife;

DefaultProperties
{
	ArmsAnimSet=AnimSet'GOM3_WP_GEN.Anim.WP_CombatKnife'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'GOM3_WP_US_BOWIE.Mesh.BowieKnife'
		PhysicsAsset=None
		AnimSets(0)=AnimSet'GOM3_WP_GEN.Anim.WP_CombatKnife'
		Animations=AnimTree'GOM3_WP_GEN.Anim.CombatKnife_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'GOM3_WP_US_BOWIE.Mesh.BowieKnife_3rd'
		PhysicsAsset=PhysicsAsset'GOM3_WP_GEN.Phy.CombatKnife_3rd_Physics'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'GOMWeapon_BowieKnife_Attach'
}
