//=============================================================================
// GOMRoleInfoSouth_MACV_MachineGunner.uc
//=============================================================================
// MACV-SOG Machine Gunner Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_MACV_MachineGunner extends GOMRoleInfoSouth_MACV;

DefaultProperties
{
	RoleType=RORIT_MachineGunner
	ClassTier=2
	ClassIndex=`ROCI_MACHINEGUNNER
	
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'ROWeap_M60_GPMG',class'GOMWeapon_RPD_SawnOff')
	
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_mg'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_mg'
}
