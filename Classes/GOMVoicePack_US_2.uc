//=============================================================================
// GOMVoicePack_US_2.uc
//=============================================================================
// US Voice Pack.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVoicePack_US_2 extends ROVoicePackUSATeam02;

defaultproperties
{
	VoiceComs[`VOICECOM_TakeCover]=(Sound="AKEvent'WW_VOX_US_Voice2.Play_US_Soldier_2_TakingFireGeneric'")
	VoiceComs[`VOICECOM_AIConfirm]=(Sound="AKEvent'WW_VOX_US_Voice2.Play_US_Soldier_2_Confirm'")
	VoiceComs[`VOICECOM_HeloIdleSituation]=(Sound="AKEvent'WW_VOX_US_Voice2.Play_US_Pilot_2_HeloIdleGround'")
}
