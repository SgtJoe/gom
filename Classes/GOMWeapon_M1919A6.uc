//=============================================================================
// GOMWeapon_M1919A6.uc
//=============================================================================
// ARVN M1919A6 Browning LMG.
// Modified to add barrel change functionality.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_M1919A6 extends ROMGWeapon
	abstract;

var Array<name> Bullets;

simulated function bool CanTransitionToIronSights()
{
	super.CanTransitionToIronSights();
	
	return (ROPawn(Instigator).bBipodDeployed || 
			ROPawn(Instigator).bBipodDeployedNoCover || 
			ROPawn(Instigator).bCanDeployNoCover || 
			Instigator.bIsProning || 
			bIronSightOnBringUp);
}

simulated state ChangingBarrels
{
	simulated function bool CanTransitionToIronSights()
	{
		return ROPawn(Instigator) != none && ROPawn(Instigator).bBipodDeployed;
	}
}

simulated function SightIndexUpdated()
{
	if( SightRotController != none )
	{
		SightRotController.BoneRotation.Pitch = SightRanges[SightRangeIndex].SightPitch * -1;
	}
	if( SightSlideController != none )
	{
		SightSlideController.BoneTranslation.Z = SightRanges[SightRangeIndex].SightSlideOffset;
	}
	IronSightPosition.Z=SightRanges[SightRangeIndex].SightPositionOffset;
	PlayerViewOffset.Z=SightRanges[SightRangeIndex].SightPositionOffset;
}

simulated exec function SwitchFireMode()
{
	ROMGOperation();
}

simulated function FireAmmunition()
{
	Super.FireAmmunition();
	
	if ( WorldInfo.NetMode != NM_DedicatedServer && AmmoBeltMesh != None && AmmoCount < Bullets.length )
	{
		AmmoBeltMesh.HideBoneByName(Bullets[AmmoCount], PBO_None);
	}
}

simulated function UnHideBulletsNotify()
{
	local int i;

	if( WorldInfo.NetMode != NM_DedicatedServer && PlayerController(Instigator.Controller) != none )
	{
		for( i=0; i<Bullets.Length; i++ )
		{
			AmmoBeltMesh.UnHideBoneByName(Bullets[i]);
		}
	}
}

simulated function SetFOV(float NewFOV)
{
	super.SetFOV(NewFOV);
	
	if(AmmoBeltMesh != none)
	{
		AmmoBeltMesh.SetFOV(NewFOV);
	}
}

simulated function PlayWeaponEquip()
{
	Super.PlayWeaponEquip();
	
	RefreshVisibleBullets();
}

simulated function RefreshVisibleBullets()
{
	local int i;
	
	if( AmmoBeltMesh != none && AmmoBeltAttachTime != WorldInfo.TimeSeconds )
	{
		UnHideBulletsNotify();
		
		for( i=(Bullets.Length-1); i>0; i-- )
		{
			if( i + 1 > AmmoCount )
			{
				AmmoBeltMesh.HideBoneByName(Bullets[i], PBO_None);
			}
			else
			{
				break;
			}
		}
	}
}

simulated state Reloading
{
	simulated function CancelWeaponAction(optional bool bAnimate, optional bool bReplicateToServer)
	{
		super.CancelWeaponAction(bAnimate, bReplicateToServer);
		RefreshVisibleBullets();
	}
}

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMWeapon_M1919A6_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'VN_UI_Textures_Three.WeaponTex.ARVN_Weap_M1919'
	
	WeaponClassType=ROWCT_LMG
	
	InvIndex=`ROII_M1919A6_LMG
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	Category=ROIC_Primary
	Weight=15 //KG
	InventoryWeight=3
	
	PlayerIronSightFOV=60.0
	
	// MAIN FIREMODE
	FiringStatesArray(0)=WeaponFiring
	WeaponFireTypes(0)=EWFT_Custom
	WeaponProjectiles(0)=class'M1919Bullet'
	bLoopHighROFSounds(0)=true
	FireInterval(0)=+0.12 // 500rpm
	DelayedRecoilTime(0)=0.0
	Spread(0)=0.0008 // 3 MOA

	// ALT FIREMODE
	FiringStatesArray(ALTERNATE_FIREMODE)=none
	WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_Custom
	WeaponProjectiles(ALTERNATE_FIREMODE)=none
	FireInterval(ALTERNATE_FIREMODE)=+0.1 // 600 RPM
	DelayedRecoilTime(ALTERNATE_FIREMODE)=0.01
	Spread(ALTERNATE_FIREMODE)=0.0008 // 3 MOA

	PreFireTraceLength=2500 //50 Meters
	FireTweenTime=0.025

	//ShoulderedSpreadMod=3.0//6.0
	//HippedSpreadMod=5.0//10.0

	// AI
	MinBurstAmount=3
	MaxBurstAmount=30
	//BurstWaitTime=1.0

	// Recoil
	maxRecoilPitch=460//180//350
	minRecoilPitch=460//130//300
	maxRecoilYaw=220 //200
	minRecoilYaw=-220 //-200
	maxDeployedRecoilPitch=80//200
	minDeployedRecoilPitch=80//200
	maxDeployedRecoilYaw=60
	minDeployedRecoilYaw=-60
	minDeployedRecoilYawAbsolute=25
	RecoilRate=0.1
	RecoilMaxYawLimit=1500
	RecoilMinYawLimit=64035
	RecoilMaxPitchLimit=1500
	RecoilMinPitchLimit=64785
	RecoilISMaxYawLimit=500
	RecoilISMinYawLimit=65035
	RecoilISMaxPitchLimit=350
	RecoilISMinPitchLimit=65035
   	RecoilBlendOutRatio=0.65
   	PostureHippedRecoilModifer=3.5
   	//PostureShoulderRecoilModifer=2
   	RecoilViewRotationScale=0.45

	// InstantHitDamage(0)=150
	// InstantHitDamage(1)=150
	InstantHitDamage(0)=154
	InstantHitDamage(1)=154

	InstantHitDamageTypes(0)=class'RODmgType_M1919Bullet'
	InstantHitDamageTypes(1)=class'RODmgType_M1919Bullet'

	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_round'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'

	// Shell eject FX
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons_Two.ShellEjects.FX_Wep_ShellEject_M1919'

	bHasIronSights=True;

	//Equip and putdown
	WeaponPutDownAnim=30Cal_putaway
	WeaponEquipAnim=30Cal_pullout
	WeaponDownAnim=30Cal_Down
	WeaponUpAnim=30Cal_Up

	// Fire Anims
	//Hip fire
	WeaponFireAnim(0)=30Cal_shoulder_shoot
	WeaponFireAnim(1)=30Cal_shoulder_shoot
	WeaponFireLastAnim=30Cal_shoulder_shoot
	//Shouldered fire
	WeaponFireShoulderedAnim(0)=30Cal_shoulder_shoot
	WeaponFireShoulderedAnim(1)=30Cal_shoulder_shoot
	WeaponFireLastShoulderedAnim=30Cal_shoulder_shoot
	//Fire using iron sights
	// WeaponFireSightedAnim(0)=30Cal_deploy_shoot
	// WeaponFireSightedAnim(1)=30Cal_deploy_shoot
	// WeaponFireLastSightedAnim=30Cal_deploy_shoot
	//Fire using bipod
	WeaponFireDeployedAnim(0)=30Cal_deploy_shoot
	WeaponFireDeployedAnim(1)=30Cal_deploy_shoot
	WeaponFireLastDeployedAnim=30Cal_deploy_shootLAST

	// Idle Anims
	//Hip Idle
	WeaponIdleAnims(0)=30Cal_shoulder_idle
	WeaponIdleAnims(1)=30Cal_shoulder_idle
	// Shouldered idle
	WeaponIdleShoulderedAnims(0)=30Cal_shoulder_idle
	WeaponIdleShoulderedAnims(1)=30Cal_shoulder_idle
	//Sighted Idle
	// WeaponIdleSightedAnims(0)=30Cal_deploy_idle
	// WeaponIdleSightedAnims(1)=30Cal_deploy_idle
	//Bipod Idle
	WeaponIdleDeployedAnims(0)=30Cal_deploy_idle
	WeaponIdleDeployedAnims(1)=30Cal_deploy_idle

	// Prone Crawl
	WeaponCrawlingAnims(0)=30Cal_CrawlF
	WeaponCrawlStartAnim=30Cal_Crawl_into
	WeaponCrawlEndAnim=30Cal_Crawl_out
	// Deployed Prone Crawl
	RedeployCrawlingAnims(0)=30Cal_Deployed_CrawlF

	//Reloading
	WeaponReloadEmptyMagAnim=30Cal_reloadempty_crouch
	WeaponReloadNonEmptyMagAnim=30Cal_reloadhalf_crouch
	WeaponRestReloadEmptyMagAnim=30Cal_reloadempty_crouch
	WeaponRestReloadNonEmptyMagAnim=30Cal_reloadhalf_crouch
	DeployReloadEmptyMagAnim=30Cal_deploy_reloadempty
	DeployReloadHalfMagAnim=30Cal_deploy_reloadhalf
	// Ammo check
	WeaponAmmoCheckAnim=30Cal_ammocheck_crouch
	WeaponRestAmmoCheckAnim=30Cal_ammocheck_crouch
	DeployAmmoCheckAnim=30Cal_deploy_ammocheck

	// Sprinting
	WeaponSprintStartAnim=30Cal_sprint_into
	WeaponSprintLoopAnim=30Cal_Sprint
	WeaponSprintEndAnim=30Cal_sprint_out
	Weapon1HSprintStartAnim=30Cal_1H_sprint_into
	Weapon1HSprintLoopAnim=30Cal_1H_sprint
	Weapon1HSprintEndAnim=30Cal_1H_sprint_out

	// Mantling
	WeaponMantleOverAnim=30Cal_Mantle

	// Cover/Blind Fire Anims
	WeaponRestAnim=30Cal_shoulder_idle
	WeaponEquipRestAnim=30Cal_shoulder_idle
	WeaponPutDownRestAnim=30Cal_shoulder_idle
	WeaponIdleToRestAnim=30Cal_shoulder_idle
	WeaponRestToIdleAnim=30Cal_shoulder_idle

	// Enemy Spotting
	WeaponSpotEnemyAnim=30Cal_shoulder_idle
	WeaponSpotEnemySightedAnim=30Cal_shoulder_idle
	WeaponSpotEnemyDeployedAnim=30Cal_deploy_idle

	ReloadMagazinEmptyCameraAnim=CameraAnim'1stperson_Cameras.Anim.Camera_MP40_reloadempty'

	EquipTime=+1.00
	PutDownTime=+0.75

	bDebugWeapon = false

	ISFocusDepth=12
	ISFocusBlendRadius=0.5

	// Ammo
	AmmoClass=class'ROAmmo_762x63_M1919Belt_250'
	MaxAmmoCount=250
	bUsesMagazines=true
	InitialNumPrimaryMags=2
	NumMagsToResupply=1
	MaxNumPrimaryMags=2
	bPlusOneLoading=false
	bCanReloadNonEmptyMag=true
	PenetrationDepth=23.5
	MaxPenetrationTests=3
	MaxNumPenetrations=2
	// Tracers
	TracerClass=class'M1919BulletTracer'
	TracerFrequency=8

	PlayerViewOffset=(X=-2.5,Y=10.0,Z=-11) //(X=0.0,Y=8.0,Z=-5)
	ZoomInRotation=(Pitch=-910,Yaw=0,Roll=2910)
	ZoomInTime=0.5//0.65
	ZoomOutTime=0.35//0.6
	ShoulderedTime=0.35
	ShoulderedPosition=(X=-1.0,Y=6.0,Z=-7.0)// (X=0,Y=1,Z=-1.4)
	ShoulderRotation=(Pitch=-500,Yaw=0,Roll=1500)
	IronSightPosition=(X=2.0,Y=0,Z=0.0)

	bUsesFreeAim=true

	// Free Aim variables
	//FreeAimMaxYawLimit=2000
	//FreeAimMinYawLimit=63535
	//FreeAimMaxPitchLimit=1500
	//FreeAimMinPitchLimit=64035
	//FreeAimISMaxYawLimit=500
	//FreeAimISMinYawLimit=65035
	//FreeAimISMaxPitchLimit=350
	//FreeAimISMinPitchLimit=65185
	//FullFreeAimISMaxYaw=350
	//FullFreeAimISMinYaw=65185
	//FullFreeAimISMaxPitch=250
	//FullFreeAimISMinPitch=65285
	//FreeAimSpeedScale=0.35
	//FreeAimISSpeedScale=0.66
	FreeAimHipfireOffsetX=50

	Begin Object Class=ForceFeedbackWaveform Name=ForceFeedbackWaveformShooting1
		Samples(0)=(LeftAmplitude=30,RightAmplitude=30,LeftFunction=WF_Constant,RightFunction=WF_Constant,Duration=0.100)
	End Object
	WeaponFireWaveForm=ForceFeedbackWaveformShooting1

	CollisionCheckLength=67.5

	// More sway for the M60 because it's so darn heavy!
	SwayScale=1.8//2
	CoverRestedSwayScale=0.3f
	DeployedSwayScale=0.1

	//LagStrengthIronSights=0.8//1.4
	//LagStrengthWalk=0.//0.9
	LagLimit=3.0

	PitchControlName=PitchControl
	BipodZCheckDist=25.0
	bHasBipod=true
	bCanBlindFire=false
	DeployAnimName=30Cal_shoulderTOdeploy
	UnDeployAnimName=30Cal_deployTOshoulder
	RestDeployAnimName=30Cal_shoulderTOdeploy
	RestUnDeployAnimName=30Cal_deployTOshoulder
	DeployToShuffleAnimName=30Cal_Deploy_TO_Shuffle
	ShuffleIdleAnimName=30Cal_Shuffle_idle
	ShuffleToDeployAnimName=30Cal_Shuffle_TO_Deploy
	RedeployProneTurnAnimName=30Cal_prone_turn_TO_Deploy
	UnDeployProneTurnAnimName=30Cal_prone_Deploy_TO_turn
	ProneTurningIdleAnimName=30Cal_prone_Deploy_turn_idle
	BipodPivotBoneName=Bipod_Pitch
	BipodOffset=(X=37.80)	// Fake the bipod location so that the gun aims like the DP28

	// 19.5 Z offset to ground from 0,0,0

	FireCameraAnim[0]=CameraAnim'1stperson_Cameras.Anim.Camera_DP28_Shoot'
	FireCameraAnim[1]=CameraAnim'1stperson_Cameras.Anim.Camera_DP28_Shoot'
	ShakeScaleControlled=0.65

	SightSlideControlName=Sight_Slide
	SightRotControlName=Sight_Rotation

	SightRanges[0]=(SightRange=100,SightSlideOffset=0.0,SightPositionOffset=0.0,AddedPitch=20)
	SightRanges[1]=(SightRange=200,SightPitch=-16384,SightSlideOffset=-0.1,SightPositionOffset=-0.075,AddedPitch=0)
	SightRanges[2]=(SightRange=300,SightPitch=-16384,SightSlideOffset=-0.08,SightPositionOffset=-0.1,AddedPitch=0)
	SightRanges[3]=(SightRange=400,SightPitch=-16384,SightSlideOffset=0.05,SightPositionOffset=-0.28,AddedPitch=-70)
	SightRanges[4]=(SightRange=500,SightPitch=-16384,SightSlideOffset=0.075,SightPositionOffset=-0.313,AddedPitch=-68)
	SightRanges[5]=(SightRange=600,SightPitch=-16384,SightSlideOffset=0.11,SightPositionOffset=-0.36,AddedPitch=-72)
	SightRanges[6]=(SightRange=700,SightPitch=-16384,SightSlideOffset=0.17,SightPositionOffset=-0.44,AddedPitch=-78)
	SightRanges[7]=(SightRange=800,SightPitch=-16384,SightSlideOffset=0.2,SightPositionOffset=-0.48,AddedPitch=-75)
	SightRanges[8]=(SightRange=900,SightPitch=-16384,SightSlideOffset=0.24,SightPositionOffset=-0.53,AddedPitch=-70)
	SightRanges[9]=(SightRange=1000,SightPitch=-16384,SightSlideOffset=0.3,SightPositionOffset=-0.61,AddedPitch=-67)
	
	ROBarrelClass=class'ROMGBarrelM1919'
  	bTrackBarrelHeat=true
  	BarrelHeatBone=Barrel
	BarrelChangeAnim=30Cal_Deploy_BarrelChange
  	InitialBarrels=2

	SuppressionPower=25

	Bullets(0)=BONE_BELT_01
	Bullets(1)=BONE_BELT_02
	Bullets(2)=BONE_BELT_03
	Bullets(3)=BONE_BELT_04
	Bullets(4)=BONE_BELT_05
	Bullets(5)=BONE_BELT_06
	Bullets(6)=BONE_BELT_07
	Bullets(7)=BONE_BELT_08
	Bullets(8)=BONE_BELT_09
	Bullets(9)=BONE_BELT_10
	Bullets(10)=BONE_BELT_11
	Bullets(11)=BONE_BELT_12
	Bullets(12)=BONE_BELT_13
	Bullets(13)=BONE_BELT_14
	Bullets(14)=BONE_BELT_15
	Bullets(15)=BONE_BELT_16
	Bullets(16)=BONE_BELT_17

	//SwayOffsetMod=700.f
	RecoilModWhenEmpty=1.25f
 	RecoilOffsetModY = 600.f
 	RecoilOffsetModZ = 600.f
 	RecoilOffsetModZDeployed = 1800.f

	PerformReloadPct=0.84f
}

