//=============================================================================
// GOMMapInfoROK.uc
//=============================================================================
// A collection of settings that are specific to ROK maps.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMMapInfoROK extends ROMapInfo;

function InitRolesForGametype(class<GameInfo> GameTypeClass, int MaxPlayers, bool bReverseRoles)
{
	bInitializedRoles = true;
}

function string GetArmyNameForTeam(byte TeamIndex, bool bGetShortName, optional byte ArmyIndex = 255)
{
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		return bGetShortName ? class'GOM3'.default.NorthArmyShortNames[`getNF] : class'GOM3'.default.NorthArmyNames[`getNF];
	}
	else
	{
		return bGetShortName ? class'GOM3'.default.SouthArmyShortNames[`getSF] : class'GOM3'.default.SouthArmyNames[`getSF];
	}
}

function int GetInitialCommanderCooldown(class<GameInfo> GameTypeClass)
{
`ifndef(RELEASE)
	return 0;
`endif
	return INITIAL_COMMANDER_COOLDOWN;
}

defaultproperties
{
	DefendingTeam=DT_North
	DefendingTeam16=DT_North
	DefendingTeam32=DT_North
	DefendingTeam64=DT_North
	
	TempCelsius=25
	
	NorthernRoles.Empty
	SouthernRoles.Empty
	
	NorthernRoles(0)=(RoleInfoClass=class'GOMRoleInfoNorth_VC_Guerilla',Count=255)
	NorthernRoles(1)=(RoleInfoClass=class'GOMRoleInfoNorth_VC_Rifleman',Count=`VC_AK_LIMIT)
	NorthernRoles(2)=(RoleInfoClass=class'GOMRoleInfoNorth_VC_Scout',Count=5)
	NorthernRoles(3)=(RoleInfoClass=class'GOMRoleInfoNorth_VC_MachineGunner',Count=3)
	NorthernRoles(4)=(RoleInfoClass=class'GOMRoleInfoNorth_VC_Sniper',Count=2)
	NorthernRoles(5)=(RoleInfoClass=class'GOMRoleInfoNorth_VC_RPG',Count=1)
	NorthernRoles(6)=(RoleInfoClass=class'GOMRoleInfoNorth_VC_Sapper',Count=4)
	NorthernRoles(7)=(RoleInfoClass=class'GOMRoleInfoNorth_NVA_VC_Radioman',Count=2)
	
	SouthernRoles(0)=(RoleInfoClass=class'GOMRoleInfoSouth_ROK_Grunt',Count=255)
	SouthernRoles(1)=(RoleInfoClass=class'GOMRoleInfoSouth_ROK_Pointman',Count=6)
	SouthernRoles(2)=(RoleInfoClass=class'GOMRoleInfoSouth_ROK_Machinegunner',Count=3)
	SouthernRoles(3)=(RoleInfoClass=class'GOMRoleInfoSouth_ROK_Grenadier',Count=3)
	SouthernRoles(4)=(RoleInfoClass=class'GOMRoleInfoSouth_ROK_Flamethrower',Count=1)
	SouthernRoles(5)=(RoleInfoClass=class'GOMRoleInfoSouth_ROK_Radioman',Count=2)
	SouthernRoles(6)=(RoleInfoClass=class'GOMRoleInfoSouth_US_Pilot_Combat',Count=0)
	SouthernRoles(7)=(RoleInfoClass=class'GOMRoleInfoSouth_US_Pilot_Transport',Count=0)
	
	NorthernForce=NFOR_NLF
}
