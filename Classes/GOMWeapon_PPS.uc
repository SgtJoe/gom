//=============================================================================
// GOMWeapon_PPS.uc
//=============================================================================
// North Vietnamese Army PPS-43 SMG.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_PPS extends ROProjectileWeapon
	abstract;

simulated event PostInitAnimTree(SkeletalMeshComponent SkelComp)
{
	super.PostInitAnimTree(SkelComp);
	
	if( SightRotController != none )
	{
		SightRotController.BoneRotation.Roll = SightRanges[SightRangeIndex].SightPitch * -1;
	}
}

simulated function SightIndexUpdated()
{
	if( SightRotController != none )
	{
		SightRotController.BoneRotation.Roll = SightRanges[SightRangeIndex].SightPitch * -1;
	}
	
	IronSightPosition.Z=SightRanges[SightRangeIndex].SightPositionOffset;
	PlayerViewOffset.Z=SightRanges[SightRangeIndex].SightPositionOffset;
}

defaultproperties
{
	WeaponContentClass(0)="GOM3.GOMWeapon_PPS_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'GOM3_UI.WP_Render.VN_Weap_PPS'
	
	WeaponClassType=ROWCT_SMG
	TeamIndex=`AXIS_TEAM_INDEX
	
	InvIndex=`WI_PPS
	bIsModWeapon=true
	
	Category=ROIC_Primary
	Weight=1.95
	InventoryWeight=2

	PlayerIronSightFOV=55.0

	// MAIN FIREMODE
	FiringStatesArray(0)=WeaponFiring
	WeaponFireTypes(0)=EWFT_Custom
	WeaponProjectiles(0)=class'PPSH41Bullet'
	FireInterval(0)=+0.093
	DelayedRecoilTime(0)=0.0
	Spread(0)=0.0022 // 8 MOA
	WeaponDryFireSnd=AkEvent'WW_WEP_Shared.Play_WEP_Generic_Dry_Fire'

	// ALT FIREMODE
	FiringStatesArray(ALTERNATE_FIREMODE)=none
	WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_None
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'PPSH41Bullet'
	bLoopHighROFSounds(ALTERNATE_FIREMODE)=false
	FireInterval(ALTERNATE_FIREMODE)=+0.093
	DelayedRecoilTime(ALTERNATE_FIREMODE)=0.0
	Spread(ALTERNATE_FIREMODE)=0.01

	PreFireTraceLength=1250 //25 Meters
	FireTweenTime=0.05

	AltFireModeType=ROAFT_None

	// AI
	MinBurstAmount=2
	MaxBurstAmount=4
	BurstWaitTime=1.0
	AISpreadScale=20.0

	// Recoil
	maxRecoilPitch=190//170
	minRecoilPitch=190//170//140
	maxRecoilYaw=80
	minRecoilYaw=-80
	RecoilRate=0.06//0.06
	RecoilMaxYawLimit=500
	RecoilMinYawLimit=65035
	RecoilMaxPitchLimit=750
	RecoilMinPitchLimit=64785
	RecoilISMaxYawLimit=600//500
	RecoilISMinYawLimit=65035
	RecoilISMaxPitchLimit=500
	RecoilISMinPitchLimit=65035
   	RecoilBlendOutRatio=1
   	//PostureHippedRecoilModifer=2.0
   	//PostureShoulderRecoilModifer=1.75

	// InstantHitDamage(0)=230
	// InstantHitDamage(1)=230
	InstantHitDamage(0)=54
	InstantHitDamage(1)=54

	InstantHitDamageTypes(0)=class'RODmgType_PPSH41Bullet'
	InstantHitDamageTypes(1)=class'RODmgType_PPSH41Bullet'

	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_SMG'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'

	// Shell eject FX
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_PPSH'

	bHasIronSights=true

	WeaponPutDownAnim=PPS42_putaway
	WeaponEquipAnim=PPS42_pullout
	WeaponDownAnim=PPS42_Down
	WeaponUpAnim=PPS42_Up

	// Fire Anims
	//Hip fire
	WeaponFireAnim(0)=PPS42_Hip_Shoot1
	WeaponFireAnim(1)=PPS42_Hip_Shoot1
	WeaponFireLastAnim=PPS42_shootLAST
	//Shouldered fire
	WeaponFireShoulderedAnim(0)=PPS42_Hip_Shoot1
	WeaponFireShoulderedAnim(1)=PPS42_Hip_Shoot1
	WeaponFireLastShoulderedAnim=PPS42_shootLAST
	//Fire using iron sights
	WeaponFireSightedAnim(0)=PPS42_iron_shoot
	WeaponFireSightedAnim(1)=PPS42_iron_shoot
	WeaponFireLastSightedAnim=PPS42_iron_shootLAST

	// Idle Anims
	//Hip Idle
	WeaponIdleAnims(0)=PPS42_shoulder_idle
	WeaponIdleAnims(1)=PPS42_shoulder_idle
	// Shouldered idle
	WeaponIdleShoulderedAnims(0)=PPS42_shoulder_idle
	WeaponIdleShoulderedAnims(1)=PPS42_shoulder_idle
	//Sighted Idle
	WeaponIdleSightedAnims(0)=PPS42_iron_idle
	WeaponIdleSightedAnims(1)=PPS42_iron_idle

	// Prone Crawl
	WeaponCrawlingAnims(0)=PPS42_CrawlF
	WeaponCrawlStartAnim=PPS42_Crawl_into
	WeaponCrawlEndAnim=PPS42_Crawl_out

	//Reloading
	WeaponReloadEmptyMagAnim=PPS42_reloadempty
	WeaponReloadNonEmptyMagAnim=PPS42_reloadhalf
	WeaponRestReloadEmptyMagAnim=PPS42_reloadempty_rest
	WeaponRestReloadNonEmptyMagAnim=PPS42_reloadhalf_rest
	// Ammo check
	WeaponAmmoCheckAnim=PPS42_ammocheck
	WeaponRestAmmoCheckAnim=PPS42_ammocheck_rest

	// Sprinting
	WeaponSprintStartAnim=PPS42_sprint_into
	WeaponSprintLoopAnim=PPS42_Sprint
	WeaponSprintEndAnim=PPS42_sprint_out
	Weapon1HSprintStartAnim=PPS42_ger_sprint_into
	Weapon1HSprintLoopAnim=PPS42_ger_sprint
	Weapon1HSprintEndAnim=PPS42_ger_sprint_out

	// Mantling
	WeaponMantleOverAnim=PPS42_Mantle

	// Cover/Blind Fire Anims
	WeaponRestAnim=PPS42_rest_idle
	WeaponEquipRestAnim=PPS42_pullout_rest
	WeaponPutDownRestAnim=PPS42_putaway_rest
	WeaponBlindFireRightAnim=PPS42_BF_Right_Shoot
	WeaponBlindFireLeftAnim=PPS42_BF_Left_Shoot
	WeaponBlindFireUpAnim=PPS42_BF_up_Shoot
	WeaponIdleToRestAnim=PPS42_idleTOrest
	WeaponRestToIdleAnim=PPS42_restTOidle
	WeaponRestToBlindFireRightAnim=PPS42_restTOBF_Right
	WeaponRestToBlindFireLeftAnim=PPS42_restTOBF_Left
	WeaponRestToBlindFireUpAnim=PPS42_restTOBF_up
	WeaponBlindFireRightToRestAnim=PPS42_BF_Right_TOrest
	WeaponBlindFireLeftToRestAnim=PPS42_BF_Left_TOrest
	WeaponBlindFireUpToRestAnim=PPS42_BF_up_TOrest
	WeaponBFLeftToUpTransAnim=PPS42_BFleft_toBFup
	WeaponBFRightToUpTransAnim=PPS42_BFright_toBFup
	WeaponBFUpToLeftTransAnim=PPS42_BFup_toBFleft
	WeaponBFUpToRightTransAnim=PPS42_BFup_toBFright
	// Blind Fire ready
	WeaponBF_Rest2LeftReady=PPS42_restTO_L_ready
	WeaponBF_LeftReady2LeftFire=PPS42_L_readyTOBF_L
	WeaponBF_LeftFire2LeftReady=PPS42_BF_LTO_L_ready
	WeaponBF_LeftReady2Rest=PPS42_L_readyTOrest
	WeaponBF_Rest2RightReady=PPS42_restTO_R_ready
	WeaponBF_RightReady2RightFire=PPS42_R_readyTOBF_R
	WeaponBF_RightFire2RightReady=PPS42_BF_RTO_R_ready
	WeaponBF_RightReady2Rest=PPS42_R_readyTOrest
	WeaponBF_Rest2UpReady=PPS42_restTO_Up_ready
	WeaponBF_UpReady2UpFire=PPS42_Up_readyTOBF_Up
	WeaponBF_UpFire2UpReady=PPS42_BF_UpTO_Up_ready
	WeaponBF_UpReady2Rest=PPS42_Up_readyTOrest
	WeaponBF_LeftReady2Up=PPS42_L_ready_toUp_ready
	WeaponBF_LeftReady2Right=PPS42_L_ready_toR_ready
	WeaponBF_UpReady2Left=PPS42_Up_ready_toL_ready
	WeaponBF_UpReady2Right=PPS42_Up_ready_toR_ready
	WeaponBF_RightReady2Up=PPS42_R_ready_toUp_ready
	WeaponBF_RightReady2Left=PPS42_R_ready_toL_ready
	WeaponBF_LeftReady2Idle=PPS42_L_readyTOidle
	WeaponBF_RightReady2Idle=PPS42_R_readyTOidle
	WeaponBF_UpReady2Idle=PPS42_Up_readyTOidle
	WeaponBF_Idle2UpReady=PPS42_idleTO_Up_ready
	WeaponBF_Idle2LeftReady=PPS42_idleTO_L_ready
	WeaponBF_Idle2RightReady=PPS42_idleTO_R_ready

	// Melee anims
	WeaponMeleeAnims(0)=PPS42_Bash
	WeaponMeleeHardAnim=PPS42_BashHard
	MeleePullbackAnim=PPS42_Pullback
	MeleeHoldAnim=PPS42_Pullback_Hold

	ReloadMagazinEmptyCameraAnim=CameraAnim'1stperson_Cameras.Anim.Camera_MP40_reloadempty'

	EquipTime=+0.75
	PutDownTime=+0.50

	bDebugWeapon = false

	BoltControllerNames[0]=BoltSlide_PPS42

	SuppressionPower=5

	ISFocusDepth=28
	ISFocusBlendRadius=0.5

	// Ammo
	AmmoClass=class'ROAmmo_762x25_PPSHStick'
	MaxAmmoCount=35
	bUsesMagazines=true
	InitialNumPrimaryMags=6
	bPlusOneLoading=false
	bCanReloadNonEmptyMag=true
	PenetrationDepth=15
	MaxPenetrationTests=3
	MaxNumPenetrations=2
	PerformReloadPct=0.85f

	PlayerViewOffset=(X=1,Y=4.5,Z=-1.5)//(X=1,Y=4,Z=-2)
	ZoomInRotation=(Pitch=-910,Yaw=0,Roll=2910)
	ShoulderedTime=0.5
	ShoulderedPosition=(X=1.75,Y=2,Z=-0.75)//(X=3,Y=2.5,Z=-1.0)
	ShoulderRotation=(Pitch=-300,Yaw=500,Roll=1500)

	bUsesFreeAim=true

	ZoomInTime=0.25//0.3
	ZoomOutTime=0.25//0.22
	IronSightPosition=(X=-1,Y=0,Z=0.04)
	// RetractedStockIronSightPosition=(X=3,Y=1,Z=-1)
	RetractedStockIronSightPosition=(X=5,Y=0.5,Z=-0.1)
	RetractedStockAlignmentScale = 0.3f
	RecoilOffsetModY = 1300.f
	RecoilOffsetModZ = 1100.f
	//SwayOffsetMod = 1200.f

	// Free Aim variables
	//FreeAimMaxYawLimit=2000
	//FreeAimMinYawLimit=63535
	//FreeAimMaxPitchLimit=1500
	//FreeAimMinPitchLimit=64035
	//FreeAimISMaxYawLimit=500
	//FreeAimISMinYawLimit=65035
	//FreeAimISMaxPitchLimit=350
	//FreeAimISMinPitchLimit=65185
	//FullFreeAimISMaxYaw=350
	//FullFreeAimISMinYaw=65185
	//FullFreeAimISMaxPitch=250
	//FullFreeAimISMinPitch=65285
	//FreeAimSpeedScale=0.35
	//FreeAimISSpeedScale=0.8
	FreeAimHipfireOffsetX=35

	Begin Object Class=ForceFeedbackWaveform Name=ForceFeedbackWaveformShooting1
		Samples(0)=(LeftAmplitude=30,RightAmplitude=30,LeftFunction=WF_Constant,RightFunction=WF_Constant,Duration=0.100)
	End Object
	WeaponFireWaveForm=ForceFeedbackWaveformShooting1

	CollisionCheckLength=35.0

	FireCameraAnim[0]=CameraAnim'1stperson_Cameras.Anim.Camera_PPSh_Shoot'
	FireCameraAnim[1]=CameraAnim'1stperson_Cameras.Anim.Camera_PPSh_Shoot'

	SightRotControlName=Sight_Rotation

	SightRanges[0]=(SightRange=100,SightPitch=0,SightPositionOffset=0.025,AddedPitch=35)
	SightRanges[1]=(SightRange=200,SightPitch=16384,SightPositionOffset=-0.125,AddedPitch=40)

	SwayScale=1.1
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_MAT49_7_62.Play_WEP_MAT49_7_62_Loop_3P', FirstPersonCue=AkEvent'WW_WEP_MAT49_7_62.Play_WEP_MAT49_7_62_Loop')
	WeaponFireSnd(ALTERNATE_FIREMODE)=(DefaultCue= AkEvent'WW_WEP_MAT49_9mm.Play_WEP_MAT49_9mm_Single_3P', FirstPersonCue=AkEvent'WW_WEP_MAT49_9mm.Play_WEP_MAT49_9mm_Fire_Single')
	
	bLoopingFireSnd(DEFAULT_FIREMODE)=true
	WeaponFireLoopEndSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_MAT49_7_62.Play_WEP_MAT49_7_62_Tail_3P', FirstPersonCue=AkEvent'WW_WEP_MAT49_7_62.Play_WEP_MAT49_7_62_Tail')
	bLoopHighROFSounds(DEFAULT_FIREMODE)=true
}

