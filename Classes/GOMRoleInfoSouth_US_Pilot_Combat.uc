//=============================================================================
// GOMRoleInfoSouth_US_Pilot_Combat.uc
//=============================================================================
// United States Army Combat Pilot Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_US_Pilot_Combat extends GOMRoleInfoSouth_US;

DefaultProperties
{
	RoleType=RORIT_AirCrew
	ClassTier=3
	ClassIndex=`ROCI_COMBATPILOT
	
	bIsPilot=true
	bBotSelectable=false
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M1911_Pistol')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		SecondaryWeapons=(class'ROWeap_M1911_Pistol',class'ROWeap_M1917_Pistol')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M1911_Pistol')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_M1911_Pistol')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_combatpilot'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_combatpilot'
}
