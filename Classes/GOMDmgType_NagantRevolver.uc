//=============================================================================
// GOMDmgType_NagantRevolver.uc
//=============================================================================
// Damage type for the Nagant Revolver bullet.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMDmgType_NagantRevolver extends RODmgType_SmallArmsBullet
	abstract;

DefaultProperties
{
	WeaponClass=GOMWeapon_NagantRevolver
	KDamageImpulse=121.5
	BloodSprayTemplate=ParticleSystem'FX_VN_Impacts.BloodNGore.FX_VN_BloodSpray_Clothes_small'
}
