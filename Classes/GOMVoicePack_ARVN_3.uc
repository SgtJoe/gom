//=============================================================================
// GOMVoicePack_ARVN_3.uc
//=============================================================================
// ARVN Voice Pack.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVoicePack_ARVN_3 extends ROVoicePackSVietTeam03;

defaultproperties
{
	VoiceComs[`VOICECOM_TakeCover]=(Sound="AKEvent'WW_VOX_ARVN_Voice3.Play_ARVN_Soldier_3_TakingFireGeneric'")
	VoiceComs[`VOICECOM_AIConfirm]=(Sound="AKEvent'WW_VOX_ARVN_Voice3.Play_ARVN_Soldier_3_Confirm'")
	VoiceComs[`VOICECOM_HeloIdleSituation]=(Sound="AKEvent'WW_VOX_ARVN_Voice3.Play_ARVN_Pilot_3_HeloIdleGround'")
}
