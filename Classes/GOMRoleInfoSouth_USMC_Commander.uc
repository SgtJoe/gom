//=============================================================================
// GOMRoleInfoSouth_USMC_Commander.uc
//=============================================================================
// United States Army Commander Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_USMC_Commander extends GOMRoleInfoSouth_US_Commander;

DefaultProperties
{
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M14_Rifle',class'ROWeap_M2_Carbine',class'ROWeap_M1A1_SMG')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M16A1_AssaultRifle',class'ROWeap_M14_Rifle',class'ROWeap_M2_Carbine')
	)}
}
