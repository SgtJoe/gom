//=============================================================================
// GOMWeapon_PPSH.uc
//=============================================================================
// North Vietnamese Army PPSH SMG.
// Modified to only carry drum ammo.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMWeapon_PPSH extends ROWeap_PPSH41_SMG
	abstract;

defaultproperties
{
	WeaponContentClass.Empty
	WeaponContentClass(0)="GOM3.GOMWeapon_PPSH_ActualContent"
	
	AmmoContentClassStart=-1
	AltAmmoLoadouts.Empty
	
	RoleSelectionImage.Empty
	RoleSelectionImage(0)=Texture2D'VN_UI_Textures.WeaponTex.VN_Weap_PPSH41_SMG_Drum'
}