//=============================================================================
// GOMRoleInfoSouth_ROK_Grenadier.uc
//=============================================================================
// Republic of Korea Grenadier Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ROK_Grenadier extends GOMRoleInfoSouth_ROK;

DefaultProperties
{
	RoleType=RORIT_Support
	ClassTier=2
	ClassIndex=`ROCI_HEAVY
	
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'ROWeap_M79_GrenadeLauncherSmoke',class'GOMWeapon_M1_Garand_M7'),
	
	SecondaryWeapons=(class'ROWeap_M2_Carbine'),
	DisableSecondaryForPrimary=(false,true),
	
	OtherItems=(class'GOMWeapon_M7RifleGrenade'),
	OtherItemsStartIndexForPrimary=(0, 0),
	NumOtherItemsForPrimary=(255, 0)
	
	)}
	
	bAllowPistolsInRealism=true
	bUseRootSecondaryWeapsForSL=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_grenadier'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_grenadier'
}
