//=============================================================================
// GOMDmgType_RPG2.uc
//=============================================================================
// Damage type for the RPG-2 rocket.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMDmgType_RPG2 extends RODmgType_RPG7Rocket
	abstract;

DefaultProperties
{
	WeaponClass=GOMWeapon_RPG2_ActualContent
	WeaponShortName="RPG2"
}
