//=============================================================================
// GOMModWeaponInfo_M2A17.uc
//=============================================================================
// UI information for the M2A1-7 Flamethrower.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfo_M2A17 extends ROModWeaponInfo;

DefaultProperties
{
	ModWeaponClass=GOMWeapon_M2A17_Flamethrower_ActualContent
	
	InventoryIconTexturePath="VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_M9_Flamer"
	
	AmmoIconTexturePath="VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M2_Flamer"
	
	KillIconTexturePath="VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_M9A1"
	bHasSquareKillIcon=true
}
