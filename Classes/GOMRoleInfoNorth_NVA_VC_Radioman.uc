//=============================================================================
// GOMRoleInfoNorth_NVA_VC_Radioman.uc
//=============================================================================
// North Vietnamese Army (Viet Cong Support) Radio Operator Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_NVA_VC_Radioman extends GOMRoleInfoNorth_NVA_Radioman;

DefaultProperties
{
	RoleType=RORIT_Radioman
	ClassTier=3
	ClassIndex=`ROCI_RADIOMAN
	bIsRadioman=true
	
	Items[RORIGM_Default]={(
		SquadLeaderItems=(class'ROItem_Binoculars',class'ROItem_TunnelTool')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		SquadLeaderItems=(class'ROItem_Binoculars',class'ROItem_TunnelTool')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		SquadLeaderItems=(class'ROItem_Binoculars',class'ROItem_TunnelTool')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		SquadLeaderItems=(class'ROItem_Binoculars',class'ROItem_TunnelTool')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_radioman'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_radioman'
}
