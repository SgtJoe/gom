//=============================================================================
// GOMDmgType_Bowie.uc
//=============================================================================
// Damage type for the Bowie Combat Knife.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMDmgType_Bowie extends RODmgType_MeleePierce
	abstract;

DefaultProperties
{
	WeaponClass=GOMWeapon_BowieKnife_ActualContent
	WeaponShortName="Knife"
	KDamageImpulse=625
}
