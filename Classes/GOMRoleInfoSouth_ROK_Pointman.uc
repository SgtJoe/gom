//=============================================================================
// GOMRoleInfoSouth_ROK_Pointman.uc
//=============================================================================
// Republic of Korea Pointman Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ROK_Pointman extends GOMRoleInfoSouth_ROK;

DefaultProperties
{
	RoleType=RORIT_Scout
	ClassTier=2
	ClassIndex=`ROCI_SCOUT
	
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'ROWeap_M2_Carbine'),
	
	OtherItems=(class'ROWeap_M8_Smoke'),
	
	SquadLeaderItems=(class'ROItem_BinocularsUS',class'ROWeap_M18_SignalSmoke')
	
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_scout'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_scout'
}
