//=============================================================================
// GOMRoleInfoSouth_ARVN_Radioman.uc
//=============================================================================
// Republic of Vietnam Army Radio Operator Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ARVN_Radioman extends GOMRoleInfoSouth_ARVN;

DefaultProperties
{
	RoleType=RORIT_Radioman
	ClassTier=3
	ClassIndex=`ROCI_RADIOMAN
	bIsRadioman=true
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M1Garand_Rifle',class'ROWeap_M2_Carbine')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_M1Garand_Rifle',class'ROWeap_M2_Carbine')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_M1Garand_Rifle',class'ROWeap_M16A1_AssaultRifle')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_radioman'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_radioman'
}
