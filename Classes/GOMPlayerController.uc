//=============================================================================
// GOMPlayerController.uc
//=============================================================================
// GOM Player Controller.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMPlayerController extends ROPlayerController
	config(Mutator_GOM_Client);

// North Roleinfos
var array< class<RORoleInfo> > NVARoles;
var array< class<RORoleInfo> > VCRoles;

// South RoleInfos
var array< class<RORoleInfo> > USRoles;
var array< class<RORoleInfo> > USMCRoles;
var array< class<RORoleInfo> > MACVRoles;
var array< class<RORoleInfo> > AUSRoles;
var array< class<RORoleInfo> > ARVNRoles;
var array< class<RORoleInfo> > ARVNRangerRoles;

var array<RORoleCount> MapNorthRoles;
var array<RORoleCount> MapSouthRoles;
var bool updatedPilots;

// Player Information Messages
var config bool DisableModWatermark;

//=============================================================================
// WARNING: All changes to MapInfo have to happen here, otherwise they
// will NOT be replicated. In addition, any function that is not declared
// as Simulated will not be replicated either.
//=============================================================================
simulated function PreBeginPlay()
{
	if (WorldInfo.NetMode == NM_Standalone)
	{
		PerformModifications();
	}
	
	super.PreBeginPlay();
}

// Have to hijack this function because GRI does not seem to exist in PreBeginPlay OR PostBeginPlay even though it's created in PreBeginPlay
simulated function ReceivedGameClass(class<GameInfo> GameClass)
{
	super.ReceivedGameClass(GameClass);
	
	if (WorldInfo.NetMode != NM_Standalone)
	{
		PerformModifications();
	}
}

simulated function PerformModifications()
{
	`gom(self @ "starting up Gameplay Overhaul Mutator" @ `BUILDVER, 'Init-Client');
	
	// Just in case the client ini hasn't been created yet
	SaveConfig();
	
	if (WorldInfo.NetMode == NM_Standalone)
	{
		`gom(self @ "calling" @ WorldInfo.Game.BaseMutator.class @ GOM3(WorldInfo.Game.BaseMutator), 'Init-Client');
		
		GOM3(WorldInfo.Game.BaseMutator).VerifyMapList();
	}
	
	`gom("GRI is now" @ WorldInfo.GRI, 'Replication');
	`gom("GRI MI:" @ GOMGameReplicationInfo(WorldInfo.GRI).RMapIndex, 'Replication');
	
	`gom(""$ WorldInfo.GRI @ "Data:" @ GOMGameReplicationInfo(WorldInfo.GRI).RNorthForce @ GOMGameReplicationInfo(WorldInfo.GRI).RSouthForce @ GOMGameReplicationInfo(WorldInfo.GRI).RMiniFaction, 'Init-Client');
	`gom(""$ self @ "Data:" @ `getNF @ `getSF @ `getMF, 'Init-Client');
	`gom("NVA on map?" @ `NVAMap, 'Init-Client');
	
	if (!`Campaign)
	{
		ReplaceGameData();
	}
	
	ReplaceSpawnFunctionality();
	
	AddWeaponInfo();
	
	ReplaceRoles();
	
	if (WorldInfo.NetMode == NM_Standalone)
	{
		GOM3(WorldInfo.Game.BaseMutator).ReplacePawns();
		GOM3(WorldInfo.Game.BaseMutator).ReplaceHelicopters();
	}
}

simulated function ReplaceGameData()
{
	// Make sure the factions in MapInfo are synced to ours, important for commander abilities
	
	if (`getSF == `USMC)
	{
		ROMapInfo(WorldInfo.GetMapInfo()).SouthernForce = SFOR_USMC;
	}
	else if (`getSF == `AUS)
	{
		ROMapInfo(WorldInfo.GetMapInfo()).SouthernForce = SFOR_AusArmy;
	}
	else if (`getSF == `ARVN || `getSF == `RANGERS)
	{
		ROMapInfo(WorldInfo.GetMapInfo()).SouthernForce = SFOR_ARVN;
	}
	else
	{
		ROMapInfo(WorldInfo.GetMapInfo()).SouthernForce = SFOR_USArmy;
	}
	
	ROMapInfo(WorldInfo.GetMapInfo()).NorthernForce = (`NVAMap) ? NFOR_NVA : NFOR_NLF;
	
`ifndef(RELEASE)
	ROMapInfo(WorldInfo.GetMapInfo()).MinimumTimeDead = 0;
	ROMapInfo(WorldInfo.GetMapInfo()).AxisReinforcementDelay16 = 3;
	ROMapInfo(WorldInfo.GetMapInfo()).AxisReinforcementDelay32 = 3;
	ROMapInfo(WorldInfo.GetMapInfo()).AxisReinforcementDelay64 = 3;
	ROMapInfo(WorldInfo.GetMapInfo()).AlliesReinforcementDelay16 = 3;
	ROMapInfo(WorldInfo.GetMapInfo()).AlliesReinforcementDelay32 = 3;
	ROMapInfo(WorldInfo.GetMapInfo()).AlliesReinforcementDelay64 = 3;
`endif
}

simulated function ReplaceSpawnFunctionality()
{
	if (`NVAMap)
	{
		// Make the NVA spawn on SL, only the VC use tunnels
		ROGameInfo(WorldInfo.Game).SquadSpawnMethod[`AXIS_TEAM_INDEX] = 1 /*ROSSM_SquadLeader*/;
		ROGameReplicationInfo(WorldInfo.GRI).SquadSpawnMethod[`AXIS_TEAM_INDEX] = 1 /*ROSSM_SquadLeader*/;
		
		`gom("Spawn method:" @ ROGameInfo(WorldInfo.Game).SquadSpawnMethod[`AXIS_TEAM_INDEX], 'Spawning');
	}
}

simulated function AddWeaponInfo()
{
	local int i;
	local ROMapInfo MI;
	MI = ROMapInfo(WorldInfo.GetMapInfo());
	
	for (i = 0; i < class'GOMModWeaponInfoList'.default.WeaponInfos.length; i++)
	{
		MI.ModWeaponInfoList.AddItem(class'GOMModWeaponInfoList'.default.WeaponInfos[i]);
	}
	
	UpdateHUDForModWeapons(MI);
}

simulated function ReplaceRoles(optional name log = 'Roles')
{
	local int i;
	local ROMapInfo MI;
	local array<RORoleCount> OldRoles;
	local array< class<RORoleInfo> > NewRoles;
	local RORoleCount NewRoleCount;
	
	if (WorldInfo.GetMapInfo().IsA('GOMMapInfoROK'))
	{
		`gom("ROK maps have roles already set", log);
		return;
	}
	
	if (WorldInfo.GRI.GameClass.static.GetGameType() == ROGT_Skirmish)
	{
		`gom("Not changing Skirmish roles", log);
		return;
	}
	
	MI = ROMapInfo(WorldInfo.GetMapInfo());
	
	if (log == name("Roles"))
	{
		CheckData();
	}
	
	StripDuplicateRoles(log);
	
	`gom("WARNING - CAMPAIGNPHASE:" @ `CampaignPhase,'Campaign');
	
	`gom("Replacing North roles", log);
	
	// Grab the old roles then clear them
	OldRoles = MI.NorthernRoles;
	MI.NorthernRoles.length = 0;
	
	// Look at the map factions to see what role array we need
	switch (`getNF)
	{
		case `NVA:
		case `NVAU:
			NewRoles = default.NVARoles;
			break;
			
		default:
			NewRoles = default.VCRoles;
	}
	
	// Don't add a Commander if the slot is empty or being filled by a non-commander class (Skirmish maps do this)
	if (MI.NorthernTeamLeader.RoleInfo != none)
	{
		if (MI.NorthernTeamLeader.RoleInfo.default.ClassIndex == `ROCI_COMMANDER)
		{
			MI.NorthernTeamLeader.RoleInfo = new NewRoles[`ROCI_COMMANDER];
		}
		else
		{
			MI.NorthernTeamLeader.RoleInfo = new NewRoles[`ROCI_RIFLEMAN];
		}
	}
	
	// North Roles
	for (i = 0; i < OldRoles.length; i++)
	{
		`gom("Old" @ static.GetRoleNameFromIndex(OldRoles[i].RoleInfoClass.default.ClassIndex) @ OldRoles[i].RoleInfoClass @ "w" @ OldRoles[i].Count @ "slots", log);
		
		NewRoleCount.RoleInfoClass = NewRoles[OldRoles[i].RoleInfoClass.default.ClassIndex];
		NewRoleCount.Count = OldRoles[i].Count;
		
		if (NewRoleCount.RoleInfoClass != none && NewRoleCount.Count != 0)
		{
			`gom("New" @ static.GetRoleNameFromIndex(NewRoleCount.RoleInfoClass.default.ClassIndex) @ NewRoleCount.RoleInfoClass @ "w" @ NewRoleCount.Count @ "slots", log);
			
			MI.NorthernRoles.AddItem(NewRoleCount);
		}
		else
		{
			`gom("Not adding empty role" @ static.GetRoleNameFromIndex(OldRoles[i].RoleInfoClass.default.ClassIndex), log);
		}
	}
	
	`gom("Double checking roles", log);
	
	// Loop through NewRoles to make sure we didn't miss any
	for (i = 0; i < `ROCI_COMMANDER; i++)
	{
		if (NewRoles[i] != none && !RoleExistsOnMap(NewRoles[i].default.ClassIndex))
		{
			`gom("Not found" @ static.GetRoleNameFromIndex(NewRoles[i].default.ClassIndex), log);
			
			if (NewRoles[i].default.ClassIndex == `ROCI_RADIOMAN)
			{
				// Don't add radiomen if there's no commander
				if (MI.NorthernTeamLeader.RoleInfo != none)
				{
					NewRoleCount.RoleInfoClass = NewRoles[i];
					NewRoleCount.Count = 1;
					
					`gom("Adding" @ NewRoleCount.Count @ NewRoleCount.RoleInfoClass, log);
					
					MI.NorthernRoles.AddItem(NewRoleCount);
				}
				else
				{
					`gom("No commander on map, not adding radioman", log);
				}
			}
			// Viet Cong Main Force with AKs
			else if (NewRoles[i].default.ClassIndex == `ROCI_HEAVY && `getNF <= `VC)
			{
				NewRoleCount.RoleInfoClass = NewRoles[i];
				NewRoleCount.Count = `VC_AK_LIMIT;
				
				// NEW: In Late War Campaign they have unlimited AKs
				if (`CampaignPhase == `CP_LATE)
				{
					NewRoleCount.Count = 255;
				}
				
				`gom("Adding" @ NewRoleCount.Count @ NewRoleCount.RoleInfoClass, log);
				
				MI.NorthernRoles.AddItem(NewRoleCount);
			}
			else
			{
				NewRoleCount.RoleInfoClass = NewRoles[i];
				NewRoleCount.Count = 2;
				
				`gom("Adding" @ NewRoleCount.Count @ NewRoleCount.RoleInfoClass, log);
				
				MI.NorthernRoles.AddItem(NewRoleCount);
			}
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	`gom("Replacing South roles", log);
	
	// Grab the old roles then clear them
	OldRoles = MI.SouthernRoles;
	MI.SouthernRoles.length = 0;
	
	// Look at the map factions to see what role array we need
	switch (`getSF)
	{
		case `USMC:
			NewRoles = default.USMCRoles;
			break;
		
		case `AUS:
			NewRoles = default.AUSRoles;
			break;
		
		case `ARVN:
			NewRoles = default.ARVNRoles;
			break;
		
		case `RANGERS:
			NewRoles = default.ARVNRangerRoles;
			break;
		
		case `MACV:
			NewRoles = default.MACVRoles;
			break;
			
		default:
			NewRoles = default.USRoles;
	}
	
	// Don't add a Commander if the slot is empty or being filled by a non-commander class (Skirmish maps do this)
	if (MI.SouthernTeamLeader.RoleInfo != none)
	{
		if (MI.SouthernTeamLeader.RoleInfo.default.ClassIndex == `ROCI_COMMANDER)
		{
			MI.SouthernTeamLeader.RoleInfo = new NewRoles[`ROCI_COMMANDER];
		}
		else
		{
			MI.SouthernTeamLeader.RoleInfo = new NewRoles[`ROCI_RIFLEMAN];
		}
	}
	
	// South Roles
	for (i = 0; i < OldRoles.length; i++)
	{
		`gom("Old" @ static.GetRoleNameFromIndex(OldRoles[i].RoleInfoClass.default.ClassIndex) @ OldRoles[i].RoleInfoClass @ "w" @ OldRoles[i].Count @ "slots", log);
		
		if (`getSF < `ARVN && `getMF == MF_ARVNSupport && 
			(OldRoles[i].RoleInfoClass.default.ClassIndex == `ROCI_RADIOMAN /*|| 
			OldRoles[i].RoleInfoClass.default.ClassIndex == `ROCI_MACHINEGUNNER || 
			OldRoles[i].RoleInfoClass.default.ClassIndex == `ROCI_SCOUT*/)
			&& !(`Campaign)
		)
		{
			NewRoleCount.RoleInfoClass = default.ARVNRoles[OldRoles[i].RoleInfoClass.default.ClassIndex];
		}
		else
		{
			NewRoleCount.RoleInfoClass = NewRoles[OldRoles[i].RoleInfoClass.default.ClassIndex];
		}
		
		NewRoleCount.Count = OldRoles[i].Count;
		
		// When replacing the USMC Loach with a Gunship Huey, we need to add 1 extra pilot role for Huey copilot
		if (OldRoles[i].RoleInfoClass.default.ClassIndex == `ROCI_COMBATPILOT && `getSF == `USMC && NewRoleCount.Count > 0 && !updatedPilots)
		{
			NewRoleCount.Count++;
			updatedPilots = true;
		}
		
		// No combat pilots in Early or Late War
		if (OldRoles[i].RoleInfoClass.default.ClassIndex == `ROCI_COMBATPILOT && `Campaign && `CampaignPhase != `CP_MID)
		{
			NewRoleCount.Count = 0;
		}
		
		if (NewRoleCount.RoleInfoClass != none && NewRoleCount.Count != 0)
		{
			`gom("New" @ static.GetRoleNameFromIndex(NewRoleCount.RoleInfoClass.default.ClassIndex) @ NewRoleCount.RoleInfoClass @ "w" @ NewRoleCount.Count @ "slots", log);
			
			MI.SouthernRoles.AddItem(NewRoleCount);
		}
		else
		{
			`gom("Not adding empty role" @ static.GetRoleNameFromIndex(OldRoles[i].RoleInfoClass.default.ClassIndex), log);
		}
		
	}
	
	`gom("Double checking roles", log);
	
	// Loop through NewRoles to make sure we didn't miss any
	for (i = 0; i < `ROCI_COMMANDER; i++)
	{
		if (NewRoles[i] != none && !RoleExistsOnMap(NewRoles[i].default.ClassIndex, true))
		{
			`gom("Not found" @ static.GetRoleNameFromIndex(NewRoles[i].default.ClassIndex), log);
			
			if (NewRoles[i].default.ClassIndex != `ROCI_RADIOMAN)
			{
				NewRoleCount.RoleInfoClass = NewRoles[i];
				NewRoleCount.Count = 2;
				
				`gom("Adding" @ NewRoleCount.Count @ NewRoleCount.RoleInfoClass, log);
				
				MI.SouthernRoles.AddItem(NewRoleCount);
			}
		}
	}
	
	if (log == name("Roles"))
	{
		CheckData();
	}
}

simulated function StripDuplicateRoles(optional name log = 'Roles')
{
	local int i, CurrentClassIndex;
	local array<int> AllRolesCount;
	local ROMapInfo MI;
	
	MI = ROMapInfo(WorldInfo.GetMapInfo());
	AllRolesCount.length = 11;
	
	// Go through all the roles and count how many there are
	for (i = 0; i < MI.NorthernRoles.length; i++)
	{
		CurrentClassIndex = MI.NorthernRoles[i].RoleInfoClass.default.ClassIndex;
		
		AllRolesCount[CurrentClassIndex] += 1;
	}
	
	// Report
	`gom("AllRolesCount NORTH:", log);
	for (i = 0; i < AllRolesCount.length; i++)
	{
		`gom("CI:" @ i @ static.GetRoleNameFromIndex(i) @ "Amount:" @ AllRolesCount[i], log);
	}
	
	// If there's more than one of a role remove the extras
	for (i = 0; i < AllRolesCount.length; i++)
	{
		while (AllRolesCount[i] > 1)
		{
			`gom("Stripping duplicate role" @ i @ static.GetRoleNameFromIndex(i), log);
			
			MI.NorthernRoles.Remove(indexOfRole(i), 1);
			
			AllRolesCount[i] -= 1;
		}
	}
	
	// Reset the array
	for (i = 0; i < AllRolesCount.length; i++)
	{
		AllRolesCount[i] = 0;
	}
	
	// Go through all the roles and count how many there are
	for (i = 0; i < MI.SouthernRoles.length; i++)
	{
		CurrentClassIndex = MI.SouthernRoles[i].RoleInfoClass.default.ClassIndex;
		
		AllRolesCount[CurrentClassIndex] += 1;
	}
	
	// Report
	`gom("AllRolesCount SOUTH:", log);
	for (i = 0; i < AllRolesCount.length; i++)
	{
		`gom("CI:" @ i @ static.GetRoleNameFromIndex(i) @ "Amount:" @ AllRolesCount[i], log);
	}
	
	// If there's more than one of a role remove the extras
	for (i = 0; i < AllRolesCount.length; i++)
	{
		while (AllRolesCount[i] > 1)
		{
			`gom("Stripping duplicate role" @ i @ static.GetRoleNameFromIndex(i), log);
			
			MI.SouthernRoles.Remove(indexOfRole(i, true), 1);
			
			AllRolesCount[i] -= 1;
		}
	}
}

static function string GetRoleNameFromIndex(int index)
{
	switch (index)
	{
		case `ROCI_RIFLEMAN: return "ROCI_RIFLEMAN";
		case `ROCI_SCOUT: return "ROCI_SCOUT";
		case `ROCI_MACHINEGUNNER: return "ROCI_MACHINEGUNNER";
		case `ROCI_SNIPER: return "ROCI_SNIPER";
		case `ROCI_ENGINEER: return "ROCI_ENGINEER";
		case `ROCI_HEAVY: return "ROCI_HEAVY";
		case `ROCI_ANTITANK: return "ROCI_ANTITANK";
		case `ROCI_RADIOMAN: return "ROCI_RADIOMAN";
		case `ROCI_COMMANDER: return "ROCI_COMMANDER";
		case `ROCI_COMBATPILOT: return "ROCI_COMBATPILOT";
		case `ROCI_TRANSPORTPILOT: return "ROCI_TRANSPORTPILOT";
		
		// Custom Roles
		case `ROCI_TANKCREW: return "ROCI_TANKCREW";
	}
	
	return "UNKNOWN ROLE";
}

static function string GetArmyNameFromIndex(int index, int team)
{
	if (team == `AXIS_TEAM_INDEX && class'GOM3'.default.NorthArmyShortNames[index] != "")
	{
		return class'GOM3'.default.NorthArmyShortNames[index];
	}
	else if (team == `ALLIES_TEAM_INDEX && class'GOM3'.default.SouthArmyShortNames[index] != "")
	{
		return class'GOM3'.default.SouthArmyShortNames[index];
	}
	
	return "UNKNOWN ARMY";
}

simulated function bool RoleExistsOnMap(int roleindex, optional bool South = false)
{
	local int i;
	local ROMapInfo MI;
	MI = ROMapInfo(WorldInfo.GetMapInfo());
	
	if (South)
	{
		for (i = 0; i < MI.SouthernRoles.length; i++)
		{
			if (MI.SouthernRoles[i].RoleInfoClass.default.ClassIndex == roleindex)
			{
				return true;
			}
		}
	}
	else
	{
		for (i = 0; i < MI.NorthernRoles.length; i++)
		{
			if (MI.NorthernRoles[i].RoleInfoClass.default.ClassIndex == roleindex)
			{
				return true;
			}
		}
		
	}
	
	return false;
}

simulated function int indexOfRole(int roleindex, optional bool South = false)
{
	local int i;
	local ROMapInfo MI;
	MI = ROMapInfo(WorldInfo.GetMapInfo());
	
	if (South)
	{
		for (i = 0; i < MI.SouthernRoles.length; i++)
		{
			if (MI.SouthernRoles[i].RoleInfoClass.default.ClassIndex == roleindex)
			{
				`gom("Southern Role" @ static.GetRoleNameFromIndex(roleindex) @ "found at position" @ i, 'RoleIndex');
				return i;
			}
		}
	}
	else
	{
		for (i = 0; i < MI.NorthernRoles.length; i++)
		{
			if (MI.NorthernRoles[i].RoleInfoClass.default.ClassIndex == roleindex)
			{
				`gom("Northern Role" @ static.GetRoleNameFromIndex(roleindex) @ "found at position" @ i, 'RoleIndex');
				return i;
			}
		}
		
	}
	
	`gom("WARNING - could not find Role" @ static.GetRoleNameFromIndex(roleindex) @ "index" @ roleindex, 'Roles');
	return -1;
}

reliable protected server function ServerJoinSquad(int NewSquadIndex, optional bool bViaInvite)
{
	// Tank Crew aren't allowed to join squads.
	if (ROPlayerReplicationInfo(PlayerReplicationInfo).RoleInfo.ClassIndex == `ROCI_TANKCREW)
	{
		return;
	}
	else
	{
		super.ServerJoinSquad(NewSquadIndex, bViaInvite);
	}
}

simulated function FindUsableActor()
{
	super.FindUsableActor();
	
	// Only Tank Crew can enter tanks (for now)
	if (bInVehicleRange && ActorAimedAt.IsA('GOMVehicleTank') && ROPlayerReplicationInfo(PlayerReplicationInfo).RoleInfo.ClassIndex != `ROCI_TANKCREW)
	{
		bInVehicleRange = false;
	}
}

// Even if muted, the announcer cuts off all other sounds, so don't play announcer lines at all if their volume is 0
function PlayAnnouncerSound(byte VoxType, byte Team, byte VOXIndex, optional byte SubIndex, optional vector PlayLocation, optional Actor Speaker, optional int SeatIndex)
{
	if (class'AudioDevice'.default.AkAnnouncerVolume > 0 || VoxType == EROAVT_Radio)
	{
		super.PlayAnnouncerSound(VoxType, Team, VOXIndex, SubIndex, PlayLocation, Speaker, SeatIndex);
	}
}

// Overridden CCM spawner function to spawn our class instead
function InitialiseCCMs()
{
	local ROCharacterPreviewActor ROCPA, CPABoth;
	local ROCharCustMannequin TempCCM;
	
	if( WorldInfo.NetMode == NM_DedicatedServer )
		return;
	
	if( ROCCC == none )
	{
		ROCCC = Spawn(class'ROCharCustController');
		
		if( ROCCC != none )
			ROCCC.ROPCRef = self;
	}
	
	foreach WorldInfo.DynamicActors(class'ROCharacterPreviewActor', ROCPA)
	{
		if( ROCPA.OwningTeam == EOT_Both )
		{
			CPABoth = ROCPA;
		}
		else if( AllCCMs[ROCPA.OwningTeam] == none )
		{
			AllCCMs[ROCPA.OwningTeam] = Spawn(class'GOMCharCustMannequin', self,, ROCPA.Location, ROCPA.Rotation);
		}
	}
	
	if( AllCCMs[0] == none || AllCCMs[1] == none )
	{
		if( CPABoth != none )
			TempCCM = Spawn(class'GOMCharCustMannequin', self,, CPABoth.Location, CPABoth.Rotation);
		else
		{
			TempCCM = Spawn(class'GOMCharCustMannequin', self, , vect(0,0,100));
			`warn("Couldn't find an ROCharacterPreviewActor, the level designer has not added one to the map! Creating a default one"@TempCCM);
		}
		
		TempCCM.SetOwningTeam(EOT_Both);
		
		if( AllCCMs[0] == none )
			AllCCMs[0] = TempCCM;
		
		if( AllCCMs[1] == none )
			AllCCMs[1] = TempCCM;
	}
}

simulated function CreateVoicePacks(byte TeamIndex)
{
	if (WorldInfo.NetMode != NM_DedicatedServer)
	{
		AnnouncerPacks[`AXIS_TEAM_INDEX] = new(Outer) AllAnnouncerPacks[0];
		AnnouncerPacks[`ALLIES_TEAM_INDEX] = new(Outer) AllAnnouncerPacks[ROMapInfo(WorldInfo.GetMapInfo()).GetSouthernNation()];
		AnnouncerPacks[`AXIS_TEAM_INDEX].InitObjCooldowns();
		AnnouncerPacks[`ALLIES_TEAM_INDEX].InitObjCooldowns();
	}
	
	NorthTeamVoicePacks[0] = class'ROVoicePackNVietTeam01';
	NorthTeamVoicePacks[1] = class'ROVoicePackNVietTeam02';
	NorthTeamVoicePacks[2] = class'ROVoicePackNVietTeam03';
	
	switch (`getSF)
	{
		case `AUS:
			SouthTeamVoicePacks[0] = class'GOMVoicePack_AUS_1';
			SouthTeamVoicePacks[1] = class'GOMVoicePack_AUS_2';
			SouthTeamVoicePacks[2] = class'GOMVoicePack_AUS_3';
			break;
		
		case `ARVN:
		case `RANGERS:
			SouthTeamVoicePacks[0] = class'GOMVoicePack_ARVN_1';
			SouthTeamVoicePacks[1] = class'GOMVoicePack_ARVN_2';
			SouthTeamVoicePacks[2] = class'GOMVoicePack_ARVN_3';
			break;
		
		case `ROK:
		case `ROKA:
		case `ROKMC:
			SouthTeamVoicePacks[0] = class'GOMVoicePack_ROK_1';
			SouthTeamVoicePacks[1] = class'GOMVoicePack_ROK_2';
			SouthTeamVoicePacks[2] = class'GOMVoicePack_ROK_3';
			break;
		
		default:
			SouthTeamVoicePacks[0] = class'GOMVoicePack_US_1';
			SouthTeamVoicePacks[1] = class'GOMVoicePack_US_2';
			SouthTeamVoicePacks[2] = class'GOMVoicePack_US_3';
			SouthTeamAltVoicePacks[0] = class'GOMVoicePack_ARVN_1';
			SouthTeamAltVoicePacks[1] = class'GOMVoicePack_ARVN_2';
			SouthTeamAltVoicePacks[2] = class'GOMVoicePack_ARVN_3';
	}
}

simulated exec function CheckData()
{
	local int i;
	local ROMapInfo MI;
	MI = ROMapInfo(WorldInfo.GetMapInfo());
	
	`gom("---------------------- CHECKING ----------------------", 'Verification');
	`gom("Checking North Roles:", 'VerificationRoles');
	`gom(" C" @ MI.NorthernTeamLeader.RoleInfo.class, 'VerificationRoles');
	for (i = 0; i < MI.NorthernRoles.length; i++)
	{
		`gom("" @ i @ MI.NorthernRoles[i].RoleInfoClass @ "count:" @ MI.NorthernRoles[i].Count, 'VerificationRoles');
	}
	
	`gom("Checking South Roles:", 'VerificationRoles');
	`gom(" C" @ MI.SouthernTeamLeader.RoleInfo.class, 'VerificationRoles');
	for (i = 0; i < MI.SouthernRoles.length; i++)
	{
		`gom("" @ i @ MI.SouthernRoles[i].RoleInfoClass @ "count:" @ MI.SouthernRoles[i].Count, 'VerificationRoles');
	}
	
	`gom("Checking Weapons:", 'VerificationWeapons');
	
	if (MI.ModWeaponInfoList.length == 0)
	{
		`gom("WARNING - ModWeaponInfoList is empty!", 'VerificationWeapons');
	}
	else
	{
		for (i = 0; i < MI.ModWeaponInfoList.length; i++)
		{
			`gom(i @ "-" @ MI.ModWeaponInfoList[i], 'VerificationWeapons');
			`gom("  ModWeaponClass:      " @ MI.ModWeaponInfoList[i].default.ModWeaponClass, 'VerificationWeapons');
			`gom("  WeaponDisplayName    " @ MI.ModWeaponInfoList[i].default.WeaponDisplayName, 'VerificationWeapons');
			`gom("  InventoryIconTexture:" @ Texture2D(DynamicLoadObject(MI.ModWeaponInfoList[i].default.InventoryIconTexturePath, class'Texture2D')), 'VerificationWeapons');
			`gom("  AmmoIconTexture:     " @ Texture2D(DynamicLoadObject(MI.ModWeaponInfoList[i].default.AmmoIconTexturePath, class'Texture2D')), 'VerificationWeapons');
			`gom("  KillIconTexture:     " @ Texture2D(DynamicLoadObject(MI.ModWeaponInfoList[i].default.KillIconTexturePath, class'Texture2D')), 'VerificationWeapons');
			`gom("  bHasSquareKillIcon:  " @ MI.ModWeaponInfoList[i].default.bHasSquareKillIcon, 'VerificationWeapons');
		}
	}
	`gom("------------------------------------------------------", 'Verification');
}

//////////////////////////////////////////////////////// CHEAT-CODES! ////////////////////////////////////////////////////////
`ifndef(RELEASE)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Third-person camera mode for Pawn debugging
exec function Camera(optional bool free = false)
{
	`gomdebug("has a" @ self.Pawn.class,'Pawns');
	
	ServerCamera(free);
}

unreliable server function ServerCamera(bool free)
{
	if (free)
	{
		SetCameraMode('FreeCam');
	}
	else
	{
		SetCameraMode('ThirdPerson');
	}
}

// List Pawn MICs to debug material application
exec function ListMats()
{
	local int i;
	
	for (i = 0; i < self.Pawn.mesh.Materials.length; i++)
	{
		`gomdebug("MIC" @ i @ MaterialInstanceConstant(self.Pawn.mesh.Materials[i]).Parent,'Pawns');
	}
}

// Display Pawn Hair info
exec function ListHair()
{
	local int i;
	
	ClientMessage(self.Pawn @ "Headgear:" @ GOMPawn(self.Pawn).ThirdPersonHeadgearMeshComponent.SkeletalMesh);
	
	for (i = 0; i < GOMPawn(self.Pawn).ThirdPersonHeadgearMeshComponent.Materials.length; i++)
	{
		ClientMessage(GOMPawn(self.Pawn) @ "Headgear MIC" @ i @ MaterialInstanceConstant(GOMPawn(self.Pawn).ThirdPersonHeadgearMeshComponent.Materials[i]).Parent);
	}
	
	ClientMessage(self.Pawn @ "Hair:" @ GOMPawn(self.Pawn).ThirdPersonHairMeshComponent.SkeletalMesh);
	
	for (i = 0; i < GOMPawn(self.Pawn).ThirdPersonHairMeshComponent.Materials.length; i++)
	{
		ClientMessage(GOMPawn(self.Pawn) @ "Hair MIC" @ i @ MaterialInstanceConstant(GOMPawn(self.Pawn).ThirdPersonHairMeshComponent.Materials[i]).Parent);
	}
}

// Destroy helicopters to test factory respawns
exec function BombAllHelis()
{
	ServerHeliBoom();
}

unreliable server function ServerHeliBoom()
{
	local ROTransportVehicleFactory Factory;
	
	foreach AllActors(class'ROTransportVehicleFactory', Factory)
	{
		Factory.ChildVehicle.Mesh.AddImpulse(vect(20,20,40));
		Factory.ChildVehicle.BlowUpVehicle();
	}
}

// Make yourself bleed to test bandaging
simulated exec function BleedTest()
{
	GOMPawn(self.Pawn).PlayerHitZones[5].ZoneBleedingStatus = ROBS_Slow;
	GOMPawn(self.Pawn).PlayerHitZones[5].bBleeding = true;
	GOMPawn(self.Pawn).BleedingInstigator = self;
	GOMPawn(self.Pawn).LastTakeHitInfo.DamageType = class'RODamageType';
	GOMPawn(self.Pawn).StartBleeding();
}

// Spawn a dummy pawn for testing kill icons and etc
simulated exec function SpawnGOMPawn(bool AxisTeam, int Faction)
{
	local ROAIController ROBot;
	local byte ChosenTeam;
	local vector CamLoc, StartShot, EndShot, X, Y, Z;
	local rotator CamRot;
	Local ROPawn ROP;
	local ROInventoryManager InvManager;
	local class<ROPawn> PawnClass;
	local string WeaponClass;
	
	GetPlayerViewPoint(CamLoc, CamRot);
	GetAxes( CamRot, X, Y, Z );
	StartShot = CamLoc;
	EndShot = StartShot + (200.0 * X);
	
	PawnClass = none;
	
	if (AxisTeam)
	{
		ChosenTeam = `AXIS_TEAM_INDEX;
		WeaponClass = "ROGameContent.ROWeap_SKS_Rifle_Content";
		
		switch (Faction)
		{
			case `VC:
				PawnClass = class'GOMPawnNorthVC';
				break;
			
			case `NVA:
				PawnClass = class'GOMPawnNorthNVA';
				break;
			
			case `NVAU:
				PawnClass = class'GOMPawnNorthNVAU';
				break;
		}
	}
	else
	{
		ChosenTeam = `ALLIES_TEAM_INDEX;
		WeaponClass = "ROGameContent.ROWeap_M16A1_AssaultRifle_Content";
		
		switch (Faction)
		{
			case `US:
			case `USMC:
				PawnClass = class'GOMPawnSouthUS';
				break;
			
			case `MACV:
				PawnClass = class'GOMPawnSouthMACV';
				break;
			
			case `ARVN:
			case `RANGERS:
				PawnClass = class'GOMPawnSouthARVN';
				break;
			
			case `AUS:
				PawnClass = class'GOMPawnSouthAUS';
				break;
		}
	}
	
	if (PawnClass != none)
	{
		ROP = Spawn(PawnClass, , , EndShot);
		ROP.SetPhysics(PHYS_Falling);
		
		InvManager = ROInventoryManager(ROP.InvManager);
		InvManager.LoadAndCreateInventory(WeaponClass, false, true);
		
		ROBot = Spawn(class'ROAIController');
		ROGameInfo(WorldInfo.Game).ChangeName(ROBot, string(PawnClass.name), false);
		ROGameInfo(WorldInfo.Game).GameReplicationInfo.Teams[ChosenTeam].AddToTeam(ROBot);
		ROBot.SetTeam(ROBot.PlayerReplicationInfo.Team.TeamIndex);
		ROBot.Possess(ROP, false);
		ROBot.GotoState('BrainDead');
		ROBot.bDisallowEvaluateObjectives = true;
	}
	else
	{
		`gomdebug("Couldn't spawn Pawn",'Pawns');
	}
}

// Spawn a B52 so we don't have to constantly wait for radio stuff
exec function TestB52Run()
{
	// ServerB52Bombing();
}

// Vehicle testing functions
exec function ShowProxies()
{
	local ROVehicle ROV;
	local int i;
	
	ROV = ROVehicle(Pawn);
	
	if (ROV == None && ROWeaponPawn(Pawn) != None)
	{
		ROV = ROWeaponPawn(Pawn).MyVehicle;
	}
	
	for (i = 0; i < ROV.SeatProxies.Length; i++)
	{
		ROV.SeatProxies[i].ProxyMeshActor.HideMesh(false);
	}
}

// Film Camera Equipment
/* exec function FilmCamera()
{
	if (ROInventoryManager(Pawn.InvManager) != none)
	{
		ROInventoryManager(Pawn.InvManager).LoadAndCreateInventory("GOM3.GOMFilmCamera_ActualContent", false, true);
	}
} */

// Media Equipment
exec function SpawnPosedPawns()
{
	local GOMPosedPawn PosedActor;
	local class<GOMPawn> PawnClass;
	local ROInventoryManager InvManager;
	local class<ROWeapon> WeaponClass;
	local EROWeaponType CurrentWeaponType;
	
	foreach AllActors(class'GOMPosedPawn', PosedActor)
	{
		if (PosedActor.GOMP != none)
		{
			PosedActor.GOMP.Destroy();
		}
		
		PawnClass = class<GOMPawn>(DynamicLoadObject(PosedActor.GetPawnType(), class'Class'));
		PosedActor.GOMP = Spawn(PawnClass, , , PosedActor.Location, PosedActor.Rotation);
		
		PosedActor.GOMP.SetPawnElementsForPosedPawn(int(PosedActor.PawnTeam), PosedActor.GetPawnArmy(), int(PosedActor.PawnRoleIndex), PosedActor.PawnLevel, ROPlayerReplicationInfo(self.PlayerReplicationInfo));
		PosedActor.GOMP.CreatePawnMesh();
		
		PosedActor.GOMP.SetPhysics(PHYS_Falling);
		
		InvManager = ROInventoryManager(PosedActor.GOMP.InvManager);
		InvManager.LoadAndCreateInventory(PosedActor.GetPoseWeapon(), false, true);
		
		PosedActor.GOMP.PlayFullBodyAnimation(PosedActor.GetPoseAnim(),, false, 0.0f/* , FRand() */);
		
		WeaponClass = class<ROWeapon>(DynamicLoadObject(PosedActor.GetPoseWeapon(), class'Class'));
		CurrentWeaponType = WeaponClass.default.AttachmentClass.default.WeaponType;
		
		PosedActor.GOMP.EnableHandsAnimation(true,0.1f);
		PosedActor.GOMP.SetHandsAnimation(WeaponClass.default.AttachmentClass.default.ThirdPersonHandsAnim);
		
		PosedActor.GOMP.EnableLeftHandIK(CurrentWeaponType == ROWT_TwoHand || CurrentWeaponType == ROWT_TwoHandHeavy || CurrentWeaponType == ROWT_Rocket ? true : false);
		PosedActor.GOMP.EnableRightHandIK(true);
		
		if (PosedActor.DisableRightHandIK)
		{
			PosedActor.GOMP.EnableRightHandIK(false);
		}
		
		if (PosedActor.DisableLeftHandIK)
		{
			PosedActor.GOMP.EnableLeftHandIK(false);
		}
		
		PosedActor.GOMP.bImmuneToSpawnProtection = true;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
`endif
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

defaultproperties
{
	TeamSelectSceneTemplate=GOMUISceneTeamSelect'GOM3_UI.UIObjects.TeamSelect'
	CharacterSceneTemplate=GOMUISceneCharacter'GOM3_UI.UIObjects.Character'
	
	updatedPilots=false
	
	NVARoles(`ROCI_RIFLEMAN)=		class'GOMRoleInfoNorth_NVA_Grunt'
	NVARoles(`ROCI_SCOUT)=			class'GOMRoleInfoNorth_NVA_Scout'
	NVARoles(`ROCI_MACHINEGUNNER)=	class'GOMRoleInfoNorth_NVA_MachineGunner'
	NVARoles(`ROCI_SNIPER)=			class'GOMRoleInfoNorth_NVA_Sniper'
	NVARoles(`ROCI_ENGINEER)=		class'GOMRoleInfoNorth_NVA_Sapper'
	NVARoles(`ROCI_HEAVY)=			none
	NVARoles(`ROCI_ANTITANK)=		class'GOMRoleInfoNorth_NVA_RPG'
	NVARoles(`ROCI_RADIOMAN)=		class'GOMRoleInfoNorth_NVA_Radioman'
	NVARoles(`ROCI_COMMANDER)=		class'GOMRoleInfoNorth_NVA_Commander'
	
	VCRoles(`ROCI_RIFLEMAN)=		class'GOMRoleInfoNorth_VC_Guerilla'
	VCRoles(`ROCI_SCOUT)=			class'GOMRoleInfoNorth_VC_Scout'
	VCRoles(`ROCI_MACHINEGUNNER)=	class'GOMRoleInfoNorth_VC_MachineGunner'
	VCRoles(`ROCI_SNIPER)=			class'GOMRoleInfoNorth_VC_Sniper'
	VCRoles(`ROCI_ENGINEER)=		class'GOMRoleInfoNorth_VC_Sapper'
	VCRoles(`ROCI_HEAVY)=			class'GOMRoleInfoNorth_VC_Rifleman'
	VCRoles(`ROCI_ANTITANK)=		class'GOMRoleInfoNorth_VC_RPG'
	VCRoles(`ROCI_RADIOMAN)=		class'GOMRoleInfoNorth_NVA_VC_Radioman'
	VCRoles(`ROCI_COMMANDER)=		class'GOMRoleInfoNorth_NVA_Commander'
	
	USRoles(`ROCI_RIFLEMAN)=		class'GOMRoleInfoSouth_US_Grunt'
	USRoles(`ROCI_SCOUT)=			class'GOMRoleInfoSouth_US_Pointman'
	USRoles(`ROCI_MACHINEGUNNER)=	class'GOMRoleInfoSouth_US_MachineGunner'
	USRoles(`ROCI_SNIPER)=			class'GOMRoleInfoSouth_US_Sniper'
	USRoles(`ROCI_ENGINEER)=		class'GOMRoleInfoSouth_US_Engineer'
	USRoles(`ROCI_HEAVY)=			class'GOMRoleInfoSouth_US_Grenadier'
	USRoles(`ROCI_ANTITANK)=		none
	USRoles(`ROCI_RADIOMAN)=		class'GOMRoleInfoSouth_US_Radioman'
	USRoles(`ROCI_COMMANDER)=		class'GOMRoleInfoSouth_US_Commander'
	USRoles(`ROCI_COMBATPILOT)=		class'GOMRoleInfoSouth_US_Pilot_Combat'
	USRoles(`ROCI_TRANSPORTPILOT)=	class'GOMRoleInfoSouth_US_Pilot_Transport'
	
	USMCRoles(`ROCI_RIFLEMAN)=		class'GOMRoleInfoSouth_USMC_Grunt'
	USMCRoles(`ROCI_SCOUT)=			class'GOMRoleInfoSouth_USMC_Pointman'
	USMCRoles(`ROCI_MACHINEGUNNER)=	class'GOMRoleInfoSouth_USMC_MachineGunner'
	USMCRoles(`ROCI_SNIPER)=		class'GOMRoleInfoSouth_USMC_Sniper'
	USMCRoles(`ROCI_ENGINEER)=		class'GOMRoleInfoSouth_USMC_Engineer'
	USMCRoles(`ROCI_HEAVY)=			class'GOMRoleInfoSouth_USMC_Grenadier'
	USMCRoles(`ROCI_ANTITANK)=		none
	USMCRoles(`ROCI_RADIOMAN)=		class'GOMRoleInfoSouth_USMC_Radioman'
	USMCRoles(`ROCI_COMMANDER)=		class'GOMRoleInfoSouth_USMC_Commander'
	USMCRoles(`ROCI_COMBATPILOT)=	class'GOMRoleInfoSouth_US_Pilot_Combat'
	USMCRoles(`ROCI_TRANSPORTPILOT)=class'GOMRoleInfoSouth_US_Pilot_Transport'
	
	MACVRoles(`ROCI_RIFLEMAN)=		class'GOMRoleInfoSouth_MACV_Operative'
	MACVRoles(`ROCI_SCOUT)=			class'GOMRoleInfoSouth_MACV_Pointman'
	MACVRoles(`ROCI_MACHINEGUNNER)=	class'GOMRoleInfoSouth_MACV_MachineGunner'
	MACVRoles(`ROCI_SNIPER)=		class'GOMRoleInfoSouth_MACV_Sniper'
	MACVRoles(`ROCI_ENGINEER)=		class'GOMRoleInfoSouth_MACV_Engineer'
	MACVRoles(`ROCI_HEAVY)=			class'GOMRoleInfoSouth_MACV_Grenadier'
	MACVRoles(`ROCI_ANTITANK)=		none
	MACVRoles(`ROCI_RADIOMAN)=		class'GOMRoleInfoSouth_MACV_Radioman'
	MACVRoles(`ROCI_COMMANDER)=		class'GOMRoleInfoSouth_MACV_Commander'
	MACVRoles(`ROCI_COMBATPILOT)=	class'GOMRoleInfoSouth_US_Pilot_Combat'
	MACVRoles(`ROCI_TRANSPORTPILOT)=class'GOMRoleInfoSouth_US_Pilot_Transport'
	
	AUSRoles(`ROCI_RIFLEMAN)=		class'GOMRoleInfoSouth_AUS_Grunt'
	AUSRoles(`ROCI_SCOUT)=			class'GOMRoleInfoSouth_AUS_Scout'
	AUSRoles(`ROCI_MACHINEGUNNER)=	class'GOMRoleInfoSouth_AUS_MachineGunner'
	AUSRoles(`ROCI_SNIPER)=			none
	AUSRoles(`ROCI_ENGINEER)=		class'GOMRoleInfoSouth_AUS_Engineer'
	AUSRoles(`ROCI_HEAVY)=			class'GOMRoleInfoSouth_AUS_Grenadier'
	AUSRoles(`ROCI_ANTITANK)=		none
	AUSRoles(`ROCI_RADIOMAN)=		class'GOMRoleInfoSouth_AUS_Radioman'
	AUSRoles(`ROCI_COMMANDER)=		class'GOMRoleInfoSouth_AUS_Commander'
	AUSRoles(`ROCI_COMBATPILOT)=	class'GOMRoleInfoSouth_AUS_Pilot_Combat'
	AUSRoles(`ROCI_TRANSPORTPILOT)=	class'GOMRoleInfoSouth_AUS_Pilot_Transport'
	
	ARVNRoles(`ROCI_RIFLEMAN)=		class'GOMRoleInfoSouth_ARVN_Grunt'
	ARVNRoles(`ROCI_SCOUT)=			class'GOMRoleInfoSouth_ARVN_Pointman'
	ARVNRoles(`ROCI_MACHINEGUNNER)=	class'GOMRoleInfoSouth_ARVN_MachineGunner'
	ARVNRoles(`ROCI_SNIPER)=		class'GOMRoleInfoSouth_ARVN_Sniper'
	ARVNRoles(`ROCI_ENGINEER)=		class'GOMRoleInfoSouth_ARVN_Engineer'
	ARVNRoles(`ROCI_HEAVY)=			class'GOMRoleInfoSouth_ARVN_Grenadier'
	ARVNRoles(`ROCI_ANTITANK)=		none
	ARVNRoles(`ROCI_RADIOMAN)=		class'GOMRoleInfoSouth_ARVN_Radioman'
	ARVNRoles(`ROCI_COMMANDER)=		class'GOMRoleInfoSouth_ARVN_Commander'
	ARVNRoles(`ROCI_COMBATPILOT)=	class'GOMRoleInfoSouth_US_Pilot_Combat'
	ARVNRoles(`ROCI_TRANSPORTPILOT)=class'GOMRoleInfoSouth_US_Pilot_Transport'
	
	ARVNRangerRoles(`ROCI_RIFLEMAN)=		class'GOMRoleInfoSouth_ARVNRanger_Grunt'
	ARVNRangerRoles(`ROCI_SCOUT)=			class'GOMRoleInfoSouth_ARVNRanger_Pointman'
	ARVNRangerRoles(`ROCI_MACHINEGUNNER)=	class'GOMRoleInfoSouth_US_MachineGunner'
	ARVNRangerRoles(`ROCI_SNIPER)=			class'GOMRoleInfoSouth_ARVN_Sniper'
	ARVNRangerRoles(`ROCI_ENGINEER)=		class'GOMRoleInfoSouth_ARVNRanger_Engineer'
	ARVNRangerRoles(`ROCI_HEAVY)=			class'GOMRoleInfoSouth_US_Grenadier'
	ARVNRangerRoles(`ROCI_ANTITANK)=		none
	ARVNRangerRoles(`ROCI_RADIOMAN)=		class'GOMRoleInfoSouth_ARVNRanger_Radioman'
	ARVNRangerRoles(`ROCI_COMMANDER)=		class'GOMRoleInfoSouth_ARVN_Commander'
	ARVNRangerRoles(`ROCI_COMBATPILOT)=		class'GOMRoleInfoSouth_US_Pilot_Combat'
	ARVNRangerRoles(`ROCI_TRANSPORTPILOT)=	class'GOMRoleInfoSouth_US_Pilot_Transport'
}