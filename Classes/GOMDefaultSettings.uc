//=============================================================================
// GOMDefaultSettings.uc
//=============================================================================
// A class that contains the default settings of the mod for
// servers that have not had their configs initialized yet.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMDefaultSettings extends Object
	config(Mutator_GOM)
	dependson(GOM3);

var config string Version;

var array<MapData> DefaultMapOptions, LockedMaps;

static function UpdateBuildVer()
{
	default.Version = `BUILDVER;
	StaticSaveConfig();
}

defaultproperties
{
	// Official maps
	DefaultMapOptions[ 0]=(MapName="VNSK-Compound",			NorthForce=`VC,		SouthForce=`USMC,	MiniFaction=MF_MilitaryPolice)
	DefaultMapOptions[ 1]=(MapName="VNTE-Compound",			NorthForce=`VC,		SouthForce=`USMC,	MiniFaction=MF_MilitaryPolice)
	DefaultMapOptions[ 2]=(MapName="VNSK-Firebase",			NorthForce=`NVAU,	SouthForce=`US,		MiniFaction=MF_MilitaryPolice)
	DefaultMapOptions[ 3]=(MapName="VNTE-Firebase",			NorthForce=`NVAU,	SouthForce=`US,		MiniFaction=MF_MilitaryPolice)
	DefaultMapOptions[ 4]=(MapName="VNSK-JungleCamp",		NorthForce=`VC,		SouthForce=`MACV,	MiniFaction=MF_None)
	DefaultMapOptions[ 5]=(MapName="VNSK-Riverbed",			NorthForce=`VC,		SouthForce=`MACV,	MiniFaction=MF_None)
	DefaultMapOptions[ 6]=(MapName="VNSK-Temple",			NorthForce=`NVAU,	SouthForce=`AUS,	MiniFaction=MF_None)
	DefaultMapOptions[ 7]=(MapName="VNSU-AnLaoValley",		NorthForce=`NVA,	SouthForce=`US,		MiniFaction=MF_ARVNSupport)
	DefaultMapOptions[ 8]=(MapName="VNTE-AnLaoValley",		NorthForce=`NVA,	SouthForce=`US,		MiniFaction=MF_ARVNSupport)
	DefaultMapOptions[ 9]=(MapName="VNSU-HueCity",			NorthForce=`NVAU,	SouthForce=`ARVN,	MiniFaction=MF_None)
	DefaultMapOptions[10]=(MapName="VNTE-HueCity",			NorthForce=`NVAU,	SouthForce=`USMC,	MiniFaction=MF_ARVNSupport)
	DefaultMapOptions[11]=(MapName="VNSU-OperationForrest",	NorthForce=`VC,		SouthForce=`AUS,	MiniFaction=MF_None)
	DefaultMapOptions[12]=(MapName="VNTE-OperationForrest",	NorthForce=`VC,		SouthForce=`AUS,	MiniFaction=MF_None)
	DefaultMapOptions[13]=(MapName="VNSU-SongBe",			NorthForce=`NVA,	SouthForce=`ARVN,	MiniFaction=MF_None)
	DefaultMapOptions[14]=(MapName="VNTE-SongBe",			NorthForce=`NVA,	SouthForce=`ARVN,	MiniFaction=MF_None)
	DefaultMapOptions[15]=(MapName="VNTE-CuChi",			NorthForce=`VC,		SouthForce=`US,		MiniFaction=MF_None)
	DefaultMapOptions[16]=(MapName="VNTE-Hill937",			NorthForce=`NVAU,	SouthForce=`US,		MiniFaction=MF_None)
	DefaultMapOptions[17]=(MapName="VNTE-LongTan",			NorthForce=`VC,		SouthForce=`AUS,	MiniFaction=MF_None)
	DefaultMapOptions[18]=(MapName="VNTE-RungSac",			NorthForce=`VC,		SouthForce=`AUS,	MiniFaction=MF_None)
	DefaultMapOptions[19]=(MapName="VNTE-NinhPhu",			NorthForce=`NVA,	SouthForce=`US,		MiniFaction=MF_ARVNSupport)
	DefaultMapOptions[20]=(MapName="VNTE-Resort",			NorthForce=`VC,		SouthForce=`USMC,	MiniFaction=MF_None)
	DefaultMapOptions[21]=(MapName="VNSK-BorderWatch",		NorthForce=`NVA,	SouthForce=`AUS,	MiniFaction=MF_None)
	DefaultMapOptions[22]=(MapName="VNSU-BorderWatch",		NorthForce=`NVA,	SouthForce=`AUS,	MiniFaction=MF_None)
	DefaultMapOptions[23]=(MapName="VNTE-BorderWatch",		NorthForce=`NVA,	SouthForce=`AUS,	MiniFaction=MF_None)
	DefaultMapOptions[24]=(MapName="VNTE-FirebaseGeorgina",	NorthForce=`NVA,	SouthForce=`US,		MiniFaction=MF_ARVNSupport)
	DefaultMapOptions[25]=(MapName="VNTE-ASau",				NorthForce=`NVAU,	SouthForce=`RANGERS,MiniFaction=MF_None)
	DefaultMapOptions[26]=(MapName="VNSU-QuangTri",			NorthForce=`NVAU,	SouthForce=`RANGERS,MiniFaction=MF_None)
	DefaultMapOptions[27]=(MapName="VNTE-QuangTri",			NorthForce=`NVAU,	SouthForce=`RANGERS,MiniFaction=MF_None)
	DefaultMapOptions[28]=(MapName="VNTE-Highway14",		NorthForce=`VC,		SouthForce=`US,		MiniFaction=MF_None)
	DefaultMapOptions[29]=(MapName="VNTE-ApacheSnow",		NorthForce=`VC,		SouthForce=`ARVN,	MiniFaction=MF_None)
	DefaultMapOptions[30]=(MapName="VNTE-DemilitarizedZone",NorthForce=`NVAU,	SouthForce=`ARVN,	MiniFaction=MF_None)
	DefaultMapOptions[31]=(MapName="VNTE-DaNangAirBase",	NorthForce=`VC,		SouthForce=`USMC,	MiniFaction=MF_None)
	DefaultMapOptions[32]=(MapName="VNTE-KheSanh",			NorthForce=`NVAU,	SouthForce=`US,		MiniFaction=MF_None)
	DefaultMapOptions[33]=(MapName="VNTE-Saigon",			NorthForce=`NVAU,	SouthForce=`RANGERS,MiniFaction=MF_None)
	
	
	// ROK maps
	DefaultMapOptions[34]=(MapName="VNTE-CuChi_ROK",		NorthForce=`VC,		SouthForce=`ROK,	MiniFaction=MF_None,Locked=true)
	DefaultMapOptions[35]=(MapName="VNSU-SongBe_ROK",		NorthForce=`VC,		SouthForce=`ROK,	MiniFaction=MF_None,Locked=true)
	DefaultMapOptions[36]=(MapName="VNTE-Lolo_ROK",			NorthForce=`NVA,	SouthForce=`ROKA,	MiniFaction=MF_None,Locked=true)
	DefaultMapOptions[37]=(MapName="VNSU-CuaViet_ROK",		NorthForce=`NVAU,	SouthForce=`ROKMC,	MiniFaction=MF_None,Locked=true)
	DefaultMapOptions[38]=(MapName="VNTE-District_ROK",		NorthForce=`VC,		SouthForce=`ROKA,	MiniFaction=MF_None,Locked=true)
	DefaultMapOptions[39]=(MapName="VNTE-Jungle_Raid_ROK",	NorthForce=`NVA,	SouthForce=`ROKA,	MiniFaction=MF_None,Locked=true)
	
	
	LockedMaps[0]=(MapName="VNTE-CuChi_ROK",		NorthForce=`VC,  SouthForce=`ROK,  MiniFaction=MF_None, Locked=true)
	LockedMaps[1]=(MapName="VNSU-SongBe_ROK",		NorthForce=`VC,  SouthForce=`ROK,  MiniFaction=MF_None, Locked=true)
	LockedMaps[2]=(MapName="VNTE-Lolo_ROK",			NorthForce=`NVA, SouthForce=`ROKA, MiniFaction=MF_None, Locked=true)
	LockedMaps[3]=(MapName="VNSU-CuaViet_ROK",		NorthForce=`NVAU,SouthForce=`ROKMC,MiniFaction=MF_None, Locked=true)
	LockedMaps[4]=(MapName="VNTE-District_ROK",		NorthForce=`VC,  SouthForce=`ROKA ,MiniFaction=MF_None, Locked=true)
	LockedMaps[5]=(MapName="VNTE-Jungle_Raid_ROK",	NorthForce=`NVA, SouthForce=`ROKA ,MiniFaction=MF_None, Locked=true)
}