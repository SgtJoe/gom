//=============================================================================
// GOMRoleInfoSouth_ROK_Flamethrower.uc
//=============================================================================
// Republic of Korea Flamethrower Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_ROK_Flamethrower extends GOMRoleInfoSouth_ROK;

DefaultProperties
{
	RoleType=RORIT_Engineer
	ClassTier=3
	ClassIndex=`ROCI_ENGINEER
	
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'GOMWeapon_M2A17_Flamethrower'),
	
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'GOM3_UI.RoleIcons.class_icon_flamer'
	ClassIconLarge=Texture2D'GOM3_UI.RoleIcons.class_icon_large_flamer'
}
