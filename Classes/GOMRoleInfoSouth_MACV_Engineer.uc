//=============================================================================
// GOMRoleInfoSouth_MACV_Engineer.uc
//=============================================================================
// MACV-SOG Combat Engineer Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoSouth_MACV_Engineer extends GOMRoleInfoSouth_MACV;

DefaultProperties
{
	RoleType=RORIT_Engineer
	ClassTier=3
	ClassIndex=`ROCI_ENGINEER
	
	Items[RORIGM_Default]={(
	
	PrimaryWeapons=(class'ROWeap_XM177E1_Carbine',class'ROWeap_M37_Shotgun',class'ROWeap_RPG7_RocketLauncher'),
	
	DisableSecondaryForPrimary=(true, true, false),
	
	OtherItems=(class'ROWeap_M34_WP',class'ROWeap_M18_Claymore',class'ROWeap_C4_Explosive'),
	OtherItemsStartIndexForPrimary=(0, 0, 0),
	NumOtherItemsForPrimary=(0, 0, 255)
	
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sapper'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_sapper'
}
