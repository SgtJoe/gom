//=============================================================================
// GOMRoleInfoNorth_VC_RPG.uc
//=============================================================================
// Viet Cong RPG Role.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMRoleInfoNorth_VC_RPG extends GOMRoleInfoNorth_VC;

DefaultProperties
{
	RoleType=RORIT_AntiTank
	ClassTier=2
	ClassIndex=`ROCI_ANTITANK
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle',class'ROWeap_IZH43_Shotgun'),
		SecondaryWeapons=(class'GOMWeapon_RPG2'),
		OtherItems=(class'ROWeap_PunjiTrap')
	)}
	
	Items[RORIGM_Campaign_Early]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle',class'ROWeap_IZH43_Shotgun'),
		SecondaryWeapons=(class'GOMWeapon_RPG2'),
		OtherItems=(class'ROWeap_PunjiTrap')
	)}
	
	Items[RORIGM_Campaign_Mid]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle'),
		SecondaryWeapons=(class'GOMWeapon_RPG2'),
		OtherItems=(class'ROWeap_PunjiTrap')
	)}
	
	Items[RORIGM_Campaign_Late]={(
		PrimaryWeapons=(class'ROWeap_SKS_Rifle'),
		SecondaryWeapons=(class'ROWeap_RPG7_RocketLauncher',class'GOMWeapon_RPG2')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_rpg'
	ClassIconLarge=Texture2D'VN_UI_Textures.menu.ProfileStats.class_icon_large_rpg'
}
