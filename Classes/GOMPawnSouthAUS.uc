//=============================================================================
// GOMPawnSouthAUS.uc
//=============================================================================
// Southern Pawn (Australia).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================
class GOMPawnSouthAUS extends GOMPawnSouth;

DefaultProperties
{
	TunicMesh=SkeletalMesh'GOM3_CHR_US_AUS.Mesh.GOM_CHR_AUS_Tunic_Rolled_Towel'
	BodyMICTemplate=MaterialInstanceConstant'CHR_VN_AUS.Materials.M_AUS_Tunic_Rolled_INST'
	
	HeadAndArmsMesh=SkeletalMesh'CHR_VN_AUS_Heads.Mesh.AUS_Head7_Mesh'
	HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_AUS_Heads.Materials.M_AUS_Head_07_Rolled_INST'
	
	HeadgearMesh=SkeletalMesh'CHR_VN_AUS_Headgear.Mesh.AUS_Headgear_Slouch'
	HeadgearMICTemplate=MaterialInstanceConstant'CHR_VN_AUS_Headgear.Materials.M_AUS_Headgear_Slouch_INST'
	
	FieldgearMesh=SkeletalMesh'GOM3_CHR_US_AUS.Mesh.GOM_CHR_AUS_Gear_Rifleman'
}
