//=============================================================================
// GOMModWeaponInfo_M1919A6.uc
//=============================================================================
// UI information for the M1919A6 Browning.
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMModWeaponInfo_M1919A6 extends ROModWeaponInfo;

DefaultProperties
{
	ModWeaponClass=GOMWeapon_M1919A6_ActualContent
	
	InventoryIconTexturePath="VN_UI_Textures_Three.HUD.WeaponSelect.UI_HUD_WeaponSelect_M1919"
	
	AmmoIconTexturePath="VN_UI_Textures_Three.HUD.Ammo.UI_HUD_Ammo_M1919"
	
	KillIconTexturePath="VN_UI_Textures_Three.HUD.DeathMessage.UI_Kill_Icon_M1919"
	bHasSquareKillIcon=false
}
