
class GOMPosedPawn extends RODebugPosedPlayer
	hidecategories(Lighting,LightColor,Force,Collision,Physics,Debug,Attachment,Navigation,Mobile)
	placeable;

var transient GOMPawn GOMP;

enum TeamEnum
{
	North,
	South
};

enum ArmyEnum
{
	NF_VietCong,
	NF_NorthVietnameseArmy,
	NF_NorthVietnameseArmyUrban,
	SF_UnitedStates,
	SF_UnitedStatesMarines,
	SF_MACV,
	SF_SouthVietnam,
	SF_SouthVietnamRangers,
	SF_Australia,
	SF_SouthKorea,
	SF_SouthKoreaArmoured,
	SF_SouthKoreaMarines
};

enum RoleEnum
{
	ROCI_RIFLEMAN,
	ROCI_SCOUT,
	ROCI_MACHINEGUNNER,
	ROCI_SNIPER,
	ROCI_ENGINEER,
	ROCI_HEAVY,
	ROCI_ANTITANK,
	ROCI_RADIOMAN,
	ROCI_COMMANDER,
	ROCI_COMBATPILOT,
	ROCI_TRANSPORTPILOT,
	ROCI_TANKCREW
};

enum PosesCommonEnum
{
	Hip_idle_rifle			<DisplayName=Standing Rifle>,
	Iron_idle_rifle			<DisplayName=Standing Rifle Aim>,
	JogF_rifle				<DisplayName=Standing Rifle Run>,
	Hip_idle_MG				<DisplayName=Standing MG>,
	JogF_MG					<DisplayName=Standing MG Run>,
	Hip_idle_pistol			<DisplayName=Standing Pistol>,
	Iron_idle_pistol		<DisplayName=Standing Pistol Aim>,
	jogF_pistol				<DisplayName=Standing Pistol Run>,
	Hip_idle_nade			<DisplayName=Standing Item>,
	Iron_idle_nade			<DisplayName=Standing Item Aim>,
	jogF_nade				<DisplayName=Standing Item Run>,
	CH_Hip_idle_rifle		<DisplayName=Crouch Rifle>,
	CH_Iron_idle_rifle		<DisplayName=Crouch Rifle Aim>,
	CH_Hip_idle_MG			<DisplayName=Crouch MG>,
	CH_Iron_idle_MG			<DisplayName=Crouch MG Aim>,
	CH_Hip_idle_pistol		<DisplayName=Crouch Pistol>,
	CH_Iron_idle_pistol		<DisplayName=Crouch Pistol Aim>,
	CH_Hip_idle_nade		<DisplayName=Crouch Item>,
	CH_Iron_idle_nade		<DisplayName=Crouch Item Aim>,
	Prone_Iron_idle_rifle	<DisplayName=Prone Rifle>,
	Prone_Iron_idle_MG		<DisplayName=Prone MG>,
	Prone_Iron_idle_pistol	<DisplayName=Prone Pistol>,
	prone_idle_nade			<DisplayName=Prone Item>
};

var			class<GOMPawn>		PawnClass;
var			string				PawnClassPackage;
var()		TeamEnum			PawnTeam;
var()		ArmyEnum			PawnArmy;
var()		RoleEnum			PawnRoleIndex;
var()		int					PawnLevel;
var()		PosesCommonEnum		PosesCommon;
var()		string				PoseOverride;
var()		bool				DisableRightHandIK;
var()		bool				DisableLeftHandIK;
var()		class<ROWeapon>		WeaponClass;
// var()	string				WeaponClassPackage;

function string GetPawnType()
{
	return PawnClassPackage$"."$string(PawnClass.name);
}

function int GetPawnArmy()
{
	return (PawnTeam == South) ? (int(PawnArmy) - 3) : int(PawnArmy);
}

function name GetPoseAnim()
{
	local string retVal;
	
	if (PoseOverride != "")
	{
		retVal = PoseOverride;
	}
	else
	{
		retVal = string(PosesCommon);
	}
	
	return name(retVal);
}

function string GetPoseWeapon()
{
	local string retVal;
	
	/* if (WeaponClassPackage != "")
	{
		retVal = WeaponClassPackage$"."$string(WeaponClass.name);
	}
	else */ if (Left(WeaponClass, 2) ~= "RO")
	{
		retVal = "ROGameContent."$string(WeaponClass.name);
	}
	else if (Left(WeaponClass, 3) ~= "GOM")
	{
		retVal = "GOM3."$string(WeaponClass.name);
	}
	else
	{
		return string(self.default.WeaponClass);
	}
	
	return retVal;
}

DefaultProperties
{
	PawnClass=			"GOMPawnNorthNVA"
	PawnClassPackage=	"GOM3"
	WeaponClass=		"ROGameContent.ROWeap_AK47_AssaultRifle_Type56"
	
	PawnLevel=0
	
	DisableRightHandIK=false
	DisableLeftHandIK=false
}
