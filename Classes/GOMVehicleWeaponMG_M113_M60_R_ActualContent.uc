//=============================================================================
// GOMVehicleWeaponMG_M113_M60_R_ActualContent.uc
//=============================================================================
// M113 ACAV Right Side M60 (Content class).
//=============================================================================
// Gameplay Overhaul Mutator for Rising Storm 2: Vietnam
// Authored by SgtJoe
//=============================================================================

class GOMVehicleWeaponMG_M113_M60_R_ActualContent extends GOMVehicleWeaponMG_M113_M60_R
	HideDropDown;

DefaultProperties
{
	BarTexture=Texture2D'ui_textures.Textures.button_128grey'
}
